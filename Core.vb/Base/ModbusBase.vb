Imports System.Text
Imports System.IO.Ports
Imports System.Reflection
Imports isr.Core.EnumExtensions

''' <summary> The modbus base. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Public MustInherit Class ModbusBase
    Implements IDisposable

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Sub New()
        MyBase.New()
        Me._RxTimeout = Me._DefaultReceiveTimeout
        Me._QueryStopWatch = New Stopwatch
        Me._SettlingTimeStopwatch = isr.Core.Constructs.TimeoutStopwatch.StartNew(Me._DefaultDeviceRefractoryPeriod)
        Me._ContinuousFailureDictionary = New Dictionary(Of InterfaceError, Integer)
        Me._ErrorCountDictionary = New Dictionary(Of InterfaceError, Long)
    End Sub

#Region " IDisposable Support "

    ''' <summary> Gets or sets the disposed. </summary>
    ''' <value> The disposed. </value>
    Protected Property Disposed As Boolean

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.Disposed Then
                If disposing Then
                End If
                Me._SettlingTimeStopwatch = Nothing
            End If
        Finally
            Me.Disposed = True
        End Try
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        ' uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

#Region " CONSTANTS "

    ''' <summary>
    ''' Modbus protocol identifier (only TCP and UDP)
    ''' </summary>
    <CLSCompliant(False)>
    Protected Const ProtocolId As UShort = &H0

    ''' <summary>
    ''' Default rx timeout in milliseconds
    ''' </summary>
    Private _DefaultReceiveTimeout As TimeSpan = TimeSpan.FromMilliseconds(100)

    ''' <summary> The default device refractory period. </summary>
    Private _DefaultDeviceRefractoryPeriod As TimeSpan = TimeSpan.FromMilliseconds(100)

    ''' <summary>
    ''' Length in bytes of MBAP header        
    ''' </summary>
    Protected Const MbapHeaderLength As Integer = 7

    ''' <summary>
    ''' Start frame character (only Modbus serial ASCII)
    ''' </summary>
    Protected Const AsciiStartFrameCharacter As Char = ":"c

    ''' <summary>
    ''' End frame first character (only Modbus serial ASCII)
    ''' </summary>
    Protected Const AsciiEndFrameFirstCharacter As Char = ChrW(&HD)

    ''' <summary>
    ''' End frame second character (only Modbus serial ASCII)
    ''' </summary>
    Protected Const AsciiEndFrameSecondCharacter As Char = ChrW(&HA)

    ''' <summary>
    ''' Max number of coil registers that can be read
    ''' </summary>
    <CLSCompliant(False)>
    Public Const MaxCoilsInReadMessage As UShort = 2000

    ''' <summary>
    ''' Max number of discrete inputs registers that can be read
    ''' </summary>
    <CLSCompliant(False)>
    Public Const MaxDiscreteInputsInReadMessage As UShort = 2000

    ''' <summary>
    ''' Max number of holding registers that can be read
    ''' </summary>
    <CLSCompliant(False)>
    Public Const MaxHoldingRegistersInReadMessage As UShort = 125

    ''' <summary>
    ''' Max number of input registers that can be read
    ''' </summary>
    <CLSCompliant(False)>
    Public Const MaxInputRegistersInReadMessage As UShort = 125

    ''' <summary>
    ''' Max number of coil registers that can be written
    ''' </summary>
    <CLSCompliant(False)>
    Public Const MaxCoilsInWriteMessage As UShort = 1968

    ''' <summary>
    ''' Max number of holding registers that can be written
    ''' </summary>
    <CLSCompliant(False)>
    Public Const MaxHoldingRegistersInWriteMessage As UShort = 123

    ''' <summary>
    ''' Max number of holding registers that can be read in a read/write message
    ''' </summary>
    <CLSCompliant(False)>
    Public Const MaxHoldingRegistersToReadInReadWriteMessage As UShort = 125

    ''' <summary>
    ''' Max number of holding registers that can be written in a read/write message
    ''' </summary>
    <CLSCompliant(False)>
    Public Const MaxHoldingRegistersToWriteInReadWriteMessage As UShort = 121

#End Region

#Region " GLOBAL VARIABLES "

    ''' <summary> Gets or sets the type of the connection. </summary>
    ''' <value> The type of the connection. </value>
    Protected Property ConnectionType As ConnectionType

    ''' <summary> Gets or sets the type of the device. </summary>
    ''' <value> The type of the device. </value>
    Protected Property DeviceType As DeviceType

    ''' <summary> Gets or sets the delay between two Modbus serial RTU frame. </summary>
    ''' <value> The inter frame delay. </value>
    Protected Property InterFrameDelay As TimeSpan

    ''' <summary> Gets or sets the delay between two Modbus serial RTU characters. </summary>
    ''' <value> The inter character delay. </value>
    Protected Property InterCharacterDelay As TimeSpan

    ''' <summary>
    ''' Gets or sets the device settling time after completing a query before a new query can be
    ''' executed.
    ''' </summary>
    ''' <value> The device settling time period. </value>
    Public Property DeviceSettlingTime As TimeSpan
        Get
            Return Me.SettlingTimeStopwatch.Duration
        End Get
        Set(value As TimeSpan)
            Me.SettlingTimeStopwatch.Duration = value
        End Set
    End Property

    ''' <summary> Gets or sets the settling time stopwatch. </summary>
    ''' <value> The settling time stopwatch. </value>
    Protected Property SettlingTimeStopwatch As isr.Core.Constructs.TimeoutStopwatch

#End Region

#Region " PARAMETERS "

    ''' <summary> Get or set reception timeout. </summary>
    ''' <value> The receive timeout. </value>
    Public Property RxTimeout() As TimeSpan

    ''' <summary> Gets or sets the poll interval. </summary>
    ''' <value> The poll interval. </value>
    Public Property PollInterval As TimeSpan

    Private ReadOnly _ErrorCountDictionary As Dictionary(Of InterfaceError, Long)

    ''' <summary> Error count. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <param name="interfaceError"> The interface error. </param>
    ''' <returns> A Long. </returns>
    Public Function ErrorCount(ByVal interfaceError As InterfaceError) As Long
        Return If(Me._ErrorCountDictionary.ContainsKey(interfaceError), Me._ErrorCountDictionary(interfaceError), 0)
    End Function

    ''' <summary> Last error count. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <returns> An Integer. </returns>
    Public Function LastErrorCount() As Long
        Return Me._ErrorCountDictionary(Me.LastError)
    End Function

    Private ReadOnly _ContinuousFailureDictionary As Dictionary(Of InterfaceError, Integer)

    ''' <summary> Continuous failure count. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <param name="interfaceError"> The interface error. </param>
    ''' <returns> An Integer. </returns>
    Public Function ContinuousFailureCount(ByVal interfaceError As InterfaceError) As Integer
        Return If(Me._ContinuousFailureDictionary.ContainsKey(interfaceError), Me._ContinuousFailureDictionary(interfaceError), 0)
    End Function

    ''' <summary> Last error continuous failure count. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <returns> An Integer. </returns>
    Public Function LastErrorContinuousFailureCount() As Integer
        Return Me.ContinuousFailureCount(Me.LastError)
    End Function

    Private _ContinuousErrorCount As Integer

    ''' <summary> Gets or sets the number of continuous errors. </summary>
    ''' <value> The number of continuous errors. </value>
    Public Property ContinuousErrorCount() As Integer
        Get
            Return Me._ContinuousErrorCount
        End Get
        Protected Set(value As Integer)
            Me._ContinuousErrorCount = value
        End Set
    End Property

    ''' <summary>
    ''' Modbus errors
    ''' </summary>
    Private _LastError As InterfaceError

    ''' <summary> Get last error code. </summary>
    ''' <value> The last error. </value>
    Public Property LastError() As InterfaceError
        Get
            Return Me._LastError
        End Get
        Protected Set(value As InterfaceError)
            If InterfaceError.NoError = value Then
                Me.ContinuousErrorCount = 0
                Me._ContinuousFailureDictionary.Clear()
            Else
                Me.ContinuousErrorCount += 1
                If value = Me.LastError Then
                    If Me._ContinuousFailureDictionary.ContainsKey(value) Then
                        Me._ContinuousFailureDictionary(value) += 1
                    Else
                        Me._ContinuousFailureDictionary.Add(value, 0)
                    End If
                End If
            End If
            If Me._ErrorCountDictionary.ContainsKey(value) Then
                Me._ErrorCountDictionary(value) += 1
            Else
                Me._ErrorCountDictionary.Add(value, 1)
            End If
            Me._LastError = value
        End Set
    End Property

    ''' <summary> Interface error caption. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <param name="interfaceError"> The interface error. </param>
    ''' <returns> A String. </returns>
    Public Function InterfaceErrorCaption(ByVal interfaceError As InterfaceError) As String
        Dim cfa As Integer = Me.ContinuousFailureCount(interfaceError)
        Return If(isr.Modbus.InterfaceError.NoError = interfaceError, String.Empty, If(1 < cfa, $"{interfaceError} #{cfa}", interfaceError.ToString))
    End Function

    ''' <summary> Interface error caption. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <returns> A String. </returns>
    Public Function InterfaceErrorCaption() As String
        Return Me.InterfaceErrorCaption(Me.LastError)
    End Function

    ''' <summary> Builds compound interface error. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildCompoundInterfaceError(ByVal value As InterfaceError) As String
        Return $"{CInt(value)},{value.Description}"
    End Function

    ''' <summary> Gets the compound interface error. </summary>
    ''' <value> The compound interface error. </value>
    Public ReadOnly Property CompoundInterfaceError As String
        Get
            Return ModbusBase.BuildCompoundInterfaceError(Me.LastError)
        End Get
    End Property

    ''' <summary> The last error details. </summary>
    Private _LastErrorDetails As String

    ''' <summary> Get last error details. </summary>
    ''' <value> The last error details. </value>
    Public Property LastErrorDetails() As String
        Get
            Return Me._LastErrorDetails
        End Get
        Protected Set(value As String)
            Me._LastErrorDetails = value
        End Set
    End Property

    ''' <summary> The query stop watch. </summary>
    Private _QueryStopWatch As Stopwatch

    ''' <summary> Gets the elapsed time of the query. </summary>
    ''' <value> The transaction elapsed time. </value>
    Public ReadOnly Property QueryElapsedTime As TimeSpan
        Get
            Return Me._QueryStopWatch.Elapsed
        End Get
    End Property

    ''' <summary> Executes the transaction started action. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Sub OnQueryStarted()

        ' start counting including the refractory period
        Me._QueryStopWatch = Stopwatch.StartNew

        ' wait for the refractory period to complete
        Me.SettlingTimeStopwatch.Wait()

    End Sub

    ''' <summary> Executes the query finished action. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Sub OnQueryFinished()

        Me._QueryStopWatch.Stop()

        ' start a new refractory period
        Me.SettlingTimeStopwatch.Restart()

    End Sub

#End Region

End Class


