''' <summary> LRC Class. </summary>
''' <remarks>
''' Each Modbus ASCII message is terminated with an error checking byte called an LRC or
''' Longitudinal Redundancy Check. <para>
''' (c) 2013 Simone Assunti. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07. Source: https://code.google.com/p/free-dotnet- modbus/. </para>
''' </remarks>
Public NotInheritable Class LRC

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Private Sub New()
    End Sub

    ''' <summary> Compute the Longitudinal redundancy check (LRC). </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values"> Data values. </param>
    ''' <param name="offset"> Offset. </param>
    ''' <param name="length"> Data length. </param>
    ''' <returns> Computed LRC. </returns>
    Public Shared Function Evaluate(ByVal values() As Byte, ByVal offset As Integer, ByVal length As Integer) As Byte
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If offset < 0 Then Throw New ArgumentOutOfRangeException(NameOf(offset), offset, "value out of range")
        If offset + length > values.Length Then Throw New ArgumentOutOfRangeException($"{NameOf(offset)}({offset})+{NameOf(length)}({length})>length of {NameOf(values)}({values.Length})")
        Dim result As Byte = 0
        Do While length > 0
            result += values(offset)
            offset += 1
            length -= 1
        Loop
        ' For ii As Integer = 0 To length - 1 : result += values(ii + offset) : Next ii
        Return CByte(-CSByte(result))
    End Function
End Class
