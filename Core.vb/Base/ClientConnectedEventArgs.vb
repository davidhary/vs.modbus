﻿Imports System.Net

''' <summary> Event arguments  for remote endpoint connection. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07 </para>
''' </remarks>
Public NotInheritable Class ClientConnectedEventArgs
    Inherits EventArgs

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> Remote EndPoint. </param>
    Public Sub New(ByVal value As IPEndPoint)
        Me.RemoteEndPoint = value
    End Sub

    ''' <summary> Remote EndPoint. </summary>
    ''' <value> The remote end point. </value>
    Public ReadOnly Property RemoteEndPoint() As IPEndPoint

End Class

