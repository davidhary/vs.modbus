Imports System.IO
Imports System.Net
Imports System.Net.Sockets

''' <summary> UDP data class. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Friend Class UDPData
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="udpClient">      UDP client. </param>
    ''' <param name="receiveData">    Received data buffer. </param>
    ''' <param name="remoteEndPoint"> Remote endpoint. </param>
    Public Sub New(ByVal udpClient As UdpClient, ByVal receiveData() As Byte, ByVal remoteEndPoint As IPEndPoint)
        MyBase.New()
        Me._Client = udpClient
        Me._RemoteEndPoint = remoteEndPoint
        Me._MemoryStream = New MemoryStream(receiveData)
    End Sub

#Region " IDisposable Support "

    ''' <summary> True to disposed value. </summary>
    Private _DisposedValue As Boolean ' To detect redundant calls

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me._DisposedValue Then
                If disposing Then
                    ' TODO: dispose managed state (managed objects).
                    If Me._MemoryStream IsNot Nothing Then
                        Me._MemoryStream.Dispose()
                        Me._MemoryStream = Nothing
                    End If
                End If
                ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                ' TODO: set large fields to null.
            End If
        Finally
            Me._DisposedValue = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
    End Sub
#End Region

#End Region

#Region " Global variables "

    ''' <summary>
    ''' Input stream
    ''' </summary>
    Private _MemoryStream As MemoryStream

    ''' <summary>
    ''' UDP Client
    ''' </summary>
    Private ReadOnly _Client As UdpClient

    ''' <summary>
    ''' Remote endpoint
    ''' </summary>
    Private ReadOnly _RemoteEndPoint As IPEndPoint

#End Region

#Region " Parameters "

    ''' <summary> Get stream length. </summary>
    ''' <value> The length. </value>
    Public ReadOnly Property Length() As Long
        Get
            Return Me._MemoryStream.Length
        End Get
    End Property

#End Region

#Region " Class functions "

    ''' <summary> Read a byte from input stream. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> Read byte or <c>-1</c> if there are any bytes. </returns>
    Public Function ReadByte() As Integer
        Return Me._MemoryStream.ReadByte()
    End Function

    ''' <summary> Send an response buffer. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="buffer"> Buffer to send. </param>
    ''' <param name="offset"> Buffer offset. </param>
    ''' <param name="size">   Data length. </param>
    Public Sub WriteResponse(ByVal buffer() As Byte, ByVal offset As Integer, ByVal size As Integer)
        Dim tmp_buffer(size - 1) As Byte
        System.Buffer.BlockCopy(buffer, offset, tmp_buffer, 0, size)
        Me._Client.Send(tmp_buffer, size, Me._RemoteEndPoint)
    End Sub

    ''' <summary> Close input stream. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Sub Close()
        If Me._MemoryStream IsNot Nothing Then
            Me._MemoryStream.Close()
        End If
    End Sub

#End Region
End Class


