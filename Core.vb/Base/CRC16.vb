''' <summary> Static class to compute CRC-16, also called CRC-16-IBM, and CRC-ANSI. </summary>
''' <remarks>
''' David, 2016-05-07. Source: https://code.google.com/p/free-dotnet-modbus/. Each Modbus RTU
''' message is terminated with two error checking bytes called a CRC or Cyclic Redundancy Check.
''' <para>
''' (c) 2013 Simone Assunti. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Friend NotInheritable Class CRC16

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Private Sub New()


    End Sub

#Region " MODULO FUNCTIONS "

    ''' <summary>
    ''' Base reversed polynomial representation.
    ''' </summary>
    Private Const _POLY As UShort = &HA001

    ''' <summary>
    ''' CRC 16 table
    ''' </summary>
    Private Shared _Table() As UShort

    ''' <summary> Initialize the CRC table. </summary>
    ''' <remarks> https://en.wikipedia.org/wiki/Cyclic_redundancy_check. </remarks>
    Private Shared Sub InitializeTable()
        Dim crc, c As UShort

        If CRC16._Table Is Nothing Then
            CRC16._Table = New UShort(255) {}
            For ii As Integer = 0 To 255
                crc = 0
                c = CUShort(ii)
                For jj As Integer = 0 To 7

                    crc = If(((crc Xor c) And &H1) = &H1, CUShort((crc >> 1) Xor _POLY), CUShort(crc >> 1))

                    c = CUShort(c >> 1)
                Next jj
                CRC16._Table(ii) = crc
            Next ii
        End If
    End Sub

    ''' <summary> Updates a CRC value. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="crc"> Actual CRC value. </param>
    ''' <param name="bt">  Data byte. </param>
    ''' <returns> Computed CRC. </returns>
    Private Shared Function Update(ByVal crc As UShort, ByVal bt As Byte) As UShort

        Dim ushort_bt As UShort = CUShort(&HFF And CUShort(bt))

        CRC16.InitializeTable()

        Dim tmp As UShort = CUShort(crc Xor ushort_bt)
        crc = CUShort((crc >> 8) Xor _Table(tmp And &HFF))

        Return crc
    End Function

    ''' <summary> Calculated buffer CRC16. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="buffer"> Data Buffer. </param>
    ''' <param name="offset"> Buffer offset. </param>
    ''' <param name="length"> Data length. </param>
    ''' <returns> Computed CRC. </returns>
    Public Shared Function Evaluate(ByVal buffer() As Byte, ByVal offset As Integer, ByVal length As Integer) As UShort
        Dim crc As UShort = &HFFFF
        For ii As Integer = 0 To length - 1
            crc = CRC16.Update(crc, buffer(offset + ii))
        Next ii
        Return crc
    End Function

#End Region

End Class


