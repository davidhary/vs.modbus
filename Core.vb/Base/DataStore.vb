''' <summary> Data store class. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Public NotInheritable Class DataStore

#Region " Constructors "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">                Device ID. </param>
    ''' <param name="discreteInputsCount">   Input registers (read-only - 1 bit) </param>
    ''' <param name="coilsCount">            Coils registers (read/write - 1 bit) </param>
    ''' <param name="inputRegistersCount">   Input registers (read-only - 16 bit) </param>
    ''' <param name="holdingRegistersCount"> Holding registers (read/write - 16 bit) </param>
    Public Sub New(ByVal unitId As Byte, ByVal discreteInputsCount As Integer, ByVal coilsCount As Integer,
                   ByVal inputRegistersCount As Integer, ByVal holdingRegistersCount As Integer)
        MyBase.New()

        ' Set device ID
        Me._UnitId = unitId

        discreteInputsCount = If(discreteInputsCount < 0, 0, If(discreteInputsCount > _MAX_ELEMENTS, _MAX_ELEMENTS, discreteInputsCount))
        Me._DiscreteInputs = New Boolean(discreteInputsCount - 1) {}

        coilsCount = If(coilsCount < 0, 0, If(coilsCount > _MAX_ELEMENTS, _MAX_ELEMENTS, coilsCount))
        Me._Coils = New Boolean(coilsCount - 1) {}

        inputRegistersCount = If(inputRegistersCount < 0, 0, If(inputRegistersCount > _MAX_ELEMENTS, _MAX_ELEMENTS, inputRegistersCount))
        Me._InputRegisters = New UShort(inputRegistersCount - 1) {}

        holdingRegistersCount = If(holdingRegistersCount < 0, 0, If(holdingRegistersCount > _MAX_ELEMENTS, _MAX_ELEMENTS, holdingRegistersCount))
        Me._HoldingRegisters = New UShort(holdingRegistersCount - 1) {}

    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> The database if initialized at the maximum capacity allowed. </remarks>
    ''' <param name="unitId"> Device ID. </param>
    Public Sub New(ByVal unitId As Byte)
        Me.New(unitId, _MAX_ELEMENTS, _MAX_ELEMENTS, _MAX_ELEMENTS, _MAX_ELEMENTS)
    End Sub

#End Region

#Region " Constants "

    ''' <summary>
    ''' Max DB elements
    ''' </summary>
    Private Const _MAX_ELEMENTS As Integer = 65536

#End Region

#Region " Modbus database "

    ''' <summary>
    ''' Discrete Inputs - Read Only - 1 bit
    ''' </summary>
    Private ReadOnly _DiscreteInputs() As Boolean

    ''' <summary> Discrete Input registers (read-only - 1 bit) </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> A Boolean() </returns>
    Public Function DiscreteInputs() As Boolean()
        Return Me._DiscreteInputs
    End Function

    ''' <summary>
    ''' Coils - Read/Write - 1 bit
    ''' </summary>
    Private ReadOnly _Coils() As Boolean

    ''' <summary> Coils registers (read/write - 1 bit) </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> A Boolean() </returns>
    Public Function Coils() As Boolean()
        Return Me._Coils
    End Function

    ''' <summary>
    ''' Input registers - Read Only - 16 bit
    ''' </summary>
    Private ReadOnly _InputRegisters() As UShort

    ''' <summary> Input registers (read-only - 16 bit) </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> An UShort() </returns>
    <CLSCompliant(False)>
    Public Function InputRegisters() As UShort()
        Return Me._InputRegisters
    End Function

    ''' <summary>
    ''' Holding registers - Read/Write - 16 bit
    ''' </summary>
    Private ReadOnly _HoldingRegisters() As UShort

    ''' <summary> Holding registers (read/write - 16 bit) </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> An UShort() </returns>
    <CLSCompliant(False)>
    Public Function HoldingRegisters() As UShort()
        Return Me._HoldingRegisters
    End Function

    ''' <summary> Device ID. </summary>
    ''' <value> The identifier of the unit. </value>
    Public ReadOnly Property UnitId() As Byte

#End Region

End Class

