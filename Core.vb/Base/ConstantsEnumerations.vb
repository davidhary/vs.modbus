Imports System.ComponentModel

''' <summary> Connection types. </summary>
''' <remarks> David, 2020-11-11. </remarks>
Public Enum ConnectionType

    ''' <summary> An enum constant representing the serial RTU option. </summary>
    <Description("Modbus serial RTU")>
    SerialRTU = 0

    ''' <summary> An enum constant representing the serial ASCII option. </summary>
    <Description("Modbus serial ASCII")>
    SerialAscii = 1

    ''' <summary> An enum constant representing the TCP IP option. </summary>
    <Description("Modbus TCP/IP")>
    TcpIP = 2

    ''' <summary> An enum constant representing the UDP IP option. </summary>
    <Description("Modbus UDP")>
    UdpIP = 3
End Enum

''' <summary> Type of modbus serial. </summary>
''' <remarks> David, 2020-11-11. </remarks>
Public Enum ModbusSerialType

    ''' <summary> An enum constant representing the MODBUS RTU option. </summary>
    <Description("Modbus RTU")>
    RTU = 0

    ''' <summary> An enum constant representing the MODBUS ASCII option. </summary>
    <Description("Modbus ASCII")>
    ASCII = 1
End Enum

''' <summary> Type of device. </summary>
''' <remarks> David, 2020-11-11. </remarks>
Public Enum DeviceType

    ''' <summary> An enum constant representing the client option. </summary>
    <Description("Modbus Client")>
    Client = 0

    ''' <summary> An enum constant representing the server option. </summary>
    <Description("Modbus Server")>
    Server = 1
End Enum

''' <summary> Modbus database tables. </summary>
''' <remarks> David, 2020-11-11. </remarks>
Public Enum ModbusDatabaseTable

    ''' <summary> An enum constant representing the discrete inputs registers option. </summary>
    <Description("Discrete Inputs Registers")>
    DiscreteInputsRegisters = 0

    ''' <summary> An enum constant representing the coils registers option. </summary>
    <Description("Coils Registers")>
    CoilsRegisters = 1

    ''' <summary> An enum constant representing the input registers option. </summary>
    <Description("Input Registers")>
    InputRegisters = 2

    ''' <summary> An enum constant representing the holding registers option. </summary>
    <Description("HoldingRegisters")>
    HoldingRegisters = 3
End Enum

''' <summary> Modbus calling codes. </summary>
''' <remarks> David, 2020-11-11. </remarks>
Friend Enum ModbusCodes

    ''' <summary> An enum constant representing the read coils option. </summary>
    <Description("Read Coils")>
    ReadCoils = &H1

    ''' <summary> An enum constant representing the read discrete inputs option. </summary>
    <Description("Read Discrete Inputs")>
    ReadDiscreteInputs = &H2

    ''' <summary> An enum constant representing the read holding registers option. </summary>
    <Description("Read Holding Registers")>
    ReadHoldingRegisters = &H3

    ''' <summary> An enum constant representing the read input registers option. </summary>
    <Description("Read Input Registers")>
    ReadInputRegisters = &H4

    ''' <summary> An enum constant representing the write single coil option. </summary>
    <Description("Write Single Coil")>
    WriteSingleCoil = &H5

    ''' <summary> An enum constant representing the write single register option. </summary>
    <Description("Write Single Register")>
    WriteSingleRegister = &H6

    ''' <summary> An enum constant representing the read exception status option. </summary>
    <Description("Read Exception Status")>
    ReadExceptionStatus = &H7

    ''' <summary> An enum constant representing the diagnostic option. </summary>
    <Description("Diagnostic")>
    Diagnostic = &H8

    ''' <summary> An enum constant representing the get com event counter option. </summary>
    <Description("Get Com Event Counter")>
    GetComEventCounter = &HB

    ''' <summary> An enum constant representing the get com event log option. </summary>
    <Description("Get Com Event Log")>
    GetComEventLog = &HC

    ''' <summary> An enum constant representing the write multiple coils option. </summary>
    <Description("Write Multiple Coils")>
    WriteMultipleCoils = &HF

    ''' <summary> An enum constant representing the write multiple registers option. </summary>
    <Description("Write Multiple Registers")>
    WriteMultipleRegisters = &H10

    ''' <summary> An enum constant representing the report server Identifier option. </summary>
    <Description("Report Server Id")>
    ReportServerId = &H11

    ''' <summary> An enum constant representing the read file record option. </summary>
    <Description("Read File Record")>
    ReadFileRecord = &H14

    ''' <summary> An enum constant representing the write file record option. </summary>
    <Description("Write File Record")>
    WriteFileRecord = &H15

    ''' <summary> An enum constant representing the mask write register option. </summary>
    <Description("Mask Write Register")>
    MaskWriteRegister = &H16

    ''' <summary> An enum constant representing the read write multiple registers option. </summary>
    <Description("Read Write Multiple Registers")>
    ReadWriteMultipleRegisters = &H17

    ''' <summary> An enum constant representing the read FIFO queue option. </summary>
    <Description("Read Fifo Queue")>
    ReadFifoQueue = &H18

    ''' <summary> An enum constant representing the read device identification option. </summary>
    <Description("Read Device Identification")>
    ReadDeviceIdentification = &H2B
End Enum

''' <summary> Interface Error codes. </summary>
''' <remarks> David, 2020-11-11. </remarks>
Public Enum InterfaceError

    ''' <summary> An enum constant representing the no error option. </summary>
    <Description("No Error")>
    NoError = 0

    ''' <summary> An enum constant representing the receive timeout option. </summary>
    <Description("Receive Timeout")>
    ReceiveTimeout = -1

    ''' <summary> An enum constant representing the wrong transaction Identifier option. </summary>
    <Description("Wrong Transaction Id")>
    WrongTransactionId = -2

    ''' <summary> An enum constant representing the wrong protocol Identifier option. </summary>
    <Description("Wrong Protocol Id")>
    WrongProtocolId = -3

    ''' <summary> An enum constant representing the wrong response unit Identifier option. </summary>
    <Description("Wrong Response Unit Id")>
    WrongResponseUnitId = -4

    ''' <summary> An enum constant representing the wrong response function code option. </summary>
    <Description("Wrong Response Function Code")>
    WrongResponseFunctionCode = -5

    ''' <summary> An enum constant representing the message too short option. </summary>
    <Description("Message Too Short")>
    MessageTooShort = -6

    ''' <summary> An enum constant representing the wrong response address option. </summary>
    <Description("Wrong Response Address")>
    WrongResponseAddress = -7

    ''' <summary> An enum constant representing the wrong response registers option. </summary>
    <Description("Wrong ResponseRegisters")>
    WrongResponseRegisters = -8

    ''' <summary> An enum constant representing the wrong response value option. </summary>
    <Description("Wrong Response Value")>
    WrongResponseValue = -9

    ''' <summary> An enum constant representing the wrong CRC option. </summary>
    <Description("Wrong Crc")>
    WrongCrc = -10

    ''' <summary> An enum constant representing the wrong lrc option. </summary>
    <Description("Wrong Lrc")>
    WrongLrc = -11

    ''' <summary> An enum constant representing the start Character not found option. </summary>
    <Description("Start Char Not Found")>
    StartCharNotFound = -12

    ''' <summary> An enum constant representing the end Characters not found option. </summary>
    <Description("End Chars Not Found")>
    EndCharsNotFound = -13

    ''' <summary> An enum constant representing the wrong response and mask option. </summary>
    <Description("Wrong Response And Mask")>
    WrongResponseAndMask = -14

    ''' <summary> An enum constant representing the wrong response or mask option. </summary>
    <Description("Wrong Response Or Mask")>
    WrongResponseOrMask = -15

    ''' <summary> An enum constant representing the thread block request option. </summary>
    <Description("Thread Block Request")>
    ThreadBlockRequest = -16

    ''' <summary> An enum constant representing the wrong write single coil value option. </summary>
    <Description("Wrong Write Single Coil Value")>
    WrongWriteSingleCoilValue = -17

    ''' <summary> An enum constant representing the too many registers requested option. </summary>
    <Description("Too Many Registers Requested")>
    TooManyRegistersRequested = -18

    ''' <summary> An enum constant representing the zero registers requested option. </summary>
    <Description("Zero Registers Requested")>
    ZeroRegistersRequested = -19

    ''' <summary> An enum constant representing the exception illegal function option. </summary>
    <Description("Exception Illegal Function")>
    ExceptionIllegalFunction = -20

    ''' <summary> An enum constant representing the exception illegal data address option. </summary>
    <Description("Exception Illegal Data Address")>
    ExceptionIllegalDataAddress = -21

    ''' <summary> An enum constant representing the exception illegal data value option. </summary>
    <Description("Exception Illegal Data Value")>
    ExceptionIllegalDataValue = -22

    ''' <summary> An enum constant representing the exception server device failure option. </summary>
    <Description("Exception Server Device Failure")>
    ExceptionServerDeviceFailure = -23

    ''' <summary> An enum constant representing the exception acknowledge option. </summary>
    <Description("Exception Acknowledge")>
    ExceptionAcknowledge = -24

    ''' <summary> An enum constant representing the exception server device busy option. </summary>
    <Description("Exception Server Device Busy")>
    ExceptionServerDeviceBusy = -25

    ''' <summary> An enum constant representing the exception memory parity error option. </summary>
    <Description("Exception Memory Parity Error")>
    ExceptionMemoryParityError = -26

    ''' <summary>
    ''' An enum constant representing the exception gateway path unavailable option.
    ''' </summary>
    <Description("Exception Gateway Path Unavailable")>
    ExceptionGatewayPathUnavailable = -27

    ''' <summary>
    ''' An enum constant representing the exception gateway target device failed to respond option.
    ''' </summary>
    <Description("Exception Gateway Target Device Failed To Respond")>
    ExceptionGatewayTargetDeviceFailedToRespond = -28

    ''' <summary> An enum constant representing the wrong register address option. </summary>
    <Description("Wrong Register Address")>
    WrongRegisterAddress = -29

    ''' <summary> An enum constant representing the message too long option. </summary>
    <Description("Message Too Long")>
    MessageTooLong = -30

End Enum

