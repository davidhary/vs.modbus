Imports System.Text
Imports System.IO.Ports
Imports System.Net.Sockets
Imports System.Threading
Imports isr.Core.BitConverterExtensions

''' <summary> Base abstract class for Modbus Server instances. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Public MustInherit Class ModbusServerBase
    Inherits ModbusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="dataStore"> Database Modbus. </param>
    Protected Sub New(ByVal dataStore() As DataStore)
        MyBase.New()
        ' Device status
        Me.DeviceType = DeviceType.Server

        ' Assign modbus database
        Me._ModbusDataStore = dataStore

        ' Initialize timer
        Me._InterCharacterTimeoutStopwatch = New Stopwatch()

    End Sub

#End Region

#Region " GLOBAL VARIABLES "

    ''' <summary> Execution status of thread that manage calls. Was volatile. </summary>
    ''' <value> The running. </value>
    Protected Property Running As Boolean = False

#End Region

#Region " INSTANCES "

    ''' <summary> Incoming calls management thread. </summary>
    ''' <value> The guest request thread. </value>
    Protected Property GuestRequestThread As Thread

    ''' <summary>
    ''' Inter-character timeout timer
    ''' </summary>
    Private ReadOnly _InterCharacterTimeoutStopwatch As Stopwatch

#End Region

#Region " PARAMETERS "

    ''' <summary>
    ''' Database Modbus
    ''' </summary>
    Private ReadOnly _ModbusDataStore() As DataStore

    ''' <summary> Database Modbus. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> A DataStore() </returns>
    Public Function ModbusDataStore() As DataStore()
        Return Me._ModbusDataStore
    End Function

#End Region

#Region " INCOMING CALLS MANAGEMENT THREAD "

    ''' <summary> Incoming calls management thread. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected MustOverride Sub GuestRequests()

#End Region

#Region " START AND STOP LISTENING MESSAGES "

    ''' <summary> Function Prototype for start listening messages. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public MustOverride Sub StartListen()

    ''' <summary> Function Prototype for stop listening messages. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public MustOverride Sub StopListen()

#End Region

#Region " PROTOCOL FUNCTIONS "

    ''' <summary>
    ''' Check if all registers from (starting_address + quantity_of_registers) are present in device
    ''' database.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">          Unit ID. </param>
    ''' <param name="table">           database modbus table. </param>
    ''' <param name="startingAddress"> Starting address (offset) in database. </param>
    ''' <param name="registersCount">  Number of registers to read/write. </param>
    ''' <returns> True if register are present, otherwise False. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Function IsAllRegistersPresent(ByVal unitId As Byte, ByVal table As ModbusDatabaseTable,
                                           ByVal startingAddress As UShort, ByVal registersCount As UShort) As Boolean
        Dim ret As Boolean = True

        Select Case table
            Case ModbusDatabaseTable.DiscreteInputsRegisters
                Try
                    Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).DiscreteInputs.ToList().GetRange(startingAddress, registersCount)
                Catch
                    ret = False
                End Try

            Case ModbusDatabaseTable.CoilsRegisters
                Try
                    Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).Coils.ToList().GetRange(startingAddress, registersCount)
                Catch
                    ret = False
                End Try

            Case ModbusDatabaseTable.InputRegisters
                Try
                    Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).InputRegisters.ToList().GetRange(startingAddress, registersCount)
                Catch
                    ret = False
                End Try

            Case ModbusDatabaseTable.HoldingRegisters
                Try
                    Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters.ToList().GetRange(startingAddress, registersCount)
                Catch
                    ret = False
                End Try

            Case Else
                ret = False
        End Select

        Return ret
    End Function

    ''' <summary> Build an exception message. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sendBuffer">     Send buffer. </param>
    ''' <param name="code">           Modbus operation code. </param>
    ''' <param name="interfaceError"> interface Error code. </param>
    Private Shared Sub BuildExceptionMessage(ByVal sendBuffer As List(Of Byte), ByVal code As ModbusCodes, ByVal interfaceError As InterfaceError)
        Dim exception_code As Byte = 0

        sendBuffer.Clear()
        Select Case interfaceError
            Case InterfaceError.ExceptionIllegalFunction
                exception_code = 1
            Case InterfaceError.ExceptionIllegalDataAddress
                exception_code = 2

            Case InterfaceError.ExceptionIllegalDataValue
                exception_code = 3

            Case InterfaceError.ExceptionServerDeviceFailure
                exception_code = 4
        End Select
        ' Add exception identifier
        sendBuffer.Add(CByte(&H80 + CByte(code)))
        ' Add exception code
        sendBuffer.Add(exception_code)
    End Sub

    ''' <summary> Read a byte from a specified flux. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="flux"> Flux of reading bytes. </param>
    ''' <returns> Read byte or -1 if no data is present. </returns>
    Private Function ReadByte(ByVal flux As Object) As Integer
        Dim ret As Integer = -1

        Select Case Me.ConnectionType
            Case ConnectionType.UdpIP
                Dim udpd As UDPData = CType(flux, UDPData)
                If udpd.Length > 0 Then
                    ret = udpd.ReadByte()
                End If

            Case ConnectionType.TcpIP
                Dim ns As NetworkStream = CType(flux, NetworkStream)
                If ns.DataAvailable Then
                    ret = ns.ReadByte()
                End If

            Case ConnectionType.SerialAscii, ConnectionType.SerialRTU
                Dim sp As SerialPort = CType(flux, SerialPort)
                ret = -1
                ' Start timeout counter when reading the first character
                If Not Me._InterCharacterTimeoutStopwatch.IsRunning Then Me._InterCharacterTimeoutStopwatch.Start()

                Do
                    If sp.BytesToRead > 0 Then ret = sp.ReadByte()
                Loop Until ret >= 0 OrElse Me._InterCharacterTimeoutStopwatch.Elapsed > Me.InterCharacterDelay

                ' Char received with no errors...reset timeout counter for next char!
                If ret >= 0 Then Me._InterCharacterTimeoutStopwatch.Stop()

        End Select

        Return ret
    End Function

    ''' <summary> Write a byte on a specified flux. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="flux">   Writing byte flux. </param>
    ''' <param name="buffer"> Buffer of bytes. </param>
    ''' <param name="offset"> Buffer starting offset. </param>
    ''' <param name="size">   Number of bytes to write. </param>
    Private Sub WriteBuffer(ByVal flux As Object, ByVal buffer() As Byte, ByVal offset As Integer, ByVal size As Integer)
        Select Case Me.ConnectionType
            Case ConnectionType.UdpIP
                Dim udpd As UDPData = CType(flux, UDPData)
                udpd.WriteResponse(buffer, offset, size)

            Case ConnectionType.TcpIP
                Dim ns As NetworkStream = CType(flux, NetworkStream)
                ns.Write(buffer, offset, size)

            Case ConnectionType.SerialAscii, ConnectionType.SerialRTU
                Dim sp As SerialPort = CType(flux, SerialPort)
                sp.Write(buffer, offset, size)
        End Select
    End Sub

    ''' <summary> Modbus Server response manager. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="sendBuffer">    Send buffer. </param>
    ''' <param name="receiveBuffer"> Receive buffer. </param>
    ''' <param name="flux">          Object for flux manager. </param>
    Protected Sub IncomingMessagePolling(ByVal sendBuffer As List(Of Byte), ByVal receiveBuffer As List(Of Byte), ByVal flux As Object)
        If sendBuffer Is Nothing Then Throw New ArgumentNullException(NameOf(sendBuffer))
        If receiveBuffer Is Nothing Then Throw New ArgumentNullException(NameOf(receiveBuffer))
        If flux Is Nothing Then Throw New ArgumentNullException(NameOf(flux))

        Dim transaction_id As UShort = 0
        Dim unit_id As Byte = 0
        Dim sw As New Stopwatch()

        ' Set errors at 0
        Me.LastError = InterfaceError.NoError
        ' Empty reception buffer
        receiveBuffer.Clear()
        ' Start reception loop
        sw.Start()
        ' true if read the first byte.
        Dim in_ric As Boolean = False
        ' count this many inter-character timeouts to determine if message completed.
        Dim messageCompleteCountDown As Integer = 3
        ' DH: new code resets the character timeout stop watch upon reading each character. 
        Me._InterCharacterTimeoutStopwatch.Stop()
        Do
            Dim data As Integer = Me.ReadByte(flux)
            If data <> -1 Then
                messageCompleteCountDown = 3
                If Not in_ric Then in_ric = True
                If Me.ConnectionType = ConnectionType.SerialAscii Then
                    If CByte(data) = Encoding.ASCII.GetBytes(New Char() {AsciiStartFrameCharacter}).First() Then
                        receiveBuffer.Clear()
                    End If
                End If
                receiveBuffer.Add(CByte(data))
            ElseIf in_ric AndAlso data < 0 Then
                messageCompleteCountDown -= 1
            Else
                isr.Core.ApplianceBase.Delay(1)
            End If
        Loop While (sw.Elapsed < Me.RxTimeout) AndAlso Me.Running AndAlso messageCompleteCountDown > 0
        Me._InterCharacterTimeoutStopwatch.Stop()
        sw.Stop()
        ' Check for a stop request
        If Not Me.Running Then
            Me.LastError = InterfaceError.ThreadBlockRequest
            Return
        End If
        ' Check for timeout error
        If sw.Elapsed > Me.RxTimeout Then
            Me.LastError = InterfaceError.ReceiveTimeout
            Return
        End If
        ' Check message length
        If receiveBuffer.Count < 3 Then
            Me.LastError = InterfaceError.MessageTooShort
            Return
        End If
        ' Message received, start decoding...
        Select Case Me.ConnectionType
            Case ConnectionType.SerialAscii
                ' Check and delete start char
                If Encoding.ASCII.GetChars(receiveBuffer.ToArray()).FirstOrDefault() <> AsciiStartFrameCharacter Then
                    Me.LastError = InterfaceError.StartCharNotFound
                    Return
                End If
                receiveBuffer.RemoveAt(0)
                ' Check and delete stop chars
                Dim end_chars() As Char = {AsciiEndFrameFirstCharacter, AsciiEndFrameSecondCharacter}
                Dim last_two() As Char = Encoding.ASCII.GetChars(receiveBuffer.GetRange(receiveBuffer.Count - 2, 2).ToArray())
                If Not end_chars.SequenceEqual(last_two) Then
                    Me.LastError = InterfaceError.EndCharsNotFound
                    Return
                End If
                receiveBuffer.RemoveRange(receiveBuffer.Count - 2, 2)
                ' Recode message in binary
                receiveBuffer = Core.BitConverterExtensions.Methods.FromAsciiBytes(receiveBuffer.ToArray()).ToList()
                ' Calculate and remove LRC
                Dim msg_lrc As Byte = receiveBuffer(receiveBuffer.Count - 1)
                Dim calc_lrc As Byte = LRC.Evaluate(receiveBuffer.ToArray(), 0, receiveBuffer.Count - 1)
                If msg_lrc <> calc_lrc Then
                    Me.LastError = InterfaceError.WrongLrc
                    Return
                End If
                receiveBuffer.RemoveAt(receiveBuffer.Count - 1)
                ' Analyze destination address, if not present in database, discard message and continue
                unit_id = receiveBuffer(0)
                If Not Me._ModbusDataStore.Any(Function(x) x.UnitId = unit_id) Then
                    Return
                End If
                receiveBuffer.RemoveAt(0)

            Case ConnectionType.SerialRTU
                ' Check CRC
                Dim msg_crc As UShort = BitConverter.ToUInt16(receiveBuffer.ToArray(), receiveBuffer.Count - 2)
                Dim calc_crc As UShort = CRC16.Evaluate(receiveBuffer.ToArray(), 0, receiveBuffer.Count - 2)
                If msg_crc <> calc_crc Then
                    Me.LastError = InterfaceError.WrongCrc
                    Return
                End If
                ' Analyze destination address, if not present in database, discard message and continue
                unit_id = receiveBuffer(0)
                If Not Me._ModbusDataStore.Any(Function(x) x.UnitId = unit_id) Then
                    Return
                End If
                ' Message is okay, remove unit_id and CRC                    
                receiveBuffer.RemoveRange(0, 1)
                receiveBuffer.RemoveRange(receiveBuffer.Count - 2, 2)

            Case ConnectionType.UdpIP, ConnectionType.TcpIP
                ' Decode MBAP Header
                transaction_id = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 0)
                ' Check protocol ID
                'INSTANT VB NOTE: The variable protocol_id was renamed since Visual Basic does not handle local variables named the same as class members well:
                Dim protocol_id_Renamed As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 2)
                If protocol_id_Renamed <> ProtocolId Then
                    Me.LastError = InterfaceError.WrongProtocolId
                    Return
                End If
                ' Acquire data length and check it                    
                Dim len As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 4)
                If (receiveBuffer.Count - 6) < len Then
                    Me.LastError = InterfaceError.MessageTooShort
                ElseIf (receiveBuffer.Count - 6) > len Then
                    Me.LastError = InterfaceError.MessageTooLong
                    Return
                End If
                ' Analyze destination address, if not present in database, discard message and continue
                unit_id = receiveBuffer(6)
                If Not Me._ModbusDataStore.Any(Function(x) x.UnitId = unit_id) Then
                    Return
                End If
                ' Message is okay, remove MBAP header for reception buffer                    
                receiveBuffer.RemoveRange(0, MbapHeaderLength)
        End Select
        ' Adjust data and build response
        Me.AdjAndReply(sendBuffer, receiveBuffer, flux, unit_id, transaction_id)
    End Sub

    ''' <summary> Adjust received data and build response. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sendBuffer">    Send buffer. </param>
    ''' <param name="receiveBuffer"> Receive buffer. </param>
    ''' <param name="flux">          Object for flux guest. </param>
    ''' <param name="unitId">        Server id. </param>
    ''' <param name="transactionId"> Transaction id of TCP Server. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AdjAndReply(ByVal sendBuffer As List(Of Byte), ByVal receiveBuffer As List(Of Byte),
                            ByVal flux As Object, ByVal unitId As Byte, ByVal transactionId As UShort)
        Dim sa, sa1, qor, qor1, bc, val, and_mask, or_mask As UShort
        Dim bytes As Integer
        Dim cv As Boolean
        Dim ret_vals() As Boolean
        Dim mdbcode As ModbusCodes

        ' Empty reception buffer
        sendBuffer.Clear()
        ' Adjust data
        mdbcode = CType(receiveBuffer(0), ModbusCodes)
        Select Case mdbcode
            Case ModbusCodes.ReadCoils
                ' Read received commands
                sa = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 1)
                qor = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 3)
                If Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).Coils.Length = 0 Then
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not ((qor >= 1) AndAlso (qor <= MaxCoilsInReadMessage)) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataValue
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.CoilsRegisters, sa, qor) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                ' Reply
                sendBuffer.Add(CByte(ModbusCodes.ReadCoils))
                bytes = (qor \ 8) + (If(qor Mod 8 = 0, 0, 1))
                sendBuffer.Add(CByte(bytes))
                ret_vals = New Boolean(bytes * 8 - 1) {}
                Try
                    Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).Coils.ToList().GetRange(sa, qor).CopyTo(ret_vals)
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try
                For ii As Integer = 0 To bytes - 1
                    sendBuffer.Add(Core.BitConverterExtensions.Methods.EightBitToByte(ret_vals, ii * 8))
                Next ii

            Case ModbusCodes.ReadDiscreteInputs
                ' Read received commands                    
                sa = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 1)
                qor = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 3)
                If Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).DiscreteInputs.Length = 0 Then
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not ((qor >= 1) AndAlso (qor <= MaxDiscreteInputsInReadMessage)) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataValue
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.DiscreteInputsRegisters, sa, qor) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                ' Reply
                sendBuffer.Add(CByte(ModbusCodes.ReadDiscreteInputs))
                bytes = (qor \ 8) + (If(qor Mod 8 = 0, 0, 1))
                sendBuffer.Add(CByte(bytes))
                ret_vals = New Boolean(bytes * 8 - 1) {}
                Try
                    Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).DiscreteInputs.ToList().GetRange(sa, qor).CopyTo(ret_vals)
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try
                For ii As Integer = 0 To bytes - 1
                    sendBuffer.Add(Core.BitConverterExtensions.Methods.EightBitToByte(ret_vals, ii * 8))
                Next ii

            Case ModbusCodes.ReadHoldingRegisters
                ' Read received commands
                sa = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 1)
                qor = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 3)
                If Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters.Length = 0 Then
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not ((qor >= 1) AndAlso (qor <= MaxHoldingRegistersInReadMessage)) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataValue
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.HoldingRegisters, sa, qor) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                ' Reply
                sendBuffer.Add(CByte(ModbusCodes.ReadHoldingRegisters))
                sendBuffer.Add(CByte(2 * qor))
                Try
                    For ii As Integer = 0 To (qor * 2) - 1 Step 2
                        sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters(sa + (ii \ 2))))
                    Next ii
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try

            Case ModbusCodes.ReadInputRegisters
                ' Read received commands
                sa = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 1)
                qor = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 3)
                If Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).InputRegisters.Length = 0 Then
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not ((qor >= 1) AndAlso (qor <= MaxInputRegistersInReadMessage)) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataValue
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.InputRegisters, sa, qor) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                ' Reply
                sendBuffer.Add(CByte(ModbusCodes.ReadInputRegisters))
                sendBuffer.Add(CByte(2 * qor))
                Try
                    For ii As Integer = 0 To (qor * 2) - 1 Step 2
                        sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).InputRegisters(sa + (ii \ 2))))
                    Next ii
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try

            Case ModbusCodes.WriteSingleCoil
                ' Adjusting
                sa = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 1)
                val = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 3)
                Select Case val
                    Case &H0
                        cv = False

                    Case &HFF00
                        cv = True

                    Case Else
                        Me.LastError = InterfaceError.ExceptionIllegalDataValue
                        cv = False ' Dummy
                End Select
                If Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).Coils.Length = 0 Then
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Me.LastError = InterfaceError.ExceptionIllegalDataValue Then
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.CoilsRegisters, sa, 1) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Me.LastError = InterfaceError.WrongWriteSingleCoilValue Then
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionIllegalDataValue)
                    Exit Select
                End If
                Try
                    Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).Coils(sa) = cv
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try
                ' Reply
                sendBuffer.Add(CByte(ModbusCodes.WriteSingleCoil))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(sa))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(val))

            Case ModbusCodes.WriteSingleRegister
                ' Adjusting
                sa = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 1)
                val = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 3)
                If Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters.Length = 0 Then
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not ((val >= &H0) AndAlso (val <= &HFFFF)) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataValue
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.HoldingRegisters, sa, 1) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                Try
                    Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters(sa) = val
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try
                ' Reply
                sendBuffer.Add(CByte(ModbusCodes.WriteSingleRegister))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(sa))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(val))

            Case ModbusCodes.WriteMultipleCoils
                ' Adjusting
                sa = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 1)
                qor = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 3)
                If Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).Coils.Length = 0 Then
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not ((qor >= 1) AndAlso (qor <= MaxCoilsInWriteMessage)) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataValue
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.CoilsRegisters, sa, qor) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                bc = receiveBuffer(5)
                Dim buffer() As Byte = receiveBuffer.GetRange(6, bc).ToArray()
                Dim ba As New BitArray(buffer)
                Try
                    ba.CopyTo(Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).Coils, sa)
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try
                ' Reply
                sendBuffer.Add(CByte(ModbusCodes.WriteMultipleCoils))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(sa))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(qor))

            Case ModbusCodes.WriteMultipleRegisters
                ' Adjusting
                sa = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 1)
                qor = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 3)
                If Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters.Length = 0 Then
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not ((qor >= 1) AndAlso (qor <= MaxHoldingRegistersInWriteMessage)) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataValue
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.HoldingRegisters, sa, qor) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                bc = receiveBuffer(5)
                Try
                    Dim ii As Integer = 0
                    Do While ii < bc
                        Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters(sa + (ii \ 2)) = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 6 + ii)
                        ii += 2
                    Loop
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try
                ' Reply
                sendBuffer.Add(CByte(ModbusCodes.WriteMultipleRegisters))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(sa))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(qor))

            Case ModbusCodes.MaskWriteRegister
                ' Adjusting
                sa = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 1)
                and_mask = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 3)
                or_mask = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 5)
                If Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters.Length = 0 Then
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not (((and_mask >= &H0) AndAlso (and_mask <= &HFFFF)) OrElse ((and_mask >= &H0) AndAlso (and_mask <= &HFFFF))) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataValue
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.HoldingRegisters, sa, 1) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                Try
                    Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters(sa) = CUShort((Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters(sa) And and_mask) Or (or_mask And ((Not and_mask))))
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try
                ' Reply
                sendBuffer.Add(CByte(ModbusCodes.MaskWriteRegister))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(sa))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(and_mask))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(or_mask))

            Case ModbusCodes.ReadWriteMultipleRegisters
                ' Adjusting
                sa = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 1)
                qor = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 3)
                sa1 = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 5)
                qor1 = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 7)
                If Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters.Length = 0 Then
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If Not (((qor >= 1) AndAlso (qor <= MaxHoldingRegistersToReadInReadWriteMessage)) OrElse ((qor1 >= 1) AndAlso (qor1 <= MaxHoldingRegistersToWriteInReadWriteMessage))) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataValue
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                If ((Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.HoldingRegisters, sa, qor))) OrElse ((Not Me.IsAllRegistersPresent(unitId, ModbusDatabaseTable.HoldingRegisters, sa1, qor1))) Then
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
                    Exit Select
                End If
                bc = receiveBuffer(9)
                ' First: Exec writing...
                Try
                    For ii As Integer = 0 To bc - 1 Step 2
                        Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters(sa1 + (ii \ 2)) = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(receiveBuffer.ToArray(), 10 + ii)
                    Next ii
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try
                ' Second: Exec reading and prepare the reply
                sendBuffer.Add(CByte(ModbusCodes.ReadWriteMultipleRegisters))
                sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(CUShort(qor * 2)))
                Try
                    For ii As Integer = 0 To (qor * 2) - 1 Step 2
                        sendBuffer.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(Me._ModbusDataStore.Single(Function(x) x.UnitId = unitId).HoldingRegisters(sa + (ii \ 2))))
                    Next ii
                Catch
                    BuildExceptionMessage(sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure)
                    Exit Select
                End Try

            Case Else
                Me.LastError = InterfaceError.ExceptionIllegalFunction
                BuildExceptionMessage(sendBuffer, mdbcode, Me.LastError)
        End Select
        ' Send response
        Me.SendReply(sendBuffer, flux, unitId, transactionId)
    End Sub

    ''' <summary> Send the response. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="send_buffer">   Send buffer. </param>
    ''' <param name="flux">          Object for flux guest. </param>
    ''' <param name="unitId">        Server ID. </param>
    ''' <param name="transactionId"> Transaction id of TCP Server. </param>
    Private Sub SendReply(ByVal send_buffer As List(Of Byte), ByVal flux As Object, ByVal unitId As Byte, ByVal transactionId As UShort)
        Select Case Me.ConnectionType
            Case ConnectionType.SerialAscii
                ' Add unit ID
                send_buffer.Insert(0, unitId)
                ' Enqueue LRC
                send_buffer.Add(LRC.Evaluate(send_buffer.ToArray(), 0, send_buffer.Count))
                ' ASCII encoding
                send_buffer = Core.BitConverterExtensions.Methods.ToAsciiBytes(send_buffer.ToArray()).ToList()
                ' Add START character
                send_buffer.Insert(0, Encoding.ASCII.GetBytes(New Char() {AsciiStartFrameCharacter}).First())
                ' Enqueue STOP chars
                send_buffer.AddRange(Encoding.ASCII.GetBytes(New Char() {AsciiEndFrameFirstCharacter, AsciiEndFrameSecondCharacter}))

            Case ConnectionType.SerialRTU

                ' Add unit ID
                send_buffer.Insert(0, unitId)

                ' Enqueue CRC
                send_buffer.AddRange(BitConverter.GetBytes(CRC16.Evaluate(send_buffer.ToArray(), 0, send_buffer.Count)))

                ' Wait for inter frame delay
                isr.Core.ApplianceBase.Delay(Me.InterFrameDelay)

            Case ConnectionType.UdpIP, ConnectionType.TcpIP
                ' Build MBAP header
                send_buffer.InsertRange(0, Core.BitConverterExtensions.Methods.BigEndInt8(transactionId))
                send_buffer.InsertRange(2, Core.BitConverterExtensions.Methods.BigEndInt8(ProtocolId))
                send_buffer.InsertRange(4, Core.BitConverterExtensions.Methods.BigEndInt8(CUShort(1 + send_buffer.Count - 4)))
                send_buffer.Insert(6, unitId)
        End Select
        ' Send the buffer
        Me.WriteBuffer(flux, send_buffer.ToArray(), 0, send_buffer.Count)
    End Sub

#End Region

End Class

