Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.IO.Ports
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Reflection
Imports System.Diagnostics

''' <summary> Modbus Server UDP Class. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Public NotInheritable Class ModbusServerUDP
    Inherits ModbusServerBase

#Region "Constructor"

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="dataStore">    Modbus databases array. </param>
    ''' <param name="localAddress"> Local listening IP addresses. </param>
    ''' <param name="port">         Listening TCP port. </param>
    Public Sub New(ByVal dataStore() As DataStore, ByVal localAddress As IPAddress, ByVal port As Integer)
        MyBase.New(dataStore)
        ' Set device states
        Me.ConnectionType = ConnectionType.UdpIP
        ' Create UDP listener
        Me._UdpListener = New UdpClient(New IPEndPoint(localAddress, port))
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.Disposed Then
                If disposing Then
                    If Me._ManualResetEvent IsNot Nothing Then
                        Me._ManualResetEvent.Dispose()
                        Me._ManualResetEvent = Nothing
                    End If
                    If Me._UdpListener IsNot Nothing Then
                        Me._UdpListener.Dispose()
                        Me._UdpListener = Nothing
                    End If
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


#End Region

#Region "Instances"

    ''' <summary>
    ''' UDP Listener
    ''' </summary>
    Private _UdpListener As UdpClient

    ''' <summary>
    ''' Manual reset event
    ''' </summary>
    Private _ManualResetEvent As New ManualResetEvent(False)

#End Region

#Region " Incoming connections callback "

    ''' <summary> Incoming connection callback. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="asyncResult"> Result. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DoAcceptUdpDataCallback(ByVal asyncResult As IAsyncResult)
        Dim udp_data As UDPData = Nothing
        Dim remote_ep As IPEndPoint = Nothing

        Try
            Dim send_buffer As New List(Of Byte)()
            Dim receive_buffer As New List(Of Byte)()

            ' Copy listener instance
            Dim listener As UdpClient = CType(asyncResult.AsyncState, UdpClient)
            ' Get input frame and remote endpoint
            Dim rx_buffer As Byte() = listener.EndReceive(asyncResult, remote_ep)
            ' Instance UDPData class
            udp_data = New UDPData(listener, rx_buffer, remote_ep)
            ' Set event
            Me._ManualResetEvent.Set()
            ' Process incoming call
            Me.IncomingMessagePolling(send_buffer, receive_buffer, udp_data)
        Catch
        Finally
            If udp_data IsNot Nothing Then
                udp_data.Close()
            End If
        End Try
    End Sub

#End Region

#Region " INCOMING CALL PROCESS THREAD "

    ''' <summary> Incoming call process thread. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Overrides Sub GuestRequests()
        Do While Me.Running

            ' Reset event
            Me._ManualResetEvent.Reset()

            ' Asynchronous call to process callback
            Me._UdpListener.BeginReceive(New AsyncCallback(AddressOf Me.DoAcceptUdpDataCallback), Me._UdpListener)

            ' wait for event
            Me._ManualResetEvent.WaitOne()
        Loop
    End Sub

#End Region

#Region " START AND STOP FUNCTIONS "

    ''' <summary> Start listening. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub StartListen()
        If Me.GuestRequestThread Is Nothing Then
            Me.Running = True
            Me.GuestRequestThread = New Thread(AddressOf Me.GuestRequests)
            Me.GuestRequestThread.Start()
        End If
    End Sub

    ''' <summary> Stop listening. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub StopListen()
        If Me.GuestRequestThread IsNot Nothing Then
            Me.Running = False
            Me.GuestRequestThread.Join()
            Me.GuestRequestThread = Nothing
        End If
        Me._UdpListener.Close()
    End Sub

#End Region

End Class
