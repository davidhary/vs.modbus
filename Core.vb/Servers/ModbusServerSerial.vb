Imports System.IO.Ports
Imports System.Threading

Imports isr.Modbus.SerialPortExtensions

''' <summary> Serial Modbus Server Class. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Public NotInheritable Class ModbusServerSerial
    Inherits ModbusServerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="dataStore"> Modbus database. </param>
    ''' <param name="type">      Type of serial modbus protocol (RTU or ASCII) </param>
    ''' <param name="port">      Serial port name. </param>
    ''' <param name="baudRate">  Baud rate. </param>
    ''' <param name="dataBits">  Data bits. </param>
    ''' <param name="parity">    Parity. </param>
    ''' <param name="stopBits">  Stop bits. </param>
    ''' <param name="handshake"> Control flux. </param>
    Public Sub New(ByVal dataStore() As DataStore, ByVal type As ModbusSerialType, ByVal port As String,
                   ByVal baudRate As Integer, ByVal dataBits As Integer, ByVal parity As Parity, ByVal stopBits As StopBits, ByVal handshake As Handshake)
        MyBase.New(dataStore)
        ' Set modbus serial protocol type
        Select Case type
            Case ModbusSerialType.ASCII
                Me.ConnectionType = ConnectionType.SerialAscii

            Case ModbusSerialType.RTU
                Me.ConnectionType = ConnectionType.SerialRTU
        End Select

        ' Set serial port instance
        Me.InitPort(port, baudRate, dataBits, parity, stopBits)
        Me._SerialPort.Handshake = handshake

        ' Get inter frame delay
        Me.InterFrameDelay = ModbusServerSerial.InterFrameDelayTimespan(Me._SerialPort)

        ' Get inter char delay
        Me.InterCharacterDelay = ModbusServerSerial.InterCharacterDelayTimespan(Me._SerialPort)

    End Sub

    ''' <summary> Initializes the port. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="port">     Serial port name. </param>
    ''' <param name="baudRate"> Baud rate. </param>
    ''' <param name="dataBits"> Data bits. </param>
    ''' <param name="parity">   Parity. </param>
    ''' <param name="stopBits"> Stop bits. </param>
    Private Sub InitPort(port As String, baudRate As Integer, dataBits As Integer, parity As Parity, stopBits As StopBits)
        Me._SerialPort = New SerialPort(port, baudRate, parity, dataBits, stopBits)
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.Disposed Then
                If disposing Then
                    If Me._SerialPort IsNot Nothing Then
                        Me._SerialPort.Dispose()
                        Me._SerialPort = Nothing
                    End If
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


#End Region

#Region " HELPERS "

    ''' <summary> Get delay time between two modbus RTU frame in milliseconds. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> Serial Port. </param>
    ''' <returns> Calculated delay (milliseconds) </returns>
    Public Shared Function InterFrameDelayTimespan(ByVal value As SerialPort) As TimeSpan
        Return TimeSpan.FromTicks(CLng(3.5 * value.CharacterTimespan.Ticks))
    End Function

    ''' <summary> Inter character delay timespan. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> Serial Port. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function InterCharacterDelayTimespan(ByVal value As SerialPort) As TimeSpan
        Return TimeSpan.FromTicks(CLng(1.5 * value.CharacterTimespan.Ticks))
    End Function

    ''' <summary> Get delay time between two modbus RTU frame in milliseconds. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> Serial Port. </param>
    ''' <returns> Calculated delay (milliseconds) </returns>
    <Obsolete("Use InterFrameDelayTimespan()")>
    Public Shared Function InterFrameDelayMilliseconds(ByVal value As SerialPort) As Integer
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim result As Integer
        If value.BaudRate > 19200 Then
            result = 2 ' Fixed value = 1.75ms up rounded
        Else
            Dim nbits As Integer = 1 + value.DataBits
            nbits += If(value.Parity = Parity.None, 0, 1)
            Select Case value.StopBits
                Case StopBits.One
                    nbits += 1
                Case StopBits.OnePointFive, StopBits.Two ' Ceiling
                    nbits += 2
            End Select
            result = Convert.ToInt32(Math.Ceiling(1 / ((CDbl(value.BaudRate) / (CDbl(nbits) * 3.5R)) / 1000)))
        End If
        Return result
    End Function

    ''' <summary>
    ''' Get max delay time in milliseconds between received chars in modbus RTU transmission.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> Serial Port. </param>
    ''' <returns> Calculated delay (milliseconds) </returns>
    <Obsolete("Use InterCharacterDelayTimespan()")>
    Public Shared Function InterCharacterDelayMilliseconds(ByVal value As SerialPort) As Integer
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim result As Integer

        If value.BaudRate > 19200 Then
            result = 1 ' Fixed value = 0.75 ms up rounded
        Else
            Dim nbits As Integer = 1 + value.DataBits
            nbits += If(value.Parity = Parity.None, 0, 1)
            Select Case value.StopBits
                Case StopBits.One
                    nbits += 1

                Case StopBits.OnePointFive, StopBits.Two ' Ceiling
                    nbits += 2
            End Select
            result = Convert.ToInt32(Math.Ceiling(1 / ((CDbl(value.BaudRate) / (CDbl(nbits) * 1.5R)) / 1000)))
        End If

        Return result
    End Function


#End Region

#Region " INSTANCES "

    ''' <summary>
    ''' Serial Port instance
    ''' </summary>
    Private _SerialPort As SerialPort

#End Region

#Region " THREAD FOR GUEST INCOMING CALLS "

    ''' <summary> Thread for guest incoming calls. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub GuestRequests()
        Try
            Dim send_buffer As New List(Of Byte)()
            Dim receive_buffer As New List(Of Byte)()
            Do While Me.Running
                Me.IncomingMessagePolling(send_buffer, receive_buffer, Me._SerialPort)
                isr.Core.ApplianceBase.Delay(1)
            Loop
        Catch
        End Try
    End Sub

#End Region

#Region " START AND STOP FUNCTIONS "

    ''' <summary> Start listening function. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub StartListen()
        Me._SerialPort.Open()
        If Me._SerialPort.IsOpen Then
            Me._SerialPort.DiscardInBuffer()
            Me._SerialPort.DiscardOutBuffer()
            If Me.GuestRequestThread Is Nothing Then
                Me.Running = True
                Me.GuestRequestThread = New Thread(AddressOf Me.GuestRequests)
                Me.GuestRequestThread.Start()
            End If
        End If
    End Sub

    ''' <summary> Stop listening function. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub StopListen()
        If Me.GuestRequestThread IsNot Nothing Then
            Me.Running = False
            Me.GuestRequestThread.Join()
            Me.GuestRequestThread = Nothing
        End If
        If Me._SerialPort IsNot Nothing Then
            If Me._SerialPort.IsOpen Then
                Me._SerialPort.Close()
            End If
        End If
    End Sub

#End Region

End Class
