Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.IO.Ports
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Reflection
Imports System.Diagnostics

''' <summary> Modbus TCP Server class. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07. https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Public NotInheritable Class ModbusServerTCP
    Inherits ModbusServerBase

#Region "Constructor"

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="dataStore">    Modbus database array. </param>
    ''' <param name="localAddress"> Local listening IP addresses. </param>
    ''' <param name="port">         Listening TCP port. </param>
    Public Sub New(ByVal dataStore() As DataStore, ByVal localAddress As IPAddress, ByVal port As Integer)
        MyBase.New(dataStore)
        ' Set device states
        Me.ConnectionType = ConnectionType.TcpIP
        ' Crete TCP listener
        Me._TcpListener = New TcpListener(localAddress, port)
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.Disposed Then
                If disposing Then
                    If Me.TcpClientConnectedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.TcpClientConnectedEvent.GetInvocationList
                            RemoveHandler Me.TcpClientConnected, CType(d, Global.System.EventHandler(Of ClientConnectedEventArgs))
                        Next
                    End If
                    If Me.TcpClientDisconnectedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.TcpClientDisconnectedEvent.GetInvocationList
                            RemoveHandler Me.TcpClientDisconnected, CType(d, Global.System.EventHandler(Of ClientConnectedEventArgs))
                        Next
                    End If
                    If Me._ManualResetEvent IsNot Nothing Then
                        Me._ManualResetEvent.Dispose()
                        Me._ManualResetEvent = Nothing
                    End If
                    If Me._TcpListener IsNot Nothing Then
                        Me._TcpListener = Nothing
                    End If
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " INSTANCES "

    ''' <summary>
    ''' Listener TCP
    ''' </summary>
    Private _TcpListener As TcpListener

    ''' <summary>
    ''' Manual reset event
    ''' </summary>
    Private _ManualResetEvent As New ManualResetEvent(False)

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Client connected event
    ''' </summary>
    Public Event TcpClientConnected As EventHandler(Of ClientConnectedEventArgs)

    ''' <summary>
    ''' Client disconnected event
    ''' </summary>
    Public Event TcpClientDisconnected As EventHandler(Of ClientConnectedEventArgs)

#End Region

#Region " PARAMETERS "

    ''' <summary> The remote clients connected. </summary>
    Private ReadOnly _RemoteClientsConnected As New List(Of IPEndPoint)()

    ''' <summary> Connected clients. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> An IPEndPoint() </returns>
    Public Function RemoteClientsConnected() As IPEndPoint()
        Return Me._RemoteClientsConnected.ToArray()
    End Function

#End Region

#Region " MODULE FUNCTIONS "

    ''' <summary> Check if TcpClient is already connected. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="client"> TcpClient instance to check. </param>
    ''' <returns> Connection status. </returns>
    Private Shared Function IsClientConnected(ByVal client As TcpClient) As Boolean
        Dim ret As Boolean = True
        If client.Client.Poll(0, SelectMode.SelectRead) Then
            Dim check_connection(0) As Byte
            If client.Client.Receive(check_connection, SocketFlags.Peek) = 0 Then
                ret = False
            End If
        End If
        Return ret
    End Function

#End Region

#Region "Input connections callback"

    ''' <summary> Input connections callback. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="asyncResult"> The result. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DoAcceptTcpClientCallback(ByVal asyncResult As IAsyncResult)
        Dim listener As TcpListener = Nothing
        Dim client As TcpClient = Nothing
        Dim ns As NetworkStream = Nothing
        Dim ipe As IPEndPoint = Nothing

        Try
            Dim send_buffer As New List(Of Byte)()
            Dim receive_buffer As New List(Of Byte)()

            ' Copy listener instance
            listener = CType(asyncResult.AsyncState, TcpListener)
            ' Get client
            client = listener.EndAcceptTcpClient(asyncResult)
            ' Fire event
            ipe = CType(client.Client.RemoteEndPoint, IPEndPoint)
            Me._RemoteClientsConnected.Add(ipe)
            Dim con_handler As EventHandler(Of ClientConnectedEventArgs) = Me.TcpClientConnectedEvent
            If con_handler IsNot Nothing Then
                con_handler(Me, New ClientConnectedEventArgs(ipe))
            End If
            ' Set manual reset event
            Me._ManualResetEvent.Set()
            ' Get network stream
            ns = client.GetStream()
            ' Processing incoming connections
            Do While Me.Running
                If Not IsClientConnected(client) Then
                    Exit Do
                End If
                Me.IncomingMessagePolling(send_buffer, receive_buffer, ns)
                isr.Core.ApplianceBase.Delay(1)
            Loop
        Catch
        Finally
            ' Close IO stream
            If ns IsNot Nothing Then
                ns.Close()
            End If
            ' Close client connection                
            If client IsNot Nothing Then
                client.Close()
            End If
            ' Fire event
            If ipe IsNot Nothing Then
                Me._RemoteClientsConnected.Remove(ipe)
                Dim discon_handler As EventHandler(Of ClientConnectedEventArgs) = Me.TcpClientDisconnectedEvent
                If discon_handler IsNot Nothing Then
                    discon_handler(Me, New ClientConnectedEventArgs(ipe))
                End If
            End If
        End Try
    End Sub

#End Region

#Region " PROCESS THREAD FOR INCOMING CONNECTIONS "

    ''' <summary> The body of the incoming call manager. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Overrides Sub GuestRequests()
        Do While Me.Running
            ' Reset event
            Me._ManualResetEvent.Reset()
            ' Asynchronous call to incoming connections
            Me._TcpListener.BeginAcceptTcpClient(New AsyncCallback(AddressOf Me.DoAcceptTcpClientCallback), Me._TcpListener)
            ' Wait for event
            Me._ManualResetEvent.WaitOne()
        Loop
    End Sub

#End Region

#Region " START AND STOP FUNCTIONS "

    ''' <summary> Start listening. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub StartListen()
        Me._TcpListener.Start()
        If Me.GuestRequestThread Is Nothing Then
            Me.Running = True
            Me.GuestRequestThread = New Thread(AddressOf Me.GuestRequests)
            Me.GuestRequestThread.Start()
        End If
    End Sub

    ''' <summary> Stop listening. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub StopListen()
        If Me.GuestRequestThread IsNot Nothing Then
            Me.Running = False
            Me._ManualResetEvent.Set()
            Me.GuestRequestThread.Join()
            Me.GuestRequestThread = Nothing
        End If
        Me._TcpListener.Stop()
    End Sub

#End Region

End Class


