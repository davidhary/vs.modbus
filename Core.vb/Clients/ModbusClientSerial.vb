Imports System.IO.Ports

''' <summary> Modbus serial Client class. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Public NotInheritable Class ModbusClientSerial
    Inherits ModbusClientBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="type"> Type of serial protocol. </param>
    ''' <param name="port"> Serial port name. </param>
    Public Sub New(ByVal type As ModbusSerialType, ByVal port As SerialPort)

        MyBase.New()

        ' Set device states
        Select Case type
            Case ModbusSerialType.RTU
                Me.ConnectionType = ConnectionType.SerialRTU
            Case ModbusSerialType.ASCII
                Me.ConnectionType = ConnectionType.SerialAscii
        End Select

        Me._SerialPort = port

        ' Get inter frame delay
        Me.InterFrameDelay = ModbusServerSerial.InterFrameDelayTimespan(port)

        ' Get inter char delay
        Me.InterCharacterDelay = ModbusServerSerial.InterCharacterDelayTimespan(port)

    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.Disposed Then
                If disposing Then
                    If Me._SerialPort IsNot Nothing Then
                        Me._SerialPort.Dispose()
                        Me._SerialPort = Nothing
                    End If
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary> Construct port. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="port">      Serial port name. </param>
    ''' <param name="baudRate">  Baud rate. </param>
    ''' <param name="dataBits">  Data bits. </param>
    ''' <param name="parity">    Parity. </param>
    ''' <param name="stopBits">  Stop bits. </param>
    ''' <param name="handshake"> Handshake. </param>
    ''' <returns> A SerialPort. </returns>
    Public Shared Function ConstructPort(ByVal port As String, ByVal baudRate As Integer,
                                         ByVal dataBits As Integer, ByVal parity As Parity,
                                         ByVal stopBits As StopBits, ByVal handshake As Handshake) As SerialPort
        ' Set serial port instance
        Dim sp As New SerialPort(port, baudRate, parity, dataBits, stopBits) With {
            .Handshake = handshake
        }
        Return sp
    End Function

#End Region

#Region " CONNECT / DISCONNECT FUNCTIONS "

    ''' <summary>
    ''' Serial port instance
    ''' </summary>
    Private _SerialPort As SerialPort

    ''' <summary> Connect function. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub Connect()
        Me._SerialPort.Open()
        If Me._SerialPort.IsOpen Then
            Me.Connected = True
        End If
    End Sub

    ''' <summary> Disconnect function. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub Disconnect()
        Me._SerialPort.Close()
        Me.Connected = False
    End Sub

#End Region

#Region " PROTOCOL FUNCTIONS "

    ''' <summary> Send transmission buffer. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="buffer"> The buffer. </param>
    Protected Overrides Sub Send(ByVal buffer As IEnumerable(Of Byte))
        Me.TxBuffer.AddRange(buffer)
        Me._SerialPort.Write(Me.TxBuffer.ToArray(), 0, Me.TxBuffer.Count)
    End Sub

    ''' <summary> Gets the status of device data available. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if a new device data is available; otherwise <c>false</c> </returns>
    Public Overrides Function DeviceDataAvailable() As Boolean
        Return Me._SerialPort.BytesToRead > 0
    End Function

    ''' <summary> Reads a byte from the device. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> Byte read or nothing if no data are present. </returns>
    Protected Overrides Function ReadByte() As Byte?

        ' Check if there are available bytes
        Do While Me.DeviceDataAvailable
            Dim value As Integer = Me._SerialPort.ReadByte()
            If value >= 0 Then
                Me.RxDeviceBuffer.Add(CByte(value And &HFF))
            Else
                Exit Do
            End If
            isr.Core.ApplianceBase.DoEventsWait(Me.InterCharacterDelay)
        Loop

        If Me.RxDeviceBuffer.Any Then
            ' There are available bytes in temporary rx buffer, read the first and delete it
            Dim ret As Byte = Me.RxDeviceBuffer(0)
            Me.RxDeviceBuffer.RemoveAt(0)
            Return ret
        Else
            Return New Byte?
        End If

    End Function

    ''' <summary> Flushes device input and output buffers and cached input buffers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Overrides Sub FlushDeviceBuffers()
        MyBase.FlushDeviceBuffers()
        If Me._SerialPort?.IsOpen Then
            Me._SerialPort.DiscardInBuffer()
            Me._SerialPort.DiscardOutBuffer()
        End If
    End Sub

#End Region

End Class

