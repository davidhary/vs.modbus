Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.IO.Ports
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Reflection
Imports System.Diagnostics

''' <summary> Modbus Client UDP class. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Public Class ModbusClientUDP
    Inherits ModbusClientBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="remoteHost"> Remote hostname or IP address. </param>
    ''' <param name="port">       Remote Modbus TCP port. </param>
    Public Sub New(ByVal remoteHost As String, ByVal port As Integer)
        MyBase.New()

        ' Set device states
        Me.ConnectionType = ConnectionType.UdpIP

        ' Set socket client
        Me._RemoteHost = remoteHost
        Me._Port = port
        Me._UdpClient = New UdpClient()
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.Disposed Then
                If disposing Then
                    If Me._UdpClient IsNot Nothing Then
                        Me._UdpClient.Dispose()
                        Me._UdpClient = Nothing
                    End If
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub



#End Region

#Region " GLOBAL VARIABLES "

    ''' <summary>
    ''' Remote host name or IP address
    ''' </summary>
    Private ReadOnly _RemoteHost As String

    ''' <summary>
    ''' Remote Modbus TCP port
    ''' </summary>
    Private ReadOnly _Port As Integer

#End Region

#Region " CONNECT / DISCONNECT FUNCTIONS "

    ''' <summary>
    ''' UDP Client
    ''' </summary>
    Private _UdpClient As UdpClient

    ''' <summary> Connect function. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub Connect()
        Me._UdpClient.Connect(Me._RemoteHost, Me._Port)
        If Me._UdpClient.Client.Connected Then
            Me.Connected = True
        End If
    End Sub

    ''' <summary> Disconnect function. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub Disconnect()
        Me._UdpClient.Close()
        Me.Connected = False
    End Sub

#End Region

#Region " PROTOCOL FUNCTIONS "

    ''' <summary> Send transmission buffer. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="buffer"> The buffer. </param>
    Protected Overrides Sub Send(ByVal buffer As IEnumerable(Of Byte))
        Me.TxBuffer.AddRange(buffer)
        Me._UdpClient.Send(Me.TxBuffer.ToArray(), Me.TxBuffer.Count)
    End Sub

    ''' <summary> Gets the status of device data available. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if a new device data is available; otherwise <c>false</c> </returns>
    Public Overrides Function DeviceDataAvailable() As Boolean
        Return Me._UdpClient.Available > 0
    End Function

    ''' <summary> Reads a byte from the device. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> Byte read or nothing if no data are present. </returns>
    Protected Overrides Function ReadByte() As Byte?

        Dim ipe As New IPEndPoint(IPAddress.Any, 0)

        ' Check if there are available bytes
        If Me.DeviceDataAvailable Then
            ' Enqueue bytes to temporary rx buffer
            Me.RxDeviceBuffer.AddRange(Me._UdpClient.Receive(ipe))
        End If

        If Me.RxDeviceBuffer.Any Then
            ' There are available bytes in temporary rx buffer, read the first and delete it
            Dim ret As Byte = Me.RxDeviceBuffer(0)
            Me.RxDeviceBuffer.RemoveAt(0)
            Return ret
        Else
            Return New Byte?
        End If
    End Function

    ''' <summary> Flushes device input and output buffers and cached input buffers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Overrides Sub FlushDeviceBuffers()
        MyBase.FlushDeviceBuffers()
        Dim ipe As New IPEndPoint(IPAddress.Any, 0)
        Do While Me._UdpClient.Available > 0
            Me._UdpClient.Receive(ipe)
        Loop
    End Sub

#End Region

End Class

