Imports System.Text
Imports System.Threading
Imports System.Windows.Threading
Imports isr.Core.DispatcherExtensions
Imports isr.Core.BitConverterExtensions

''' <summary> Base abstract class for Modbus client instances. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Public MustInherit Class ModbusClientBase
    Inherits ModbusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Sub New()
        MyBase.New()
        Me._TxPayload = New List(Of Byte)()
        Me._TxBuffer = New List(Of Byte)()
        Me._RxPayload = New List(Of Byte)()
        Me._RxBuffer = New List(Of Byte)()
        Me._RxDeviceBuffer = New List(Of Byte)()
        Me.DeviceType = DeviceType.Client
        Me._RxTimeSeries = New TimeSeriesCollection
    End Sub

#Region " BUFFER "

    ''' <summary> The transmit payload. </summary>
    Private ReadOnly _TxPayload As List(Of Byte)

    ''' <summary> Transmission payload buffer. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> A List(Of Byte) </returns>
    Protected Function TxPayload() As List(Of Byte)
        Return Me._TxPayload
    End Function

    ''' <summary> Format transmit payload. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The formatted transmit payload. </returns>
    Public Function FormatTxPayload() As String
        Return ModbusClientBase.FormatBuffer(Me.TxPayload)
    End Function

    ''' <summary> Buffer for transmit data. </summary>
    Private ReadOnly _TxBuffer As List(Of Byte)

    ''' <summary> Transmission Buffer. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> A List(Of Byte) </returns>
    Protected Function TxBuffer() As List(Of Byte)
        Return Me._TxBuffer
    End Function

    ''' <summary> Format transmit Buffer. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The formatted transmit Buffer. </returns>
    Public Function FormatTxBuffer() As String
        Return ModbusClientBase.FormatBuffer(Me.TxBuffer)
    End Function

    ''' <summary> The receive payload. </summary>
    Private ReadOnly _RxPayload As List(Of Byte)

    ''' <summary> Received payload after trimming the transmission bits. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> A List(Of Byte) </returns>
    Protected Function RxPayload() As List(Of Byte)
        Return Me._RxPayload
    End Function

    ''' <summary> Returns the formatted received payload. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The formatted received payload. </returns>
    Public Function FormatRxPayload() As String
        Return ModbusClientBase.FormatBuffer(Me.RxPayload)
    End Function

    ''' <summary> Buffer for receive device data. </summary>
    Private ReadOnly _RxDeviceBuffer As List(Of Byte)

    ''' <summary> Received Buffer including the transmission bits. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> A List(Of Byte) </returns>
    Protected Function RxDeviceBuffer() As List(Of Byte)
        Return Me._RxDeviceBuffer
    End Function

    ''' <summary> Buffer for receive data. </summary>
    Private ReadOnly _RxBuffer As List(Of Byte)

    ''' <summary> Received Buffer including the transmission bits. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> A List(Of Byte) </returns>
    Protected Function RxBuffer() As List(Of Byte)
        Return Me._RxBuffer
    End Function

    ''' <summary> Returns the formatted received Buffer. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The formatted received Buffer. </returns>
    Public Function FormatRxBuffer() As String
        Return ModbusClientBase.FormatBuffer(Me.RxBuffer)
    End Function

    ''' <summary> Gets or sets the received time series. </summary>
    ''' <value> The received time series for use in debugging. </value>
    Public ReadOnly Property RxTimeSeries As TimeSeriesCollection

    ''' <summary> Modbus transaction ID (only Modbus TCP/UDP) </summary>
    ''' <value> The identifier of the transaction. </value>
    <CLSCompliant(False)>
    Protected Property TransactionId As UShort = 0

    ''' <summary> Format buffer. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="buffer"> The buffer. </param>
    ''' <returns> The formatted buffer. </returns>
    Public Shared Function FormatBuffer(ByVal buffer As IEnumerable(Of Byte)) As String
        Dim builder As New System.Text.StringBuilder()
        If buffer?.Any Then
            For Each b As Byte In buffer
                If builder.Length > 0 Then builder.Append(",")
                builder.AppendFormat("{0:X2}", b)
            Next
        End If
        Return builder.ToString()
    End Function

#End Region

#Region " PARAMETERS "

    ''' <summary>
    ''' Remote host connection status
    ''' </summary>
    Private _Connected As Boolean = False

    ''' <summary> Get remote host connection status. </summary>
    ''' <value> The connected. </value>
    Public Property Connected() As Boolean
        Get
            Return Me._Connected
        End Get
        Protected Set(value As Boolean)
            Me._Connected = value
        End Set
    End Property

#End Region

#End Region

#Region " CONNECT/DISCONNECT FUNCTIONS "

    ''' <summary> Open connection. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public MustOverride Sub Connect()

    ''' <summary> Close connection. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public MustOverride Sub Disconnect()

#End Region

#Region " SEND/RECEIVE FUNCTIONS "

    ''' <summary> Function To send transmission buffer. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="buffer"> The buffer. </param>
    Protected MustOverride Sub Send(ByVal buffer As IEnumerable(Of Byte))

    ''' <summary> Gets the status of device data available. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if a new device data is available; otherwise <c>false</c> </returns>
    Public MustOverride Function DeviceDataAvailable() As Boolean

    ''' <summary> Determines if we either device or buffered device data is available. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function RxDataAvailable() As Boolean
        Return Me.RxDeviceBuffer.Any OrElse Me.DeviceDataAvailable
    End Function

    ''' <summary> Waits for available device data. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="timeout">       The timeout. </param>
    ''' <param name="sleepInterval"> The sleep interval. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function AwaitDeviceDataAvailable(ByVal timeout As TimeSpan, ByVal sleepInterval As TimeSpan) As Boolean
        If timeout > TimeSpan.Zero Then
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do Until Me.DeviceDataAvailable OrElse sw.Elapsed > timeout
                isr.Core.ApplianceBase.DoEvents()
                If sleepInterval > TimeSpan.Zero Then isr.Core.ApplianceBase.Delay(sleepInterval)
            Loop
        End If
        Return Me.DeviceDataAvailable
    End Function

    ''' <summary> Waits for the next byte. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function AwaitNextByteAvailable() As Boolean
        Return Me.RxDataAvailable OrElse Me.AwaitDeviceDataAvailable(Me.InterFrameDelay, Me.InterCharacterDelay)
    End Function

    ''' <summary> Waits for the first byte. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function AwaitFirstByteAvailable() As Boolean
        Return Me.AwaitDeviceDataAvailable(Me.RxTimeout, Me.PollInterval)
    End Function

    ''' <summary> Reads a byte from the device. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> Byte read or nothing if no data are present. </returns>
    Protected MustOverride Function ReadByte() As Byte?

    ''' <summary> Reads a byte from a device after a waiting for the inter character delay. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> Byte read or nothing if no data are present. </returns>
    Protected Function WaitReadByte() As Byte?
        Return If(Me.AwaitNextByteAvailable, Me.ReadByte, New Byte?)
    End Function

    ''' <summary> Flushes device input and output buffers and cached input buffers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Overridable Sub FlushDeviceBuffers()
        Me.RxDeviceBuffer.Clear()
    End Sub

#End Region

#Region " TRANSMIT PROTOCOL FUNCTIONS "

    ''' <summary> Initialize a new Modbus TCP/UDP message. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Sub InitTcpUdpClientMessage()
        ' Increase transaction_id
        Me.TransactionId += CUShort(1)
        ' Tx buffer emptying
        Me.TxPayload.Clear()
        Me.TxBuffer.Clear()
    End Sub

    ''' <summary> Build Modbus Application Protocol Header (MBAP) header for Modbus TCP/UDP. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="buffer">             The buffer. </param>
    ''' <param name="destinationAddress"> Destination address. </param>
    ''' <param name="messageLength">      Message length. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build header in this collection.
    ''' </returns>
    Protected Function BuildHeader(ByVal buffer As IEnumerable(Of Byte), ByVal destinationAddress As Byte, ByVal messageLength As Integer) As IEnumerable(Of Byte)

        Dim result As New List(Of Byte)(buffer)

        ' Transaction ID (incremented by 1 on each transmission)
        result.InsertRange(0, Core.BitConverterExtensions.Methods.BigEndInt8(Me.TransactionId))

        ' Protocol ID (fixed value)
        result.InsertRange(2, Core.BitConverterExtensions.Methods.BigEndInt8(ModbusBase.ProtocolId))

        ' Message length
        result.InsertRange(4, Core.BitConverterExtensions.Methods.BigEndInt8(CType(messageLength, UShort)))

        ' Remote unit ID
        result.Insert(6, destinationAddress)

        Return result

    End Function

    ''' <summary> Build Modbus Application Protocol Header (MBAP) header for Modbus TCP/UDP. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="buffer">             The buffer. </param>
    ''' <param name="destinationAddress"> Destination address. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build header in this collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Protected Function BuildHeader(ByVal buffer As IEnumerable(Of Byte), ByVal destinationAddress As Byte) As IEnumerable(Of Byte)

        Dim result As New List(Of Byte)(buffer)

        ' Insert 'unit_id' in front of the message
        result.Insert(0, destinationAddress)

        ' Append CRC16
        result.AddRange(BitConverter.GetBytes(CRC16.Evaluate(result.ToArray(), 0, result.Count)))

        Return result

    End Function

    ''' <summary>
    ''' Build Modbus Application Protocol Header (MBAP) header for Modbus Serial Ascii.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="buffer">             The buffer. </param>
    ''' <param name="destinationAddress"> Destination address. </param>
    ''' <param name="termination">        The termination. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build header in this collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Protected Function BuildHeader(ByVal buffer As IEnumerable(Of Byte), ByVal destinationAddress As Byte, ByVal termination As Char()) As IEnumerable(Of Byte)

        Dim result As New List(Of Byte)(buffer)

        ' Add destination device address
        result.Insert(0, destinationAddress)

        ' Calculate message LCR
        Dim v As Byte = isr.Modbus.LRC.Evaluate(result.ToArray(), 0, result.Count)

        Dim lrc() As Byte = Core.BitConverterExtensions.Methods.ToAsciiBytes(New Byte() {v})

        ' Convert 'send_buffer' from binary to ASCII
        Dim sb As Byte() = Core.BitConverterExtensions.Methods.ToAsciiBytes(result.ToArray())
        result.Clear()
        result.AddRange(sb)

        ' Add LRC at the end of the message
        result.AddRange(lrc)

        ' Insert the start frame char
        result.Insert(0, Encoding.ASCII.GetBytes(New Char() {ModbusBase.AsciiStartFrameCharacter}).First())

        ' Insert stop frame chars
        result.AddRange(Encoding.ASCII.GetBytes(termination))

        Return result

    End Function

    ''' <summary> Executes a write to a destination Client. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="payload">       The payload. </param>
    ''' <param name="unitId">        Server device address. </param>
    ''' <param name="messageLength"> Message length. </param>
    Private Sub WriteThis(ByVal payload As IEnumerable(Of Byte), ByVal unitId As Byte, ByVal messageLength As Integer)

        ' Set errors to null
        Me.LastError = InterfaceError.NoError

        ' Start to build message
        Select Case Me.ConnectionType

            Case ConnectionType.SerialAscii

                Dim end_frame() As Char = {ModbusBase.AsciiEndFrameFirstCharacter, ModbusBase.AsciiEndFrameSecondCharacter}

                Me.Send(Me.BuildHeader(payload, unitId, end_frame))

            Case ConnectionType.SerialRTU

                ' Wait for inter-frame delay
                isr.Core.ApplianceBase.Delay(Me.InterFrameDelay)

                Me.Send(Me.BuildHeader(payload, unitId))

            Case ConnectionType.UdpIP, ConnectionType.TcpIP

                Me.Send(Me.BuildHeader(payload, unitId, messageLength))

        End Select

    End Sub

#End Region

#Region " RECEIVE PROTOCOL FUNCTIONS "

    ''' <summary> Gets the length of the minimum frame. </summary>
    ''' <value> The length of the minimum frame. </value>
    Public ReadOnly Property MinimumFrameLength As Integer
        Get
            Dim value As Integer
            Select Case Me.ConnectionType
                Case ConnectionType.SerialAscii
                    value = 11
                Case ConnectionType.UdpIP, ConnectionType.TcpIP
                    value = 9
                Case Else
                    value = 5
            End Select
            Return value
        End Get
    End Property

    ''' <summary> Parse the received data. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="expectedFrameId"> Identifier for the expected frame. </param>
    ''' <param name="buffer">          The buffer. </param>
    ''' <returns> <c>true</c> if receive frame complete; otherwise <c>false</c> </returns>
    Private Function IsReceiveFrameComplete(ByVal expectedFrameId As Byte, ByVal buffer As IEnumerable(Of Byte)) As Boolean

        Dim result As Boolean = False
        Dim rx As New List(Of Byte)(buffer)

        Select Case Me.ConnectionType

            Case ConnectionType.SerialAscii

                ' Check message consistency
                If rx(0) <> expectedFrameId Then
                    ' parsing is expected to detect an error
                    result = True
                    Exit Select
                End If

                If rx.Count < Me.MinimumFrameLength Then
                    result = False
                    Exit Select
                End If

                ' remove start char
                rx.RemoveRange(0, 1)
                ' Check and remove stop chars
                Dim orig_end_frame() As Char = {ModbusBase.AsciiEndFrameFirstCharacter, ModbusBase.AsciiEndFrameSecondCharacter}
                Dim rec_end_frame() As Char = Encoding.ASCII.GetChars(rx.GetRange(rx.Count - 2, 2).ToArray())
                If Not orig_end_frame.SequenceEqual(rec_end_frame) Then
                    ' not completed yet
                    result = False
                    Exit Select
                End If

                ' remove termination characters.
                rx.RemoveRange(rx.Count - 2, 2)

                ' Convert receive buffer from ASCII to binary
                Dim rb As Byte() = Core.BitConverterExtensions.Methods.FromAsciiBytes(rx.ToArray())
                rx.Clear()
                rx.AddRange(rb)

                ' Check and remove message LRC
                Dim lrc_calculated As Byte = LRC.Evaluate(rx.ToArray(), 0, rx.Count - 1)
                Dim lrc_received As Byte = rx(rx.Count - 1)
                If lrc_calculated = lrc_received Then
                    ' completed
                    result = True
                    Exit Select
                End If

            Case ConnectionType.SerialRTU

                ' Check message consistency
                If rx(0) <> expectedFrameId Then
                    ' parsing is expected to detect an error
                    result = True
                    Exit Select
                End If

                If rx.Count < Me.MinimumFrameLength Then
                    result = False
                    Exit Select
                End If

                ' Check message 16-bit CRC
                Dim calc_crc As UShort = CRC16.Evaluate(rx.ToArray(), 0, rx.Count - 2)
                Dim rec_crc As UShort = BitConverter.ToUInt16(rx.ToArray(), rx.Count - 2)
                If rec_crc = calc_crc Then
                    ' completed
                    result = True
                    Exit Select
                End If

            Case ConnectionType.UdpIP, ConnectionType.TcpIP

                If rx.Count < Me.MinimumFrameLength Then
                    result = False
                    Exit Select
                End If

                ' Check MBAP header
                Dim tid As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(rx.ToArray(), 0)
                If tid <> Me.TransactionId Then
                    ' parsing is expected to detect an error
                    result = True
                    Exit Select
                End If
                Dim pid As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(rx.ToArray(), 2)
                If pid <> ProtocolId Then
                    ' parsing is expected to detect an error
                    result = True
                    Exit Select
                End If
                Dim uid As Byte = rx(6)
                If uid <> expectedFrameId Then
                    ' parsing is expected to detect an error
                    result = True
                    Exit Select
                End If
                Dim len As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(rx.ToArray(), 4)
                If (rx.Count - ModbusBase.MbapHeaderLength + 1) >= len Then
                    result = True
                    Exit Select
                End If
        End Select

        ' Control eventual error message 
        If rx.Count > 1 AndAlso rx(0) > &H80 AndAlso rx(1) >= 1 AndAlso rx(1) <= 4 Then
            ' parsing is expected to detect an error
            result = True
        End If
        Return result

    End Function

    ''' <summary>
    ''' Fills the receive buffer checking completion after receiving the minimum frame length.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="timeoutStopwatch"> The timeout stopwatch. </param>
    Private Sub FillReceiveBuffer(ByVal timeoutStopwatch As isr.Core.Constructs.TimeoutStopwatch)

        Dim frameId As Byte = 0
        Select Case Me.ConnectionType
            Case ConnectionType.SerialAscii, ConnectionType.SerialRTU
                frameId = Me.TxBuffer(0)
            Case ConnectionType.TcpIP, ConnectionType.UdpIP
                frameId = Me.TxBuffer(6)
        End Select


        Do
            Dim rcv As Byte? = Me.WaitReadByte()
            Me.RxTimeSeries.Add(timeoutStopwatch.Elapsed, rcv)
            If rcv.HasValue Then
                If Me.ConnectionType = ConnectionType.SerialAscii Then
                    If CByte(rcv) = Encoding.ASCII.GetBytes(New Char() {AsciiStartFrameCharacter}).First() Then
                        ' this is the first character, reset the reading.
                        Me.RxBuffer.Clear()
                    End If
                End If
                Me.RxBuffer.Add(CByte(rcv))
            End If
            ' loop while more data is available or not (completed or timeout)
        Loop While Me.AwaitNextByteAvailable OrElse Not (Me.IsReceiveFrameComplete(frameId, Me.RxBuffer) OrElse timeoutStopwatch.IsTimeout)

        If Me.LastError = InterfaceError.NoError AndAlso Me.RxBuffer.Count < Me.MinimumFrameLength Then
            Me.LastError = InterfaceError.MessageTooShort
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Rx {Me.FormatRxBuffer} has fewer than {Me.MinimumFrameLength} values; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
        End If

    End Sub

    ''' <summary> Reads this. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Private Sub ReadThis()

        ' Set errors to null
        Me.LastError = InterfaceError.NoError

        ' Start receiving...
        Me.RxPayload.Clear()
        Me.RxBuffer.Clear()
        Me.RxDeviceBuffer.Clear()

        Me.RxTimeSeries.Clear()

        ' start timing before waiting to get a clue on how long it take to get a response from the device.
        Dim stopwatch As isr.Core.Constructs.TimeoutStopwatch = isr.Core.Constructs.TimeoutStopwatch.StartNew(Me.RxTimeout)

        ' wait for first byte.
        If Me.AwaitFirstByteAvailable Then
            ' fill the receive buffer with data
            Me.FillReceiveBuffer(stopwatch)
            ' Me._FillReceiveBuffer(stopwatch)
        Else
            Me.LastError = InterfaceError.ReceiveTimeout
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Timeout waiting for the first character"
        End If

    End Sub

    ''' <summary> Parse the received data. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="expectedFrameId"> Identifier for the expected frame. </param>
    ''' <param name="buffer">          The buffer. </param>
    Private Sub ParseThis(ByVal expectedFrameId As Byte, ByVal buffer As IEnumerable(Of Byte))

        ' Set errors to null
        Me.LastError = InterfaceError.NoError

        ' build the payload.
        Me.RxPayload.AddRange(buffer)

        Select Case Me.ConnectionType
            Case ConnectionType.SerialAscii

                ' Check message consistency
                If Me.RxPayload(0) <> expectedFrameId Then
                    Me.LastError = InterfaceError.StartCharNotFound
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Invalid start. Rx: '{ModbusClientBase.FormatBuffer(buffer)}(0)<>{expectedFrameId}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Exit Select
                End If

                ' remove start char
                Me.RxPayload.RemoveRange(0, 1)

                ' Check and remove stop chars
                Dim orig_end_frame() As Char = {ModbusBase.AsciiEndFrameFirstCharacter, ModbusBase.AsciiEndFrameSecondCharacter}
                Dim rec_end_frame() As Char = Encoding.ASCII.GetChars(Me.RxPayload.GetRange(Me.RxPayload.Count - 2, 2).ToArray())
                If Not orig_end_frame.SequenceEqual(rec_end_frame) Then
                    Me.LastError = InterfaceError.EndCharsNotFound
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Invalid termination. Rx: '{ModbusClientBase.FormatBuffer(buffer)}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Exit Select
                End If
                Me.RxPayload.RemoveRange(Me.RxPayload.Count - 2, 2)
                ' Convert receive buffer from ASCII to binary
                Dim rb As Byte() = isr.Core.BitConverterExtensions.Methods.FromAsciiBytes(Me.RxPayload.ToArray())
                Me.RxPayload.Clear()
                Me.RxPayload.AddRange(rb)

                ' Check and remove message LRC
                Dim lrc_calculated As Byte = LRC.Evaluate(Me.RxPayload.ToArray(), 0, Me.RxPayload.Count - 1)
                Dim lrc_received As Byte = Me.RxPayload(Me.RxPayload.Count - 1)
                If lrc_calculated <> lrc_received Then
                    Me.LastError = InterfaceError.WrongLrc
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong LRC {lrc_received}<>{lrc_calculated}. Rx: '{ModbusClientBase.FormatBuffer(buffer)}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Exit Select
                End If
                Me.RxPayload.RemoveRange(Me.RxPayload.Count - 1, 1)
                ' Remove address byte
                Me.RxPayload.RemoveRange(0, 1)

            Case ConnectionType.SerialRTU

                ' Check message consistency
                If Me.RxPayload(0) <> expectedFrameId Then
                    Me.LastError = InterfaceError.WrongResponseAddress
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Invalid start. Rx: '{ModbusClientBase.FormatBuffer(buffer)}(0)<>{expectedFrameId}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Exit Select
                End If

                ' Check message 16-bit CRC
                Dim calc_crc As UShort = CRC16.Evaluate(Me.RxPayload.ToArray(), 0, Me.RxPayload.Count - 2)
                Dim rec_crc As UShort = BitConverter.ToUInt16(Me.RxPayload.ToArray(), Me.RxPayload.Count - 2)
                If rec_crc <> calc_crc Then
                    Me.LastError = InterfaceError.WrongCrc
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong CRC {rec_crc}<>{calc_crc}. Rx: '{ModbusClientBase.FormatBuffer(buffer)}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Exit Select
                End If

                ' Remove address
                Me.RxPayload.RemoveRange(0, 1)

                ' Remove CRC
                Me.RxPayload.RemoveRange(Me.RxPayload.Count - 2, 2)

            Case ConnectionType.UdpIP, ConnectionType.TcpIP
                ' Check MBAP header
                Dim tid As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), 0)
                If tid <> Me.TransactionId Then
                    Me.LastError = InterfaceError.WrongTransactionId
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong transaction id {tid}<>{Me.TransactionId}. Rx: '{ModbusClientBase.FormatBuffer(buffer)}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Exit Select
                End If
                Dim pid As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), 2)
                If pid <> ModbusBase.ProtocolId Then
                    Me.LastError = InterfaceError.WrongProtocolId
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong protocol id {pid}<>{ModbusBase.ProtocolId}. Rx: '{ModbusClientBase.FormatBuffer(buffer)}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Exit Select
                End If
                Dim len As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), 4)
                Dim receivedLength As Integer = Me.RxPayload.Count - ModbusBase.MbapHeaderLength + 1
                If receivedLength < len Then
                    Me.LastError = InterfaceError.MessageTooShort
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Message too short {receivedLength}<{len}. Rx: '{ModbusClientBase.FormatBuffer(buffer)}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Exit Select
                End If
                Dim uid As Byte = Me.RxPayload(6)
                If uid <> expectedFrameId Then
                    Me.LastError = InterfaceError.WrongResponseUnitId
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Invalid response id. Rx: '{ModbusClientBase.FormatBuffer(buffer)}(6)<>{expectedFrameId}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Exit Select
                End If
                ' Let only useful bytes in receive buffer                       
                Me.RxPayload.RemoveRange(0, MbapHeaderLength)
        End Select

        ' Control eventual error message 
        If Me.LastError = InterfaceError.NoError AndAlso Me.RxPayload(0) > &H80 Then
            ' parse error codes
            Select Case Me.RxPayload(1)
                Case 1
                    Me.LastError = InterfaceError.ExceptionIllegalFunction
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Illegal function. Rx: '{ModbusClientBase.FormatBuffer(buffer)}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                Case 2
                    Me.LastError = InterfaceError.ExceptionIllegalDataAddress
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Illegal data address. Rx: '{ModbusClientBase.FormatBuffer(buffer)}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                Case 3
                    Me.LastError = InterfaceError.ExceptionIllegalDataValue
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Illegal data value. Rx: '{ModbusClientBase.FormatBuffer(buffer)}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                Case 4
                    Me.LastError = InterfaceError.ExceptionServerDeviceFailure
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Server failure. Rx: '{ModbusClientBase.FormatBuffer(buffer)}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
            End Select
        End If
    End Sub

    ''' <summary> Executes a query to a destination Client. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">        Salve device address. </param>
    ''' <param name="messageLength"> Message length. </param>
    <CLSCompliant(False)>
    Protected Sub Query(ByVal unitId As Byte, ByVal messageLength As Integer)

        Me.OnQueryStarted()

        ' flush device buffers before starting a new query
        Me.FlushDeviceBuffers()

        Me.WriteThis(Me.TxPayload, unitId, messageLength)

        If Me.LastError = InterfaceError.NoError Then Me.ReadThis()

        If Me.LastError = InterfaceError.NoError Then
            Dim frameId As Byte = 0
            Select Case Me.ConnectionType
                Case ConnectionType.SerialAscii, ConnectionType.SerialRTU
                    frameId = Me.TxBuffer(0)
                Case ConnectionType.TcpIP, ConnectionType.UdpIP
                    frameId = Me.TxBuffer(6)
            End Select
            Me.ParseThis(frameId, Me.RxBuffer)
        End If

        Me.OnQueryFinished()

    End Sub

    ''' <summary> Read coil registers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">       Server device address. </param>
    ''' <param name="startAddress"> Address of first register to be read. </param>
    ''' <param name="len">          Number of registers to be read. </param>
    ''' <returns> Array of read registers. </returns>
    <CLSCompliant(False)>
    Public Function ReadCoils(ByVal unitId As Byte, ByVal startAddress As UShort, ByVal len As UShort) As Boolean()
        If len < 1 Then
            Me.LastError = InterfaceError.ZeroRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Incorrect Read Coils argument; {len}<1"
            Return Nothing
        End If
        If len > ModbusBase.MaxCoilsInReadMessage Then
            Me.LastError = InterfaceError.TooManyRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Incorrect Read Coils argument; {len}>{ModbusBase.MaxCoilsInReadMessage}"
            Return Nothing
        End If
        Dim msg_len As UShort = 6
        Me.InitTcpUdpClientMessage()
        Me.TxPayload.Add(CByte(ModbusCodes.ReadCoils))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(startAddress))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(len))
        Me.Query(unitId, msg_len)
        If Me.LastError = InterfaceError.NoError Then
            Dim ba As New BitArray(Me.RxPayload.GetRange(2, Me.RxPayload.Count - 2).ToArray())
            Dim ret(ba.Count - 1) As Boolean
            ba.CopyTo(ret, 0)
            Return ret
        Else
            Return Nothing
        End If
    End Function

    ''' <summary> Read a set of discrete inputs. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">       Server device address. </param>
    ''' <param name="startAddress"> Address of first register to be read. </param>
    ''' <param name="len">          Number of registers to be read. </param>
    ''' <returns> Array of read registers. </returns>
    <CLSCompliant(False)>
    Public Function ReadDiscreteInputs(ByVal unitId As Byte, ByVal startAddress As UShort, ByVal len As UShort) As Boolean()
        If len < 1 Then
            Me.LastError = InterfaceError.ZeroRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Incorrect Read Discrete Inputs argument; {len}<1"
            Return Nothing
        End If
        If len > ModbusBase.MaxDiscreteInputsInReadMessage Then
            Me.LastError = InterfaceError.TooManyRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Incorrect Read Discrete Inputs argument; {len}>{ModbusBase.MaxDiscreteInputsInReadMessage}"
            Return Nothing
        End If
        Dim msg_len As UShort = 6
        Me.InitTcpUdpClientMessage()
        Me.TxPayload.Add(CByte(ModbusCodes.ReadDiscreteInputs))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(startAddress))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(len))
        Me.Query(unitId, msg_len)
        If Me.LastError = InterfaceError.NoError Then
            Dim ba As New BitArray(Me.RxPayload.GetRange(2, Me.RxPayload.Count - 2).ToArray())
            Dim ret(ba.Count - 1) As Boolean
            ba.CopyTo(ret, 0)
            Return ret
        Else
            Return Nothing
        End If
    End Function

    ''' <summary> Tries to read a holding registers value. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">       Server device address. </param>
    ''' <param name="startAddress"> Address of first register to be read. </param>
    ''' <param name="value">        [in,out] Value to write. </param>
    ''' <returns> <c>true</c> if the test passes, <c>false</c> if the test fails. </returns>
    <CLSCompliant(False)>
    Public Function TryReadHoldingRegistersValue(ByVal unitId As Byte, ByVal startAddress As UShort, ByRef value As UShort) As Boolean
        Dim values As UShort() = Me.ReadHoldingRegisters(unitId, startAddress, 1)
        If Me.LastError = InterfaceError.NoError Then
            If values?.Any Then
                value = values.First
            Else
                Me.LastError = InterfaceError.WrongResponseValue
                Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Unable to get first element; Rx: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
            End If
        End If
        Return Me.LastError = InterfaceError.NoError
    End Function

    ''' <summary> Tries to read a holding registers value. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">       Server device address. </param>
    ''' <param name="startAddress"> Address of first register to be read. </param>
    ''' <param name="value">        [in,out] Value to write. </param>
    ''' <returns> <c>true</c> if the test passes, <c>false</c> if the test fails. </returns>
    <CLSCompliant(False)>
    Public Function TryReadHoldingRegistersValue(ByVal unitId As Byte, ByVal startAddress As UShort, ByRef value As Single) As Boolean
        Dim values As UShort() = Me.ReadHoldingRegisters(unitId, startAddress, 2)
        If Me.LastError = InterfaceError.NoError Then
            If values?.Any Then
                If values.Count >= 2 Then
                    value = Core.BitConverterExtensions.Methods.BigEndSingle(values, 0)
                Else
                    Me.LastError = InterfaceError.WrongResponseValue
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Insufficient data for parsing a floating point value; Rx: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                End If
            Else
                Me.LastError = InterfaceError.WrongResponseValue
                Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Receive value is empty; Rx: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
            End If
        End If
        Return Me.LastError = InterfaceError.NoError
    End Function

    ''' <summary> Read a set of holding registers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">       Server device address. </param>
    ''' <param name="startAddress"> Address of first register to be read. </param>
    ''' <param name="len">          Number of registers to be read. </param>
    ''' <returns> Array of read registers. </returns>
    <CLSCompliant(False)>
    Public Function ReadHoldingRegisters(ByVal unitId As Byte, ByVal startAddress As UShort, ByVal len As UShort) As UShort()
        If len < 1 Then
            Me.LastError = InterfaceError.ZeroRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Incorrect Read Holding Registers argument:; {len}<1"
            Return Nothing
        End If
        If len > MaxHoldingRegistersInReadMessage Then
            Me.LastError = InterfaceError.TooManyRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Incorrect Read Holding Registers argument:; {len}>{MaxHoldingRegistersInReadMessage}"
            Return Nothing
        End If
        Dim msg_len As UShort = 6
        Me.InitTcpUdpClientMessage()
        Me.TxPayload.Add(CByte(ModbusCodes.ReadHoldingRegisters))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(startAddress))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(len))
        Me.Query(unitId, msg_len)
        If Me.LastError = InterfaceError.NoError Then
            Dim ret As New List(Of UShort)(Me.ExtractValues())
            Return If(Me.LastError = InterfaceError.NoError, ret.ToArray(), Nothing)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary> Extracts the values. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The extracted values. </returns>
    Private Function ExtractValues() As UShort()
        Dim ret As New List(Of UShort)()
        Dim offset As Integer = 2
        If Me.RxPayload.Any Then
            If Me.RxPayload.Count < offset Then
                Me.LastError = InterfaceError.WrongResponseValue
                Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Rx too short: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
            Else
                If Me.RxPayload.Count < Me.RxPayload(1) + offset Then
                    Me.LastError = InterfaceError.WrongResponseValue
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Rx shorter than specified: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                Else
                    ret = New List(Of UShort)(ModbusClientBase.ExtractValues(Me.RxPayload(1) \ 2, offset, Me.RxPayload.ToArray))
                End If
            End If
        Else
            Me.LastError = InterfaceError.WrongResponseValue
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Empty Rx: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
        End If
        Return ret.ToArray()
    End Function

    ''' <summary> Extracts the values. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="count">  Number of. </param>
    ''' <param name="offset"> The offset. </param>
    ''' <param name="values"> Array of values to be write. </param>
    ''' <returns> The extracted values. </returns>
    Private Shared Function ExtractValues(ByVal count As Integer, ByVal offset As Integer, ByVal values As Byte()) As UShort()
        Dim ret As New List(Of UShort)()
        Dim stepSize As Integer = 2
        Do While ret.Count < count
            ret.Add(Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(values, offset))
            offset += stepSize
        Loop
        Return ret.ToArray()
    End Function

    ''' <summary> Read a set of input registers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">       Server device address. </param>
    ''' <param name="startAddress"> Address of first register to be read. </param>
    ''' <param name="len">          Number of registers to be read. </param>
    ''' <returns> Array of read registers. </returns>
    <CLSCompliant(False)>
    Public Function ReadInputRegisters(ByVal unitId As Byte, ByVal startAddress As UShort, ByVal len As UShort) As UShort()
        If len < 1 Then
            Me.LastError = InterfaceError.ZeroRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Incorrect Read Input Registers argument:; {len}<0"
            Return Nothing
        End If
        If len > ModbusBase.MaxInputRegistersInReadMessage Then
            Me.LastError = InterfaceError.TooManyRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Incorrect Read Input Registers argument:; {len}>{ModbusBase.MaxInputRegistersInReadMessage}"
            Return Nothing
        End If
        Dim msg_len As UShort = 6
        Me.InitTcpUdpClientMessage()
        Me.TxPayload.Add(CByte(ModbusCodes.ReadInputRegisters))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(startAddress))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(len))
        Me.Query(unitId, msg_len)
        If Me.LastError = InterfaceError.NoError Then
            Dim ret As New List(Of UShort)(Me.ExtractValues())
            Return If(Me.LastError = InterfaceError.NoError, ret.ToArray(), Nothing)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary> Write a coil register. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">  Server device address. </param>
    ''' <param name="address"> Register address. </param>
    ''' <param name="value">   Value to write. </param>
    <CLSCompliant(False)>
    Public Sub WriteSingleCoil(ByVal unitId As Byte, ByVal address As UShort, ByVal value As Boolean)
        Dim msg_len As UShort = 6
        Me.InitTcpUdpClientMessage()
        Me.TxPayload.Add(CByte(ModbusCodes.WriteSingleCoil))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(address))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(CUShort(If(value = True, &HFF00, &H0))))
        Me.Query(unitId, msg_len)
        If Me.LastError = InterfaceError.NoError Then
            If Me.RxPayload.Any Then
                Dim offset As Integer = 1
                If Me.RxPayload.Count < offset Then
                    Me.LastError = InterfaceError.MessageTooShort
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Insufficient elements ({Me.RxPayload.Count}<{offset}) to validate address: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If
                Dim addr As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), offset)
                If addr <> address Then
                    Me.LastError = InterfaceError.WrongResponseAddress
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong response address ({addr}<{address}): '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If

                offset = 3
                If Me.RxPayload.Count < offset Then
                    Me.LastError = InterfaceError.MessageTooShort
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Insufficient elements ({Me.RxPayload.Count}<{offset}) to validate value: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If

                Dim regval As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), offset)
                If (regval = &HFF00) AndAlso (Not value) Then
                    Me.LastError = InterfaceError.WrongResponseValue
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong response value ({regval}=&HFF00 AND NOT {value}): '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If

            Else
                Me.LastError = InterfaceError.MessageTooShort
                Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Empty Rx: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
            End If
        End If
    End Sub

    ''' <summary> Write an holding register. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">  Server device address. </param>
    ''' <param name="address"> Register address. </param>
    ''' <param name="value">   Value to write. </param>
    <CLSCompliant(False)>
    Public Sub WriteSingleRegister(ByVal unitId As Byte, ByVal address As UShort, ByVal value As UShort)
        Dim msg_len As UShort = 6
        Me.InitTcpUdpClientMessage()
        Me.TxPayload.Add(CByte(ModbusCodes.WriteSingleRegister))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(address))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(value))
        Me.Query(unitId, msg_len)
        If Me.LastError = InterfaceError.NoError Then
            If Me.RxPayload.Any Then
                Dim offset As Integer = 1
                If Me.RxPayload.Count < offset Then
                    Me.LastError = InterfaceError.MessageTooShort
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Insufficient elements ({Me.RxPayload.Count}<{offset}) to validate address: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If
                Dim addr As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), offset)
                If addr <> address Then
                    Me.LastError = InterfaceError.WrongResponseAddress
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong response address ({addr}<{address}): '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If
            Else
                Me.LastError = InterfaceError.MessageTooShort
                Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Empty Rx: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
            End If
        End If
    End Sub

    ''' <summary> Write a set of coil registers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">       Server device address. </param>
    ''' <param name="startAddress"> Address of first register to be write. </param>
    ''' <param name="values">       Array of values to write. </param>
    <CLSCompliant(False)>
    Public Sub WriteMultipleCoils(ByVal unitId As Byte, ByVal startAddress As UShort, ByVal values() As Boolean)
        If values Is Nothing Then
            Me.LastError = InterfaceError.ZeroRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Empty argument '{NameOf(values)}' in Write Multiple Coils"
            Return
        End If
        If values.Length < 1 Then
            Me.LastError = InterfaceError.ZeroRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Zero registers requested in Write Multiple Coils argument:; {values.Length}<1"
            Return
        End If
        If values.Length > ModbusBase.MaxCoilsInWriteMessage Then
            Me.LastError = InterfaceError.TooManyRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Too many registers requested in Write Multiple Coils argument:; {values.Length}>{ModbusBase.MaxCoilsInWriteMessage}"
            Return
        End If
        Dim byte_cnt As Byte = CByte((values.Length \ 8) + (If((values.Length Mod 8) = 0, 0, 1)))
        Dim msg_len As UShort = CUShort(1 + 6 + byte_cnt)
        Dim data(byte_cnt - 1) As Byte
        Dim ba As New BitArray(values)
        ba.CopyTo(data, 0)
        Me.InitTcpUdpClientMessage()
        Me.TxPayload.Add(CByte(ModbusCodes.WriteMultipleCoils))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(startAddress))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(CUShort(values.Length)))
        Me.TxPayload.Add(byte_cnt)
        Me.TxPayload.AddRange(data)
        Me.Query(unitId, msg_len)
        If Me.LastError = InterfaceError.NoError Then
            If Me.RxPayload.Any Then
                Dim offset As Integer = 1
                If Me.RxPayload.Count < offset Then
                    Me.LastError = InterfaceError.MessageTooShort
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Insufficient elements ({Me.RxPayload.Count}<{offset}) to validate coil address: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If
                Dim sa As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), offset)
                If sa <> startAddress Then
                    Me.LastError = InterfaceError.WrongResponseAddress
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong response address ({sa}<{startAddress}): '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If

                offset = 3
                If Me.RxPayload.Count < offset Then
                    Me.LastError = InterfaceError.MessageTooShort
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Insufficient elements ({Me.RxPayload.Count}<{offset}) to validate coil: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If

                Dim nr As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), offset)
                If nr <> values.Length Then
                    Me.LastError = InterfaceError.WrongResponseRegisters
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong response register ({nr}<{values.Length}): '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If
            Else
                Me.LastError = InterfaceError.MessageTooShort
                Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Empty Rx: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
            End If
        End If
    End Sub

    ''' <summary> Writes the registers value. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">       Server device address. </param>
    ''' <param name="startAddress"> Address of first register to be write. </param>
    ''' <param name="value">        Value to write. </param>
    <CLSCompliant(False)>
    Public Sub WriteRegistersValue(ByVal unitId As Byte, ByVal startAddress As UShort, ByVal value As Single)
        Dim bytes As Byte() = value.BigEndBytes
        Dim values As UShort() = New UShort() {bytes.BigEndUnsignedShort(2), bytes.BigEndUnsignedShort(0)}
        Me.WriteMultipleRegisters(unitId, startAddress, values)
    End Sub

    ''' <summary> Writes the registers value. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">       Server device address. </param>
    ''' <param name="startAddress"> Address of first register to be write. </param>
    ''' <param name="value">        Value to write. </param>
    <CLSCompliant(False)>
    Public Sub WriteRegistersValue(ByVal unitId As Byte, ByVal startAddress As UShort, ByVal value As UShort)
        Me.WriteMultipleRegisters(unitId, startAddress, New UShort() {value})
    End Sub

    ''' <summary> Write a set of holding registers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">       Server device address. </param>
    ''' <param name="startAddress"> Address of first register to be write. </param>
    ''' <param name="values">       Array of values to write. </param>
    <CLSCompliant(False)>
    Public Sub WriteMultipleRegisters(ByVal unitId As Byte, ByVal startAddress As UShort, ByVal values() As UShort)
        If values Is Nothing Then
            Me.LastError = InterfaceError.ZeroRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Empty argument '{NameOf(values)}' in Write Multiple Registers"
            Return
        End If
        If values.Length < 1 Then
            Me.LastError = InterfaceError.ZeroRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Zero registers requested in Write Multiple Registers argument:; {values.Length}<1"
            Return
        End If
        If values.Length > ModbusBase.MaxHoldingRegistersInWriteMessage Then
            Me.LastError = InterfaceError.TooManyRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Too many registers requested in Write Multiple Registers argument:; {values.Length}>{ModbusBase.MaxHoldingRegistersInWriteMessage}"
            Return
        End If

        Dim msg_len As UShort = CUShort(7 + (values.Length * 2))
        Me.InitTcpUdpClientMessage()
        Me.TxPayload.Add(CByte(ModbusCodes.WriteMultipleRegisters))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(startAddress))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(CUShort(values.Length)))
        Me.TxPayload.Add(CByte(values.Length * 2))
        For ii As Integer = 0 To values.Length - 1
            Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(values(ii)))
        Next ii
        Me.Query(unitId, msg_len)
        If Me.LastError = InterfaceError.NoError Then
            If Me.RxPayload.Any Then
                Dim offset As Integer = 1
                If Me.RxPayload.Count < offset Then
                    Me.LastError = InterfaceError.MessageTooShort
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Insufficient elements to validate register address: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If
                Dim sa As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), offset)
                If sa <> startAddress Then
                    Me.LastError = InterfaceError.WrongResponseAddress
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong response address ({sa}<{startAddress}): '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If

                offset = 3
                If Me.RxPayload.Count < offset Then
                    Me.LastError = InterfaceError.MessageTooShort
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Insufficient elements to validate register: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If

                Dim nr As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), offset)
                If nr <> values.Length Then
                    Me.LastError = InterfaceError.WrongResponseRegisters
                    Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong response register ({nr}<{values.Length}): '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                    Return
                End If
            Else
                Me.LastError = InterfaceError.MessageTooShort
                Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Empty Rx: '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
            End If
        End If
    End Sub

    ''' <summary> Make an AND and OR mask of a single holding register. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">  Server device address. </param>
    ''' <param name="address"> Register address. </param>
    ''' <param name="andMask"> AND mask. </param>
    ''' <param name="orMask">  OR mask. </param>
    <CLSCompliant(False)>
    Public Sub MaskWriteRegister(ByVal unitId As Byte, ByVal address As UShort, ByVal andMask As UShort, ByVal orMask As UShort)
        Dim msg_len As UShort = 8
        Me.InitTcpUdpClientMessage()
        Me.TxPayload.Add(CByte(ModbusCodes.MaskWriteRegister))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(address))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(andMask))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(orMask))
        Me.Query(unitId, msg_len)
        If Me.LastError = InterfaceError.NoError Then
            ' Check address
            Dim addr As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), 1)
            If address <> addr Then
                Me.LastError = InterfaceError.WrongResponseAddress
                Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong response address ({addr}<{address}): '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                Return
            End If
            ' Check AND mask
            Dim am As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), 3)
            If andMask <> am Then
                Me.LastError = InterfaceError.WrongResponseAndMask
                Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong response AND mask ({am}<{andMask}): '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                Return
            End If
            ' Check OR mask
            Dim om As UShort = Core.BitConverterExtensions.Methods.BigEndUnsignedInt16(Me.RxPayload.ToArray(), 5)
            If orMask <> om Then
                Me.LastError = InterfaceError.WrongResponseOrMask
                Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Wrong response OR mask ({om}<{orMask}): '{Me.FormatRxPayload}'; Time series: {Me.RxTimeSeries.FormatTimeSeries}"
                Return
            End If
        End If
    End Sub

    ''' <summary> Read and write a set of holding registers in a single shot. </summary>
    ''' <remarks> Write is the first operation, than the read operation. </remarks>
    ''' <param name="unitId">            Server device address. </param>
    ''' <param name="readStartAddress">  Address of first registers to be read. </param>
    ''' <param name="readLen">           Number of registers to be read. </param>
    ''' <param name="writeStartAddress"> Address of first registers to be write. </param>
    ''' <param name="values">            Array of values to be write. </param>
    ''' <returns> Array of read registers. </returns>
    <CLSCompliant(False)>
    Public Function ReadWriteMultipleRegisters(ByVal unitId As Byte, ByVal readStartAddress As UShort, ByVal readLen As UShort,
                                               ByVal writeStartAddress As UShort, ByVal values() As UShort) As UShort()

        If values Is Nothing Then
            Me.LastError = InterfaceError.ZeroRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Empty argument '{NameOf(values)}' in Read Write Multiple Registers"
            Return Nothing
        End If
        If (readLen < 1) OrElse (values.Length < 1) Then
            Me.LastError = InterfaceError.ZeroRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Zero registers requested in Read Write Multiple Registers argument:; {readLen}<1 or {values.Length}<1"
            Return Nothing
        End If
        If (readLen > ModbusBase.MaxHoldingRegistersToReadInReadWriteMessage) OrElse
            (values.Length > ModbusBase.MaxHoldingRegistersToWriteInReadWriteMessage) Then
            Me.LastError = InterfaceError.TooManyRegistersRequested
            Me.LastErrorDetails = $"{Me.InterfaceErrorCaption}: Too many registers requested in Read Write Multiple Registers argument:; {readLen}>{ModbusBase.MaxHoldingRegistersToReadInReadWriteMessage} or {values.Length}>{ModbusBase.MaxHoldingRegistersInWriteMessage}"
            Return Nothing
        End If
        Dim msg_len As UShort = CUShort(11 + (values.Length * 2))
        Me.InitTcpUdpClientMessage()
        Me.TxPayload.Add(CByte(ModbusCodes.ReadWriteMultipleRegisters))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(readStartAddress))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(readLen))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(writeStartAddress))
        Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(CUShort(values.Length)))
        Me.TxPayload.Add(CByte(values.Length * 2))
        For ii As Integer = 0 To values.Length - 1
            Me.TxPayload.AddRange(Core.BitConverterExtensions.Methods.BigEndInt8(values(ii)))
        Next ii
        Me.Query(unitId, msg_len)
        If Me.LastError = InterfaceError.NoError Then
            Dim ret As New List(Of UShort)(Me.ExtractValues())
            Return If(Me.LastError <> InterfaceError.NoError, Nothing, ret.ToArray())
        Else
            Return Nothing
        End If
    End Function

#End Region

End Class

''' <summary> A time series value. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Structure TimeSeriesValue

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="timespan"> The timespan. </param>
    ''' <param name="value">    The value. </param>
    Public Sub New(ByVal timespan As TimeSpan, ByVal value As Byte?)
        Me.Timespan = timespan
        Me.Value = value
    End Sub

    ''' <summary> Gets or sets the timespan. </summary>
    ''' <value> The timespan. </value>
    Public Property Timespan As TimeSpan

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value As Byte?

    ''' <summary> Format byte value. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="timespanFormat"> The timespan format. </param>
    ''' <returns> The formatted byte value. </returns>
    Public Function FormatByteValue(ByVal timespanFormat As String) As String
        Return $"({Me.Timespan.ToString(timespanFormat)},{If(Me.Value.HasValue, Me.Value.Value.ToString("X2"), "..")})"
    End Function

#Region " EQUALS "

    ''' <summary> Compares two Time Series Values. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="left">  Specifies the Time Series Value to compare to. </param>
    ''' <param name="right"> Specifies the Time Series Value to compare. </param>
    ''' <returns> <c>True</c> if the Time Series Values are equal. </returns>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return TimeSeriesValue.Equals(CType(left, TimeSeriesValue), CType(right, TimeSeriesValue))
    End Function

    ''' <summary> Compares two Time Series Values. </summary>
    ''' <remarks>
    ''' The two Time Series Values are the same if they have the same X and Y coordinates.
    ''' </remarks>
    ''' <param name="left">  Specifies the Time Series Value to compare to. </param>
    ''' <param name="right"> Specifies the Time Series Value to compare. </param>
    ''' <returns> <c>True</c> if the Time Series Values are equal. </returns>
    Public Overloads Shared Function Equals(ByVal left As TimeSeriesValue, ByVal right As TimeSeriesValue) As Boolean
        Return left.Equals(right)
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, TimeSeriesValue))
    End Function

    ''' <summary> Compares two Time Series Values. </summary>
    ''' <remarks>
    ''' The two Time Series Values are the same if they have the same X1 and Y1 coordinates.
    ''' </remarks>
    ''' <param name="other"> Specifies the other Time Series Value. </param>
    ''' <returns> <c>True</c> if the Time Series Values are equal. </returns>
    Public Overloads Function Equals(ByVal other As TimeSeriesValue) As Boolean
        Return other.Timespan.Equals(Me.Timespan) AndAlso other.Value.Equals(Me.Value)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As TimeSeriesValue, ByVal right As TimeSeriesValue) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As TimeSeriesValue, ByVal right As TimeSeriesValue) As Boolean
        Return Not left.Equals(right)
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Timespan.GetHashCode Xor Me.Value.GetHashCode
    End Function

#End Region

End Structure

''' <summary> Collection of time series. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-1-12 </para>
''' </remarks>
Public Class TimeSeriesCollection
    Inherits ObjectModel.Collection(Of TimeSeriesValue)

    ''' <summary> Adds timespan. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="timespan"> The timespan. </param>
    ''' <param name="value">    The value. </param>
    Public Overloads Sub Add(ByVal timespan As TimeSpan, ByVal value As Byte?)
        MyBase.Add(New TimeSeriesValue(timespan, value))
    End Sub

    ''' <summary> Format values. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The formatted values. </returns>
    Public Function FormatValues() As String
        Dim builder As New System.Text.StringBuilder()
        For Each v As TimeSeriesValue In Me
            If builder.Length > 0 Then builder.Append(",")
            If v.Value.HasValue Then
                builder.AppendFormat("{0:X2}", v.Value)
            Else
                builder.Append("..")
            End If
        Next
        Return builder.ToString()
    End Function

    ''' <summary> Format time series. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The formatted time series. </returns>
    Public Function FormatTimeSeries() As String
        Dim builder As New System.Text.StringBuilder
        For Each v As TimeSeriesValue In Me
            If builder.Length > -0 Then builder.Append(",")
            builder.Append(v.FormatByteValue("s\.ffff"))
        Next
        Return builder.ToString
    End Function

End Class


