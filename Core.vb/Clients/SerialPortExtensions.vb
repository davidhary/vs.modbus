Imports System.IO.Ports
Imports System.Runtime.CompilerServices

Namespace SerialPortExtensions

    ''' <summary> Serial port extension methods. </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary>
        ''' Returns the bit count for the port baud date, data bits, stop bits and parity.
        ''' </summary>
        ''' <remarks> David, 2020-11-11. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="serialPort"> The serial port. </param>
        ''' <returns> An Integer. </returns>
        <Extension()>
        Public Function BitCount(ByVal serialPort As SerialPort) As Integer
            If serialPort Is Nothing Then Throw New ArgumentNullException(NameOf(serialPort))
            Dim result As Integer = 1 + serialPort.DataBits
            result += If(serialPort.Parity = Parity.None, 0, 1)
            Select Case serialPort.StopBits
                Case StopBits.One
                    result += 1
                Case StopBits.OnePointFive, StopBits.Two ' Ceiling
                    result += 2
            End Select
            If result <= 0 Then Throw New InvalidOperationException($"Bit count for {serialPort.PortName}:{serialPort.BaudRate}:{serialPort.DataBits}:{serialPort.StopBits}:{serialPort.Parity} of {result} is invalid")
            Return result
        End Function

        ''' <summary> Returns the estimated character rate per second. </summary>
        ''' <remarks> David, 2020-11-11. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="serialPort"> Serial Port. </param>
        ''' <returns> A Double. </returns>
        <Extension()>
        Public Function CharacterRate(ByVal serialPort As SerialPort) As Double
            If serialPort Is Nothing Then Throw New ArgumentNullException(NameOf(serialPort))
            Return serialPort.BaudRate / serialPort.BitCount
        End Function

        ''' <summary> Character timespan. </summary>
        ''' <remarks> David, 2020-11-11. </remarks>
        ''' <param name="serialPort"> Serial Port. </param>
        ''' <returns> A TimeSpan. </returns>
        <Extension()>
        Public Function CharacterTimespan(ByVal serialPort As SerialPort) As TimeSpan
            Return TimeSpan.FromTicks(CLng(TimeSpan.TicksPerMillisecond * (1000 / serialPort.CharacterRate)))
        End Function

    End Module

End Namespace
