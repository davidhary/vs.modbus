Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.IO.Ports
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Reflection
Imports System.Diagnostics
Imports System.Windows.Threading
Imports isr.Core.DispatcherExtensions

''' <summary> Modbus Client TCP Class. </summary>
''' <remarks>
''' (c) 2013 Simone Assunti. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
''' </remarks>
Public Class ModbusClientTCP
    Inherits ModbusClientBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="remoteHost"> Remote hostname or IP address. </param>
    ''' <param name="port">       Remote host Modbus TCP listening port. </param>
    Public Sub New(ByVal remoteHost As String, ByVal port As Integer)
        MyBase.New
        ' Set device states
        Me.ConnectionType = ConnectionType.TcpIP
        ' Set socket client
        Me._RemoteHost = remoteHost
        Me._Port = port
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.Disposed Then
                If disposing Then
                    If Me._NetworkStream IsNot Nothing Then
                        Me._NetworkStream.Dispose()
                        Me._NetworkStream = Nothing
                    End If
                    If Me._TcpClient IsNot Nothing Then
                        Me._TcpClient.Dispose()
                        Me._TcpClient = Nothing
                    End If
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " CONNECT/DISCONNECT FUNCTIONS "

    ''' <summary>
    ''' Remote hostname or IP address
    ''' </summary>
    Private ReadOnly _RemoteHost As String

    ''' <summary>
    ''' Remote host Modbus TCP listening port
    ''' </summary>
    Private ReadOnly _Port As Integer

    ''' <summary>
    ''' TCP Client
    ''' </summary>
    Private _TcpClient As TcpClient

    ''' <summary>
    ''' Network stream
    ''' </summary>
    Private _NetworkStream As NetworkStream

    ''' <summary> Connect function. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub Connect()
        If Me._TcpClient Is Nothing Then
            Me._TcpClient = New TcpClient()
        End If
        Me._TcpClient.Connect(Me._RemoteHost, Me._Port)
        If Me._TcpClient.Connected Then
            Me._NetworkStream = Me._TcpClient.GetStream()
            Me.Connected = True
        End If
    End Sub

    ''' <summary> Disconnect function. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub Disconnect()
        Me._NetworkStream.Close()
        Me._TcpClient.Close()
        Me._TcpClient = Nothing
        Me.Connected = False
    End Sub

#End Region

#Region " PROTOCOL FUNCTIONS "

    ''' <summary> Send transmission buffer. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="buffer"> The buffer. </param>
    Protected Overrides Sub Send(ByVal buffer As IEnumerable(Of Byte))
        Me.TxBuffer.AddRange(buffer)
        Me._NetworkStream.Write(Me.TxBuffer.ToArray(), 0, Me.TxBuffer.Count)
    End Sub

    ''' <summary> Gets the status of device data available. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if a new device data is available; otherwise <c>false</c> </returns>
    Public Overrides Function DeviceDataAvailable() As Boolean
        Return Me._NetworkStream.DataAvailable
    End Function

    ''' <summary> Reads a byte from the device. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> Byte read or nothing if no data are present. </returns>
    Protected Overrides Function ReadByte() As Byte?

        ' Check if there are available bytes
        Do While Me.DeviceDataAvailable
            Dim value As Integer = Me._NetworkStream.ReadByte
            If value >= 0 Then
                Me.RxDeviceBuffer.Add(CByte(value And &HFF))
            Else
                Exit Do
            End If
            isr.Core.ApplianceBase.DoEvents()
        Loop

        If Me.RxDeviceBuffer.Any Then
            ' There are available bytes in temporary rx buffer, read the first and delete it
            Dim ret As Byte = Me.RxDeviceBuffer(0)
            Me.RxDeviceBuffer.RemoveAt(0)
            Return ret
        Else
            Return New Byte?
        End If

    End Function

    ''' <summary> Flushes device input and output buffers and cached input buffers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Overrides Sub FlushDeviceBuffers()
        MyBase.FlushDeviceBuffers()
        If Me._NetworkStream.DataAvailable Then Me._NetworkStream.Flush()
    End Sub

#End Region

End Class

