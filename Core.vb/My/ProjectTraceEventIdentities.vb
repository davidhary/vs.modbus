Imports System.ComponentModel
Namespace My

    ''' <summary> Values that represent project trace event identifiers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Enum ProjectTraceEventId

        ''' <summary> An enum constant representing the none option. </summary>
        <Description("Not specified")>
        None

        <Description("Modbus Core")>
        Core = isr.Core.ProjectTraceEventId.Modbus

        <Description("Watlow Library")>
        WatlowLibrary = ProjectTraceEventId.Core + &H1

        <Description("Watlow Console")>
        WatlowConsole = ProjectTraceEventId.Core + &HF
    End Enum

End Namespace


