Imports System.Configuration

''' <summary> A RUT Client Test Info. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-2-12 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
 Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
 Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class RtuClientTestInfo
    Inherits isr.Core.ApplicationSettingsBase

#Region " SINGLETON "

    ''' <summary>
    ''' Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
    ''' class to its default state.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(RtuClientTestInfo)} Editor", RtuClientTestInfo.Get)
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As RtuClientTestInfo

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As RtuClientTestInfo
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New RtuClientTestInfo()), RtuClientTestInfo)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " CONFIGURATION INFORMATION "

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property Exists As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Property Verbose As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Returns true to enable this device. </summary>
    ''' <value> The device enable option. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property Enabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets all. </summary>
    ''' <value> all. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property All As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

#Region " RTU CLIENT INFORMATION "

    ''' <summary> Name of the port. </summary>
    Private _PortName As String

    ''' <summary> Gets the name of the port. </summary>
    ''' <value> The name of the port. </value>
    Public ReadOnly Property PortName As String
        Get
            If String.IsNullOrEmpty(_PortName) Then
                If My.Computer.Ports.SerialPortNames.Contains(Me.FirstPortName) Then
                    Me._PortName = Me.FirstPortName
                ElseIf My.Computer.Ports.SerialPortNames.Contains(Me.AlternatePortName) Then
                    Me._PortName = Me.AlternatePortName
                Else
                    Me._PortName = String.Empty
                End If
            End If
            Return Me._PortName
        End Get
    End Property

    ''' <summary> Gets or sets the name of the first port. </summary>
    ''' <value> The name of the first port. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("COM3")>
    Public Property FirstPortName As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the name of the alternate port. </summary>
    ''' <value> The name of the alternate port. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("COM4")>
    Public Property AlternatePortName As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the baud rate. </summary>
    ''' <value> The baud rate. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("38400")>
    Public Property BaudRate As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the identifier of the unit. </summary>
    ''' <value> The identifier of the unit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1")>
    Public Property UnitId As Byte
        Get
            Return Me.AppSettingGetter(CByte(0))
        End Get
        Set(value As Byte)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the type of the alarm. </summary>
    ''' <value> The type of the alarm. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("76")>
    Public Property AlarmType As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

#Region " MEASUREMENTS "

    ''' <summary> Gets or sets the ambient temperature. </summary>
    ''' <value> The ambient temperature. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("76")>
    Public Property AmbientTemperature As Single
        Get
            Return Me.AppSettingGetter(0F)
        End Get
        Set(value As Single)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the ambient temperature epsilon. </summary>
    ''' <value> The ambient temperature epsilon. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("76")>
    Public Property AmbientTemperatureEpsilon As Single
        Get
            Return Me.AppSettingGetter(0F)
        End Get
        Set(value As Single)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

End Class

