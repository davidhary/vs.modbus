Imports System.IO.Ports

Imports isr.Core.BitConverterExtensions

''' <summary> A RTD client test. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-04-06 </para>
''' </remarks>
<TestClass(), TestCategory("EZ-Zone")>
Public Class RtuClientTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Modbus.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
            RtuClientTests.Initialize()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
        RtuClientTests.Cleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")

        Assert.IsTrue(RtuClientTestInfo.Get.Exists, $"{GetType(RtuClientTestInfo)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " RTU CLIENT: SHARED  "

    ''' <summary> Gets or sets the identifier of the unit. </summary>
    ''' <value> The identifier of the unit. </value>
    Private Shared ReadOnly Property UnitId As Byte = RtuClientTestInfo.Get.UnitId

    ''' <summary> Gets or sets the name of the port. </summary>
    ''' <value> The name of the port. </value>
    Private Shared ReadOnly Property PortName As String = RtuClientTestInfo.Get.PortName

    ''' <summary> Gets or sets the modbus client. </summary>
    ''' <value> The modbus client. </value>
    Private Shared ReadOnly Property ModbusClient As Modbus.ModbusClientSerial

    ''' <summary> Runs code before running the first test in the class. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Shared Sub Initialize()
        ' Crete instance of modbus serial RTU 
        RtuClientTests._ModbusClient = New Modbus.ModbusClientSerial(Modbus.ModbusSerialType.RTU,
                                                                     Modbus.ModbusClientSerial.ConstructPort(RtuClientTests.PortName, 38400, 8,
                                                                                                             Parity.None, StopBits.One, Handshake.None))
        ' Connect
        RtuClientTests.ModbusClient.Connect()
    End Sub

    ''' <summary> Cleanups this object. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Shared Sub Cleanup()
        ' Disconnect
        RtuClientTests.ModbusClient?.Disconnect()
    End Sub


#End Region

#Region " RTD CLIENT TESTS "

    ''' <summary> (Unit Test Method) connects the test. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub ConnectTest()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
    End Sub

    ''' <summary> (Unit Test Method) tests read watlow alarm 1 type. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub ReadWatlowAlarm1TypeTest()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 1508
        Dim expectedValues As UShort() = New UShort() {62, 76}
        Dim receivedValues As UShort() = RtuClientTests.ModbusClient.ReadHoldingRegisters(RtuClientTests.UnitId, registerAddress, 1)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Read error")
        Assert.IsTrue(expectedValues.Contains(receivedValues.First()), $"{receivedValues.First()} should be either {expectedValues(0)} or {expectedValues(1)}")
    End Sub

    ''' <summary> (Unit Test Method) tests try read watlow alarm 1 type. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub TryReadWatlowAlarm1TypeTest()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 1508
        Dim expectedValues As UShort() = New UShort() {62, 76}
        Dim actualValue As UShort = 0
        Dim outcome As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
        Assert.AreEqual(True, outcome)
        Assert.IsTrue(expectedValues.Contains(actualValue), $"{actualValue} should be either {expectedValues(0)} or {expectedValues(1)}")
    End Sub

    ''' <summary> (Unit Test Method) writes the watlow alarm 1 type test. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub WriteWatlowAlarm1TypeTest()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 1508
        Dim toggleOnTimeout As TimeSpan = TimeSpan.FromSeconds(3)
        Dim toggleOffTimeout As TimeSpan = TimeSpan.FromSeconds(4)
        Dim offValue As UShort = 62
        Dim processValue As UShort = 76
        Dim actualValue As UShort = 0
        Dim outcome As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
        Assert.AreEqual(True, outcome)
        If actualValue = offValue Then
            RtuClientTests.ModbusClient.WriteRegistersValue(RtuClientTests.UnitId, registerAddress, processValue)
            isr.Core.ApplianceBase.DoEventsWait(toggleOnTimeout)
            Dim unused3 As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
            Assert.AreEqual(processValue, actualValue)
            RtuClientTests.ModbusClient.WriteRegistersValue(RtuClientTests.UnitId, registerAddress, offValue)
            isr.Core.ApplianceBase.DoEventsWait(toggleOffTimeout)
            Dim unused As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)

            Assert.AreEqual(offValue, actualValue)
        ElseIf actualValue = processValue Then
            RtuClientTests.ModbusClient.WriteRegistersValue(RtuClientTests.UnitId, registerAddress, offValue)
            isr.Core.ApplianceBase.DoEventsWait(toggleOffTimeout)
            Dim unused1 As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
            Assert.AreEqual(offValue, actualValue)
            RtuClientTests.ModbusClient.WriteRegistersValue(RtuClientTests.UnitId, registerAddress, processValue)
            isr.Core.ApplianceBase.DoEventsWait(toggleOnTimeout)
            Dim unused2 As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
            Assert.AreEqual(processValue, actualValue)

        Else
            Assert.AreEqual(offValue, actualValue)
        End If

    End Sub

    ''' <summary> (Unit Test Method) tests read watlow input error. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub ReadWatlowInputErrorTest()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 362
        Dim expectedValue As UShort = 61
        Dim actualValue As UShort = RtuClientTests.ModbusClient.ReadHoldingRegisters(RtuClientTests.UnitId, registerAddress, 1).First()
        Assert.AreEqual(expectedValue, actualValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Read error")
    End Sub

    ''' <summary> (Unit Test Method) tests try read watlow input error. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub TryReadWatlowInputErrorTest()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 362
        Dim expectedValue As UShort = 61
        Dim actualValue As UShort = 0
        Dim outcome As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
        Assert.AreEqual(True, outcome)
        Assert.AreEqual(expectedValue, actualValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Read error")
    End Sub

    ''' <summary> (Unit Test Method) tests try read analog input 1. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub TryReadAnalogInput1Test()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 360
        Dim expectedValue As Single = RtuClientTestInfo.Get.AmbientTemperature
        Dim actualValue As Single = 0
        Dim outcome As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Read error")
        Assert.AreEqual(expectedValue, actualValue, RtuClientTestInfo.Get.AmbientTemperature)
    End Sub

    ''' <summary> (Unit Test Method) tests try read analog input 1 with input error. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub TryReadAnalogInput1WithInputErrorTest()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 360
        Dim expectedValue As Single = RtuClientTestInfo.Get.AmbientTemperature
        Dim actualValue As Single = 0
        Dim unused As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Read error")
        Assert.AreEqual(expectedValue, actualValue, RtuClientTestInfo.Get.AmbientTemperature)
        isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(100))

        registerAddress = 362
        Dim expectedShort As UShort = 61

        Dim actualShort As UShort = 0
        Dim outcome As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualShort)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Input error Interface error")
        Assert.AreEqual(True, outcome, "Read Input error failed")
        Assert.AreEqual(expectedShort, actualShort)

    End Sub

    ''' <summary> (Unit Test Method) tests try read setpoint. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub TryReadSetpointTest()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 2160
        Dim expectedValue As Single = 75
        Dim actualValue As Single = 0
        Dim outcome As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Read error")
        Assert.AreEqual(expectedValue, actualValue, 1)
    End Sub

    ''' <summary> (Unit Test Method) writes the setpoint 1 test. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub WriteSetpoint1Test()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Connection error")
        Dim registerAddress As UShort = 2160
        Dim expectedValue As Single = 75
        Dim bytes As Byte() = expectedValue.BigEndBytes
        Dim values As UShort() = New UShort() {bytes.BigEndUnsignedShort(2), bytes.BigEndUnsignedShort(0)}
        RtuClientTests.ModbusClient.WriteMultipleRegisters(RtuClientTests.UnitId, registerAddress, values)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Write error")
    End Sub

    ''' <summary> (Unit Test Method) writes the read setpoint 1 test. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub WriteReadSetpoint1Test()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 2160
        Dim expectedValue As Single = 76
        Dim actualValue As Single = 0
        RtuClientTests.ModbusClient.WriteRegistersValue(RtuClientTests.UnitId, registerAddress, expectedValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Write 1 error")

        Dim postWriteRefractoryTimespan As TimeSpan = TimeSpan.FromMilliseconds(10)
        isr.Core.ApplianceBase.DoEventsWait(postWriteRefractoryTimespan)

        Dim outcome As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)

        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Read 1 error")
        Assert.AreEqual(True, outcome)

        Assert.AreEqual(expectedValue, actualValue, 1)

        Dim postReadRefractoryTimespan As TimeSpan = TimeSpan.FromMilliseconds(10)
        isr.Core.ApplianceBase.DoEventsWait(postReadRefractoryTimespan)

        expectedValue = 75
        RtuClientTests.ModbusClient.WriteRegistersValue(RtuClientTests.UnitId, registerAddress, expectedValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Write 2 error")
        isr.Core.ApplianceBase.DoEventsWait(postWriteRefractoryTimespan)

        outcome = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Read 2 error")
        Assert.AreEqual(True, outcome)
        Assert.AreEqual(expectedValue, actualValue, 1)
        isr.Core.ApplianceBase.DoEventsWait(postReadRefractoryTimespan)
    End Sub

    ''' <summary> (Unit Test Method) writes the recovery test. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    <TestMethod()>
    Public Sub WriteRecoveryTest()

        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 2160
        ' user an incorrect type to elicit an error.

        Dim incorrectExpectedType As UShort = 76

        ' write should fail because of the wrong type; but no error is reported. 
        RtuClientTests.ModbusClient.WriteRegistersValue(RtuClientTests.UnitId, registerAddress, incorrectExpectedType)
        ' this does not fail. Test not working as expected.
        ' but recovery was tested because other tests failed and were recoverable.
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Write 1 error")

        Dim postWriteRefractoryTimespan As TimeSpan = TimeSpan.FromMilliseconds(10)
        isr.Core.ApplianceBase.DoEventsWait(postWriteRefractoryTimespan)
        Dim actualValue As Single = 0
        Dim expectedValue As Single = 75

        ' the protocol should allow writing the correct value type.
        RtuClientTests.ModbusClient.WriteRegistersValue(RtuClientTests.UnitId, registerAddress, expectedValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Write 1 error")
        isr.Core.ApplianceBase.DoEventsWait(postWriteRefractoryTimespan)

        Dim outcome As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Read 1 error")
        Assert.AreEqual(True, outcome)
        Assert.AreEqual(expectedValue, actualValue, 1)

        Dim postReadRefractoryTimespan As TimeSpan = TimeSpan.FromMilliseconds(10)
        isr.Core.ApplianceBase.DoEventsWait(postReadRefractoryTimespan)

    End Sub

    ''' <summary> (Unit Test Method) tests read recover. </summary>
    ''' <remarks> Test was unable to check failure. </remarks>
    <TestMethod()>
    Public Sub ReadRecoverTest()
        ' Checks if connected
        Assert.AreEqual(True, RtuClientTests.ModbusClient?.Connected)
        Dim registerAddress As UShort = 2160
        Dim expectedValue As Single = 75
        Dim actualValueWrongType As UShort = 0
        Dim actualValue As Single = 0

        ' write should be okay
        RtuClientTests.ModbusClient.WriteRegistersValue(RtuClientTests.UnitId, registerAddress, expectedValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Write 1 error")


        Dim postWriteRefractoryTimespan As TimeSpan = TimeSpan.FromMilliseconds(10)
        isr.Core.ApplianceBase.DoEventsWait(postWriteRefractoryTimespan)

        ' read should fail because the wrong part.
        Dim unused As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValueWrongType)
        ' this does not fail. Test not working as expected.
        ' but recovery was tested because other tests failed and were recoverable.
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "CRC Error")

        Dim postReadRefractoryTimespan As TimeSpan = TimeSpan.FromMilliseconds(10)
        isr.Core.ApplianceBase.DoEventsWait(postReadRefractoryTimespan)


        ' the protocol should allow reading the correct value
        Dim outcome As Boolean = RtuClientTests.ModbusClient.TryReadHoldingRegistersValue(RtuClientTests.UnitId, registerAddress, actualValue)
        Assert.AreEqual(Modbus.InterfaceError.NoError, RtuClientTests.ModbusClient.LastError, "Read 1 error")
        Assert.AreEqual(True, outcome)
        Assert.AreEqual(expectedValue, actualValue, 1)
        isr.Core.ApplianceBase.DoEventsWait(postReadRefractoryTimespan)

    End Sub
#End Region

End Class

