Imports System.ComponentModel
Imports System.IO.Ports
Imports System.Timers

Imports Arebis.TypedUnits

Imports isr.Automata.Finite.Engines
Imports isr.Core
Imports isr.Core.EnumExtensions
Imports isr.Modbus.Watlow.ExceptionExtensions

Imports Stateless

''' <summary> A device manager for the Watlow EZ-Zone controller. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-5-10 </para>
''' </remarks>
Public Class EasyZone
    Inherits isr.Core.Models.ViewModelTalkerBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">   Identifier for the unit. </param>
    ''' <param name="portName"> Name of the port. </param>
    Public Sub New(ByVal unitId As Integer, ByVal portName As String)
        MyBase.New
        Me._TransactionElapsedTime = New Arebis.TypedUnits.Amount(0, Arebis.StandardUnits.TimeUnits.Millisecond)
        Me._UnitId = CByte(unitId)
        Me._PortName = portName
        Me.AssignSoakEngine(New SoakEngine("Soak"))
        ' set tracing to information level otherwise with automaton the error could sound quite often.
        Me.ModbusErrorTraceEventType = TraceEventType.Information
        Me.ContinuousFailureCountTryAgainLimit = 5
        Me._InputErrorCountDictionary = New Dictionary(Of InputError, Long)
        Me._ContinuousInputErrorCountDictionary = New Dictionary(Of InputError, Integer)
        Me._CandidateSetpoint = New isr.Core.Constructs.ConcurrentToken(Of Double?)
        Me._CandidateOvenControlMode = New isr.Core.Constructs.ConcurrentToken(Of OvenControlMode?)
        Me.TemperatureUnit = Arebis.StandardUnits.TemperatureUnits.DegreeFahrenheit
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets or sets the disposed condition indicator. </summary>
    ''' <value> The disposed condition indicator. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases
    ''' the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me._CandidateSetpoint?.Dispose()
                Me.Talker?.Listeners.Clear()
                Me.AssignSoakEngine(Nothing)
                If Me.ModbusClient IsNot Nothing Then
                    If Me.ModbusClient.Connected Then Me.ModbusClient.Disconnect()
                End If
                Me._ModbusClient?.Dispose()
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#End Region

#Region " KNOWN STATE "

    ''' <summary> Clears the known state. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Sub ClearKnownState()
        Me.AnalogInputRead = Nothing
        Me.SetpointRead = Nothing
        Me.ClearLastInterfaceError()
    End Sub

#End Region

#Region " INTERFACE "

    ''' <summary> Gets or sets the identifier of the unit. </summary>
    ''' <value> The identifier of the unit. </value>
    Public Property UnitId As Byte

    ''' <summary> Gets or sets the name of the port. </summary>
    ''' <value> The name of the port. </value>
    Public Property PortName As String

    ''' <summary> Gets or sets the modbus client. </summary>
    ''' <value> The modbus client. </value>
    Public ReadOnly Property ModbusClient As ModbusClientSerial

    ''' <summary> Attempts to open modbus client from the given data. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryOpenModbusClient() As Boolean
        Dim sw As Stopwatch = Stopwatch.StartNew
        Me.ClearLastInterfaceError()
        Me._ModbusClient = New ModbusClientSerial(ModbusSerialType.RTU,
                                                  ModbusClientSerial.ConstructPort(Me.PortName,
                                                                                   38400, 8, Parity.None, StopBits.One, Handshake.None))
        ' Connect
        Me.ModbusClient.Connect()
        Me.IsDeviceOpen = Me.ModbusClient.Connected
        If Me.IsDeviceOpen Then
            Me.UpdateLastInterfaceError()
            Me.Title = $"RTU#{Me.UnitId}:{Me.PortName}"
        Else
            Me.ClearLastInterfaceError()
            Me.Title = $"RTU Closed"
        End If
        Me.TransactionElapsedTime = New Arebis.TypedUnits.Amount(sw.ElapsedMilliseconds, Arebis.StandardUnits.TimeUnits.Millisecond)
        Me.InputErrorRead = InputError.NoError
        Return Me.ModbusClient.Connected
    End Function

    ''' <summary> Closes modbus client. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Sub CloseModbusClient()
        Me.ClearLastInterfaceError()
        Me.ModbusClient?.Disconnect()
        Me.IsDeviceOpen = (Me.ModbusClient?.Connected).GetValueOrDefault(False)
        If Me.IsDeviceOpen Then
            Me.UpdateLastInterfaceError()
            Me.Title = $"RTU#{Me.UnitId}:{Me.PortName}"
        Else
            Me.ClearLastInterfaceError()
            Me.Title = $"RTU Closed"
        End If
    End Sub

    ''' <summary> True to enable, false to disable. </summary>
    Private _Enabled As Boolean

    ''' <summary>
    ''' Gets or sets the Enabled sentinel of the device. A device is enabled when hardware can be
    ''' used.
    ''' </summary>
    ''' <value> <c>True</c> if hardware device is enabled; <c>False</c> otherwise. </value>
    Public Overridable Property Enabled As Boolean
        Get
            Return Me._Enabled
        End Get
        Set(ByVal value As Boolean)
            If Me.Enabled <> value Then
                Me._Enabled = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> True if device is open, false if not. </summary>
    Private _IsDeviceOpen As Boolean

    ''' <summary> Gets or sets the is device open. </summary>
    ''' <value> The is device open. </value>
    Public Property IsDeviceOpen As Boolean
        Get
            Return Me._IsDeviceOpen
        End Get
        Protected Set(value As Boolean)
            If value <> Me.IsDeviceOpen Then
                Me._IsDeviceOpen = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The title. </summary>
    Private _Title As String

    ''' <summary> Gets or sets the title. </summary>
    ''' <value> The title. </value>
    Public Property Title As String
        Get
            Return Me._Title
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Title) Then
                Me._Title = value
                Me.AsyncNotifyPropertyChanged()
            End If

        End Set
    End Property

#End Region

#Region " TRANSACTION CONTROL "

    ''' <summary>
    ''' Gets or sets the timespan for recovery from a reading before a new write can be made.
    ''' </summary>
    ''' <value> The timespan for settling after a read query. </value>
    Public Property DefaultReadSettlingTimespan As TimeSpan = TimeSpan.FromMilliseconds(10)

    ''' <summary> Gets or sets next settling timespan. </summary>
    ''' <value> The next settling timespan. </value>
    Public Property SettlingTimespan As TimeSpan
        Get
            Return Me.ModbusClient.DeviceSettlingTime
        End Get
        Set(value As TimeSpan)
            If value <> Me.SettlingTimespan Then
                Me.ModbusClient.DeviceSettlingTime = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Executes the transaction ended action. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="settlingTime"> The settling time before the next transaction. </param>
    Private Sub OnTransactionEnded(ByVal settlingTime As TimeSpan)
        Me.TransactionElapsedTime = New Arebis.TypedUnits.Amount(Me.ModbusClient.QueryElapsedTime.TotalMilliseconds,
                                                                 Arebis.StandardUnits.TimeUnits.Millisecond)
        ' set the recovery time following this last transaction
        Me.SettlingTimespan = settlingTime
    End Sub

    ''' <summary> The transaction elapsed time. </summary>
    Private _TransactionElapsedTime As Arebis.TypedUnits.Amount

    ''' <summary> Gets or sets the transaction elapsed time. </summary>
    ''' <value> The transaction elapsed time. </value>
    Public Property TransactionElapsedTime As Arebis.TypedUnits.Amount
        Get
            Return Me._TransactionElapsedTime
        End Get
        Set(value As Arebis.TypedUnits.Amount)
            If value <> Me.TransactionElapsedTime Then
                Me._TransactionElapsedTime = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " INTERFACE ERROR "

    ''' <summary> Gets or sets the type of the modbus error trace event. </summary>
    ''' <value> The type of the modbus error trace event. </value>
    Public Property ModbusErrorTraceEventType As TraceEventType = TraceEventType.Information

    ''' <summary> Updates the last interface error from the <see cref="EasyZone.ModbusClient"/>. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    Private Sub UpdateLastInterfaceError()
        Me.LastInterfaceError = Me.ModbusClient.LastError
        Me.LastInterfaceErrorDetails = Me.ModbusClient.LastErrorDetails
        Me.LastInterfaceErrorContinuousCount = Me.ModbusClient.LastErrorContinuousFailureCount
    End Sub

    Private _LastInterfaceError As InterfaceError

    ''' <summary> Gets or sets the last interface error. </summary>
    ''' <value> The last interface error. </value>
    Public Property LastInterfaceError As InterfaceError
        Get
            Return Me._LastInterfaceError
        End Get
        Protected Set(value As InterfaceError)
            Me._LastInterfaceError = value
            Me.AsyncNotifyPropertyChanged()
        End Set
    End Property

    Private _ContinuousFailureCountTryAgainLimit As Integer

    ''' <summary> Gets or sets the continuous failure count try again limit. </summary>
    ''' <value> The continuous failure count try again limit. </value>
    Public Property ContinuousFailureCountTryAgainLimit As Integer
        Get
            Return Me._ContinuousFailureCountTryAgainLimit
        End Get
        Set(value As Integer)
            If value <> Me.ContinuousFailureCountTryAgainLimit Then
                Me._ContinuousFailureCountTryAgainLimit = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _LastInterfaceErrorContinuousCount As Integer

    ''' <summary> Gets or sets the last interface error continuous count. </summary>
    ''' <value> The last interface error continuous count. </value>
    Public Property LastInterfaceErrorContinuousCount As Integer
        Get
            Return Me._LastInterfaceErrorContinuousCount
        End Get
        Protected Set(value As Integer)
            Me._LastInterfaceErrorContinuousCount = value
            Me.AsyncNotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The last interface error details. </summary>
    Private _LastInterfaceErrorDetails As String

    ''' <summary> Gets or sets the last interface error details. </summary>
    ''' <value> The last interface error. </value>
    Public Property LastInterfaceErrorDetails As String
        Get
            Return Me._LastInterfaceErrorDetails
        End Get
        Protected Set(value As String)
            Me._LastInterfaceErrorDetails = value
            Me.AsyncNotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Clears the last interface error. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Sub ClearLastInterfaceError()
        If Me.LastInterfaceError <> InterfaceError.NoError Then
            Me.LastInterfaceError = InterfaceError.NoError
            Me.LastInterfaceErrorDetails = String.Empty
            Me.LastInterfaceErrorContinuousCount = 0
        End If
    End Sub

#End Region

#Region " INPUT ERROR "

    Private _ContinuousInputErrorCount As Integer

    ''' <summary> Gets or sets the Continuous Input Error count. </summary>
    ''' <value> The Input Error read. </value>
    Public Property ContinuousInputErrorCount As Integer
        Get
            Return Me._ContinuousInputErrorCount
        End Get
        Protected Set(value As Integer)
            If value <> Me.ContinuousInputErrorCount Then
                Me._ContinuousInputErrorCount = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    Private ReadOnly _InputErrorCountDictionary As Dictionary(Of InputError, Long)

    ''' <summary> Input error count. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <param name="inputError"> The input error. </param>
    ''' <returns> An Integer. </returns>
    Public Function InputErrorCount(ByVal inputError As InputError) As Long
        Return If(Me._InputErrorCountDictionary.ContainsKey(inputError), Me._InputErrorCountDictionary(inputError), 0)
    End Function

    Private ReadOnly _ContinuousInputErrorCountDictionary As Dictionary(Of InputError, Integer)

    ''' <summary> Input error continuous count. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <param name="inputError"> The input error. </param>
    ''' <returns> An Integer. </returns>
    Public Function InputErrorContinuousCount(ByVal inputError As InputError) As Integer
        Return If(Me._ContinuousInputErrorCountDictionary.ContainsKey(inputError), Me._ContinuousInputErrorCountDictionary(inputError), 0)
    End Function

    ''' <summary> Builds compound input error. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildCompoundInputError(ByVal value As InputError) As String
        Return $"{CInt(value)},{value.Description}"
    End Function

    ''' <summary> The input error read. </summary>
    Private _InputErrorRead As InputError

    ''' <summary> Gets or sets the Input Error read. </summary>
    ''' <value> The Input Error read. </value>
    Public Property InputErrorRead As InputError
        Get
            Return Me._InputErrorRead
        End Get
        Protected Set(value As InputError)
            If InputError.NoError = value Then
                Me.ContinuousInputErrorCount = 0
                Me._ContinuousInputErrorCountDictionary.Clear()
            Else
                Me.ContinuousInputErrorCount += 1
                If value = Me.InputErrorRead Then
                    If Me._ContinuousInputErrorCountDictionary.ContainsKey(value) Then
                        Me._ContinuousInputErrorCountDictionary(value) += 1
                    Else
                        Me._ContinuousInputErrorCountDictionary.Add(value, 0)
                    End If
                End If
            End If
            If Me._InputErrorCountDictionary.ContainsKey(value) Then
                Me._InputErrorCountDictionary(value) += 1
            Else
                Me._InputErrorCountDictionary.Add(value, 1)
            End If
            If value <> Me.InputErrorRead Then
                Me._InputErrorRead = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Input Error register. </summary>
    ''' <value> The Input Error register. </value>
    Public Property InputErrorRegister As Integer = 362

    ''' <summary> Reads Input Error. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The Input Error. </returns>
    Public Function ReadInputError() As InputError
        Dim actualValue As UShort = 0
        Me.ClearLastInterfaceError()
        Dim activity As String = "reading input error"
        If Me.ModbusClient.TryReadHoldingRegistersValue(Me.UnitId, CUShort(Me.InputErrorRegister), actualValue) Then
            Me.InputErrorRead = If([Enum].IsDefined(GetType(InputError), CInt(actualValue)), CType(actualValue, InputError), InputError.None)
        End If
        Me.UpdateLastInterfaceError()
        If Me.LastInterfaceError = InterfaceError.NoError Then
            Me.PublishVerbose($"Success {activity}: {Me.ModbusClient.FormatRxPayload};. ")
        Else
            Me.PublishDefaultErrorLevel($"Failed {activity}: {Me.LastInterfaceErrorDetails}")
        End If
        Me.OnTransactionEnded(Me.DefaultReadSettlingTimespan)
        Return Me.InputErrorRead
    End Function

#End Region

#Region " ALARM TYPE "

    ''' <summary> The alarm type read. </summary>
    Private _AlarmTypeRead As AlarmType

    ''' <summary> Gets or sets the Alarm Type read. </summary>
    ''' <value> The Alarm Type read. </value>
    Public Property AlarmTypeRead As AlarmType
        Get
            Return Me._AlarmTypeRead
        End Get
        Protected Set(value As AlarmType)
            If value <> Me.AlarmTypeRead Then
                Me._AlarmTypeRead = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The alarm type written. </summary>
    Private _AlarmTypeWritten As AlarmType

    ''' <summary> Gets or sets the Alarm Type Written. </summary>
    ''' <value> The Alarm Type Written. </value>
    Public Property AlarmTypeWritten As AlarmType
        Get
            Return Me._AlarmTypeWritten
        End Get
        Protected Set(value As AlarmType)
            If value <> Me.AlarmTypeWritten Then
                Me._AlarmTypeWritten = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Alarm Type register. </summary>
    ''' <value> The Alarm Type register. </value>
    Public Property AlarmTypeRegister As Integer = 1508

    ''' <summary> Applies the Alarm Type described by value. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> An AlarmType. </returns>
    Public Function ApplyAlarmType(ByVal value As AlarmType) As AlarmType
        Me.WriteAlarmType(value)
        If Me.LastInterfaceError = InterfaceError.NoError Then Me.ReadAlarmType()
        Return Me.AlarmTypeRead
    End Function

    ''' <summary> Reads Alarm Type. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The Alarm Type. </returns>
    Public Function ReadAlarmType() As AlarmType
        Me.ClearLastInterfaceError()
        Dim actualValue As UShort = 0
        Dim activity As String = "reading alarm type"
        If Me.ModbusClient.TryReadHoldingRegistersValue(Me.UnitId, CUShort(Me.AlarmTypeRegister), actualValue) Then
            Me.AlarmTypeRead = If([Enum].IsDefined(GetType(AlarmType), CInt(actualValue)), CType(actualValue, AlarmType), AlarmType.None)
        End If
        Me.UpdateLastInterfaceError()
        If Me.LastInterfaceError = InterfaceError.NoError Then
            Me.PublishVerbose($"Success {activity}: {Me.ModbusClient.FormatRxPayload};. ")
        Else
            Me.PublishDefaultErrorLevel($"Failed {activity}: {Me.LastInterfaceErrorDetails}")
        End If
        Me.OnTransactionEnded(Me.DefaultReadSettlingTimespan)
        Return Me.AlarmTypeRead
    End Function

    ''' <summary> Gets or sets the Alarm Type settling time. </summary>
    ''' <value> The Alarm Type settling time. </value>
    Public Property AlarmTypeSettlingTime As TimeSpan = TimeSpan.FromSeconds(2)

    ''' <summary> Writes an Alarm Type. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> An AlarmType. </returns>
    Public Function WriteAlarmType(ByVal value As AlarmType) As AlarmType
        Me.ClearLastInterfaceError()
        Dim activity As String = "writing alarm type"
        Me.ModbusClient.WriteRegistersValue(Me.UnitId, CUShort(Me.AlarmTypeRegister), CUShort(value))
        Me.UpdateLastInterfaceError()
        If Me.LastInterfaceError = InterfaceError.NoError Then
            Me.PublishVerbose($"Success {activity}: {Me.ModbusClient.FormatRxPayload};. ")
        Else
            Me.PublishDefaultErrorLevel($"Failed {activity}: {Me.LastInterfaceErrorDetails}")
        End If
        Me.AlarmTypeWritten = value
        Me.OnTransactionEnded(Me.AlarmTypeSettlingTime)
        Return Me.AlarmTypeWritten
    End Function

#End Region

#Region " ANALOG INPUT "

    ''' <summary> The analog input read. </summary>
    Private _AnalogInputRead As Amount

    ''' <summary> Gets or sets the Analog Input. </summary>
    ''' <value> The Input Error read. </value>
    Public Property AnalogInputRead As Amount
        Get
            Return Me._AnalogInputRead
        End Get
        Protected Set(value As Amount)
            If value <> Me.AnalogInputRead Then
                Me._AnalogInputRead = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Analog Input register. </summary>
    ''' <value> The Analog Input register. </value>
    Public Property AnalogInputRegister As Integer = 360

    ''' <summary> Reads Analog Input. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The Analog Input. </returns>
    Public Function ReadAnalogInput() As Amount
        Me.ClearLastInterfaceError()
        Dim actualValue As Single = 0
        Dim activity As String = "reading analog input"
        If Me.ModbusClient.TryReadHoldingRegistersValue(Me.UnitId, CUShort(Me.AnalogInputRegister), actualValue) Then
            activity = "parse analog input"
            Me.AnalogInputRead = New Arebis.TypedUnits.Amount(CDbl(actualValue), Me.TemperatureUnit)
        End If
        Me.UpdateLastInterfaceError()
        If Me.LastInterfaceError = InterfaceError.NoError Then
            Me.PublishVerbose($"Success {activity}: {Me.ModbusClient.FormatRxPayload};. ")
        Else
            Me.PublishDefaultErrorLevel($"Failed {activity};. Details: {Me.LastInterfaceErrorDetails}")
        End If
        Me.OnTransactionEnded(Me.DefaultReadSettlingTimespan)
        Return Me.AnalogInputRead
    End Function

#End Region

#Region " SETPOINT "

    ''' <summary> The setpoint read. </summary>
    Private _SetpointRead As Amount

    ''' <summary> Gets or sets the Setpoint read. </summary>
    ''' <value> The Setpoint read. </value>
    Public Property SetpointRead As Amount
        Get
            Return Me._SetpointRead
        End Get
        Protected Set(value As Amount)
            If value <> Me.SetpointRead Then
                Me._SetpointRead = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The setpoint written. </summary>
    Private _SetpointWritten As Amount

    ''' <summary> Gets or sets the Setpoint Written. </summary>
    ''' <value> The Setpoint Written. </value>
    Public Property SetpointWritten As Amount
        Get
            Return Me._SetpointWritten
        End Get
        Protected Set(value As Amount)
            If value <> Me.SetpointWritten Then
                Me._SetpointWritten = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the Setpoint register. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The Setpoint register. </value>
    Public Property SetpointRegister As Integer = 2160

    ''' <summary> Applies the Setpoint described by value. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Setpoint. </returns>
    Public Function ApplySetpoint(ByVal value As Amount) As Amount
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Me.WriteSetpoint(value)
        If Me.LastInterfaceError = InterfaceError.NoError Then Me.ReadSetpoint()
        Return Me.SetpointRead
    End Function

    ''' <summary> Reads Setpoint. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> The Setpoint. </returns>
    Public Function ReadSetpoint() As Amount
        Me.ClearLastInterfaceError()
        Dim actualValue As Single = 0
        Dim activity As String = "reading setpoint"
        If Me.ModbusClient.TryReadHoldingRegistersValue(Me.UnitId, CUShort(Me.SetpointRegister), actualValue) Then
            activity = "parsing setpoint"
            Me.SetpointRead = New Arebis.TypedUnits.Amount(CDbl(actualValue), Me.TemperatureUnit)
        End If
        Me.UpdateLastInterfaceError()
        If Me.LastInterfaceError = InterfaceError.NoError Then
            Me.PublishVerbose($"Success {activity}: {Me.ModbusClient.FormatRxPayload};. ")
        Else
            Me.PublishDefaultErrorLevel($"Failed {activity};. Details: {Me.LastInterfaceErrorDetails}")
        End If
        Me.OnTransactionEnded(Me.DefaultReadSettlingTimespan)
        Return Me.SetpointRead
    End Function

    ''' <summary> Reads the setpoint. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function TryReadSetpoint() As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim activity As String = String.Empty
        Try
            activity = "reading setpoint"
            Me.ReadSetpoint()
            If Me.LastInterfaceError <> InterfaceError.NoError Then
                result = (False, $"Interface error '{Me.LastInterfaceError.Description}' reading setpoint")
            End If
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            result = (False, activity)
        End Try
        Return result
    End Function

    ''' <summary> Gets the setpoint settling time. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The time it takes the new setpoint to settle. </value>
    Public Property SetpointSettlingTime As TimeSpan = TimeSpan.FromMilliseconds(400)

    ''' <summary> Writes an Setpoint. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Setpoint. </returns>
    Public Function WriteSetpoint(ByVal value As Amount) As Amount
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Me.ClearLastInterfaceError()
        Me.ModbusClient.WriteRegistersValue(Me.UnitId, CUShort(Me.SetpointRegister), CSng(value.Value))
        Me.UpdateLastInterfaceError()
        Me.SetpointWritten = value
        Me.OnTransactionEnded(Me.SetpointSettlingTime)
        Return Me.SetpointWritten
    End Function

#End Region

#Region " MODBUS CLIENT UNITS "

    Private _TemperatureUnit As Unit

    ''' <summary> Defines the unit of temperature measurements. </summary>
    ''' <value> The unit of temperature measurements. </value>
    Public Property TemperatureUnit As Unit
        Get
            Return Me._TemperatureUnit
        End Get
        Set(value As Unit)
            If Not Unit.Equals(value, Me.TemperatureUnit) Then
                Me._TemperatureUnit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " HANDLER EXCEPTION HANDLER "

    ''' <summary> Executes the handler exception action. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="ex"> The ex. </param>
    Protected Overridable Sub OnHandlerException(ByVal ex As Exception)
        Me.SyncNotifyHandlerExceptionOccurred(New IO.ErrorEventArgs(ex))
    End Sub

    ''' <summary> Notifies of the HandlerExceptionOccurred event. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnHandlerExceptionOccurred(ByVal e As IO.ErrorEventArgs)
        Me.SyncNotifyHandlerExceptionOccurred(e)
    End Sub

    ''' <summary> Removes the HandlerExceptionOccurred event handlers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Sub RemoveHandlerExceptionOccurredEventHandlers()
        Me._HandlerExceptionOccurredEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The HandlerExceptionOccurred event handlers. </summary>
    Private ReadOnly _HandlerExceptionOccurredEventHandlers As New EventHandlerContextCollection(Of IO.ErrorEventArgs)

    ''' <summary> Event queue for all listeners interested in HandlerExceptionOccurred events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event HandlerExceptionOccurred As EventHandler(Of IO.ErrorEventArgs)
        AddHandler(value As EventHandler(Of IO.ErrorEventArgs))
            Me._HandlerExceptionOccurredEventHandlers.Add(New EventHandlerContext(Of IO.ErrorEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of IO.ErrorEventArgs))
            Me._HandlerExceptionOccurredEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As IO.ErrorEventArgs)
            Me._HandlerExceptionOccurredEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="HandlerExceptionOccurred">HandlerExceptionOccurred Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="e"> The <see cref="IO.ErrorEventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyHandlerExceptionOccurred(ByVal e As IO.ErrorEventArgs)
        Me._HandlerExceptionOccurredEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Asynchronous notify handler exception occurred. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="e"> Error event information. </param>
    Protected Sub AsyncNotifyHandlerExceptionOccurred(ByVal e As IO.ErrorEventArgs)
        Me._HandlerExceptionOccurredEventHandlers.Post(Me, e)
    End Sub

    ''' <summary> Unsafe notify handler exception occurred. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="e"> Error event information. </param>
    Protected Sub UnsafeNotifyHandlerExceptionOccurred(ByVal e As IO.ErrorEventArgs)
        Me._HandlerExceptionOccurredEventHandlers.UnsafeInvoke(Me, e)
    End Sub

#End Region

#Region " SEND/POST TESTERS "

    ''' <summary> The event time builder. </summary>
    Private _EventTimeBuilder As System.Text.StringBuilder

    ''' <summary> The event stopwatch. </summary>
    Private _EventStopwatch As Stopwatch

    ''' <summary> Synchronizes the notify handler exception. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Friend Sub SyncNotifyHandlerException()
        Dim ex As Exception = New HandlerFailedException
        Me._EventTimeBuilder = New System.Text.StringBuilder
        Me._EventStopwatch = Stopwatch.StartNew
        Me._EventTimeBuilder.AppendFormat($">> {Me._EventStopwatch.ElapsedTicks}")
        Me.SyncNotifyHandlerExceptionOccurred(New IO.ErrorEventArgs(ex))
    End Sub

    ''' <summary> Asynchronous notify handler exception. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Friend Sub AsyncNotifyHandlerException()
        Dim ex As Exception = New HandlerFailedException
        Me._EventTimeBuilder = New System.Text.StringBuilder
        Me._EventStopwatch = Stopwatch.StartNew
        Me._EventTimeBuilder.AppendFormat($">> {Me._EventStopwatch.ElapsedTicks}")
        Me.AsyncNotifyHandlerExceptionOccurred(New IO.ErrorEventArgs(ex))
    End Sub

    ''' <summary> Unsafe invoke handler exception. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Friend Sub UnsafeInvokeHandlerException()
        Dim ex As Exception = New HandlerFailedException
        Me._EventTimeBuilder = New System.Text.StringBuilder
        Me._EventStopwatch = Stopwatch.StartNew
        Me._EventTimeBuilder.AppendFormat($">> {Me._EventStopwatch.ElapsedTicks}")
        Me.UnsafeNotifyHandlerExceptionOccurred(New IO.ErrorEventArgs(ex))
    End Sub

    ''' <summary> Registers the handler exception occurred evetn. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Error event information. </param>
    Friend Sub RegisterHandlerExceptionOccurred(sender As Object, e As System.IO.ErrorEventArgs)
        Me._EventTimeBuilder.AppendFormat($" <<{Me._EventStopwatch.ElapsedTicks} ")
        Me.PublishInfo(Me._EventTimeBuilder.ToString)
        Threading.Thread.Sleep(10)
    End Sub

#End Region

#Region " SETPOINT INFO "

    ''' <summary> Gets or sets the target setpoint. </summary>
    ''' <value> Information describing the target setpoint. </value>
    Private Property TargetSetpointInfo As SetpointInfo

    ''' <summary> Gets information describing the active setpoint. </summary>
    ''' <value> Information describing the active setpoint. </value>
    Private Property ActiveSetpointInfo As SetpointInfo

    ''' <summary> Query if a new target setpoint temperature changed. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns>
    ''' <c>true</c> if a new target setpoint temperature is requested; otherwise <c>false</c>
    ''' </returns>
    Private Function IsTargetSetpointTemperatureChanged() As Boolean
        Return Me.TargetSetpointInfo IsNot Nothing AndAlso Me.ActiveSetpointInfo IsNot Nothing AndAlso
               Not Me.TargetSetpointInfo.SetpointApproximates(Me.ActiveSetpointInfo.SetpointTemperature)
    End Function

    Private ReadOnly _CandidateSetpoint As isr.Core.Constructs.ConcurrentToken(Of Double?)

    ''' <summary>
    ''' Gets or sets the candidate setpoint temperature.
    ''' </summary>
    ''' <value>
    ''' The candidate setpoint temperature.
    ''' </value>
    Public Property CandidateSetpointTemperature As Double?
        Get
            Return Me._CandidateSetpoint.Value
        End Get
        Set(value As Double?)
            If Me.CandidateSetpointTemperature <> value Then
                Me._CandidateSetpoint.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Query if a candidate setpoint temperature changed. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns>
    ''' <c>true</c> if a candidate setpoint temperature is requested; otherwise <c>false</c>
    ''' </returns>
    Private Function IsCandidateSetpointChanged() As Boolean
        Return Me.CandidateSetpointTemperature.HasValue AndAlso Me.ActiveSetpointInfo IsNot Nothing AndAlso
               Not Me.ActiveSetpointInfo.SetpointApproximates(Me.CandidateSetpointTemperature.Value)
    End Function

    ''' <summary> Query if a new target oven control mode is requested. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns>
    ''' <c>true</c> if a new target oven control mode is requested; otherwise <c>false</c>
    ''' </returns>
    Private Function IsTargetOvenControlModeChanged() As Boolean
        Return Me.TargetSetpointInfo IsNot Nothing AndAlso Me.ActiveSetpointInfo IsNot Nothing AndAlso
               Me.TargetSetpointInfo.OvenControlMode <> Me.ActiveSetpointInfo.OvenControlMode
    End Function

    Private ReadOnly _CandidateOvenControlMode As isr.Core.Constructs.ConcurrentToken(Of OvenControlMode?)

    ''' <summary>
    ''' Gets or sets the candidate <see cref="isr.Modbus.Watlow.OvenControlMode"/>.
    ''' </summary>
    ''' <value>
    ''' The candidate <see cref="isr.Modbus.Watlow.OvenControlMode"/>.
    ''' </value>
    Public Property CandidateOvenControlMode As OvenControlMode?
        Get
            Return Me._CandidateOvenControlMode.Value
        End Get
        Set(value As OvenControlMode?)
            If Not Nullable.Equals(Me.CandidateOvenControlMode, value) Then
                Me._CandidateOvenControlMode.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Query if a candidate oven control mode changed. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns>
    ''' <c>true</c> if a new candidate oven control mode is requested; otherwise <c>false</c>
    ''' </returns>
    Private Function IsCandidateOvenControlModeChanged() As Boolean
        Return Me.CandidateOvenControlMode.HasValue AndAlso Me.ActiveSetpointInfo IsNot Nothing AndAlso
               (Me.CandidateOvenControlMode.Value <> Me.ActiveSetpointInfo.OvenControlMode)
    End Function

#End Region

#Region " AUTOMATON "

    ''' <summary> Changes the <see cref="isr.Automata.Finite.Engines.SoakEngine"/> setpoint. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="setpointInfo"> Information describing the setpoint. </param>
    ''' <param name="restart">      True to restart the soak from the idle state. </param>
    Public Sub ChangeTargetAutomatonSetpoint(ByVal setpointInfo As SetpointInfo, ByVal restart As Boolean)
        If setpointInfo Is Nothing Then Throw New ArgumentNullException(NameOf(setpointInfo))
        If setpointInfo.OvenControlMode = OvenControlMode.None Then Throw New InvalidOperationException($"Oven control mode not set as '{setpointInfo.OvenControlMode}'")
        Me.PublishInfo($"changing setpoint to {setpointInfo.SetpointTemperature} degrees;. ")
        Me.TargetSetpointInfo = setpointInfo
        Me.CandidateSetpointTemperature = New Double?
        Me.CandidateOvenControlMode = New OvenControlMode?
        ' restart if requested or current temperature is outside the setpoint window.
        restart = restart OrElse Me.Temperature Is Nothing OrElse
            Not SetpointInfo.Approximates(Me.Temperature.Value, setpointInfo.SetpointTemperature, setpointInfo.Window)
        If restart Then Me.Engage(setpointInfo)
    End Sub

    ''' <summary> Activates the soak automaton described by setpointInfo. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="setpointInfo"> Information describing the setpoint. </param>
    Public Sub ActivateSoakAutomaton(ByVal setpointInfo As SetpointInfo)
        If setpointInfo Is Nothing Then Throw New ArgumentNullException(NameOf(setpointInfo))
        If setpointInfo.OvenControlMode = OvenControlMode.None Then Throw New InvalidOperationException($"Oven control mode not set as '{setpointInfo.OvenControlMode}'")
        ' clear the candidate settings
        Me.TargetSetpointInfo = Nothing
        Me.CandidateSetpointTemperature = New Double?
        Me.CandidateOvenControlMode = New OvenControlMode?
        ' clear to make sure this is read once.
        Me.InputErrorRead = InputError.None
        Me.Talker.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "starting soak state machine;. ")
        Me.Engage(setpointInfo)
        Me.TargetSetpointInfo = setpointInfo
        Me.PublishInfo($"Starting setpoint at {setpointInfo.SetpointTemperature} degrees;. ")
    End Sub

#End Region

#Region " SOAK ENGINE ACTIONS "

    ''' <summary> Gets or sets the temperature. </summary>
    ''' <value> The temperature. </value>
    Public ReadOnly Property Temperature As Amount

    ''' <summary> Reads the temperature. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function TryReadTemperature() As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim activity As String = String.Empty
        Try
            activity = $"reading analog input"
            Me.ReadAnalogInput()
            If Me.LastInterfaceError = InterfaceError.NoError Then
                If Me.AnalogInputRead Is Nothing Then
                    result = (False, $"Unknown error reading temperature")
                Else
                    Me._Temperature = New Arebis.TypedUnits.Amount(Me.AnalogInputRead)
                End If
            Else
                result = (False, $"Failed {activity}: {Me.LastInterfaceErrorDetails}")
            End If
            If Not result.Success Then Me.PublishDefaultErrorLevel(result.Details)
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            result = (False, activity)
        End Try
        Return result
    End Function

    ''' <summary> The sample lock. </summary>
    Private ReadOnly _SampleLock As New Object

    ''' <summary> Sample temperature. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function TrySampleTemperature() As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        SyncLock Me._SampleLock
            If Me.SoakEngine.IsSampleIntervalElapsed Then
                result = Me.TryReadTemperature()
                Me.SoakEngine.OnSampleHandled()
            End If
            Return result
        End SyncLock
    End Function

    ''' <summary> Reads the temperature. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function TryReadTemperatureAndError() As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim activity As String = String.Empty
        Try
            activity = $"reading analog input"
            Me.ReadAnalogInput()
            If Me.LastInterfaceError = InterfaceError.NoError Then
                activity = $"reading input error"
                Me.ReadInputError()
                If Me.InputErrorRead = InputError.NoError Then
                    Me._Temperature = New Arebis.TypedUnits.Amount(Me.AnalogInputRead)
                Else
                    result = (False, $"Failed {activity}: {Me.LastInterfaceErrorDetails}")
                End If
            Else
                result = (False, $"Failed {activity}: {Me.LastInterfaceErrorDetails}")
            End If
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            result = (False, activity)
        End Try
        Return result
    End Function

    ''' <summary> Query if this object is setpoint ready. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if setpoint ready; otherwise <c>false</c> </returns>
    Public Function IsSetpointReady() As Boolean
        Return Me.ActiveSetpointInfo.IsAtSetpoint(Me.SetpointRead)
    End Function

    ''' <summary>
    ''' Query if this oven is ready, that is off, quite and alarm type off or on and alarm type
    ''' process.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if oven ready; otherwise <c>false</c> </returns>
    Public Function IsOvenReady() As Boolean
        Return Me.ActiveSetpointInfo.OvenFunctionInfo.IsOff OrElse
            (Me.ActiveSetpointInfo.OvenFunctionInfo.IsQuiet AndAlso Me.AlarmTypeRead = AlarmType.Off) OrElse
            (Me.ActiveSetpointInfo.OvenFunctionInfo.IsOn AndAlso Me.AlarmTypeRead = AlarmType.Process)
    End Function

    ''' <summary> Updates the setpoint. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="stateIdentity">      The state identity. </param>
    ''' <param name="setpointTemperature"> The setpoint temperature in the <see cref="TemperatureUnit"/>. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Function UpdateSetpoint(ByVal stateIdentity As SoakState, ByVal setpointTemperature As Double) As (Success As Boolean, TryAgain As Boolean, Details As String)
        Dim result As (Success As Boolean, TryAgain As Boolean, Details As String) = (True, False, String.Empty)
        Dim activity As String = String.Empty
        Try
            ' t = T
            If Me.SetpointWritten Is Nothing OrElse Not Me.ActiveSetpointInfo.SetpointApproximates(Me.SetpointWritten.Value, setpointTemperature) Then
                ' setpoint not set or out of range, write a new value
                activity = $"Setting setpoint {setpointTemperature:G4)} at '{stateIdentity}' state" : Me.PublishVerbose($"{activity };. ")
                Me.WriteSetpoint(New Arebis.TypedUnits.Amount(CDbl(setpointTemperature), Me.TemperatureUnit))
            Else
                ' t = 2 * T
                ' if setpoint was written, read setpoint status to validate
                activity = $"reading setpoint at '{stateIdentity}' state" : Me.PublishVerbose($"{activity };. ")
                Me.ReadSetpoint()
                If Me.LastInterfaceError = InterfaceError.NoError Then
                    ' If value read without error, check if in range.
                    If Me.SetpointRead Is Nothing Then
                        ' if nothing, try treading once again on the next cycle.
                        result = If(Me.LastInterfaceErrorContinuousCount < Me.ContinuousFailureCountTryAgainLimit,
                                        (False, True, $"Failed {activity}; trying again"),
                                        (False, False, $"Failed {activity}; aborting"))
                        Me.PublishInfo($"{result.Details};. ")
                    ElseIf Me.ActiveSetpointInfo.SetpointApproximates(Me.SetpointRead.Value, setpointTemperature) Then
                        result = (True, False, $"Setpoint set at '{stateIdentity}' state") : Me.PublishVerbose($"{result.Details};. ")
                        If Me.IsTargetSetpointTemperatureChanged() Then
                            ' if a new target setpoint is requested, update the setpoint thus clearing the request
                            Me.ActiveSetpointInfo.SetpointTemperature = setpointTemperature
                        End If
                        If Me.CandidateSetpointTemperature.HasValue AndAlso Me.ActiveSetpointInfo.SetpointApproximates(Me.CandidateSetpointTemperature.Value) Then
                            ' if the candidate temperature is already accomplished, clear its value
                            Me.CandidateSetpointTemperature = New Double?
                        End If
                    Else
                        ' if value is incorrect, must do a repeat write
                        result = (False, True, $"Incorrect value. Clearing written setpoint at '{stateIdentity}' state to force a new write")
                        Me.PublishDefaultErrorLevel($"{result.Details};. ")
                        Me._SetpointWritten = Nothing
                    End If
                Else
                    ' if error, read again after the next sample interval.
                    result = If(Me.LastInterfaceErrorContinuousCount < Me.ContinuousFailureCountTryAgainLimit,
                                    (False, True, $"Failed {activity}: {Me.LastInterfaceErrorDetails}; trying again"),
                                    (False, False, $"Failed {activity}: {Me.LastInterfaceErrorDetails}; aborting"))
                    Me.PublishDefaultErrorLevel($"{result.Details};. ")
                End If
            End If
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            result = (False, True, activity)
        End Try
        Return result
    End Function

    ''' <summary> Reads input error. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="stateIdentity"> The state identity. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Function ReadInputError(ByVal stateIdentity As SoakState) As (Success As Boolean, TryAgain As Boolean, Details As String)
        Dim activity As String = String.Empty
        Dim result As (Success As Boolean, TryAgain As Boolean, Details As String)
        Try
            ' if starting, input error is none. So read the input error to determine the status of the sensors.
            activity = $"reading input error at '{stateIdentity}' state" : Me.PublishVerbose($"{activity};. ")
            Me.ReadInputError()
            If Me.InputErrorRead = InputError.None Then
                If Me.LastInterfaceError = InterfaceError.NoError Then
                    activity = $"Failed {activity} without interface error"
                    result = (False, True, activity)
                Else
                    result = If(Me.LastInterfaceErrorContinuousCount < Me.ContinuousFailureCountTryAgainLimit,
                                    (False, True, $"Failed {activity}: {Me.LastInterfaceErrorDetails}; trying again"),
                                    (False, False, $"Failed {activity}: {Me.LastInterfaceErrorDetails}; aborting"))
                End If
                Me.PublishDefaultErrorLevel($"{result.Details};. ")
            ElseIf Me.InputErrorRead <> InputError.NoError Then
                activity = $"failed {activity} with input error '{Me.InputErrorRead.Description}'"
                result = If(Me.LastInterfaceErrorContinuousCount < Me.ContinuousFailureCountTryAgainLimit,
                                    (False, True, $"{activity}; trying again"),
                                    (False, False, $"{activity}; aborting"))
                Me.PublishDefaultErrorLevel($"{result.Details};. ")
            Else
                activity = $"Success {activity}: '{Me.InputErrorRead.Description}'"
                Me.PublishVerbose($"{activity};. ")
                result = (True, False, activity)
            End If
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            result = (False, True, activity)
        End Try
        Return result
    End Function

    Private _OvenReadyContinuousFailureCount As Integer

    ''' <summary> Gets or sets the number of oven ready continuous failures. </summary>
    ''' <value> The number of oven ready continuous failures. </value>
    Public Property OvenReadyContinuousFailureCount As Integer
        Get
            Return Me._OvenReadyContinuousFailureCount
        End Get
        Set(value As Integer)
            If value <> Me.OvenReadyContinuousFailureCount Then
                Me._OvenReadyContinuousFailureCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Turn alarm off. </summary>
    ''' <remarks>
    ''' David, 2020-11-11. <para>
    ''' The alarm signal is used to toggle the oven state on or off depending on the oven function.
    ''' <see cref="IsOvenReady()"/> indicate if the function was a success.
    ''' </para>
    ''' </remarks>
    ''' <param name="stateIdentity"> The state identity. </param>
    ''' <returns> A SoakAutomatonSymbol? </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Function TurnAlarmOff(ByVal stateIdentity As SoakState) As (Success As Boolean, TryAgain As Boolean, Details As String)
        Dim result As (Success As Boolean, TryAgain As Boolean, Details As String)
        Dim activity As String = String.Empty
        Try
            If Me.AlarmTypeWritten = AlarmType.Off Then

                ' if process was set to the controller, read alarm type to validate.
                activity = $"reading alarm type at '{stateIdentity}' state" : Me.PublishVerbose($"{activity};. ")
                If Me.AlarmTypeRead = AlarmType.Off Then
                    ' T = 6 * T
                    ' if oven is quiet, allow moving on
                    If Me.IsOvenReady Then
                        activity = $"Oven control mode {Me.ActiveSetpointInfo.OvenControlMode} allows skipping turning oven on at '{stateIdentity}' state"
                        Me.PublishVerbose($"{activity};. ")
                        result = (True, False, activity)
                    Else
                        activity = $"Oven control mode {Me.ActiveSetpointInfo.OvenControlMode}. Oven not ready at '{stateIdentity}' state"
                        Me.PublishInfo($"{activity};. ")
                        result = (False, True, activity)
                    End If

                Else

                    ' t = 5 * T
                    Me.ReadAlarmType()
                    If Me.LastInterfaceError = InterfaceError.NoError Then
                        If Me.ReadAlarmType() = AlarmType.Off Then
                            ' if oven is off, allow moving on the next cycle.
                            If Me.IsOvenReady Then
                                activity = $"Oven control mode {Me.ActiveSetpointInfo.OvenControlMode} is as expected at '{stateIdentity}' state"
                                Me.PublishVerbose($"{activity};. ")
                                result = (True, False, activity)
                            Else
                                activity = $"Oven control mode {Me.ActiveSetpointInfo.OvenControlMode} not expected. Oven not ready at '{stateIdentity}' state"
                                Me.PublishDefaultErrorLevel($"{activity};. ")
                                result = (False, True, activity)
                            End If
                        Else
                            activity = $"Failed setting alarm type to {AlarmType.Off} at '{stateIdentity}' state. trying again."
                            Me.PublishDefaultErrorLevel($"{activity};. ")
                            result = (False, True, activity)
                            ' if not set, turn off the previous value to force write on the next machine cycle
                            Me._AlarmTypeWritten = AlarmType.None
                        End If
                    Else
                        activity = $"Failed {activity}: {Me.LastInterfaceErrorDetails}"
                        result = If(Me.LastInterfaceErrorContinuousCount < Me.ContinuousFailureCountTryAgainLimit,
                                    (False, True, $"{activity}; trying again"),
                                    (False, False, $"{activity}; aborting"))
                        Me.PublishDefaultErrorLevel($"{result.Details};. ")
                    End If
                End If
                ' clear the requesting messages.
                If result.Success AndAlso Me.IsTargetOvenControlModeChanged Then
                    ' if the new target was accomplished, update the control mode.
                    Me.ActiveSetpointInfo.OvenControlMode = Me.TargetSetpointInfo.OvenControlMode
                End If
                ' clear any candidate control mode
                Me.CandidateOvenControlMode = New OvenControlMode?
                Me.OvenReadyContinuousFailureCount = If(result.Success, 0, Me.OvenReadyContinuousFailureCount + 1)
            Else
                activity = $"Setting alarm type to '{AlarmType.Off.Description}' at '{stateIdentity}' state"
                Me.PublishVerbose($"{activity};. ")
                Me.WriteAlarmType(AlarmType.Off)
                If Me.LastInterfaceError = InterfaceError.NoError Then
                    activity = $"Success {activity}"
                    result = (True, False, activity)
                    Me.PublishVerbose($"{activity};. ")
                Else
                    activity = $"Failed {activity}: {Me.LastInterfaceErrorDetails}"
                    result = If(Me.LastInterfaceErrorContinuousCount < Me.ContinuousFailureCountTryAgainLimit,
                                    (False, True, $"{activity}; trying again"),
                                    (False, False, $"{activity}; aborting"))
                    Me.PublishDefaultErrorLevel($"{result.Details};. ")
                End If
            End If
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            result = (False, True, activity)
        End Try
        Return result
    End Function

    ''' <summary> Turn alarm on. </summary>
    ''' <remarks>
    ''' David, 2020-11-11. <para>
    ''' The alarm signal is used to toggle the oven state on or off depending on the oven function.
    ''' <see cref="IsOvenReady()"/> indicate if the function was a success.
    ''' </para>
    ''' </remarks>
    ''' <param name="stateIdentity"> The state identity. </param>
    ''' <returns> A SoakAutomatonSymbol? </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Function TurnAlarmOn(ByVal stateIdentity As SoakState) As (Success As Boolean, TryAgain As Boolean, Details As String)
        Dim result As (Success As Boolean, TryAgain As Boolean, Details As String)
        Dim activity As String = String.Empty
        Try
            If Me.AlarmTypeWritten = AlarmType.Process Then
                ' t = 5 * T
                ' if process was set to the controller, read alarm type to validate.
                activity = $"reading written {AlarmType.Process} alarm type at '{stateIdentity}' state" : Me.PublishVerbose($"{activity};. ")
                If Me.AlarmTypeRead = AlarmType.Process Then
                    ' t = 6 * T
                    ' if alarm type set, try to move to the next state
                    If Me.IsOvenReady Then
                        activity = $"Oven control mode {Me.ActiveSetpointInfo.OvenControlMode} is as expected at '{stateIdentity}' state"
                        Me.PublishVerbose($"{activity};. ")
                        result = (True, False, activity)
                    Else
                        activity = $"Oven control mode {Me.ActiveSetpointInfo.OvenControlMode} but oven is not ready at '{stateIdentity}' state"
                        Me.PublishInfo($"{activity};. ")
                        result = (False, True, activity)
                    End If
                Else
                    Me.ReadAlarmType()
                    If Me.LastInterfaceError = InterfaceError.NoError Then
                        If Me.AlarmTypeRead() = AlarmType.Process Then
                            ' if alarm type set, try to move to the next state
                            If Me.IsOvenReady Then
                                activity = $"Oven control mode {Me.ActiveSetpointInfo.OvenControlMode} is as expected at '{stateIdentity}' state"
                                Me.PublishVerbose($"{activity};. ")
                                result = (True, False, activity)
                            Else
                                activity = $"Oven control mode {Me.ActiveSetpointInfo.OvenControlMode} not expected. Oven not ready at '{stateIdentity}' state"
                                Me.PublishDefaultErrorLevel($"{activity};. ")
                                result = (False, True, activity)
                            End If
                        Else
                            activity = $"Failed {activity}; trying again."
                            result = (False, True, activity)
                            Me.PublishDefaultErrorLevel($"{activity};. ")
                            ' if not set, turn off the previous value to force write on the next machine cycle
                            Me._AlarmTypeWritten = AlarmType.None
                        End If
                    Else
                        activity = $"failed {activity}: {Me.LastInterfaceErrorDetails}"
                        result = If(Me.LastInterfaceErrorContinuousCount < Me.ContinuousFailureCountTryAgainLimit,
                                    (False, True, $"{activity}; trying again"),
                                    (False, False, $"{activity}; aborting"))
                        Me.PublishDefaultErrorLevel($"{result.Details};. ")
                    End If
                End If
                ' clear the requesting messages.
                If result.Success AndAlso Me.IsTargetOvenControlModeChanged Then
                    ' if the new target was accomplished, update the control mode.
                    Me.ActiveSetpointInfo.OvenControlMode = Me.TargetSetpointInfo.OvenControlMode
                End If
                ' clear any candidate control mode
                Me.CandidateOvenControlMode = New OvenControlMode?
                Me.OvenReadyContinuousFailureCount = If(result.Success, 0, Me.OvenReadyContinuousFailureCount + 1)
            Else
                activity = $"Setting alarm type to '{AlarmType.Process.Description}' at '{stateIdentity}' state"
                Me.PublishVerbose($"{activity};. ")
                Me.WriteAlarmType(AlarmType.Process)
                If Me.LastInterfaceError = InterfaceError.NoError Then
                    activity = $"Success {activity}"
                    result = (True, False, activity)
                Else
                    activity = $"Failed {activity}: {Me.LastInterfaceErrorDetails}"
                    result = If(Me.LastInterfaceErrorContinuousCount < Me.ContinuousFailureCountTryAgainLimit,
                                    (False, True, $"{activity}; trying again"),
                                    (False, False, $"{activity}; aborting"))
                    Me.PublishDefaultErrorLevel($"{result.Details};. ")
                End If
            End If
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            result = (False, True, activity)
        End Try
        Return result
    End Function

    ''' <summary> Updates the oven control mode described by sender. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="stateIdentity"> The state identity. </param>
    ''' <param name="functionInfo">  Information describing the function. </param>
    ''' <returns> A SoakAutomatonSymbol? </returns>
    Private Function UpdateOvenControlMode(ByVal stateIdentity As SoakState, ByVal functionInfo As OvenFunctionInfo) As (Success As Boolean, TryAgain As Boolean, Details As String)
        Dim result As (Success As Boolean, TryAgain As Boolean, Details As String) = (True, False, String.Empty)
        If functionInfo Is Nothing Then Throw New ArgumentNullException(NameOf(functionInfo))

        If Me.InputErrorRead = InputError.None Then

            result = Me.ReadInputError(stateIdentity)

        ElseIf functionInfo.IsQuiet Then

            ' oven monitoring is on but fans are off.
            ' t = 4 * T
            result = Me.TurnAlarmOff(stateIdentity)

            If Me.CandidateOvenControlMode.HasValue AndAlso New OvenFunctionInfo(Me.CandidateOvenControlMode.Value).IsQuiet Then
                ' if the interim request already accomplished, clear the request
                Me.CandidateOvenControlMode = New OvenControlMode?
                ' wrong candidate? Also, already done in the function? 
                ' Me.CandidateSetpointTemperature = New Double?
            End If

            ' t = 6 * T or 5 * T

        ElseIf functionInfo.IsOn Then

            ' t = 4 * T
            result = Me.TurnAlarmOn(stateIdentity)

            If Me.CandidateOvenControlMode.HasValue AndAlso New OvenFunctionInfo(Me.CandidateOvenControlMode.Value).IsOn Then
                ' if the interim request already accomplished, clear the request
                Me.CandidateOvenControlMode = New OvenControlMode?
                ' wrong candidate? Also, already done in the function? 
                ' Me.CandidateSetpointTemperature = New Double?
            End If
            ' t = 6 * T or 5 * T

        Else
            Debug.Assert(Not Debugger.IsAttached, $"Unhandled case in Soak Automation {stateIdentity} state; Oven function is neither on or off")
        End If
        Return result

    End Function

    ''' <summary> Updates the setpoint information. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Private Sub UpdateSetpointInfo()
        If Me.TargetSetpointInfo Is Nothing Then
            ' if no target, move on.
        ElseIf Me.ActiveSetpointInfo Is Nothing Then
            Me.ActiveSetpointInfo = New SetpointInfo(Me.TargetSetpointInfo)
            Me.TargetSetpointInfo = Nothing
        ElseIf Me.ActiveSetpointInfo.Equals(Me.TargetSetpointInfo) Then
            ' if all changes were applied, clear the target setpoint
            Me.TargetSetpointInfo = Nothing
        Else
            ' if the setpoint needs to be set, apply the immediate changes
            Me.ActiveSetpointInfo.Window = Me.TargetSetpointInfo.Window
            Me.ActiveSetpointInfo.RampTimeout = Me.TargetSetpointInfo.RampTimeout
            Me.ActiveSetpointInfo.Hysteresis = Me.TargetSetpointInfo.Hysteresis
            Me.ActiveSetpointInfo.SampleInterval = Me.TargetSetpointInfo.SampleInterval
            Me.ActiveSetpointInfo.SoakDuration = Me.TargetSetpointInfo.SoakDuration
            Me.ActiveSetpointInfo.SoakResetDelay = Me.TargetSetpointInfo.SoakResetDelay
            Me.ActiveSetpointInfo.SetpointTemperature = Me.TargetSetpointInfo.SetpointTemperature
            Me.ActiveSetpointInfo.OvenControlMode = Me.TargetSetpointInfo.OvenControlMode
        End If
        Me.UpdateSoakEngineInfo(Me.ActiveSetpointInfo)
    End Sub

    ''' <summary> Has oven control mode candidate. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <returns> A (Affirmative As Boolean, functionInfo As OvenFunctionInfo) </returns>
    Private Function HasOvenControlModeCandidate() As (Affirmative As Boolean, OvenControlMode As OvenControlMode)
        If Me.IsTargetOvenControlModeChanged Then
            Return (True, Me.TargetSetpointInfo.OvenControlMode)
        ElseIf Me.IsCandidateOvenControlModeChanged Then
            Return (True, Me.CandidateOvenControlMode.Value)
        Else
            Return (False, Me.ActiveSetpointInfo.OvenControlMode)
        End If
    End Function

    ''' <summary> Updates the oven control mode described by sender. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <param name="stateIdentity"> The state identity. </param>
    ''' <returns> A SoakAutomatonSymbol? </returns>
    Private Function UpdateOvenControlMode(ByVal stateIdentity As SoakState) As (Success As Boolean, TryAgain As Boolean, Details As String)
        Me.ActiveSetpointInfo.OvenControlMode = Me.HasOvenControlModeCandidate.OvenControlMode
        Return Me.UpdateOvenControlMode(stateIdentity, Me.ActiveSetpointInfo.OvenFunctionInfo)
    End Function

    ''' <summary> Query if this object has target temperature candidate. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <returns> True if target temperature candidate, false if not. </returns>
    Private Function HasTargetTemperatureCandidate() As (Affirmative As Boolean, Value As Double)
        If Me.IsTargetSetpointTemperatureChanged Then
            Return (True, Me.TargetSetpointInfo.SetpointTemperature)
        ElseIf Me.IsCandidateSetpointChanged Then
            Return (True, Me.CandidateSetpointTemperature.Value)
        Else
            Return (False, Me.ActiveSetpointInfo.SetpointTemperature)
        End If
    End Function

    ''' <summary> Updates the target temperature described by stateIdentity. </summary>
    ''' <remarks> David, 2020-11-25. </remarks>
    ''' <param name="stateIdentity"> The state identity. </param>
    ''' <returns> The (Success As Boolean, TryAgain As Boolean, Details As String) </returns>
    Private Function UpdateSetpoint(ByVal stateIdentity As SoakState) As (Success As Boolean, TryAgain As Boolean, Details As String)
        Me.ActiveSetpointInfo.SetpointTemperature = Me.HasTargetTemperatureCandidate.Value
        Return If(Me.ActiveSetpointInfo.IsAtSetpoint(Me.SetpointRead),
                    (True, False, "setpoint already set"),
                    Me.UpdateSetpoint(stateIdentity, Me.ActiveSetpointInfo.SetpointTemperature))
    End Function

    ''' <summary> Pause soak automaton. </summary>
    ''' <remarks> David, 2020-11-24. </remarks>
    Public Sub PauseSoakAutomaton()
        Me.SoakEngine.Pause()
    End Sub

    ''' <summary> Resume soak automaton. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Sub ResumeSoakAutomaton()
        Me.SoakEngine.Resume()
    End Sub

#End Region

#Region " SOAK ENGINE "

    ''' <summary> Gets or sets the Soak engine. </summary>
    ''' <value> The Soak engine. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SoakEngine As isr.Automata.Finite.Engines.SoakEngine

    ''' <summary> Assign Soak Engine. </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignSoakEngine(ByVal value As isr.Automata.Finite.Engines.SoakEngine)
        If Me.SoakEngine IsNot Nothing Then
            Me.SoakEngine.UnregisterEngineFailureAction(Sub(e) Me.TriggerSoakFailed(e))
            If Me.SoakEngine?.IsActive Then
                Me.SoakEngine.StopRequested = True
                isr.Core.ApplianceBase.DoEventsWaitUntil(TimeSpan.FromMilliseconds(100), Function() Not Me.SoakEngine.IsActive)
            End If
            Me.SoakEngine?.Dispose()
            Me._SoakEngine = Nothing
        End If
        Me._SoakEngine = value
        If value IsNot Nothing Then
            Me.SoakEngine.RegisterEngineFailureAction(Sub(e) Me.TriggerSoakFailed(e))
            Me.SoakEngine.UsingFireAsync = True
            Me.SoakEngine.StateMachine.OnTransitionedAsync(Function(t As StateMachine(Of SoakState, SoakTrigger).Transition)
                                                               Return Task.Run(Sub() Me.OnTransitioned(t))
                                                           End Function)
        End If
    End Sub

    ''' <summary> Handles the state entry actions. </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="transition"> The transition. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnTransitioned(ByVal transition As StateMachine(Of SoakState, SoakTrigger).Transition)
        Dim activity As String = String.Empty
        Try
            If transition Is Nothing Then
                activity = Me.PublishVerbose($"Entering the {Me.SoakEngine.StateMachine.State} state")
                Me.OnActivated(Me.SoakEngine.StateMachine.State)
            Else
                activity = Me.PublishVerbose($"transitioning {Me.SoakEngine.Name}: {transition.Source} --> {transition.Destination}")
                If transition.IsReentry Then
                    Me.OnReentry(transition.Destination)
                Else
                    Me.OnExit(transition.Source)
                    Me.OnActivated(transition.Destination)
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the 'exit' action. </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="source"> Source for the. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub OnExit(ByVal source As SoakState)
        Select Case source
            Case SoakState.Soak
        End Select
    End Sub

    ''' <summary>
    ''' Executes the 'reentry' action. Is used to update the setpoint information and take new
    ''' readings.
    ''' </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="destination"> Destination for the. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub OnReentry(ByVal destination As SoakState)
        Dim activity As String = $"{Me.SoakEngine.Name} reentered {destination.Description} state"
        Dim result As (Success As Boolean, TryAgain As Boolean, Details As String)
        Try
            ' update the setpoint info
            Me.UpdateSetpointInfo()
            ' If Me.TargetSetpointInfo Is Nothing Then Return
            Select Case destination
                Case SoakState.Idle
                    If Me.SoakEngine.IsSampleIntervalElapsed Then
                        Me.SoakEngine.OnSampleHandled()
                    End If
                Case SoakState.Engaged
                    If Me.SoakEngine.IsSampleIntervalElapsed Then
                        Me.SoakEngine.OnSampleHandled()
                        ' update setpoint if changed
                        result = If(Me.ActiveSetpointInfo.IsAtSetpoint(Me.SetpointRead), (True, False, String.Empty),
                                            Me.UpdateSetpoint(destination))
                        If result.Success AndAlso Me.ActiveSetpointInfo.IsAtSetpoint(Me.SetpointRead) Then
                            Me.SoakEngine.ApplySetpoint(Me.SetpointRead.Value)
                        ElseIf Not (result.Success OrElse result.TryAgain) Then
                            Me.SoakEngine.OnEngineFailed(New OperationFailedException($"{result.Details} @{Environment.StackTrace()}"))
                        End If
                    End If
                Case SoakState.HasSetpoint
                    If Me.SoakEngine.IsSampleIntervalElapsed Then
                        Me.SoakEngine.OnSampleHandled()
                        result = If(Me.IsOvenReady(), (True, False, String.Empty), Me.UpdateOvenControlMode(destination))
                        If result.Success AndAlso Me.IsOvenReady Then
                            Me.SoakEngine.Fire(SoakTrigger.Ramp)
                        ElseIf Not (result.Success OrElse result.TryAgain) Then
                            Me.SoakEngine.OnEngineFailed(New OperationFailedException($"{result.Details} @{Environment.StackTrace()}")) '@{NameOf(EasyZone).Length{NameOf(EasyZone.OnReentry)}"))
                        End If
                    End If
                Case SoakState.Ramp, SoakState.Soak, SoakState.OffSoak, SoakState.AtTemp, SoakState.OffTemp
                    If Me.SoakEngine.IsSampleIntervalElapsed Then
                        Me.SoakEngine.OnSampleHandled()
                        If Me.HasTargetTemperatureCandidate.Affirmative Then
                            result = Me.UpdateSetpoint(destination)
                            If Not (result.Success OrElse result.TryAgain) Then
                                Me.SoakEngine.OnEngineFailed(New OperationFailedException($"{result.Details} @{Environment.StackTrace()}"))
                            End If
                        ElseIf Me.TryReadTemperature.Success Then
                            Me.SoakEngine.ApplyTemperature(Me.Temperature.Value)
                        End If
                    End If
            End Select
        Catch ex As Exception
            ex.Data.Add($"info {ex.Data?.Count}", $"Exception {activity}")
            Me.OnHandlerException(ex)
            Me.PublishException(activity, ex)
        Finally
            ' The process timer must be resumed because it is paused for handling the process. 
            Me.SoakEngine.Resume()
        End Try
    End Sub

    ''' <summary> Executes the 'activated' action. </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="destination"> Destination for the. </param>
    Private Sub OnActivated(ByVal destination As SoakState)
        Dim activity As String = String.Empty
        Select Case destination
            Case SoakState.Idle
                Me._Temperature = Nothing
                Me.AnalogInputRead = Nothing
                Me.SetpointRead = Nothing
                Me.SetpointWritten = Nothing
                Me.AlarmTypeRead = AlarmType.None
                Me.AlarmTypeWritten = AlarmType.None
                activity = "Soak automaton inactive"
            Case SoakState.Engaged
                activity = "Soak automaton started"
            Case SoakState.HasSetpoint
                activity = $"Setpoint @{Me.SetpointRead}"
            Case SoakState.Ramp
                activity = $"Ramping started; Oven control @{Me.ActiveSetpointInfo.OvenControlMode}"
            Case SoakState.Soak
                activity = "Soaking started"
            Case SoakState.OffSoak
                activity = "Soaking ended"
            Case SoakState.AtTemp
                activity = $"Soak temp is {Me.Temperature:G4)}"
            Case SoakState.OffTemp
                activity = $"Soak temp is {Me.Temperature:G4)}"
            Case SoakState.Terminal
                activity = "Soak automaton stopped"
        End Select
        Me.PublishInfo($"{activity} at the '{destination}' state;. ")
        If Me.SoakEngine.StopRequested Then Me.SoakEngine.ProcessStopRequest()
    End Sub

    ''' <summary> Trigger Soak failed. </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="e"> Error event information. </param>
    Private Sub TriggerSoakFailed(e As System.IO.ErrorEventArgs)
        If e Is Nothing Then
            Me.PublishWarning($"{Me.SoakEngine.Name} failed;. {Me.SoakEngine.StateMachineInfo}")
        Else
            Me.PublishException($"{Me.SoakEngine.Name} failed;. {Me.SoakEngine.StateMachineInfo}", e.GetException)
        End If
        Me.SoakEngine.Terminate()
    End Sub

#Region " COMMANDS "

    ''' <summary> Engages the Soak engine using the given setpoint information. </summary>
    ''' <remarks> David, 2020-11-24. </remarks>
    ''' <param name="setpointInfo"> Information describing the setpoint. </param>
    Public Sub Engage(ByVal setpointInfo As SetpointInfo)
        Me.UpdateSoakEngineInfo(setpointInfo)
        Me.SoakEngine.Engage(setpointInfo.TemperatureSampleInterval)
    End Sub

    ''' <summary> Updates the soak engine information described by setpointInfo. </summary>
    ''' <remarks> David, 2020-11-24. </remarks>
    ''' <param name="setpointInfo"> Information describing the setpoint. </param>
    Public Sub UpdateSoakEngineInfo(ByVal setpointInfo As SetpointInfo)
        Me.ActiveSetpointInfo = New SetpointInfo(setpointInfo)
        Me.SoakEngine.Window = setpointInfo.Window
        Me.SoakEngine.TemperatureResolution = setpointInfo.SetpointEpsilon
        Me.SoakEngine.RampTimeout = setpointInfo.RampTimeout
        Me.SoakEngine.Hysteresis = setpointInfo.Hysteresis
        Me.SoakEngine.SampleInterval = setpointInfo.SampleInterval
        Me.SoakEngine.SoakDuration = setpointInfo.SoakDuration
        Me.SoakEngine.AllowedOffStateDuration = setpointInfo.SoakResetDelay
        Me.SoakEngine.AllowedOffStateCount = 0
        Me.SoakEngine.SoakCount = 0
    End Sub

#End Region


#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the
    ''' message.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return MyBase.Publish(New TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

    ''' <summary> Publish default. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishDefaultErrorLevel(ByVal activity As String) As String
        Return MyBase.Publish(New TraceMessage(Me.ModbusErrorTraceEventType, My.MyLibrary.TraceEventId, activity))
    End Function


#End Region

End Class

''' <summary> Values that represent alarm type. </summary>
''' <remarks> David, 2020-11-11. </remarks>
Public Enum AlarmType

    <Description("Not specified")>
    None = 0

    <Description("Off")>
    Off = 62

    <Description("Process")>
    Process = 76

    <Description("Deviation")>
    Deviation = 24
End Enum

''' <summary> Values that represent input errors. </summary>
''' <remarks> David, 2020-11-11. </remarks>
Public Enum InputError

    <Description("Not specified")>
    None = 0

    <Description("No Error")>
    NoError = 61

    <Description("Open")>
    Open = 65

    <Description("Fail")>
    Fail = 32

    <Description("Shorted")>
    Shorted = 127

    <Description("Measurement Error")>
    MeasurementError = 140

    <Description("Bad Calibration Data")>
    BadCalibrationData = 139

    <Description("Ambient Error")>
    AmbientError = 9

    <Description("RTD Error")>
    RtdError = 141

    <Description("Not Sourced")>
    NotSourced = 246
End Enum

