Imports isr.Modbus.Watlow
Imports isr.Core.TimeSpanExtensions

''' <summary> Information about the setpoint. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-8-22 </para>
''' </remarks>
Public Class SetpointInfo

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.SetpointEpsilon = 0.05
        Me.TemperatureUnit = Arebis.StandardUnits.TemperatureUnits.DegreeFahrenheit
    End Sub

    ''' <summary> The copy constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As SetpointInfo)
        Me.New()
        If value IsNot Nothing Then
            Me.TemperatureUnit = value.TemperatureUnit
            Me.SetpointEpsilon = value.SetpointEpsilon
            Me.SetpointTemperature = value.SetpointTemperature
            Me.Window = value.Window
            Me.RampTimeout = value.RampTimeout
            Me.OvenControlMode = value.OvenControlMode
            Me.Hysteresis = value.Hysteresis
            Me.SampleInterval = value.SampleInterval
            Me.SoakDuration = value.SoakDuration
            Me.SoakResetDelay = value.SoakResetDelay
        End If
    End Sub

    ''' <summary> The constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="temperatureUnit">     The temperature unit. </param>
    ''' <param name="setpointTemperature"> The setpoint temperature in the
    '''                                    <paramref name="temperatureUnit"/> . </param>
    ''' <param name="sampleInterval">      The sample interval. </param>
    ''' <param name="soakDuration">        The soak duration. </param>
    ''' <param name="window">              The window. </param>
    ''' <param name="hysteresis">          The hysteresis. </param>
    ''' <param name="soakResetDelay">      The soak reset delay. </param>
    ''' <param name="ovenControlMode">     The oven control mode. </param>
    ''' <param name="rampTimeout">         The ramp timeout. </param>
    Public Sub New(ByVal temperatureUnit As Arebis.TypedUnits.Unit, ByVal setpointTemperature As Double,
                   ByVal sampleInterval As TimeSpan, ByVal soakDuration As TimeSpan,
                   ByVal window As Double, ByVal hysteresis As Double, ByVal soakResetDelay As TimeSpan,
                   ByVal ovenControlMode As OvenControlMode, ByVal rampTimeout As TimeSpan)
        Me.New()
        Me.TemperatureUnit = temperatureUnit
        Me.SetpointTemperature = setpointTemperature
        Me.RampTimeout = rampTimeout
        Me.OvenControlMode = ovenControlMode
        Me.SampleInterval = sampleInterval
        Me.SoakDuration = soakDuration
        Me.Window = window
        Me.Hysteresis = hysteresis
        Me.SoakResetDelay = soakResetDelay
    End Sub

    ''' <summary> Tests if this SetpointInfo is considered equal to another. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="other"> The setpoint information to compare to this object. </param>
    ''' <returns> True if the objects are considered equal, false if they are not. </returns>
    Public Overloads Function Equals(ByVal other As SetpointInfo) As Boolean
        Return other IsNot Nothing AndAlso
                Arebis.TypedUnits.Unit.Equals(Me.TemperatureUnit, other.TemperatureUnit) AndAlso
                Double.Equals(Me.SetpointEpsilon, other.SetpointEpsilon) AndAlso
                Me.SetpointApproximates(Me.SetpointTemperature, other.SetpointTemperature) AndAlso
                Me.RampTimeout.Approximates(other.RampTimeout, TimeSpan.FromSeconds(0.1)) AndAlso
                (Me.OvenControlMode = other.OvenControlMode) AndAlso
                Me.SampleInterval.Approximates(other.SampleInterval, TimeSpan.FromSeconds(0.1)) AndAlso
                Me.SoakDuration.Approximates(other.SoakDuration, TimeSpan.FromSeconds(0.1)) AndAlso
                (Math.Abs(Me.Window - other.Window) < 0.1) AndAlso
                (Math.Abs(Me.Hysteresis - other.Hysteresis) < 0.1) AndAlso
                Me.SoakResetDelay.Approximates(other.SoakResetDelay, TimeSpan.FromSeconds(0.1))
    End Function

    ''' <summary> Query if 'value' is at setpoint. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <returns>
    ''' <c>
    ''' true</c>
    ''' if at setpoint; otherwise <c>
    ''' false</c>
    ''' </returns>
    Public Function IsAtSetpoint(ByVal value As Arebis.TypedUnits.Amount) As Boolean
        Return value IsNot Nothing AndAlso Me.SetpointApproximates(value.Value)
    End Function

    ''' <summary>
    ''' Gets or sets the minimum observable different of the control temperature in the <see cref="TemperatureUnit"/>, which
    ''' determines how accurate the setpoint must be set at.
    ''' </summary>
    ''' <value> The setpoint control temperature epsilon. </value>
    Public Property SetpointEpsilon As Double

    ''' <summary>
    ''' Query if the value equals the setpoint temperature withing the <see cref="SetpointEpsilon"/>.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if approximately equals; otherwise <c>false</c> </returns>
    Public Function SetpointApproximates(ByVal value As Double) As Boolean
        Return Me.SetpointApproximates(value, Me.SetpointTemperature)
    End Function

    ''' <summary> Queries if the values are equal within the <see cref="SetpointEpsilon"/>. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="leftHandValue">  The left hand value. </param>
    ''' <param name="rightHandValue"> The right hand value. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function SetpointApproximates(ByVal leftHandValue As Double, ByVal rightHandValue As Double) As Boolean
        Return SetpointInfo.Approximates(leftHandValue, rightHandValue, Me.SetpointEpsilon)
    End Function

    ''' <summary> Approximates. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="leftHandValue">  The left hand value. </param>
    ''' <param name="rightHandValue"> The right hand value. </param>
    ''' <param name="epsilon">        The epsilon. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Shared Function Approximates(ByVal leftHandValue As Double, ByVal rightHandValue As Double, ByVal epsilon As Double) As Boolean
        Return Math.Abs(leftHandValue - rightHandValue) <= epsilon
    End Function

    ''' <summary> Gets or sets the setpoint target temperature in the <see cref="TemperatureUnit"/>. </summary>
    ''' <value> The target setpoint temperature. </value>
    Public Property SetpointTemperature As Double

    ''' <summary> Gets or sets the target window around the setpoint temperature. </summary>
    ''' <value> The target window around the setpoint temperature. </value>
    Public Property Window As Double

    ''' <summary> Gets or sets the ramp timeout. </summary>
    ''' <value> The ramp timeout. </value>
    Public Property RampTimeout As TimeSpan

    ''' <summary> Checks if ramp timeout elapsed. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="elapsedTime"> The elapsed time. </param>
    ''' <returns> <c>true</c> if ramp timeout elapsed; otherwise <c>false</c> </returns>
    Public Function IsRampTimeoutElapsed(ByVal elapsedTime As TimeSpan) As Boolean
        Return Me.RampTimeout > TimeSpan.Zero AndAlso elapsedTime > Me.RampTimeout
    End Function

    ''' <summary> The oven control mode. </summary>
    Private _OvenControlMode As OvenControlMode

    ''' <summary> Gets or sets the oven control mode. </summary>
    ''' <value> The oven control mode. </value>
    Public Property OvenControlMode As OvenControlMode
        Get
            Return Me._OvenControlMode
        End Get
        Set(value As OvenControlMode)
            Me._OvenControlMode = value
            Me._OvenFunctionInfo = New OvenFunctionInfo(value)
        End Set
    End Property

    ''' <summary> Gets information describing the oven function. </summary>
    ''' <value> Information describing the oven function. </value>
    Public ReadOnly Property OvenFunctionInfo As OvenFunctionInfo

    ''' <summary> Gets the hysteresis. </summary>
    ''' <value> The hysteresis. </value>
    Public Property Hysteresis As Double

    ''' <summary> Gets the duration of the soak. </summary>
    ''' <value> The soak duration. </value>
    Public Property SoakDuration As TimeSpan

    ''' <summary> Gets the sample interval. </summary>
    ''' <value> The sample interval. </value>
    Public Property SampleInterval As TimeSpan

    ''' <summary> Gets the soak sequencer interval. </summary>
    ''' <remarks>
    ''' This is the state machine interval. Initially set to 15 seconds but reduced here to as fast
    ''' as the control loop runs.
    ''' </remarks>
    ''' <value> The soak sequencer interval. </value>
    Public ReadOnly Property SoakSequencerInterval As TimeSpan
        Get
            Return Me.SampleInterval
        End Get
    End Property

    ''' <summary> Gets the temperature sample interval. </summary>
    ''' <remarks>
    ''' This interval is used when oven control is off for determining if it is time to read the
    ''' temperature. Initially set to 15 seconds but reduced here to as fast as the control loop runs.
    ''' </remarks>
    ''' <value> The temperature sample interval. </value>
    Public ReadOnly Property TemperatureSampleInterval As TimeSpan
        Get
            Return Me.SampleInterval
        End Get
    End Property

    ''' <summary> Gets or sets the soak reset delay. </summary>
    ''' <value> The soak reset delay. </value>
    Public Property SoakResetDelay As TimeSpan

    ''' <summary> Defines the unit of temperature measurement. </summary>
    ''' <value> The unit. </value>
    Public Property TemperatureUnit As Arebis.TypedUnits.Unit

End Class

