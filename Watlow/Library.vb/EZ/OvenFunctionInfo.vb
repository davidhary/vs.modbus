Imports isr.Core.EnumExtensions

Imports System.ComponentModel

''' <summary> Information about the Oven Function Bits. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-08-04, . based on test level structure. </para>
''' </remarks>
Public Class OvenFunctionInfo

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="ovenFunctionBit"> The oven function bit. </param>
    Public Sub New(ByVal ovenFunctionBit As Integer)
        Me.New(CType(ovenFunctionBit, OvenFunctionCodes))
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="ovenFunctionBit"> The oven function bit. </param>
    Public Sub New(ByVal ovenFunctionBit As OvenFunctionCodes)
        Me.New()
        Me._OvenFunctionBit = ovenFunctionBit
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As OvenFunctionInfo)
        Me.New()
        If value IsNot Nothing Then
            Me._OvenFunctionBit = value.OvenFunctionBit
        End If
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As OvenControlMode)
        Me.New(CType(value, OvenFunctionCodes))
    End Sub

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Private Sub ClearKnownStateThis()
        Me._OvenFunctionBit = OvenFunctionCodes.None
    End Sub

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Sub ClearKnownState()
        Me.ClearKnownStateThis()
    End Sub

#End Region

#Region " VALUE "

    ''' <summary> The oven function bit. </summary>
    Private _OvenFunctionBit As OvenFunctionCodes

    ''' <summary> Gets the Oven Function Bits. </summary>
    ''' <value> The Oven Function Bits. </value>
    Public ReadOnly Property OvenFunctionBit As OvenFunctionCodes
        Get
            Return Me._OvenFunctionBit
        End Get
    End Property

    ''' <summary>
    ''' Updates the <see cref="OvenFunctionBit">value</see> using the specified value.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value">      The value. </param>
    ''' <param name="ignoreBits"> The ignore bits. </param>
    ''' <returns> <c>true</c> if value was changed; otherwise <c>false</c> </returns>
    Public Function UpdateValue(ByVal value As OvenFunctionCodes, ByVal ignoreBits As OvenFunctionCodes) As Boolean
        Return Me.UpdateValue(value And Not ignoreBits)
    End Function

    ''' <summary>
    ''' Updates the <see cref="OvenFunctionBit">value</see> using the specified value.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if value was changed; otherwise <c>false</c> </returns>
    Public Function UpdateValue(ByVal value As OvenFunctionCodes) As Boolean
        Dim valueChanged As Boolean = Me._OvenFunctionBit <> value
        Me._OvenFunctionBit = value
        Return valueChanged
    End Function

#End Region

#Region " BIT VALUES "

    ''' <summary>
    ''' Query if the Oven Function Is of the <paramref name="bits">Oven Function Bits</paramref> type.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <param name="bits">  The bits. </param>
    ''' <returns> <c>true</c> if Oven Function; otherwise <c>false</c> </returns>
    Public Shared Function IsOvenFunction(ByVal value As OvenFunctionCodes, ByVal bits As OvenFunctionCodes) As Boolean
        Return (value And bits) <> 0
    End Function

    ''' <summary>
    ''' Query if the Oven Function Is of the <paramref name="OvenFunction">Oven Function
    ''' Bits</paramref> type.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="ovenFunction"> The Oven Function Bit number. </param>
    ''' <returns> <c>true</c> if Oven Function; otherwise <c>false</c> </returns>
    Public Function IsOvenFunction1(ByVal ovenFunction As OvenFunction) As Boolean
        Return Me.IsOvenFunction(CType(CInt(2 ^ CInt(ovenFunction)), OvenFunctionCodes))
    End Function

    ''' <summary>
    ''' Query if the Oven Function Is of the <paramref name="bits">Oven Function Bits</paramref> type.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="bits"> The bits. </param>
    ''' <returns> <c>true</c> if Oven Function; otherwise <c>false</c> </returns>
    Public Function IsOvenFunction(ByVal bits As OvenFunctionCodes) As Boolean
        Return (Me.OvenFunctionBit And bits) <> 0
    End Function

    ''' <summary> Query if this oven monitoring is off. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if off; otherwise <c>false</c> </returns>
    Public Function IsOff() As Boolean
        Return Me.IsOvenFunction(OvenFunctionCodes.Off)
    End Function

    ''' <summary> Query if this oven monitoring is on. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if on; otherwise <c>false</c> </returns>
    Public Function IsOn() As Boolean
        Return Me.IsOvenFunction(OvenFunctionCodes.On)
    End Function

    ''' <summary> Query if this oven monitoring is on but quiet. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> <c>true</c> if on but quiet; otherwise <c>false</c> </returns>
    Public Function IsQuiet() As Boolean
        Return Me.IsOvenFunction(OvenFunctionCodes.Quiet)
    End Function

    ''' <summary> Gets the description. </summary>
    ''' <value> The description. </value>
    Public ReadOnly Property Description As String
        Get
            Return Me.OvenFunctionBit.Description
        End Get
    End Property

    ''' <summary> Query if any <see cref="OvenFunctionBit">bits</see> are. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="included"> The included. </param>
    ''' <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
    Public Function IsIncluded(ByVal included As OvenFunctionCodes) As Boolean
        Return OvenFunctionInfo.IsIncluded(Me.OvenFunctionBit, included)
    End Function

#End Region

#Region " HELPERS "

    ''' <summary> Builds Oven Function Bits. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value">    The value. </param>
    ''' <param name="excluded"> The excluded. </param>
    ''' <param name="included"> The included. </param>
    ''' <returns> The BinBits. </returns>
    Public Shared Function BuildOvenFunctionBits(ByVal value As OvenFunctionCodes, ByVal excluded As OvenFunctionCodes, ByVal included As OvenFunctionCodes) As OvenFunctionCodes
        Return (value Or included) And Not excluded
    End Function

    ''' <summary> Query if 'value' is included. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value">    The value. </param>
    ''' <param name="included"> The included. </param>
    ''' <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
    Public Shared Function IsIncluded(ByVal value As OvenFunctionCodes, ByVal included As OvenFunctionCodes) As Boolean
        Return (value And included) <> OvenFunctionCodes.None
    End Function

#End Region

End Class

''' <summary> Values that represent the Oven Functions. </summary>
''' <remarks> David, 2020-11-11. </remarks>
Public Enum OvenFunction

    <Description("Off")>
    Off = 0

    <Description("On")>
    [On] = 1

    <Description("Quiet")>
    Quiet = 2
End Enum

''' <summary> A bit field of flags for detecting the Oven Function. </summary>
''' <remarks> David, 2020-11-11. </remarks>
<Flags()>
Public Enum OvenFunctionCodes

    <Description("None")>
    None = 0

    ''' <summary> Oven monitoring is off. </summary>
    <Description("Off")>
    Off = CInt(2 ^ CInt(OvenFunction.Off))

    ''' <summary> Oven monitoring is on. </summary>
    <Description("On")>
    [On] = CInt(2 ^ CInt(OvenFunction.On))

    ''' <summary> Oven monitoring is on but oven fans are off. </summary>
    <Description("Quiet")>
    Quiet = CInt(2 ^ CInt(OvenFunction.Quiet))
End Enum

''' <summary> Values that represent oven control modes. </summary>
''' <remarks> David, 2020-11-11. </remarks>
Public Enum OvenControlMode

    <Description("None")>
    None = 0

    <Description("Off")>
    Off = OvenFunctionCodes.Off

    <Description("On")>
    [On] = OvenFunctionCodes.On

    <Description("Quiet")>
    Quiet = OvenFunctionCodes.On + OvenFunctionCodes.Quiet
End Enum

