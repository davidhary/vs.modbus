Imports isr.Core
Imports isr.Core.ExceptionExtensions
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-11-11. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Modbus.My.ProjectTraceEventId.WatlowLibrary

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Modbus Watlow Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Modbus Watlow Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Modbus.Watlow"

        ''' <summary> Applies the given logger. </summary>
        ''' <remarks> David, 2020-11-20. </remarks>
        ''' <param name="logger"> The logger. </param>
        Public Shared Sub Apply(ByVal logger As isr.Core.Logger)
            MyLibrary.Appliance.Apply(logger)
            isr.Modbus.My.MyLibrary.Appliance.Apply(logger)
        End Sub

    End Class

End Namespace
