﻿Imports isr.Core
Imports isr.Core.ExceptionExtensions
Namespace My

    ''' <summary> my application. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Modbus.My.ProjectTraceEventId.WatlowConsole

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Easy Zone Console"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Easy Zone Console"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Easy.Zone.Console"

    End Class


End Namespace

