## ISR Easy Zone Console<sub>&trade;</sub>: Console for Watlow EZ-Zone PM devices.

Test application for Watlow Ez-Zone device.
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*1.0.5969 2016-05-07*  
Created.

\(C\) 2016 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Automata Libraries](https://bitbucket.org/davidhary/vs.automata.moore)  
[MODBUS Libraries](https://bitbucket.org/davidhary/vs.modbus)

Core Message Box Library:  
[Safe Copy from
Clipboard](http://stackoverflow.com/questions/899350/how-to-copy-the-contents-of-a-string-to-the-clipboard-in-c)

Core Forms Library:  
[Drop Shadow and Fade Form](http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx)  
[Notification
Window](http://www.codeproject.com/KB/dialog/notificationwindow.aspx)  
[Office Style Splash
Screen](http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen)  
[Typed Units](http://www.codeproject.com/Articles/611731/Working-with-Units-and-Amounts)

MODBUS Libraries:  
[Free .Net Modbus](https://code.google.com/p/free-dotnet-modbus)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:

Core Message Box Library:  
[- SQL Exception
Message](https://msdn.microsoft.com/en-us/library/ms365274.aspx)
