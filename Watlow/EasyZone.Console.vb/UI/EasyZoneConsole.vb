Imports isr.Easy.Zone.Console.ExceptionExtensions
''' <summary> An easy zone console. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-5-10 </para>
''' </remarks>
Public Class EasyZoneConsole
    Inherits isr.Core.Forma.FormBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' set the form caption
            Me.Text = Core.ApplicationInfo.BuildApplicationTitleCaption(Me.Name)
            Me._EasyZoneView.AddListener(My.Application.Logger)
            Me._EasyZoneView.ApplyTalkerTraceLevel(isr.Core.ListenerType.Logger, My.Settings.TraceLevel)
            Me._EasyZoneView.ApplyTalkerTraceLevel(isr.Core.ListenerType.Logger, My.Settings.TraceLevel)
            My.Application.Identify(Me._EasyZoneView.Talker)
            Windows.Forms.Application.DoEvents()
        Catch ex As Exception

            ex.Data.Add("@isr", "Exception loading the form.")
            My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
            If isr.Core.MyDialogResult.Abort = isr.Core.WindowsForms.ShowDialogAbortIgnore(ex, isr.Core.MyMessageBoxIcon.Error) Then
                Application.Exit()
            End If

        Finally

            MyBase.OnLoad(e)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()

        End Try

    End Sub

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As System.EventArgs)

        Try

            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' allow form rendering time to complete: process all messages currently in the queue.
            Windows.Forms.Application.DoEvents()

        Catch ex As Exception

            ex.Data.Add("@isr", "Exception showing the form.")
            My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
            If isr.Core.MyDialogResult.Abort = isr.Core.WindowsForms.ShowDialogAbortIgnore(ex, isr.Core.MyMessageBoxIcon.Error) Then
                Application.Exit()
            End If

        Finally

            MyBase.OnShown(e)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()

        End Try

    End Sub

#End Region

End Class
