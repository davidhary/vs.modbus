<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class EasyZoneConsole

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EasyZoneConsole))
        Me._EasyZoneView = New isr.Modbus.Watlow.Forms.EasyZoneView()
        Me.SuspendLayout()
        '
        '_EasyZoneView
        '
        Me._EasyZoneView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._EasyZoneView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EasyZoneView.Location = New System.Drawing.Point(0, 0)
        Me._EasyZoneView.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EasyZoneView.Name = "_EasyZoneView"
        Me._EasyZoneView.Size = New System.Drawing.Size(386, 503)
        Me._EasyZoneView.TabIndex = 0
        '
        'EasyZoneConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(386, 503)
        Me.Controls.Add(Me._EasyZoneView)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "EasyZoneConsole"
        Me.Text = "Easy Zone Console"
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _EasyZoneView As isr.Modbus.Watlow.Forms.EasyZoneView
End Class
