using System;
using System.ComponentModel;

using isr.Core.EnumExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Modbus.Watlow
{

    /// <summary> Information about the Oven Function Bits. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-08-04, . based on test level structure. </para>
    /// </remarks>
    public class OvenFunctionInfo
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public OvenFunctionInfo() : base()
        {
            this.ClearKnownStateThis();
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="ovenFunctionBit"> The oven function bit. </param>
        public OvenFunctionInfo( int ovenFunctionBit ) : this( ( OvenFunctionCodes ) Conversions.ToInteger( ovenFunctionBit ) )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="ovenFunctionBit"> The oven function bit. </param>
        public OvenFunctionInfo( OvenFunctionCodes ovenFunctionBit ) : this()
        {
            this.OvenFunctionBit = ovenFunctionBit;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> The value. </param>
        public OvenFunctionInfo( OvenFunctionInfo value ) : this()
        {
            if ( value is object )
            {
                this.OvenFunctionBit = value.OvenFunctionBit;
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> The value. </param>
        public OvenFunctionInfo( OvenControlMode value ) : this( ( OvenFunctionCodes ) Conversions.ToInteger( value ) )
        {
        }

        /// <summary>
        /// Clears to known (clear) state; Clears select values to their initial state.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        private void ClearKnownStateThis()
        {
            this.OvenFunctionBit = OvenFunctionCodes.None;
        }

        /// <summary>
        /// Clears to known (clear) state; Clears select values to their initial state.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public void ClearKnownState()
        {
            this.ClearKnownStateThis();
        }

        #endregion

        #region " VALUE "

        /// <summary> Gets the Oven Function Bits. </summary>
        /// <value> The Oven Function Bits. </value>
        public OvenFunctionCodes OvenFunctionBit { get; private set; }

        /// <summary>
        /// Updates the <see cref="OvenFunctionBit">value</see> using the specified value.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value">      The value. </param>
        /// <param name="ignoreBits"> The ignore bits. </param>
        /// <returns> <c>true</c> if value was changed; otherwise <c>false</c> </returns>
        public bool UpdateValue( OvenFunctionCodes value, OvenFunctionCodes ignoreBits )
        {
            return this.UpdateValue( value & ~ignoreBits );
        }

        /// <summary>
        /// Updates the <see cref="OvenFunctionBit">value</see> using the specified value.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if value was changed; otherwise <c>false</c> </returns>
        public bool UpdateValue( OvenFunctionCodes value )
        {
            bool valueChanged = this.OvenFunctionBit != value;
            this.OvenFunctionBit = value;
            return valueChanged;
        }

        #endregion

        #region " BIT VALUES "

        /// <summary>
        /// Query if the Oven Function Is of the <paramref name="bits">Oven Function Bits</paramref> type.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="bits">  The bits. </param>
        /// <returns> <c>true</c> if Oven Function; otherwise <c>false</c> </returns>
        public static bool IsOvenFunction( OvenFunctionCodes value, OvenFunctionCodes bits )
        {
            return (value & bits) != 0;
        }

        /// <summary>
        /// Query if the Oven Function Is of the <paramref name="bits">Oven Function Bits</paramref> type.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="bits"> The bits. </param>
        /// <returns> <c>true</c> if Oven Function; otherwise <c>false</c> </returns>
        public bool IsOvenFunction( OvenFunctionCodes bits )
        {
            return (this.OvenFunctionBit & bits) != 0;
        }

        /// <summary> Query if this oven monitoring is off. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if off; otherwise <c>false</c> </returns>
        public bool IsOff()
        {
            return this.IsOvenFunction( OvenFunctionCodes.Off );
        }

        /// <summary> Query if this oven monitoring is on. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if on; otherwise <c>false</c> </returns>
        public bool IsOn()
        {
            return this.IsOvenFunction( OvenFunctionCodes.On );
        }

        /// <summary> Query if this oven monitoring is on but quiet. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if on but quiet; otherwise <c>false</c> </returns>
        public bool IsQuiet()
        {
            return this.IsOvenFunction( OvenFunctionCodes.Quiet );
        }

        /// <summary> Gets the description. </summary>
        /// <value> The description. </value>
        public string Description => this.OvenFunctionBit.Description();

        /// <summary> Query if any <see cref="OvenFunctionBit">bits</see> are. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="included"> The included. </param>
        /// <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
        public bool IsIncluded( OvenFunctionCodes included )
        {
            return IsIncluded( this.OvenFunctionBit, included );
        }

        #endregion

        #region " HELPERS "

        /// <summary> Builds Oven Function Bits. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="excluded"> The excluded. </param>
        /// <param name="included"> The included. </param>
        /// <returns> The BinBits. </returns>
        public static OvenFunctionCodes BuildOvenFunctionBits( OvenFunctionCodes value, OvenFunctionCodes excluded, OvenFunctionCodes included )
        {
            return (value | included) & ~excluded;
        }

        /// <summary> Query if 'value' is included. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="included"> The included. </param>
        /// <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
        public static bool IsIncluded( OvenFunctionCodes value, OvenFunctionCodes included )
        {
            return (value & included) != OvenFunctionCodes.None;
        }

        #endregion

    }

    /// <summary> A bit field of flags for detecting the Oven Function. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    [Flags()]
    public enum OvenFunctionCodes
    {
        /// <summary>   A binary constant representing the none flag. </summary>
        [Description( "None" )]
        None = 0,

        /// <summary> Oven monitoring is off. </summary>
        [Description( "Off" )]
        Off = 1,

        /// <summary> Oven monitoring is on. </summary>
        [Description( "On" )]
        On = 2,

        /// <summary> Oven monitoring is on but oven fans are off. </summary>
        [Description( "Quiet" )]
        Quiet = 4,
    }

    /// <summary> Values that represent oven control modes. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    public enum OvenControlMode
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [Description( "None" )]
        None = 0,

        /// <summary>   An enum constant representing the off option. </summary>
        [Description( "Off" )]
        Off = OvenFunctionCodes.Off,

        /// <summary>   An enum constant representing the on option. </summary>
        [Description( "On" )]
        On = OvenFunctionCodes.On,

        /// <summary>   An enum constant representing the quiet option. </summary>
        [Description( "Quiet" )]
        Quiet = OvenFunctionCodes.On + OvenFunctionCodes.Quiet
    }
}
