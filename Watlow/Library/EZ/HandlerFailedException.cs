using System;

namespace isr.Modbus.Watlow
{

    /// <summary> Reports handler failed exceptions. </summary>
    /// <remarks>
    /// Use this class to handle exceptions that might be thrown exercising open, close, hardware
    /// access, and other similar operations.  <para>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-04-23, 4.5956.x. </para>
    /// </remarks>
    [Serializable()]
    public class HandlerFailedException : Core.ExceptionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> The default message. </summary>
        private const string _DefaultMessage = "Handler failed.";

        /// <summary>
        /// Initializes a new instance of the <see cref="T:OperationFailedException" /> class. Uses the
        /// internal default message.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public HandlerFailedException() : base( _DefaultMessage )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:OperationFailedException" /> class.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="message"> The message. </param>
        public HandlerFailedException( string message ) : base( message )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:OperationFailedException" /> class.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="message">        The message. </param>
        /// <param name="innerException"> Specifies the exception that was trapped for throwing this
        /// exception. </param>
        public HandlerFailedException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:OperationFailedException" /> class.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="format"> The format. </param>
        /// <param name="args">   The arguments. </param>
        public HandlerFailedException( string format, params object[] args ) : base( format, args )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:OperationFailedException" /> class.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="innerException"> Specifies the InnerException. </param>
        /// <param name="format">         Specifies the exception formatting. </param>
        /// <param name="args">           Specifies the message arguments. </param>
        public HandlerFailedException( Exception innerException, string format, params object[] args ) : base( innerException, format, args )
        {
        }

        /// <summary> Initializes a new instance of the class with serialized data. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
        /// that holds the serialized object data about the exception being
        /// thrown. </param>
        /// <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
        /// that contains contextual information about the source or destination.
        /// </param>
        protected HandlerFailedException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

        #region " GET OBJECT DATA "

        /// <summary> Overrides the GetObjectData method to serialize custom values. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="info">    Represents the SerializationInfo of the exception. </param>
        /// <param name="context"> Represents the context information of the exception. </param>
        [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, SerializationFormatter = true )]
        public override void GetObjectData( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context )
        {
            base.GetObjectData( info, context );
        }

        #endregion

    }
}
