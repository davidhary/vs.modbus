using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Threading.Tasks;

using Arebis.TypedUnits;

using isr.Automata.Finite.Engines;
using isr.Core;
using isr.Core.EnumExtensions;
using isr.Modbus.Watlow.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

using Stateless;

namespace isr.Modbus.Watlow
{

    /// <summary> A device manager for the Watlow EZ-Zone controller. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-5-10 </para>
    /// </remarks>
    public class EasyZone : Core.Models.ViewModelTalkerBase, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">   Identifier for the unit. </param>
        /// <param name="portName"> Name of the port. </param>
        public EasyZone( int unitId, string portName ) : base()
        {
            this._TransactionElapsedTime = new Amount( 0d, Arebis.StandardUnits.TimeUnits.Millisecond );
            this.UnitId = ( byte ) unitId;
            this.PortName = portName;
            this.AssignSoakEngine( new SoakEngine( "Soak" ) );
            // set tracing to information level otherwise with automaton the error could sound quite often.
            this.ModbusErrorTraceEventType = TraceEventType.Information;
            this.ContinuousFailureCountTryAgainLimit = 5;
            this._InputErrorCountDictionary = new Dictionary<InputError, long>();
            this._ContinuousInputErrorCountDictionary = new Dictionary<InputError, int>();
            this._CandidateSetpoint = new Core.Concurrent.ConcurrentToken<double?>();
            this._CandidateOvenControlMode = new Core.Concurrent.ConcurrentToken<OvenControlMode?>();
            this.TemperatureUnit = Arebis.StandardUnits.TemperatureUnits.DegreeFahrenheit;
        }

        #region " Disposable Support "

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the disposed condition indicator. </summary>
        /// <value> The disposed condition indicator. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases
        /// the managed resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this._CandidateSetpoint?.Dispose();
                    this.Talker?.Listeners.Clear();
                    this.AssignSoakEngine( null );
                    if ( this.ModbusClient is object )
                    {
                        if ( this.ModbusClient.Connected )
                            this.ModbusClient.Disconnect();
                    }

                    this.ModbusClient?.Dispose();
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        ~EasyZone()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " KNOWN STATE "

        /// <summary> Clears the known state. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public void ClearKnownState()
        {
            this.AnalogInputRead = null;
            this.SetpointRead = null;
            this.ClearLastInterfaceError();
        }

        #endregion

        #region " INTERFACE "

        /// <summary> Gets or sets the identifier of the unit. </summary>
        /// <value> The identifier of the unit. </value>
        public byte UnitId { get; set; }

        /// <summary> Gets or sets the name of the port. </summary>
        /// <value> The name of the port. </value>
        public string PortName { get; set; }

        /// <summary> Gets or sets the modbus client. </summary>
        /// <value> The modbus client. </value>
        public ModbusClientSerial ModbusClient { get; private set; }

        /// <summary> Attempts to open modbus client from the given data. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryOpenModbusClient()
        {
            var sw = Stopwatch.StartNew();
            this.ClearLastInterfaceError();
            this.ModbusClient = new ModbusClientSerial( ModbusSerialType.RTU, ModbusClientSerial.ConstructPort( this.PortName, 38400, 8, Parity.None, StopBits.One, Handshake.None ) );
            // Connect
            this.ModbusClient.Connect();
            this.IsDeviceOpen = this.ModbusClient.Connected;
            if ( this.IsDeviceOpen )
            {
                this.UpdateLastInterfaceError();
                this.Title = $"RTU#{this.UnitId}:{this.PortName}";
            }
            else
            {
                this.ClearLastInterfaceError();
                this.Title = $"RTU Closed";
            }

            this.TransactionElapsedTime = new Amount( sw.ElapsedMilliseconds, Arebis.StandardUnits.TimeUnits.Millisecond );
            this.InputErrorRead = InputError.NoError;
            return this.ModbusClient.Connected;
        }

        /// <summary> Closes modbus client. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public void CloseModbusClient()
        {
            this.ClearLastInterfaceError();
            this.ModbusClient?.Disconnect();
            this.IsDeviceOpen = (this.ModbusClient?.Connected).GetValueOrDefault( false );
            if ( this.IsDeviceOpen )
            {
                this.UpdateLastInterfaceError();
                this.Title = $"RTU#{this.UnitId}:{this.PortName}";
            }
            else
            {
                this.ClearLastInterfaceError();
                this.Title = $"RTU Closed";
            }
        }

        /// <summary> True to enable, false to disable. </summary>
        private bool _Enabled;

        /// <summary>
        /// Gets or sets the Enabled sentinel of the device. A device is enabled when hardware can be
        /// used.
        /// </summary>
        /// <value> <c>True</c> if hardware device is enabled; <c>False</c> otherwise. </value>
        public virtual bool Enabled
        {
            get => this._Enabled;

            set {
                if ( this.Enabled != value )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                    ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> True if device is open, false if not. </summary>
        private bool _IsDeviceOpen;

        /// <summary> Gets or sets the is device open. </summary>
        /// <value> The is device open. </value>
        public bool IsDeviceOpen
        {
            get => this._IsDeviceOpen;

            protected set {
                if ( value != this.IsDeviceOpen )
                {
                    this._IsDeviceOpen = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The title. </summary>
        private string _Title;

        /// <summary> Gets or sets the title. </summary>
        /// <value> The title. </value>
        public string Title
        {
            get => this._Title;

            set {
                if ( !string.Equals( value, this.Title ) )
                {
                    this._Title = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " TRANSACTION CONTROL "

        /// <summary>
        /// Gets or sets the timespan for recovery from a reading before a new write can be made.
        /// </summary>
        /// <value> The timespan for settling after a read query. </value>
        public TimeSpan DefaultReadSettlingTimespan { get; set; } = TimeSpan.FromMilliseconds( 10d );

        /// <summary> Gets or sets next settling timespan. </summary>
        /// <value> The next settling timespan. </value>
        public TimeSpan SettlingTimespan
        {
            get => this.ModbusClient.DeviceSettlingTime;

            set {
                if ( value != this.SettlingTimespan )
                {
                    this.ModbusClient.DeviceSettlingTime = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Executes the transaction ended action. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="settlingTime"> The settling time before the next transaction. </param>
        private void OnTransactionEnded( TimeSpan settlingTime )
        {
            this.TransactionElapsedTime = new Amount( this.ModbusClient.QueryElapsedTime.TotalMilliseconds, Arebis.StandardUnits.TimeUnits.Millisecond );
            // set the recovery time following this last transaction
            this.SettlingTimespan = settlingTime;
        }

        /// <summary> The transaction elapsed time. </summary>
        private Amount _TransactionElapsedTime;

        /// <summary> Gets or sets the transaction elapsed time. </summary>
        /// <value> The transaction elapsed time. </value>
        public Amount TransactionElapsedTime
        {
            get => this._TransactionElapsedTime;

            set {
                if ( value != this.TransactionElapsedTime )
                {
                    this._TransactionElapsedTime = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " INTERFACE ERROR "

        /// <summary> Gets or sets the type of the modbus error trace event. </summary>
        /// <value> The type of the modbus error trace event. </value>
        public TraceEventType ModbusErrorTraceEventType { get; set; } = TraceEventType.Information;

        /// <summary> Updates the last interface error from the <see cref="EasyZone.ModbusClient"/>. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        private void UpdateLastInterfaceError()
        {
            this.LastInterfaceError = this.ModbusClient.LastError;
            this.LastInterfaceErrorDetails = this.ModbusClient.LastErrorDetails;
            this.LastInterfaceErrorContinuousCount = this.ModbusClient.LastErrorContinuousFailureCount();
        }

        private InterfaceError _LastInterfaceError;

        /// <summary> Gets or sets the last interface error. </summary>
        /// <value> The last interface error. </value>
        public InterfaceError LastInterfaceError
        {
            get => this._LastInterfaceError;

            protected set {
                this._LastInterfaceError = value;
                this.AsyncNotifyPropertyChanged();
            }
        }

        private int _ContinuousFailureCountTryAgainLimit;

        /// <summary> Gets or sets the continuous failure count try again limit. </summary>
        /// <value> The continuous failure count try again limit. </value>
        public int ContinuousFailureCountTryAgainLimit
        {
            get => this._ContinuousFailureCountTryAgainLimit;

            set {
                if ( value != this.ContinuousFailureCountTryAgainLimit )
                {
                    this._ContinuousFailureCountTryAgainLimit = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        private int _LastInterfaceErrorContinuousCount;

        /// <summary> Gets or sets the last interface error continuous count. </summary>
        /// <value> The last interface error continuous count. </value>
        public int LastInterfaceErrorContinuousCount
        {
            get => this._LastInterfaceErrorContinuousCount;

            protected set {
                this._LastInterfaceErrorContinuousCount = value;
                this.AsyncNotifyPropertyChanged();
            }
        }

        /// <summary> The last interface error details. </summary>
        private string _LastInterfaceErrorDetails;

        /// <summary> Gets or sets the last interface error details. </summary>
        /// <value> The last interface error. </value>
        public string LastInterfaceErrorDetails
        {
            get => this._LastInterfaceErrorDetails;

            protected set {
                this._LastInterfaceErrorDetails = value;
                this.AsyncNotifyPropertyChanged();
            }
        }

        /// <summary> Clears the last interface error. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected void ClearLastInterfaceError()
        {
            if ( this.LastInterfaceError != InterfaceError.NoError )
            {
                this.LastInterfaceError = InterfaceError.NoError;
                this.LastInterfaceErrorDetails = string.Empty;
                this.LastInterfaceErrorContinuousCount = 0;
            }
        }

        #endregion

        #region " INPUT ERROR "

        private int _ContinuousInputErrorCount;

        /// <summary> Gets or sets the Continuous Input Error count. </summary>
        /// <value> The Input Error read. </value>
        public int ContinuousInputErrorCount
        {
            get => this._ContinuousInputErrorCount;

            protected set {
                if ( value != this.ContinuousInputErrorCount )
                {
                    this._ContinuousInputErrorCount = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        private readonly Dictionary<InputError, long> _InputErrorCountDictionary;

        /// <summary> Input error count. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <param name="inputError"> The input error. </param>
        /// <returns> An Integer. </returns>
        public long InputErrorCount( InputError inputError )
        {
            return this._InputErrorCountDictionary.ContainsKey( inputError ) ? this._InputErrorCountDictionary[inputError] : 0L;
        }

        private readonly Dictionary<InputError, int> _ContinuousInputErrorCountDictionary;

        /// <summary> Input error continuous count. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <param name="inputError"> The input error. </param>
        /// <returns> An Integer. </returns>
        public int InputErrorContinuousCount( InputError inputError )
        {
            return this._ContinuousInputErrorCountDictionary.ContainsKey( inputError ) ? this._ContinuousInputErrorCountDictionary[inputError] : 0;
        }

        /// <summary> Builds compound input error. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String. </returns>
        public static string BuildCompoundInputError( InputError value )
        {
            return $"{( int ) value},{value.Description()}";
        }

        /// <summary> The input error read. </summary>
        private InputError _InputErrorRead;

        /// <summary> Gets or sets the Input Error read. </summary>
        /// <value> The Input Error read. </value>
        public InputError InputErrorRead
        {
            get => this._InputErrorRead;

            protected set {
                if ( InputError.NoError == value )
                {
                    this.ContinuousInputErrorCount = 0;
                    this._ContinuousInputErrorCountDictionary.Clear();
                }
                else
                {
                    this.ContinuousInputErrorCount += 1;
                    if ( value == this.InputErrorRead )
                    {
                        if ( this._ContinuousInputErrorCountDictionary.ContainsKey( value ) )
                        {
                            this._ContinuousInputErrorCountDictionary[value] += 1;
                        }
                        else
                        {
                            this._ContinuousInputErrorCountDictionary.Add( value, 0 );
                        }
                    }
                }

                if ( this._InputErrorCountDictionary.ContainsKey( value ) )
                {
                    this._InputErrorCountDictionary[value] += 1L;
                }
                else
                {
                    this._InputErrorCountDictionary.Add( value, 1L );
                }

                if ( value != this.InputErrorRead )
                {
                    this._InputErrorRead = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Input Error register. </summary>
        /// <value> The Input Error register. </value>
        public int InputErrorRegister { get; set; } = 362;

        /// <summary> Reads Input Error. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The Input Error. </returns>
        public InputError ReadInputError()
        {
            ushort actualValue = 0;
            this.ClearLastInterfaceError();
            string activity = "reading input error";
            if ( this.ModbusClient.TryReadHoldingRegistersValue( this.UnitId, ( ushort ) this.InputErrorRegister, ref actualValue ) )
            {
                this.InputErrorRead = Enum.IsDefined( typeof( InputError ), ( int ) actualValue ) ? ( InputError ) Conversions.ToInteger( actualValue ) : InputError.None;
            }

            this.UpdateLastInterfaceError();
            _ = this.LastInterfaceError == InterfaceError.NoError
                ? this.PublishVerbose( $"Success {activity}: {this.ModbusClient.FormatRxPayload()};. " )
                : this.PublishDefaultErrorLevel( $"Failed {activity}: {this.LastInterfaceErrorDetails}" );

            this.OnTransactionEnded( this.DefaultReadSettlingTimespan );
            return this.InputErrorRead;
        }

        #endregion

        #region " ALARM TYPE "

        /// <summary> The alarm type read. </summary>
        private AlarmType _AlarmTypeRead;

        /// <summary> Gets or sets the Alarm Type read. </summary>
        /// <value> The Alarm Type read. </value>
        public AlarmType AlarmTypeRead
        {
            get => this._AlarmTypeRead;

            protected set {
                if ( value != this.AlarmTypeRead )
                {
                    this._AlarmTypeRead = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> The alarm type written. </summary>
        private AlarmType _AlarmTypeWritten;

        /// <summary> Gets or sets the Alarm Type Written. </summary>
        /// <value> The Alarm Type Written. </value>
        public AlarmType AlarmTypeWritten
        {
            get => this._AlarmTypeWritten;

            protected set {
                if ( value != this.AlarmTypeWritten )
                {
                    this._AlarmTypeWritten = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Alarm Type register. </summary>
        /// <value> The Alarm Type register. </value>
        public int AlarmTypeRegister { get; set; } = 1508;

        /// <summary> Applies the Alarm Type described by value. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> An AlarmType. </returns>
        public AlarmType ApplyAlarmType( AlarmType value )
        {
            _ = this.WriteAlarmType( value );
            if ( this.LastInterfaceError == InterfaceError.NoError )
                _ = this.ReadAlarmType();
            return this.AlarmTypeRead;
        }

        /// <summary> Reads Alarm Type. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The Alarm Type. </returns>
        public AlarmType ReadAlarmType()
        {
            this.ClearLastInterfaceError();
            ushort actualValue = 0;
            string activity = "reading alarm type";
            if ( this.ModbusClient.TryReadHoldingRegistersValue( this.UnitId, ( ushort ) this.AlarmTypeRegister, ref actualValue ) )
            {
                this.AlarmTypeRead = Enum.IsDefined( typeof( AlarmType ), ( int ) actualValue ) ? ( AlarmType ) Conversions.ToInteger( actualValue ) : AlarmType.None;
            }

            this.UpdateLastInterfaceError();
            _ = this.LastInterfaceError == InterfaceError.NoError
                ? this.PublishVerbose( $"Success {activity}: {this.ModbusClient.FormatRxPayload()};. " )
                : this.PublishDefaultErrorLevel( $"Failed {activity}: {this.LastInterfaceErrorDetails}" );

            this.OnTransactionEnded( this.DefaultReadSettlingTimespan );
            return this.AlarmTypeRead;
        }

        /// <summary> Gets or sets the Alarm Type settling time. </summary>
        /// <value> The Alarm Type settling time. </value>
        public TimeSpan AlarmTypeSettlingTime { get; set; } = TimeSpan.FromSeconds( 2d );

        /// <summary> Writes an Alarm Type. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> An AlarmType. </returns>
        public AlarmType WriteAlarmType( AlarmType value )
        {
            this.ClearLastInterfaceError();
            string activity = "writing alarm type";
            this.ModbusClient.WriteRegistersValue( this.UnitId, ( ushort ) this.AlarmTypeRegister, ( ushort ) value );
            this.UpdateLastInterfaceError();
            _ = this.LastInterfaceError == InterfaceError.NoError
                ? this.PublishVerbose( $"Success {activity}: {this.ModbusClient.FormatRxPayload()};. " )
                : this.PublishDefaultErrorLevel( $"Failed {activity}: {this.LastInterfaceErrorDetails}" );

            this.AlarmTypeWritten = value;
            this.OnTransactionEnded( this.AlarmTypeSettlingTime );
            return this.AlarmTypeWritten;
        }

        #endregion

        #region " ANALOG INPUT "

        /// <summary> The analog input read. </summary>
        private Amount _AnalogInputRead;

        /// <summary> Gets or sets the Analog Input. </summary>
        /// <value> The Input Error read. </value>
        public Amount AnalogInputRead
        {
            get => this._AnalogInputRead;

            protected set {
                if ( value != this.AnalogInputRead )
                {
                    this._AnalogInputRead = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Analog Input register. </summary>
        /// <value> The Analog Input register. </value>
        public int AnalogInputRegister { get; set; } = 360;

        /// <summary> Reads Analog Input. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The Analog Input. </returns>
        public Amount ReadAnalogInput()
        {
            this.ClearLastInterfaceError();
            float actualValue = 0f;
            string activity = "reading analog input";
            if ( this.ModbusClient.TryReadHoldingRegistersValue( this.UnitId, ( ushort ) this.AnalogInputRegister, ref actualValue ) )
            {
                activity = "parse analog input";
                this.AnalogInputRead = new Amount( actualValue, this.TemperatureUnit );
            }

            this.UpdateLastInterfaceError();
            _ = this.LastInterfaceError == InterfaceError.NoError
                ? this.PublishVerbose( $"Success {activity}: {this.ModbusClient.FormatRxPayload()};. " )
                : this.PublishDefaultErrorLevel( $"Failed {activity};. Details: {this.LastInterfaceErrorDetails}" );

            this.OnTransactionEnded( this.DefaultReadSettlingTimespan );
            return this.AnalogInputRead;
        }

        #endregion

        #region " SETPOINT "

        /// <summary> The setpoint read. </summary>
        private Amount _SetpointRead;

        /// <summary> Gets or sets the Setpoint read. </summary>
        /// <value> The Setpoint read. </value>
        public Amount SetpointRead
        {
            get => this._SetpointRead;

            protected set {
                if ( value != this.SetpointRead )
                {
                    this._SetpointRead = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> The setpoint written. </summary>
        private Amount _SetpointWritten;

        /// <summary> Gets or sets the Setpoint Written. </summary>
        /// <value> The Setpoint Written. </value>
        public Amount SetpointWritten
        {
            get => this._SetpointWritten;

            protected set {
                if ( value != this.SetpointWritten )
                {
                    this._SetpointWritten = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the Setpoint register. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The Setpoint register. </value>
        public int SetpointRegister { get; set; } = 2160;

        /// <summary> Applies the Setpoint described by value. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> An Setpoint. </returns>
        public Amount ApplySetpoint( Amount value )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            _ = this.WriteSetpoint( value );
            if ( this.LastInterfaceError == InterfaceError.NoError )
                _ = this.ReadSetpoint();
            return this.SetpointRead;
        }

        /// <summary> Reads Setpoint. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The Setpoint. </returns>
        public Amount ReadSetpoint()
        {
            this.ClearLastInterfaceError();
            float actualValue = 0f;
            string activity = "reading setpoint";
            if ( this.ModbusClient.TryReadHoldingRegistersValue( this.UnitId, ( ushort ) this.SetpointRegister, ref actualValue ) )
            {
                activity = "parsing setpoint";
                this.SetpointRead = new Amount( actualValue, this.TemperatureUnit );
            }

            this.UpdateLastInterfaceError();
            _ = this.LastInterfaceError == InterfaceError.NoError
                ? this.PublishVerbose( $"Success {activity}: {this.ModbusClient.FormatRxPayload()};. " )
                : this.PublishDefaultErrorLevel( $"Failed {activity};. Details: {this.LastInterfaceErrorDetails}" );

            this.OnTransactionEnded( this.DefaultReadSettlingTimespan );
            return this.SetpointRead;
        }

        /// <summary> Reads the setpoint. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public (bool Success, string Details) TryReadSetpoint()
        {
            (bool Success, string Details) result = (true, string.Empty);
            string activity = string.Empty;
            try
            {
                activity = "reading setpoint";
                _ = this.ReadSetpoint();
                if ( this.LastInterfaceError != InterfaceError.NoError )
                {
                    result = (false, $"Interface error '{this.LastInterfaceError.Description()}' reading setpoint");
                }
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                result = (false, activity);
            }

            return result;
        }

        /// <summary> Gets the setpoint settling time. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The time it takes the new setpoint to settle. </value>
        public TimeSpan SetpointSettlingTime { get; set; } = TimeSpan.FromMilliseconds( 400d );

        /// <summary> Writes an Setpoint. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> An Setpoint. </returns>
        public Amount WriteSetpoint( Amount value )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            this.ClearLastInterfaceError();
            this.ModbusClient.WriteRegistersValue( this.UnitId, ( ushort ) this.SetpointRegister, ( float ) value.Value );
            this.UpdateLastInterfaceError();
            this.SetpointWritten = value;
            this.OnTransactionEnded( this.SetpointSettlingTime );
            return this.SetpointWritten;
        }

        #endregion

        #region " MODBUS CLIENT UNITS "

        private Unit _TemperatureUnit;

        /// <summary> Defines the unit of temperature measurements. </summary>
        /// <value> The unit of temperature measurements. </value>
        public Unit TemperatureUnit
        {
            get => this._TemperatureUnit;

            set {
                if ( !Equals( value, this.TemperatureUnit ) )
                {
                    this._TemperatureUnit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " HANDLER EXCEPTION HANDLER "

        /// <summary> Executes the handler exception action. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="ex"> The ex. </param>
        protected virtual void OnHandlerException( Exception ex )
        {
            this.SyncNotifyHandlerExceptionOccurred( new System.IO.ErrorEventArgs( ex ) );
        }

        /// <summary> Notifies of the HandlerExceptionOccurred event. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnHandlerExceptionOccurred( System.IO.ErrorEventArgs e )
        {
            this.SyncNotifyHandlerExceptionOccurred( e );
        }

        /// <summary> Removes the HandlerExceptionOccurred event handlers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected void RemoveHandlerExceptionOccurredEventHandlers()
        {
            this._HandlerExceptionOccurredEventHandlers?.RemoveAll();
        }

        /// <summary> The HandlerExceptionOccurred event handlers. </summary>
        private readonly EventHandlerContextCollection<System.IO.ErrorEventArgs> _HandlerExceptionOccurredEventHandlers = new ();

        /// <summary> Event queue for all listeners interested in HandlerExceptionOccurred events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<System.IO.ErrorEventArgs> HandlerExceptionOccurred
        {
            add {
                this._HandlerExceptionOccurredEventHandlers.Add( new EventHandlerContext<System.IO.ErrorEventArgs>( value ) );
            }

            remove {
                this._HandlerExceptionOccurredEventHandlers.RemoveValue( value );
            }
        }

        /// <summary>   Notifies of the HandlerExceptionOccurred event. </summary>
        /// <remarks>   David, 2020-11-27. </remarks>
        /// <param name="sender">   The sender. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnHandlerExceptionOccurred( object sender, System.IO.ErrorEventArgs e )
        {
            this._HandlerExceptionOccurredEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously <see cref="EventHandlerContextCollection{TEventArgs}.Send">sends</see> or invokes the
        /// <see cref="HandlerExceptionOccurred">HandlerExceptionOccurred Event</see>.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> The <see cref="System.IO.ErrorEventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyHandlerExceptionOccurred( System.IO.ErrorEventArgs e )
        {
            this._HandlerExceptionOccurredEventHandlers.Send( this, e );
        }

        /// <summary> Asynchronous notify handler exception occurred. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> Error event information. </param>
        protected void AsyncNotifyHandlerExceptionOccurred( System.IO.ErrorEventArgs e )
        {
            this._HandlerExceptionOccurredEventHandlers.Post( this, e );
        }

        /// <summary> Unsafe notify handler exception occurred. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> Error event information. </param>
        protected void UnsafeNotifyHandlerExceptionOccurred( System.IO.ErrorEventArgs e )
        {
            this._HandlerExceptionOccurredEventHandlers.UnsafeInvoke( this, e );
        }

        #endregion

        #region " SEND/POST TESTERS "

        /// <summary> The event time builder. </summary>
        private System.Text.StringBuilder _EventTimeBuilder;

        /// <summary> The event stopwatch. </summary>
        private Stopwatch _EventStopwatch;

        /// <summary> Synchronizes the notify handler exception. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        internal void SyncNotifyHandlerException()
        {
            Exception ex = new HandlerFailedException();
            this._EventTimeBuilder = new System.Text.StringBuilder();
            this._EventStopwatch = Stopwatch.StartNew();
            _ = this._EventTimeBuilder.AppendFormat( $">> {this._EventStopwatch.ElapsedTicks}" );
            this.SyncNotifyHandlerExceptionOccurred( new System.IO.ErrorEventArgs( ex ) );
        }

        /// <summary> Asynchronous notify handler exception. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        internal void AsyncNotifyHandlerException()
        {
            Exception ex = new HandlerFailedException();
            this._EventTimeBuilder = new System.Text.StringBuilder();
            this._EventStopwatch = Stopwatch.StartNew();
            _ = this._EventTimeBuilder.AppendFormat( $">> {this._EventStopwatch.ElapsedTicks}" );
            this.AsyncNotifyHandlerExceptionOccurred( new System.IO.ErrorEventArgs( ex ) );
        }

        /// <summary> Unsafe invoke handler exception. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        internal void UnsafeInvokeHandlerException()
        {
            Exception ex = new HandlerFailedException();
            this._EventTimeBuilder = new System.Text.StringBuilder();
            this._EventStopwatch = Stopwatch.StartNew();
            _ = this._EventTimeBuilder.AppendFormat( $">> {this._EventStopwatch.ElapsedTicks}" );
            this.UnsafeNotifyHandlerExceptionOccurred( new System.IO.ErrorEventArgs( ex ) );
        }

        /// <summary> Registers the handler exception occurred evetn. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Error event information. </param>
        internal void RegisterHandlerExceptionOccurred( object sender, System.IO.ErrorEventArgs e )
        {
            _ = this._EventTimeBuilder.AppendFormat( $" <<{this._EventStopwatch.ElapsedTicks} " );
            _ = this.PublishInfo( this._EventTimeBuilder.ToString() );
            System.Threading.Thread.Sleep( 10 );
        }

        #endregion

        #region " SETPOINT INFO "

        /// <summary> Gets or sets the target setpoint. </summary>
        /// <value> Information describing the target setpoint. </value>
        private SetpointInfo TargetSetpointInfo { get; set; }

        /// <summary> Gets information describing the active setpoint. </summary>
        /// <value> Information describing the active setpoint. </value>
        private SetpointInfo ActiveSetpointInfo { get; set; }

        /// <summary> Query if a new target setpoint temperature changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns>
        /// <c>true</c> if a new target setpoint temperature is requested; otherwise <c>false</c>
        /// </returns>
        private bool IsTargetSetpointTemperatureChanged()
        {
            return this.TargetSetpointInfo is object && this.ActiveSetpointInfo is object && !this.TargetSetpointInfo.SetpointApproximates( this.ActiveSetpointInfo.SetpointTemperature );
        }

        private readonly Core.Concurrent.ConcurrentToken<double?> _CandidateSetpoint;

        /// <summary>
        /// Gets or sets the candidate setpoint temperature.
        /// </summary>
        /// <value>
        /// The candidate setpoint temperature.
        /// </value>
        public double? CandidateSetpointTemperature
        {
            get => this._CandidateSetpoint.Value;

            set {
                if ( this.CandidateSetpointTemperature != value == true )
                {
                    this._CandidateSetpoint.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Query if a candidate setpoint temperature changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns>
        /// <c>true</c> if a candidate setpoint temperature is requested; otherwise <c>false</c>
        /// </returns>
        private bool IsCandidateSetpointChanged()
        {
            return this.CandidateSetpointTemperature.HasValue && this.ActiveSetpointInfo is object && !this.ActiveSetpointInfo.SetpointApproximates( this.CandidateSetpointTemperature.Value );
        }

        /// <summary> Query if a new target oven control mode is requested. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns>
        /// <c>true</c> if a new target oven control mode is requested; otherwise <c>false</c>
        /// </returns>
        private bool IsTargetOvenControlModeChanged()
        {
            return this.TargetSetpointInfo is object && this.ActiveSetpointInfo is object && this.TargetSetpointInfo.OvenControlMode != this.ActiveSetpointInfo.OvenControlMode;
        }

        private readonly Core.Concurrent.ConcurrentToken<OvenControlMode?> _CandidateOvenControlMode;

        /// <summary>
        /// Gets or sets the candidate <see cref="isr.Modbus.Watlow.OvenControlMode"/>.
        /// </summary>
        /// <value>
        /// The candidate <see cref="isr.Modbus.Watlow.OvenControlMode"/>.
        /// </value>
        public OvenControlMode? CandidateOvenControlMode
        {
            get => this._CandidateOvenControlMode.Value;

            set {
                if ( !Nullable.Equals( this.CandidateOvenControlMode, value ) )
                {
                    this._CandidateOvenControlMode.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Query if a candidate oven control mode changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns>
        /// <c>true</c> if a new candidate oven control mode is requested; otherwise <c>false</c>
        /// </returns>
        private bool IsCandidateOvenControlModeChanged()
        {
            return this.CandidateOvenControlMode.HasValue && this.ActiveSetpointInfo is object && this.CandidateOvenControlMode.Value != this.ActiveSetpointInfo.OvenControlMode;
        }

        #endregion

        #region " AUTOMATON "

        /// <summary> Changes the <see cref="isr.Automata.Finite.Engines.SoakEngine"/> setpoint. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="setpointInfo"> Information describing the setpoint. </param>
        /// <param name="restart">      True to restart the soak from the idle state. </param>
        public void ChangeTargetAutomatonSetpoint( SetpointInfo setpointInfo, bool restart )
        {
            if ( setpointInfo is null )
                throw new ArgumentNullException( nameof( setpointInfo ) );
            if ( setpointInfo.OvenControlMode == OvenControlMode.None )
                throw new InvalidOperationException( $"Oven control mode not set as '{setpointInfo.OvenControlMode}'" );
            _ = this.PublishInfo( $"changing setpoint to {setpointInfo.SetpointTemperature} degrees;. " );
            this.TargetSetpointInfo = setpointInfo;
            this.CandidateSetpointTemperature = new double?();
            this.CandidateOvenControlMode = new OvenControlMode?();
            // restart if requested or current temperature is outside the setpoint window.
            restart = restart || this.Temperature is null || !SetpointInfo.Approximates( this.Temperature.Value, setpointInfo.SetpointTemperature, setpointInfo.Window );
            if ( restart )
                this.Engage( setpointInfo );
        }

        /// <summary> Activates the soak automaton described by setpointInfo. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="setpointInfo"> Information describing the setpoint. </param>
        public void ActivateSoakAutomaton( SetpointInfo setpointInfo )
        {
            if ( setpointInfo is null )
                throw new ArgumentNullException( nameof( setpointInfo ) );
            if ( setpointInfo.OvenControlMode == OvenControlMode.None )
                throw new InvalidOperationException( $"Oven control mode not set as '{setpointInfo.OvenControlMode}'" );
            // clear the candidate settings
            this.TargetSetpointInfo = null;
            this.CandidateSetpointTemperature = new double?();
            this.CandidateOvenControlMode = new OvenControlMode?();
            // clear to make sure this is read once.
            this.InputErrorRead = InputError.None;
            _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "starting soak state machine;. " );
            this.Engage( setpointInfo );
            this.TargetSetpointInfo = setpointInfo;
            _ = this.PublishInfo( $"Starting setpoint at {setpointInfo.SetpointTemperature} degrees;. " );
        }

        #endregion

        #region " SOAK ENGINE ACTIONS "

        /// <summary> Gets or sets the temperature. </summary>
        /// <value> The temperature. </value>
        public Amount Temperature { get; private set; }

        /// <summary> Reads the temperature. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public (bool Success, string Details) TryReadTemperature()
        {
            (bool Success, string Details) result = (true, string.Empty);
            string activity = string.Empty;
            try
            {
                activity = $"reading analog input";
                _ = this.ReadAnalogInput();
                if ( this.LastInterfaceError == InterfaceError.NoError )
                {
                    if ( this.AnalogInputRead is null )
                    {
                        result = (false, $"Unknown error reading temperature");
                    }
                    else
                    {
                        this.Temperature = new Amount( this.AnalogInputRead );
                    }
                }
                else
                {
                    result = (false, $"Failed {activity}: {this.LastInterfaceErrorDetails}");
                }

                if ( !result.Success )
                    _ = this.PublishDefaultErrorLevel( result.Details );
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                result = (false, activity);
            }

            return result;
        }

        /// <summary> The sample lock. </summary>
        private readonly object _SampleLock = new ();

        /// <summary> Sample temperature. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public (bool Success, string Details) TrySampleTemperature()
        {
            (bool Success, string Details) result = (true, string.Empty);
            lock ( this._SampleLock )
            {
                if ( this.SoakEngine.IsSampleIntervalElapsed() )
                {
                    result = this.TryReadTemperature();
                    this.SoakEngine.OnSampleHandled();
                }

                return result;
            }
        }

        /// <summary> Reads the temperature. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public (bool Success, string Details) TryReadTemperatureAndError()
        {
            (bool Success, string Details) result = (true, string.Empty);
            string activity = string.Empty;
            try
            {
                activity = $"reading analog input";
                _ = this.ReadAnalogInput();
                if ( this.LastInterfaceError == InterfaceError.NoError )
                {
                    activity = $"reading input error";
                    _ = this.ReadInputError();
                    if ( this.InputErrorRead == InputError.NoError )
                    {
                        this.Temperature = new Amount( this.AnalogInputRead );
                    }
                    else
                    {
                        result = (false, $"Failed {activity}: {this.LastInterfaceErrorDetails}");
                    }
                }
                else
                {
                    result = (false, $"Failed {activity}: {this.LastInterfaceErrorDetails}");
                }
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                result = (false, activity);
            }

            return result;
        }

        /// <summary> Query if this object is setpoint ready. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if setpoint ready; otherwise <c>false</c> </returns>
        public bool IsSetpointReady()
        {
            return this.ActiveSetpointInfo.IsAtSetpoint( this.SetpointRead );
        }

        /// <summary>
        /// Query if this oven is ready, that is off, quite and alarm type off or on and alarm type
        /// process.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if oven ready; otherwise <c>false</c> </returns>
        public bool IsOvenReady()
        {
            return this.ActiveSetpointInfo.OvenFunctionInfo.IsOff() || this.ActiveSetpointInfo.OvenFunctionInfo.IsQuiet() && this.AlarmTypeRead == AlarmType.Off || this.ActiveSetpointInfo.OvenFunctionInfo.IsOn() && this.AlarmTypeRead == AlarmType.Process;
        }

        /// <summary> Updates the setpoint. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="stateIdentity">      The state identity. </param>
        /// <param name="setpointTemperature"> The setpoint temperature in the <see cref="TemperatureUnit"/>. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private (bool Success, bool TryAgain, string Details) UpdateSetpoint( SoakState stateIdentity, double setpointTemperature )
        {
            (bool Success, bool TryAgain, string Details) result = (true, false, string.Empty);
            string activity = string.Empty;
            try
            {
                // t = T
                if ( this.SetpointWritten is null || !this.ActiveSetpointInfo.SetpointApproximates( this.SetpointWritten.Value, setpointTemperature ) )
                {
                    // setpoint not set or out of range, write a new value
                    activity = $"Setting setpoint {setpointTemperature:G4)} at '{stateIdentity}' state";
                    _ = this.PublishVerbose( $"{activity};. " );
                    _ = this.WriteSetpoint( new Amount( setpointTemperature, this.TemperatureUnit ) );
                }
                else
                {
                    // t = 2 * T
                    // if setpoint was written, read setpoint status to validate
                    activity = $"reading setpoint at '{stateIdentity}' state";
                    _ = this.PublishVerbose( $"{activity};. " );
                    _ = this.ReadSetpoint();
                    if ( this.LastInterfaceError == InterfaceError.NoError )
                    {
                        // If value read without error, check if in range.
                        if ( this.SetpointRead is null )
                        {
                            // if nothing, try treading once again on the next cycle.
                            result = this.LastInterfaceErrorContinuousCount < this.ContinuousFailureCountTryAgainLimit ? (false, true, $"Failed {activity}; trying again") : (false, false, $"Failed {activity}; aborting");
                            _ = this.PublishInfo( $"{result.Details};. " );
                        }
                        else if ( this.ActiveSetpointInfo.SetpointApproximates( this.SetpointRead.Value, setpointTemperature ) )
                        {
                            result = (true, false, $"Setpoint set at '{stateIdentity}' state");
                            _ = this.PublishVerbose( $"{result.Details};. " );
                            if ( this.IsTargetSetpointTemperatureChanged() )
                            {
                                // if a new target setpoint is requested, update the setpoint thus clearing the request
                                this.ActiveSetpointInfo.SetpointTemperature = setpointTemperature;
                            }

                            if ( this.CandidateSetpointTemperature.HasValue && this.ActiveSetpointInfo.SetpointApproximates( this.CandidateSetpointTemperature.Value ) )
                            {
                                // if the candidate temperature is already accomplished, clear its value
                                this.CandidateSetpointTemperature = new double?();
                            }
                        }
                        else
                        {
                            // if value is incorrect, must do a repeat write
                            result = (false, true, $"Incorrect value. Clearing written setpoint at '{stateIdentity}' state to force a new write");
                            _ = this.PublishDefaultErrorLevel( $"{result.Details};. " );
                            this._SetpointWritten = null;
                        }
                    }
                    else
                    {
                        // if error, read again after the next sample interval.
                        result = this.LastInterfaceErrorContinuousCount < this.ContinuousFailureCountTryAgainLimit ? (false, true, $"Failed {activity}: {this.LastInterfaceErrorDetails}; trying again") : (false, false, $"Failed {activity}: {this.LastInterfaceErrorDetails}; aborting");
                        _ = this.PublishDefaultErrorLevel( $"{result.Details};. " );
                    }
                }
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                result = (false, true, activity);
            }

            return result;
        }

        /// <summary> Reads input error. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="stateIdentity"> The state identity. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private (bool Success, bool TryAgain, string Details) ReadInputError( SoakState stateIdentity )
        {
            string activity = string.Empty;
            (bool Success, bool TryAgain, string Details) result;
            try
            {
                // if starting, input error is none. So read the input error to determine the status of the sensors.
                activity = $"reading input error at '{stateIdentity}' state";
                _ = this.PublishVerbose( $"{activity};. " );
                _ = this.ReadInputError();
                if ( this.InputErrorRead == InputError.None )
                {
                    if ( this.LastInterfaceError == InterfaceError.NoError )
                    {
                        activity = $"Failed {activity} without interface error";
                        result = (false, true, activity);
                    }
                    else
                    {
                        result = this.LastInterfaceErrorContinuousCount < this.ContinuousFailureCountTryAgainLimit ? (false, true, $"Failed {activity}: {this.LastInterfaceErrorDetails}; trying again") : (false, false, $"Failed {activity}: {this.LastInterfaceErrorDetails}; aborting");
                    }

                    _ = this.PublishDefaultErrorLevel( $"{result.Details};. " );
                }
                else if ( this.InputErrorRead != InputError.NoError )
                {
                    activity = $"failed {activity} with input error '{this.InputErrorRead.Description()}'";
                    result = this.LastInterfaceErrorContinuousCount < this.ContinuousFailureCountTryAgainLimit ? (false, true, $"{activity}; trying again") : (false, false, $"{activity}; aborting");
                    _ = this.PublishDefaultErrorLevel( $"{result.Details};. " );
                }
                else
                {
                    activity = $"Success {activity}: '{this.InputErrorRead.Description()}'";
                    _ = this.PublishVerbose( $"{activity};. " );
                    result = (true, false, activity);
                }
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                result = (false, true, activity);
            }

            return result;
        }

        private int _OvenReadyContinuousFailureCount;

        /// <summary> Gets or sets the number of oven ready continuous failures. </summary>
        /// <value> The number of oven ready continuous failures. </value>
        public int OvenReadyContinuousFailureCount
        {
            get => this._OvenReadyContinuousFailureCount;

            set {
                if ( value != this.OvenReadyContinuousFailureCount )
                {
                    this._OvenReadyContinuousFailureCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Turn alarm off. </summary>
        /// <remarks>
        /// David, 2020-11-11. <para>
        /// The alarm signal is used to toggle the oven state on or off depending on the oven function.
        /// <see cref="IsOvenReady()"/> indicate if the function was a success.
        /// </para>
        /// </remarks>
        /// <param name="stateIdentity"> The state identity. </param>
        /// <returns> A SoakAutomatonSymbol? </returns>
        private (bool Success, bool TryAgain, string Details) TurnAlarmOff( SoakState stateIdentity )
        {
            (bool Success, bool TryAgain, string Details) result;
            string activity = string.Empty;
            try
            {
                if ( this.AlarmTypeWritten == AlarmType.Off )
                {

                    // if process was set to the controller, read alarm type to validate.
                    activity = $"reading alarm type at '{stateIdentity}' state";
                    _ = this.PublishVerbose( $"{activity};. " );
                    if ( this.AlarmTypeRead == AlarmType.Off )
                    {
                        // T = 6 * T
                        // if oven is quiet, allow moving on
                        if ( this.IsOvenReady() )
                        {
                            activity = $"Oven control mode {this.ActiveSetpointInfo.OvenControlMode} allows skipping turning oven on at '{stateIdentity}' state";
                            _ = this.PublishVerbose( $"{activity};. " );
                            result = (true, false, activity);
                        }
                        else
                        {
                            activity = $"Oven control mode {this.ActiveSetpointInfo.OvenControlMode}. Oven not ready at '{stateIdentity}' state";
                            _ = this.PublishInfo( $"{activity};. " );
                            result = (false, true, activity);
                        }
                    }
                    else
                    {

                        // t = 5 * T
                        _ = this.ReadAlarmType();
                        if ( this.LastInterfaceError == InterfaceError.NoError )
                        {
                            if ( this.ReadAlarmType() == AlarmType.Off )
                            {
                                // if oven is off, allow moving on the next cycle.
                                if ( this.IsOvenReady() )
                                {
                                    activity = $"Oven control mode {this.ActiveSetpointInfo.OvenControlMode} is as expected at '{stateIdentity}' state";
                                    _ = this.PublishVerbose( $"{activity};. " );
                                    result = (true, false, activity);
                                }
                                else
                                {
                                    activity = $"Oven control mode {this.ActiveSetpointInfo.OvenControlMode} not expected. Oven not ready at '{stateIdentity}' state";
                                    _ = this.PublishDefaultErrorLevel( $"{activity};. " );
                                    result = (false, true, activity);
                                }
                            }
                            else
                            {
                                activity = $"Failed setting alarm type to {AlarmType.Off} at '{stateIdentity}' state. trying again.";
                                _ = this.PublishDefaultErrorLevel( $"{activity};. " );
                                result = (false, true, activity);
                                // if not set, turn off the previous value to force write on the next machine cycle
                                this._AlarmTypeWritten = AlarmType.None;
                            }
                        }
                        else
                        {
                            activity = $"Failed {activity}: {this.LastInterfaceErrorDetails}";
                            result = this.LastInterfaceErrorContinuousCount < this.ContinuousFailureCountTryAgainLimit ? (false, true, $"{activity}; trying again") : (false, false, $"{activity}; aborting");
                            _ = this.PublishDefaultErrorLevel( $"{result.Details};. " );
                        }
                    }
                    // clear the requesting messages.
                    if ( result.Success && this.IsTargetOvenControlModeChanged() )
                    {
                        // if the new target was accomplished, update the control mode.
                        this.ActiveSetpointInfo.OvenControlMode = this.TargetSetpointInfo.OvenControlMode;
                    }
                    // clear any candidate control mode
                    this.CandidateOvenControlMode = new OvenControlMode?();
                    this.OvenReadyContinuousFailureCount = result.Success ? 0 : this.OvenReadyContinuousFailureCount + 1;
                }
                else
                {
                    activity = $"Setting alarm type to '{AlarmType.Off.Description()}' at '{stateIdentity}' state";
                    _ = this.PublishVerbose( $"{activity};. " );
                    _ = this.WriteAlarmType( AlarmType.Off );
                    if ( this.LastInterfaceError == InterfaceError.NoError )
                    {
                        activity = $"Success {activity}";
                        result = (true, false, activity);
                        _ = this.PublishVerbose( $"{activity};. " );
                    }
                    else
                    {
                        activity = $"Failed {activity}: {this.LastInterfaceErrorDetails}";
                        result = this.LastInterfaceErrorContinuousCount < this.ContinuousFailureCountTryAgainLimit ? (false, true, $"{activity}; trying again") : (false, false, $"{activity}; aborting");
                        _ = this.PublishDefaultErrorLevel( $"{result.Details};. " );
                    }
                }
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                result = (false, true, activity);
            }

            return result;
        }

        /// <summary> Turn alarm on. </summary>
        /// <remarks>
        /// David, 2020-11-11. <para>
        /// The alarm signal is used to toggle the oven state on or off depending on the oven function.
        /// <see cref="IsOvenReady()"/> indicate if the function was a success.
        /// </para>
        /// </remarks>
        /// <param name="stateIdentity"> The state identity. </param>
        /// <returns> A SoakAutomatonSymbol? </returns>
        private (bool Success, bool TryAgain, string Details) TurnAlarmOn( SoakState stateIdentity )
        {
            (bool Success, bool TryAgain, string Details) result;
            string activity = string.Empty;
            try
            {
                if ( this.AlarmTypeWritten == AlarmType.Process )
                {
                    // t = 5 * T
                    // if process was set to the controller, read alarm type to validate.
                    activity = $"reading written {AlarmType.Process} alarm type at '{stateIdentity}' state";
                    _ = this.PublishVerbose( $"{activity};. " );
                    if ( this.AlarmTypeRead == AlarmType.Process )
                    {
                        // t = 6 * T
                        // if alarm type set, try to move to the next state
                        if ( this.IsOvenReady() )
                        {
                            activity = $"Oven control mode {this.ActiveSetpointInfo.OvenControlMode} is as expected at '{stateIdentity}' state";
                            _ = this.PublishVerbose( $"{activity};. " );
                            result = (true, false, activity);
                        }
                        else
                        {
                            activity = $"Oven control mode {this.ActiveSetpointInfo.OvenControlMode} but oven is not ready at '{stateIdentity}' state";
                            _ = this.PublishInfo( $"{activity};. " );
                            result = (false, true, activity);
                        }
                    }
                    else
                    {
                        _ = this.ReadAlarmType();
                        if ( this.LastInterfaceError == InterfaceError.NoError )
                        {
                            if ( this.AlarmTypeRead == AlarmType.Process )
                            {
                                // if alarm type set, try to move to the next state
                                if ( this.IsOvenReady() )
                                {
                                    activity = $"Oven control mode {this.ActiveSetpointInfo.OvenControlMode} is as expected at '{stateIdentity}' state";
                                    _ = this.PublishVerbose( $"{activity};. " );
                                    result = (true, false, activity);
                                }
                                else
                                {
                                    activity = $"Oven control mode {this.ActiveSetpointInfo.OvenControlMode} not expected. Oven not ready at '{stateIdentity}' state";
                                    _ = this.PublishDefaultErrorLevel( $"{activity};. " );
                                    result = (false, true, activity);
                                }
                            }
                            else
                            {
                                activity = $"Failed {activity}; trying again.";
                                result = (false, true, activity);
                                _ = this.PublishDefaultErrorLevel( $"{activity};. " );
                                // if not set, turn off the previous value to force write on the next machine cycle
                                this._AlarmTypeWritten = AlarmType.None;
                            }
                        }
                        else
                        {
                            activity = $"failed {activity}: {this.LastInterfaceErrorDetails}";
                            result = this.LastInterfaceErrorContinuousCount < this.ContinuousFailureCountTryAgainLimit ? (false, true, $"{activity}; trying again") : (false, false, $"{activity}; aborting");
                            _ = this.PublishDefaultErrorLevel( $"{result.Details};. " );
                        }
                    }
                    // clear the requesting messages.
                    if ( result.Success && this.IsTargetOvenControlModeChanged() )
                    {
                        // if the new target was accomplished, update the control mode.
                        this.ActiveSetpointInfo.OvenControlMode = this.TargetSetpointInfo.OvenControlMode;
                    }
                    // clear any candidate control mode
                    this.CandidateOvenControlMode = new OvenControlMode?();
                    this.OvenReadyContinuousFailureCount = result.Success ? 0 : this.OvenReadyContinuousFailureCount + 1;
                }
                else
                {
                    activity = $"Setting alarm type to '{AlarmType.Process.Description()}' at '{stateIdentity}' state";
                    _ = this.PublishVerbose( $"{activity};. " );
                    _ = this.WriteAlarmType( AlarmType.Process );
                    if ( this.LastInterfaceError == InterfaceError.NoError )
                    {
                        activity = $"Success {activity}";
                        result = (true, false, activity);
                    }
                    else
                    {
                        activity = $"Failed {activity}: {this.LastInterfaceErrorDetails}";
                        result = this.LastInterfaceErrorContinuousCount < this.ContinuousFailureCountTryAgainLimit ? (false, true, $"{activity}; trying again") : (false, false, $"{activity}; aborting");
                        _ = this.PublishDefaultErrorLevel( $"{result.Details};. " );
                    }
                }
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                result = (false, true, activity);
            }

            return result;
        }

        /// <summary> Updates the oven control mode described by sender. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="stateIdentity"> The state identity. </param>
        /// <param name="functionInfo">  Information describing the function. </param>
        /// <returns> A SoakAutomatonSymbol? </returns>
        private (bool Success, bool TryAgain, string Details) UpdateOvenControlMode( SoakState stateIdentity, OvenFunctionInfo functionInfo )
        {
            (bool Success, bool TryAgain, string Details) result = (true, false, string.Empty);
            if ( functionInfo is null )
                throw new ArgumentNullException( nameof( functionInfo ) );
            if ( this.InputErrorRead == InputError.None )
            {
                result = this.ReadInputError( stateIdentity );
            }
            else if ( functionInfo.IsQuiet() )
            {

                // oven monitoring is on but fans are off.
                // t = 4 * T
                result = this.TurnAlarmOff( stateIdentity );
                if ( this.CandidateOvenControlMode.HasValue && new OvenFunctionInfo( this.CandidateOvenControlMode.Value ).IsQuiet() )
                {
                    // if the interim request already accomplished, clear the request
                    this.CandidateOvenControlMode = new OvenControlMode?();
                    // wrong candidate? Also, already done in the function? 
                    // Me.CandidateSetpointTemperature = New Double?
                }
            }

            // t = 6 * T or 5 * T

            else if ( functionInfo.IsOn() )
            {

                // t = 4 * T
                result = this.TurnAlarmOn( stateIdentity );
                if ( this.CandidateOvenControlMode.HasValue && new OvenFunctionInfo( this.CandidateOvenControlMode.Value ).IsOn() )
                {
                    // if the interim request already accomplished, clear the request
                    this.CandidateOvenControlMode = new OvenControlMode?();
                    // wrong candidate? Also, already done in the function? 
                    // Me.CandidateSetpointTemperature = New Double?
                }
            }
            // t = 6 * T or 5 * T

            else
            {
                Debug.Assert( !Debugger.IsAttached, $"Unhandled case in Soak Automation {stateIdentity} state; Oven function is neither on or off" );
            }

            return result;
        }

        /// <summary> Updates the setpoint information. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        private void UpdateSetpointInfo()
        {
            if ( this.TargetSetpointInfo is null )
            {
            }
            // if no target, move on.
            else if ( this.ActiveSetpointInfo is null )
            {
                this.ActiveSetpointInfo = new SetpointInfo( this.TargetSetpointInfo );
                this.TargetSetpointInfo = null;
            }
            else if ( this.ActiveSetpointInfo.Equals( this.TargetSetpointInfo ) )
            {
                // if all changes were applied, clear the target setpoint
                this.TargetSetpointInfo = null;
            }
            else
            {
                // if the setpoint needs to be set, apply the immediate changes
                this.ActiveSetpointInfo.Window = this.TargetSetpointInfo.Window;
                this.ActiveSetpointInfo.RampTimeout = this.TargetSetpointInfo.RampTimeout;
                this.ActiveSetpointInfo.Hysteresis = this.TargetSetpointInfo.Hysteresis;
                this.ActiveSetpointInfo.SampleInterval = this.TargetSetpointInfo.SampleInterval;
                this.ActiveSetpointInfo.SoakDuration = this.TargetSetpointInfo.SoakDuration;
                this.ActiveSetpointInfo.SoakResetDelay = this.TargetSetpointInfo.SoakResetDelay;
                this.ActiveSetpointInfo.SetpointTemperature = this.TargetSetpointInfo.SetpointTemperature;
                this.ActiveSetpointInfo.OvenControlMode = this.TargetSetpointInfo.OvenControlMode;
            }

            this.UpdateSoakEngineInfo( this.ActiveSetpointInfo );
        }

        /// <summary> Has oven control mode candidate. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <returns> A (Affirmative As Boolean, functionInfo As OvenFunctionInfo) </returns>
        private (bool Affirmative, OvenControlMode OvenControlMode) HasOvenControlModeCandidate()
        {
            return this.IsTargetOvenControlModeChanged()
                ? (true, this.TargetSetpointInfo.OvenControlMode)
                : this.IsCandidateOvenControlModeChanged()
                    ? (true, this.CandidateOvenControlMode.Value)
                    : (false, this.ActiveSetpointInfo.OvenControlMode);
        }

        /// <summary> Updates the oven control mode described by sender. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <param name="stateIdentity"> The state identity. </param>
        /// <returns> A SoakAutomatonSymbol? </returns>
        private (bool Success, bool TryAgain, string Details) UpdateOvenControlMode( SoakState stateIdentity )
        {
            this.ActiveSetpointInfo.OvenControlMode = this.HasOvenControlModeCandidate().OvenControlMode;
            return this.UpdateOvenControlMode( stateIdentity, this.ActiveSetpointInfo.OvenFunctionInfo );
        }

        /// <summary> Query if this object has target temperature candidate. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <returns> True if target temperature candidate, false if not. </returns>
        private (bool Affirmative, double Value) HasTargetTemperatureCandidate()
        {
            return this.IsTargetSetpointTemperatureChanged()
                ? (true, this.TargetSetpointInfo.SetpointTemperature)
                : this.IsCandidateSetpointChanged()
                    ? (true, this.CandidateSetpointTemperature.Value)
                    : (false, this.ActiveSetpointInfo.SetpointTemperature);
        }

        /// <summary> Updates the target temperature described by stateIdentity. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <param name="stateIdentity"> The state identity. </param>
        /// <returns> The (Success As Boolean, TryAgain As Boolean, Details As String) </returns>
        private (bool Success, bool TryAgain, string Details) UpdateSetpoint( SoakState stateIdentity )
        {
            this.ActiveSetpointInfo.SetpointTemperature = this.HasTargetTemperatureCandidate().Value;
            return this.ActiveSetpointInfo.IsAtSetpoint( this.SetpointRead ) ? (true, false, "setpoint already set") : this.UpdateSetpoint( stateIdentity, this.ActiveSetpointInfo.SetpointTemperature );
        }

        /// <summary> Pause soak automaton. </summary>
        /// <remarks> David, 2020-11-24. </remarks>
        public void PauseSoakAutomaton()
        {
            this.SoakEngine.Pause();
        }

        /// <summary> Resume soak automaton. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public void ResumeSoakAutomaton()
        {
            this.SoakEngine.Resume();
        }

        #endregion

        #region " SOAK ENGINE "

        /// <summary> Gets or sets the Soak engine. </summary>
        /// <value> The Soak engine. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SoakEngine SoakEngine { get; private set; }

        /// <summary> Assign Soak Engine. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignSoakEngine( SoakEngine value )
        {
            if ( this.SoakEngine is object )
            {
                this.SoakEngine.UnregisterEngineFailureAction( e => this.TriggerSoakFailed( e ) );
                if ( this.SoakEngine?.IsActive() == true )
                {
                    this.SoakEngine.StopRequested = true;
                    _ = ApplianceBase.DoEventsWaitUntil( TimeSpan.FromMilliseconds( 100d ), () => !this.SoakEngine.IsActive() );
                }

                this.SoakEngine?.Dispose();
                this.SoakEngine = null;
            }

            this.SoakEngine = value;
            if ( value is object )
            {
                this.SoakEngine.RegisterEngineFailureAction( e => this.TriggerSoakFailed( e ) );
                this.SoakEngine.UsingFireAsync = true;
                this.SoakEngine.StateMachine.OnTransitionedAsync( ( t ) => Task.Run( () => this.OnTransitioned( t ) ) );
            }
        }

        /// <summary> Handles the state entry actions. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="transition"> The transition. </param>
        private void OnTransitioned( StateMachine<SoakState, SoakTrigger>.Transition transition )
        {
            string activity = string.Empty;
            try
            {
                if ( transition is null )
                {
                    activity = this.PublishVerbose( $"Entering the {this.SoakEngine.StateMachine.State} state" );
                    this.OnActivated( this.SoakEngine.StateMachine.State );
                }
                else
                {
                    activity = this.PublishVerbose( $"transitioning {this.SoakEngine.Name}: {transition.Source} --> {transition.Destination}" );
                    if ( transition.IsReentry )
                    {
                        this.OnReentry( transition.Destination );
                    }
                    else
                    {
                        this.OnExit( transition.Source );
                        this.OnActivated( transition.Destination );
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Executes the 'exit' action. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="source"> Source for the. </param>
        private void OnExit( SoakState source )
        {
            switch ( source )
            {
                case SoakState.Soak:
                    {
                        break;
                    }
            }
        }

        /// <summary>
        /// Executes the 'reentry' action. Is used to update the setpoint information and take new
        /// readings.
        /// </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="destination"> Destination for the. </param>
        private void OnReentry( SoakState destination )
        {
            string activity = $"{this.SoakEngine.Name} reentered {destination.Description()} state";
            (bool Success, bool TryAgain, string Details) result;
            try
            {
                // update the setpoint info
                this.UpdateSetpointInfo();
                // If Me.TargetSetpointInfo Is Nothing Then Return
                switch ( destination )
                {
                    case SoakState.Idle:
                        {
                            if ( this.SoakEngine.IsSampleIntervalElapsed() )
                            {
                                this.SoakEngine.OnSampleHandled();
                            }

                            break;
                        }

                    case SoakState.Engaged:
                        {
                            if ( this.SoakEngine.IsSampleIntervalElapsed() )
                            {
                                this.SoakEngine.OnSampleHandled();
                                // update setpoint if changed
                                result = this.ActiveSetpointInfo.IsAtSetpoint( this.SetpointRead ) ? (true, false, string.Empty) : this.UpdateSetpoint( destination );
                                if ( result.Success && this.ActiveSetpointInfo.IsAtSetpoint( this.SetpointRead ) )
                                {
                                    this.SoakEngine.ApplySetpoint( this.SetpointRead.Value );
                                }
                                else if ( !(result.Success || result.TryAgain) )
                                {
                                    this.SoakEngine.OnEngineFailed( new OperationFailedException( $"{result.Details} @{Environment.StackTrace}" ) );
                                }
                            }

                            break;
                        }

                    case SoakState.HasSetpoint:
                        {
                            if ( this.SoakEngine.IsSampleIntervalElapsed() )
                            {
                                this.SoakEngine.OnSampleHandled();
                                result = this.IsOvenReady() ? (true, false, string.Empty) : this.UpdateOvenControlMode( destination );
                                if ( result.Success && this.IsOvenReady() )
                                {
                                    this.SoakEngine.Fire( SoakTrigger.Ramp );
                                }
                                else if ( !(result.Success || result.TryAgain) )
                                {
                                    this.SoakEngine.OnEngineFailed( new OperationFailedException( $"{result.Details} @{Environment.StackTrace}" ) ); // @{NameOf(EasyZone).Length{NameOf(EasyZone.OnReentry)}"))
                                }
                            }

                            break;
                        }

                    case SoakState.Ramp:
                    case SoakState.Soak:
                    case SoakState.OffSoak:
                    case SoakState.AtTemp:
                    case SoakState.OffTemp:
                        {
                            if ( this.SoakEngine.IsSampleIntervalElapsed() )
                            {
                                this.SoakEngine.OnSampleHandled();
                                if ( this.HasTargetTemperatureCandidate().Affirmative )
                                {
                                    result = this.UpdateSetpoint( destination );
                                    if ( !(result.Success || result.TryAgain) )
                                    {
                                        this.SoakEngine.OnEngineFailed( new OperationFailedException( $"{result.Details} @{Environment.StackTrace}" ) );
                                    }
                                }
                                else if ( this.TryReadTemperature().Success )
                                {
                                    this.SoakEngine.ApplyTemperature( this.Temperature.Value );
                                }
                            }

                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"info {ex.Data?.Count}", $"Exception {activity}" );
                this.OnHandlerException( ex );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                // The process timer must be resumed because it is paused for handling the process. 
                this.SoakEngine.Resume();
            }
        }

        /// <summary> Executes the 'activated' action. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="destination"> Destination for the. </param>
        private void OnActivated( SoakState destination )
        {
            string activity = string.Empty;
            switch ( destination )
            {
                case SoakState.Idle:
                    {
                        this.Temperature = null;
                        this.AnalogInputRead = null;
                        this.SetpointRead = null;
                        this.SetpointWritten = null;
                        this.AlarmTypeRead = AlarmType.None;
                        this.AlarmTypeWritten = AlarmType.None;
                        activity = "Soak automaton inactive";
                        break;
                    }

                case SoakState.Engaged:
                    {
                        activity = "Soak automaton started";
                        break;
                    }

                case SoakState.HasSetpoint:
                    {
                        activity = $"Setpoint @{this.SetpointRead}";
                        break;
                    }

                case SoakState.Ramp:
                    {
                        activity = $"Ramping started; Oven control @{this.ActiveSetpointInfo.OvenControlMode}";
                        break;
                    }

                case SoakState.Soak:
                    {
                        activity = "Soaking started";
                        break;
                    }

                case SoakState.OffSoak:
                    {
                        activity = "Soaking ended";
                        break;
                    }

                case SoakState.AtTemp:
                    {
                        activity = $"Soak temp is {this.Temperature:G4)}";
                        break;
                    }

                case SoakState.OffTemp:
                    {
                        activity = $"Soak temp is {this.Temperature:G4)}";
                        break;
                    }

                case SoakState.Terminal:
                    {
                        activity = "Soak automaton stopped";
                        break;
                    }
            }

            _ = this.PublishInfo( $"{activity} at the '{destination}' state;. " );
            if ( this.SoakEngine.StopRequested )
                this.SoakEngine.ProcessStopRequest();
        }

        /// <summary> Trigger Soak failed. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="e"> Error event information. </param>
        private void TriggerSoakFailed( System.IO.ErrorEventArgs e )
        {
            _ = e is null
                ? this.PublishWarning( $"{this.SoakEngine.Name} failed;. {this.SoakEngine.StateMachineInfo}" )
                : this.PublishException( $"{this.SoakEngine.Name} failed;. {this.SoakEngine.StateMachineInfo}", e.GetException() );

            this.SoakEngine.Terminate();
        }

        #region " COMMANDS "

        /// <summary> Engages the Soak engine using the given setpoint information. </summary>
        /// <remarks> David, 2020-11-24. </remarks>
        /// <param name="setpointInfo"> Information describing the setpoint. </param>
        public void Engage( SetpointInfo setpointInfo )
        {
            this.UpdateSoakEngineInfo( setpointInfo );
            this.SoakEngine.Engage( setpointInfo.TemperatureSampleInterval );
        }

        /// <summary> Updates the soak engine information described by setpointInfo. </summary>
        /// <remarks> David, 2020-11-24. </remarks>
        /// <param name="setpointInfo"> Information describing the setpoint. </param>
        public void UpdateSoakEngineInfo( SetpointInfo setpointInfo )
        {
            this.ActiveSetpointInfo = new SetpointInfo( setpointInfo );
            this.SoakEngine.Window = setpointInfo.Window;
            this.SoakEngine.TemperatureResolution = setpointInfo.SetpointEpsilon;
            this.SoakEngine.RampTimeout = setpointInfo.RampTimeout;
            this.SoakEngine.Hysteresis = setpointInfo.Hysteresis;
            this.SoakEngine.SampleInterval = setpointInfo.SampleInterval;
            this.SoakEngine.SoakDuration = setpointInfo.SoakDuration;
            this.SoakEngine.AllowedOffStateDuration = setpointInfo.SoakResetDelay;
            this.SoakEngine.AllowedOffStateCount = 0;
            this.SoakEngine.SoakCount = 0;
        }

        #endregion


        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the
        /// message.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        /// <summary> Publish default. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <returns> A String. </returns>
        protected string PublishDefaultErrorLevel( string activity )
        {
            return this.Publish( new TraceMessage( this.ModbusErrorTraceEventType, My.MyLibrary.TraceEventId, activity ) );
        }


        #endregion

    }

    /// <summary> Values that represent alarm type. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    public enum AlarmType
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [Description( "Not specified" )]
        None = 0,

        /// <summary>   An enum constant representing the off option. </summary>
        [Description( "Off" )]
        Off = 62,

        /// <summary>   An enum constant representing the process option. </summary>
        [Description( "Process" )]
        Process = 76,

        /// <summary>   An enum constant representing the deviation option. </summary>
        [Description( "Deviation" )]
        Deviation = 24
    }

    /// <summary> Values that represent input errors. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    public enum InputError
    {

        /// <summary>   An enum constant representing the not specified option. </summary>
        [Description( "Not specified" )]
        None = 0,

        /// <summary>   An enum constant representing the no error option. </summary>
        [Description( "No Error" )]
        NoError = 61,

        /// <summary>   An enum constant representing the open option. </summary>
        [Description( "Open" )]
        Open = 65,

        /// <summary>   An enum constant representing the fail option. </summary>
        [Description( "Fail" )]
        Fail = 32,

        /// <summary>   An enum constant representing the shorted option. </summary>
        [Description( "Shorted" )]
        Shorted = 127,

        /// <summary>   An enum constant representing the measurement error option. </summary>
        [Description( "Measurement Error" )]
        MeasurementError = 140,

        /// <summary>   An enum constant representing the bad calibration data option. </summary>
        [Description( "Bad Calibration Data" )]
        BadCalibrationData = 139,

        /// <summary>   An enum constant representing the ambient error option. </summary>
        [Description( "Ambient Error" )]
        AmbientError = 9,

        /// <summary>   An enum constant representing the rtd error option. </summary>
        [Description( "RTD Error" )]
        RtdError = 141,

        /// <summary>   An enum constant representing the not sourced option. </summary>
        [Description( "Not Sourced" )]
        NotSourced = 246
    }
}
