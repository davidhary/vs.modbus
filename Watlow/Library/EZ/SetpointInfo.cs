using System;

using isr.Core.TimeSpanExtensions;

namespace isr.Modbus.Watlow
{

    /// <summary> Information about the setpoint. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-8-22 </para>
    /// </remarks>
    public class SetpointInfo
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public SetpointInfo() : base()
        {
            this.SetpointEpsilon = 0.05d;
            this.TemperatureUnit = Arebis.StandardUnits.TemperatureUnits.DegreeFahrenheit;
        }

        /// <summary> The copy constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> The value. </param>
        public SetpointInfo( SetpointInfo value ) : this()
        {
            if ( value is object )
            {
                this.TemperatureUnit = value.TemperatureUnit;
                this.SetpointEpsilon = value.SetpointEpsilon;
                this.SetpointTemperature = value.SetpointTemperature;
                this.Window = value.Window;
                this.RampTimeout = value.RampTimeout;
                this.OvenControlMode = value.OvenControlMode;
                this.Hysteresis = value.Hysteresis;
                this.SampleInterval = value.SampleInterval;
                this.SoakDuration = value.SoakDuration;
                this.SoakResetDelay = value.SoakResetDelay;
            }
        }

        /// <summary> The constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="temperatureUnit">     The temperature unit. </param>
        /// <param name="setpointTemperature"> The setpoint temperature in the
        /// <paramref name="temperatureUnit"/> . </param>
        /// <param name="sampleInterval">      The sample interval. </param>
        /// <param name="soakDuration">        The soak duration. </param>
        /// <param name="window">              The window. </param>
        /// <param name="hysteresis">          The hysteresis. </param>
        /// <param name="soakResetDelay">      The soak reset delay. </param>
        /// <param name="ovenControlMode">     The oven control mode. </param>
        /// <param name="rampTimeout">         The ramp timeout. </param>
        public SetpointInfo( Arebis.TypedUnits.Unit temperatureUnit, double setpointTemperature, TimeSpan sampleInterval, TimeSpan soakDuration, double window, double hysteresis, TimeSpan soakResetDelay, OvenControlMode ovenControlMode, TimeSpan rampTimeout ) : this()
        {
            this.TemperatureUnit = temperatureUnit;
            this.SetpointTemperature = setpointTemperature;
            this.RampTimeout = rampTimeout;
            this.OvenControlMode = ovenControlMode;
            this.SampleInterval = sampleInterval;
            this.SoakDuration = soakDuration;
            this.Window = window;
            this.Hysteresis = hysteresis;
            this.SoakResetDelay = soakResetDelay;
        }

        /// <summary> Tests if this SetpointInfo is considered equal to another. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="other"> The setpoint information to compare to this object. </param>
        /// <returns> True if the objects are considered equal, false if they are not. </returns>
        public bool Equals( SetpointInfo other )
        {
            return other is object && Equals( this.TemperatureUnit, other.TemperatureUnit ) && Equals( this.SetpointEpsilon, other.SetpointEpsilon ) && this.SetpointApproximates( this.SetpointTemperature, other.SetpointTemperature ) && this.RampTimeout.Approximates( other.RampTimeout, TimeSpan.FromSeconds( 0.1d ) ) && this.OvenControlMode == other.OvenControlMode && this.SampleInterval.Approximates( other.SampleInterval, TimeSpan.FromSeconds( 0.1d ) ) && this.SoakDuration.Approximates( other.SoakDuration, TimeSpan.FromSeconds( 0.1d ) ) && Math.Abs( this.Window - other.Window ) < 0.1d && Math.Abs( this.Hysteresis - other.Hysteresis ) < 0.1d && this.SoakResetDelay.Approximates( other.SoakResetDelay, TimeSpan.FromSeconds( 0.1d ) );
        }

        /// <summary> Query if 'value' is at setpoint. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value">   The value. </param>
        /// <returns>
        /// <c>
        /// true</c>
        /// if at setpoint; otherwise <c>
        /// false</c>
        /// </returns>
        public bool IsAtSetpoint( Arebis.TypedUnits.Amount value )
        {
            return value is object && this.SetpointApproximates( value.Value );
        }

        /// <summary>
        /// Gets or sets the minimum observable different of the control temperature in the <see cref="TemperatureUnit"/>, which
        /// determines how accurate the setpoint must be set at.
        /// </summary>
        /// <value> The setpoint control temperature epsilon. </value>
        public double SetpointEpsilon { get; set; }

        /// <summary>
        /// Query if the value equals the setpoint temperature withing the <see cref="SetpointEpsilon"/>.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if approximately equals; otherwise <c>false</c> </returns>
        public bool SetpointApproximates( double value )
        {
            return this.SetpointApproximates( value, this.SetpointTemperature );
        }

        /// <summary> Queries if the values are equal within the <see cref="SetpointEpsilon"/>. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="leftHandValue">  The left hand value. </param>
        /// <param name="rightHandValue"> The right hand value. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool SetpointApproximates( double leftHandValue, double rightHandValue )
        {
            return Approximates( leftHandValue, rightHandValue, this.SetpointEpsilon );
        }

        /// <summary> Approximates. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="leftHandValue">  The left hand value. </param>
        /// <param name="rightHandValue"> The right hand value. </param>
        /// <param name="epsilon">        The epsilon. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Approximates( double leftHandValue, double rightHandValue, double epsilon )
        {
            return Math.Abs( leftHandValue - rightHandValue ) <= epsilon;
        }

        /// <summary> Gets or sets the setpoint target temperature in the <see cref="TemperatureUnit"/>. </summary>
        /// <value> The target setpoint temperature. </value>
        public double SetpointTemperature { get; set; }

        /// <summary> Gets or sets the target window around the setpoint temperature. </summary>
        /// <value> The target window around the setpoint temperature. </value>
        public double Window { get; set; }

        /// <summary> Gets or sets the ramp timeout. </summary>
        /// <value> The ramp timeout. </value>
        public TimeSpan RampTimeout { get; set; }

        /// <summary> Checks if ramp timeout elapsed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="elapsedTime"> The elapsed time. </param>
        /// <returns> <c>true</c> if ramp timeout elapsed; otherwise <c>false</c> </returns>
        public bool IsRampTimeoutElapsed( TimeSpan elapsedTime )
        {
            return this.RampTimeout > TimeSpan.Zero && elapsedTime > this.RampTimeout;
        }

        /// <summary> The oven control mode. </summary>
        private OvenControlMode _OvenControlMode;

        /// <summary> Gets or sets the oven control mode. </summary>
        /// <value> The oven control mode. </value>
        public OvenControlMode OvenControlMode
        {
            get => this._OvenControlMode;

            set {
                this._OvenControlMode = value;
                this.OvenFunctionInfo = new OvenFunctionInfo( value );
            }
        }

        /// <summary> Gets information describing the oven function. </summary>
        /// <value> Information describing the oven function. </value>
        public OvenFunctionInfo OvenFunctionInfo { get; private set; }

        /// <summary> Gets the hysteresis. </summary>
        /// <value> The hysteresis. </value>
        public double Hysteresis { get; set; }

        /// <summary> Gets the duration of the soak. </summary>
        /// <value> The soak duration. </value>
        public TimeSpan SoakDuration { get; set; }

        /// <summary> Gets the sample interval. </summary>
        /// <value> The sample interval. </value>
        public TimeSpan SampleInterval { get; set; }

        /// <summary> Gets the soak sequencer interval. </summary>
        /// <remarks>
        /// This is the state machine interval. Initially set to 15 seconds but reduced here to as fast
        /// as the control loop runs.
        /// </remarks>
        /// <value> The soak sequencer interval. </value>
        public TimeSpan SoakSequencerInterval => this.SampleInterval;

        /// <summary> Gets the temperature sample interval. </summary>
        /// <remarks>
        /// This interval is used when oven control is off for determining if it is time to read the
        /// temperature. Initially set to 15 seconds but reduced here to as fast as the control loop runs.
        /// </remarks>
        /// <value> The temperature sample interval. </value>
        public TimeSpan TemperatureSampleInterval => this.SampleInterval;

        /// <summary> Gets or sets the soak reset delay. </summary>
        /// <value> The soak reset delay. </value>
        public TimeSpan SoakResetDelay { get; set; }

        /// <summary> Defines the unit of temperature measurement. </summary>
        /// <value> The unit. </value>
        public Arebis.TypedUnits.Unit TemperatureUnit { get; set; }
    }
}
