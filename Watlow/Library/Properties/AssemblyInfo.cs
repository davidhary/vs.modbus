﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Modbus.Watlow.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Modbus.Watlow.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Modbus.Watlow.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
