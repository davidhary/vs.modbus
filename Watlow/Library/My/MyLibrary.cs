﻿using isr.Core;

namespace isr.Modbus.Watlow.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Modbus.My.ProjectTraceEventId.WatlowLibrary;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Modbus Watlow Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Modbus Watlow Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Modbus.Watlow";

        /// <summary> Applies the given logger. </summary>
        /// <remarks> David, 2020-11-20. </remarks>
        /// <param name="logger"> The logger. </param>
        public static void Apply( Logger logger )
        {
            Appliance.Apply( logger );
            Modbus.My.MyLibrary.Appliance.Apply( logger );
        }
    }
}