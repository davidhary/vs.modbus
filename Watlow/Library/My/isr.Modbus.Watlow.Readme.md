## ISR Modbus Watlow<sub>&trade;</sub>: Modbus Watlow Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [About EZ-Zone](#About-EZ-Zone)

### Revision History [](#){name=Revision-History}

*1.2.7637 2020-11-27*  
Converted to C#.

*1.2.7636 2020-11-26*  
Uses Stateless state machine.

*1.2.7633 2020-11-23*  
Move the Easy Zone control to the Forms library.

*1.1.6440 2017-08-19*  
Uses modified trace message and logger.

*1.0.6290 2017-03-22*  
Adds a function to determine if the soak automaton is
staying At Temp..

*1.0.6289 2017-03-21*  
Adds transition from Off Soak to Ramp.

*1.0.6288 2017-03-20*  
Fixes soak time stop watch.

*1.0.6274 2017-03-06*  
Fixes how the target and interim setpoint changes
interact.

*1.0.6265 2017-02-25*  
Adds control of temporary or new setpoint during soak
automation to allow moving the setpoint.

*1.0.6220 2017-01-11*  
Reports buffer contents on failures and successes.
Streamline the soak machine algorithm. Updates assembly product name to
2017.

*1.0.6170 2016-11-22*  
Uses cancel details event arguments in place of
reference strings to report failures reading the bus.

*1.0.6064 2016-08-08*  
Updates soak state machine: Clears values on entering
idle state; Moves setpoint to idle state; Moves oven on to Ready state.
Adds oven control mode to allow setting a setpoint with oven off.

*1.0.6047 2016-07-22*  
Changes code for reading byte from the serial port and
determining the end of record based. Uses time span type for timing.
Adds Soak state machine.

*1.0.5992 2016-05-28*  
Adds refractory time spans to all operations.

*1.0.5969 2016-05-07*  
Created.

\(C\) 2016 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Typed Units Libraries](https://bitbucket.org/davidhary/arebis.typedunits)  
[MODBUS Libraries](https://bitbucket.org/davidhary/vs.modbus)

### About EZ-Zone  [](#){name=About-EZ-Zone}
Additional information about EZ-Zone is available at the following sites:  
[Watlow resources and support](https://www.watlow.com/resources-and-support)

