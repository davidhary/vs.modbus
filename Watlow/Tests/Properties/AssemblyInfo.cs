﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Modbus.Watlow.Tests.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Modbus.Watlow.Tests.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Modbus.Watlow.Tests.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
