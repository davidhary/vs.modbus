﻿using System.Configuration;

namespace isr.Modbus.Watlow.Tests
{

    /// <summary> A RUT Client Test Info. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-2-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class RtuClientTestInfo : Core.ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        private RtuClientTestInfo() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(RtuClientTestInfo)} Editor", Get());
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static RtuClientTestInfo _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static RtuClientTestInfo Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (RtuClientTestInfo)Synchronized(new RtuClientTestInfo());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("True")]
        public bool Exists
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("False")]
        public bool Verbose
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("True")]
        public bool Enabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("True")]
        public bool All
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " RTU CLIENT INFORMATION "

        /// <summary> Name of the port. </summary>
        private string _PortName;

        /// <summary> Gets the name of the port. </summary>
        /// <value> The name of the port. </value>
        public string PortName
        {
            get
            {
                if (string.IsNullOrEmpty(_PortName))
                {
                    if (My.MyProject.Computer.Ports.SerialPortNames.Contains(FirstPortName))
                    {
                        _PortName = FirstPortName;
                    }
                    else if (My.MyProject.Computer.Ports.SerialPortNames.Contains(AlternatePortName))
                    {
                        _PortName = AlternatePortName;
                    }
                    else
                    {
                        _PortName = string.Empty;
                    }
                }

                return _PortName;
            }
        }

        /// <summary> Gets or sets the name of the first port. </summary>
        /// <value> The name of the first port. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("COM3")]
        public string FirstPortName
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the name of the alternate port. </summary>
        /// <value> The name of the alternate port. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("COM4")]
        public string AlternatePortName
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the baud rate. </summary>
        /// <value> The baud rate. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("38400")]
        public int BaudRate
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the identifier of the unit. </summary>
        /// <value> The identifier of the unit. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("1")]
        public byte UnitId
        {
            get
            {
                return AppSettingGetter((byte)0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the type of the alarm. </summary>
        /// <value> The type of the alarm. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("76")]
        public int AlarmType
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " MEASUREMENTS "

        /// <summary> Gets or sets the ambient temperature. </summary>
        /// <value> The ambient temperature. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("76")]
        public float AmbientTemperature
        {
            get
            {
                return AppSettingGetter(0f);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the ambient temperature epsilon. </summary>
        /// <value> The ambient temperature epsilon. </value>
        [UserScopedSetting()]
        [DefaultSettingValue("76")]
        public float AmbientTemperatureEpsilon
        {
            get
            {
                return AppSettingGetter(0f);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

    }
}