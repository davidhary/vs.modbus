using System;
using System.IO.Ports;
using System.Linq;

using isr.Core.BitConverterExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Modbus.Watlow.Tests
{

    /// <summary> A RTD client test. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-06 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "EZ-Zone" )]
    public class RtuClientTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Modbus.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
                Initialize();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
            Cleanup();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( RtuClientTestInfo.Get().Exists, $"{typeof( RtuClientTestInfo )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " RTU CLIENT: SHARED  "

        /// <summary> Gets or sets the identifier of the unit. </summary>
        /// <value> The identifier of the unit. </value>
        private static byte UnitId { get; set; } = RtuClientTestInfo.Get().UnitId;

        /// <summary> Gets or sets the name of the port. </summary>
        /// <value> The name of the port. </value>
        private static string PortName { get; set; } = RtuClientTestInfo.Get().PortName;

        /// <summary> Gets or sets the modbus client. </summary>
        /// <value> The modbus client. </value>
        private static ModbusClientSerial ModbusClient { get; set; }

        /// <summary> Runs code before running the first test in the class. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public static void Initialize()
        {
            // Crete instance of modbus serial RTU 
            ModbusClient = new ModbusClientSerial( ModbusSerialType.RTU, ModbusClientSerial.ConstructPort( PortName, 38400, 8, Parity.None, StopBits.One, Handshake.None ) );
            // Connect
            ModbusClient.Connect();
        }

        /// <summary> Cleanups this object. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public static void Cleanup()
        {
            // Disconnect
            ModbusClient?.Disconnect();
        }


        #endregion

        #region " RTD CLIENT TESTS "

        /// <summary> (Unit Test Method) connects the test. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void ConnectTest()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
        }

        /// <summary> (Unit Test Method) tests read watlow alarm 1 type. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void ReadWatlowAlarm1TypeTest()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 1508;
            var expectedValues = new ushort[] { 62, 76 };
            var receivedValues = ModbusClient.ReadHoldingRegisters( UnitId, registerAddress, 1 );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Read error" );
            Assert.IsTrue( expectedValues.Contains( receivedValues.First() ), $"{receivedValues.First()} should be either {expectedValues[0]} or {expectedValues[1]}" );
        }

        /// <summary> (Unit Test Method) tests try read watlow alarm 1 type. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void TryReadWatlowAlarm1TypeTest()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 1508;
            var expectedValues = new ushort[] { 62, 76 };
            ushort actualValue = 0;
            bool outcome = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
            Assert.AreEqual( true, outcome );
            Assert.IsTrue( expectedValues.Contains( actualValue ), $"{actualValue} should be either {expectedValues[0]} or {expectedValues[1]}" );
        }

        /// <summary> (Unit Test Method) writes the watlow alarm 1 type test. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void WriteWatlowAlarm1TypeTest()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 1508;
            var toggleOnTimeout = TimeSpan.FromSeconds( 3d );
            var toggleOffTimeout = TimeSpan.FromSeconds( 4d );
            ushort offValue = 62;
            ushort processValue = 76;
            ushort actualValue = 0;
            bool outcome = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
            Assert.AreEqual( true, outcome );
            if ( actualValue == offValue )
            {
                ModbusClient.WriteRegistersValue( UnitId, registerAddress, processValue );
                Core.ApplianceBase.DoEventsWait( toggleOnTimeout );
                _ = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
                Assert.AreEqual( processValue, actualValue );
                ModbusClient.WriteRegistersValue( UnitId, registerAddress, offValue );
                Core.ApplianceBase.DoEventsWait( toggleOffTimeout );
                _ = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
                Assert.AreEqual( offValue, actualValue );
            }
            else if ( actualValue == processValue )
            {
                ModbusClient.WriteRegistersValue( UnitId, registerAddress, offValue );
                Core.ApplianceBase.DoEventsWait( toggleOffTimeout );
                _ = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
                Assert.AreEqual( offValue, actualValue );
                ModbusClient.WriteRegistersValue( UnitId, registerAddress, processValue );
                Core.ApplianceBase.DoEventsWait( toggleOnTimeout );
                _ = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
                Assert.AreEqual( processValue, actualValue );
            }
            else
            {
                Assert.AreEqual( offValue, actualValue );
            }
        }

        /// <summary> (Unit Test Method) tests read watlow input error. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void ReadWatlowInputErrorTest()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 362;
            ushort expectedValue = 61;
            ushort actualValue = ModbusClient.ReadHoldingRegisters( UnitId, registerAddress, 1 ).First();
            Assert.AreEqual( expectedValue, actualValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Read error" );
        }

        /// <summary> (Unit Test Method) tests try read watlow input error. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void TryReadWatlowInputErrorTest()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 362;
            ushort expectedValue = 61;
            ushort actualValue = 0;
            bool outcome = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
            Assert.AreEqual( true, outcome );
            Assert.AreEqual( expectedValue, actualValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Read error" );
        }

        /// <summary> (Unit Test Method) tests try read analog input 1. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void TryReadAnalogInput1Test()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 360;
            float expectedValue = RtuClientTestInfo.Get().AmbientTemperature;
            float actualValue = 0f;
            _ = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Read error" );
            Assert.AreEqual( expectedValue, actualValue, RtuClientTestInfo.Get().AmbientTemperature );
        }

        /// <summary> (Unit Test Method) tests try read analog input 1 with input error. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void TryReadAnalogInput1WithInputErrorTest()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 360;
            float expectedValue = RtuClientTestInfo.Get().AmbientTemperature;
            float actualValue = 0f;
            _ = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Read error" );
            Assert.AreEqual( expectedValue, actualValue, RtuClientTestInfo.Get().AmbientTemperature );
            Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 100d ) );
            registerAddress = 362;
            ushort expectedShort = 61;
            ushort actualShort = 0;
            bool outcome = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualShort );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Input error Interface error" );
            Assert.AreEqual( true, outcome, "Read Input error failed" );
            Assert.AreEqual( expectedShort, actualShort );
        }

        /// <summary> (Unit Test Method) tests try read setpoint. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void TryReadSetpointTest()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 2160;
            float expectedValue = 75f;
            float actualValue = 0f;
            _ = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Read error" );
            Assert.AreEqual( expectedValue, actualValue, 1f );
        }

        /// <summary> (Unit Test Method) writes the setpoint 1 test. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void WriteSetpoint1Test()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Connection error" );
            ushort registerAddress = 2160;
            float expectedValue = 75f;
            var bytes = expectedValue.BigEndBytes();
            var values = new ushort[] { bytes.BigEndUnsignedShort( 2 ), bytes.BigEndUnsignedShort( 0 ) };
            ModbusClient.WriteMultipleRegisters( UnitId, registerAddress, values );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Write error" );
        }

        /// <summary> (Unit Test Method) writes the read setpoint 1 test. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void WriteReadSetpoint1Test()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 2160;
            float expectedValue = 76f;
            float actualValue = 0f;
            ModbusClient.WriteRegistersValue( UnitId, registerAddress, expectedValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Write 1 error" );
            var postWriteRefractoryTimespan = TimeSpan.FromMilliseconds( 10d );
            Core.ApplianceBase.DoEventsWait( postWriteRefractoryTimespan );
            bool outcome = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Read 1 error" );
            Assert.AreEqual( true, outcome );
            Assert.AreEqual( expectedValue, actualValue, 1f );
            var postReadRefractoryTimespan = TimeSpan.FromMilliseconds( 10d );
            Core.ApplianceBase.DoEventsWait( postReadRefractoryTimespan );
            expectedValue = 75f;
            ModbusClient.WriteRegistersValue( UnitId, registerAddress, expectedValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Write 2 error" );
            Core.ApplianceBase.DoEventsWait( postWriteRefractoryTimespan );
            outcome = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Read 2 error" );
            Assert.AreEqual( true, outcome );
            Assert.AreEqual( expectedValue, actualValue, 1f );
            Core.ApplianceBase.DoEventsWait( postReadRefractoryTimespan );
        }

        /// <summary> (Unit Test Method) writes the recovery test. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [TestMethod()]
        public void WriteRecoveryTest()
        {

            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 2160;
            // user an incorrect type to elicit an error.

            ushort incorrectExpectedType = 76;

            // write should fail because of the wrong type; but no error is reported. 
            ModbusClient.WriteRegistersValue( UnitId, registerAddress, incorrectExpectedType );
            // this does not fail. Test not working as expected.
            // but recovery was tested because other tests failed and were recoverable.
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Write 1 error" );
            var postWriteRefractoryTimespan = TimeSpan.FromMilliseconds( 10d );
            Core.ApplianceBase.DoEventsWait( postWriteRefractoryTimespan );
            float actualValue = 0f;
            float expectedValue = 75f;

            // the protocol should allow writing the correct value type.
            ModbusClient.WriteRegistersValue( UnitId, registerAddress, expectedValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Write 1 error" );
            Core.ApplianceBase.DoEventsWait( postWriteRefractoryTimespan );
            bool outcome = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Read 1 error" );
            Assert.AreEqual( true, outcome );
            Assert.AreEqual( expectedValue, actualValue, 1f );
            var postReadRefractoryTimespan = TimeSpan.FromMilliseconds( 10d );
            Core.ApplianceBase.DoEventsWait( postReadRefractoryTimespan );
        }

        /// <summary> (Unit Test Method) tests read recover. </summary>
        /// <remarks> Test was unable to check failure. </remarks>
        [TestMethod()]
        public void ReadRecoverTest()
        {
            // Checks if connected
            Assert.AreEqual( true, ( object ) ModbusClient?.Connected );
            ushort registerAddress = 2160;
            float expectedValue = 75f;
            ushort actualValueWrongType = 0;
            float actualValue = 0f;

            // write should be okay
            ModbusClient.WriteRegistersValue( UnitId, registerAddress, expectedValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Write 1 error" );
            var postWriteRefractoryTimespan = TimeSpan.FromMilliseconds( 10d );
            Core.ApplianceBase.DoEventsWait( postWriteRefractoryTimespan );

            // read should fail because the wrong part.
            _ = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValueWrongType );
            // this does not fail. Test not working as expected.
            // but recovery was tested because other tests failed and were recoverable.
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "CRC Error" );
            var postReadRefractoryTimespan = TimeSpan.FromMilliseconds( 10d );
            Core.ApplianceBase.DoEventsWait( postReadRefractoryTimespan );


            // the protocol should allow reading the correct value
            bool outcome = ModbusClient.TryReadHoldingRegistersValue( UnitId, registerAddress, ref actualValue );
            Assert.AreEqual( InterfaceError.NoError, ModbusClient.LastError, "Read 1 error" );
            Assert.AreEqual( true, outcome );
            Assert.AreEqual( expectedValue, actualValue, 1f );
            Core.ApplianceBase.DoEventsWait( postReadRefractoryTimespan );
        }
        #endregion

    }
}
