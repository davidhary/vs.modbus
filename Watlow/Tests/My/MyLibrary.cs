﻿
namespace isr.Modbus.Watlow.Tests.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    public sealed class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Modbus Unit Tests";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Modbus Unit Tests";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Modbus.Tests";
    }
}