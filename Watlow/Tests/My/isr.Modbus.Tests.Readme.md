## ISR Modbus Tests Library<sub>&trade;</sub>: Unit Tests Library for the MODBUS Libraries
Instruments
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)

### Revision History [](#){name=Revision-History}

*1.2.7637 2020-11-28*  
Converted to C#.

*1.1.6585 2018-01-11*  
Created.

\(C\) 2018 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/arebis.typedunits)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Modbus Libraries](https://bitbucket.org/davidhary/vs.IOmodbus)

