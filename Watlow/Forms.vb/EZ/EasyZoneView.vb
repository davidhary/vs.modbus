Imports System.ComponentModel
Imports System.IO
Imports System.Windows.Forms

Imports isr.Core
Imports isr.Core.EnumExtensions
Imports isr.Automata.Finite.Engines
Imports Stateless
Imports isr.Modbus.Watlow
Imports isr.Modbus.Watlow.Forms.ExceptionExtensions

''' <summary> View for accessing the Watlow EZ-Zone controller. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-05-09 </para>
''' </remarks>
Public Class EasyZoneView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializingComponents = True
        Me.InitializeComponent()

        ' note that the caption is not set if this is run inside the On Load function.
        ' set defaults for the messages box.
        Me._TraceMessagesBox.ResetCount = 500
        Me._TraceMessagesBox.PresetCount = 250
        Me._TraceMessagesBox.TabCaption = "Log"
        'Me._TraceMessagesBox.ContainerPanel = Me._TreePanel.Panel2
        'Me._TraceMessagesBox.AlertsToggleControl = Me._TreePanel.Panel2
        Me._TraceMessagesBox.CommenceUpdates()

        ' Add any initialization after the InitializeComponent() call.
        Me._SetpointNumeric.NumericUpDownControl.DecimalPlaces = 1
        Me._SetpointNumeric.NumericUpDownControl.Minimum = -200
        Me._SetpointNumeric.NumericUpDownControl.Maximum = 400
        Me._SetpointNumeric.NumericUpDownControl.Value = 78

        Me._SoakSetpointNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._SoakSetpointNumeric.NumericUpDownControl.Minimum = -200
        Me._SoakSetpointNumeric.NumericUpDownControl.Maximum = 400
        Me._SoakSetpointNumeric.NumericUpDownControl.Value = 90

        Me._SoakTimeNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._SoakTimeNumeric.NumericUpDownControl.Minimum = 0
        Me._SoakTimeNumeric.NumericUpDownControl.Maximum = 3600
        Me._SoakTimeNumeric.NumericUpDownControl.Value = 60

        Me._SoakWindowNumeric.NumericUpDownControl.DecimalPlaces = 1
        Me._SoakWindowNumeric.NumericUpDownControl.Increment = 0.1D
        Me._SoakWindowNumeric.NumericUpDownControl.Minimum = 0
        Me._SoakWindowNumeric.NumericUpDownControl.Maximum = 10
        Me._SoakWindowNumeric.NumericUpDownControl.Value = 2

        Me._SoakHysteresisNumeric.NumericUpDownControl.DecimalPlaces = 1
        Me._SoakHysteresisNumeric.NumericUpDownControl.Increment = 0.1D
        Me._SoakHysteresisNumeric.NumericUpDownControl.Minimum = 0
        Me._SoakHysteresisNumeric.NumericUpDownControl.Maximum = 10
        Me._SoakHysteresisNumeric.NumericUpDownControl.Value = 1

        Me._SampleIntervalNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._SampleIntervalNumeric.NumericUpDownControl.Minimum = 0
        Me._SampleIntervalNumeric.NumericUpDownControl.Maximum = 100
        Me._SampleIntervalNumeric.NumericUpDownControl.Value = 2

        Me._SoakResetDelayNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._SoakResetDelayNumeric.NumericUpDownControl.Minimum = 0
        Me._SoakResetDelayNumeric.NumericUpDownControl.Maximum = 600
        Me._SoakResetDelayNumeric.NumericUpDownControl.Value = 20

        Me._RampTimeoutNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._RampTimeoutNumeric.NumericUpDownControl.Minimum = 0
        Me._RampTimeoutNumeric.NumericUpDownControl.Maximum = 6000
        Me._RampTimeoutNumeric.NumericUpDownControl.Value = 0

        Me._OvenControlModeComboBox.ComboBox.DataSource = Nothing
        Me._OvenControlModeComboBox.ComboBox.Items.Clear()
        Me._OvenControlModeComboBox.ComboBox.DataSource = isr.Core.EnumExtensions.Methods.ValueDescriptionPairs(GetType(OvenControlMode)).ToList()
        Me._OvenControlModeComboBox.ComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._OvenControlModeComboBox.ComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)

        Me.InitializingComponents = False

        Me._EventNotificationTestSplitButton.Visible = Debugger.IsAttached
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="unitId">   Identifier for the unit. </param>
    ''' <param name="portName"> Name of the port. </param>
    Public Sub New(ByVal unitId As Integer, ByVal portName As String)
        Me.New()
        Me.IsDeviceOwner = True
        Me.AssignDeviceThis(New EasyZone(CByte(unitId), portName))
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="device"> The device. </param>
    Public Sub New(ByVal device As EasyZone)
        Me.New()
        Me.IsDeviceOwner = False
        Me.AssignDeviceThis(device)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                Me._TraceMessagesBox.SuspendUpdatesReleaseIndicators()
                Me.AssignDevice(Nothing)
            End If
            If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Try
            Me._TraceMessagesBox.ContainerPanel = Me._MessagesTabPage
            Me.AddPrivateListener(Me._TraceMessagesBox)
            Me._ErrorStatusLabel.Text = String.Empty
            Me._ComplianceToolStripStatusLabel.Visible = False
            Me._TbdToolStripStatusLabel.Visible = False
            Me._TransactionElapsedTimeLabel.Text = "0 ms"
            Me._PortComboBox.Items.Clear()
            Me._PortComboBox.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames)
            Me.OnDeviceOpenChanged(Me.EasyZoneClient)
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

#End Region

#Region " MODBUS CLIENT "

    ''' <summary>
    ''' Gets or sets the sentinel indicating if this panel owns the device and, therefore, needs to
    ''' dispose of this device.
    ''' </summary>
    ''' <value> The is device owner. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property IsDeviceOwner As Boolean

    ''' <summary> Assign device. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Private Sub AssignDeviceThis(ByVal value As EasyZone)
        If Me.EasyZoneClient IsNot Nothing Then
            Me.EasyZoneClient.Talker.RemovePrivateListener(Me._TraceMessagesBox)
            Me.RemovePrivateListener(Me._TraceMessagesBox)
            RemoveHandler Me.EasyZoneClient.PropertyChanged, AddressOf Me.EasyZoneClient_PropertyChanged
            If Me.IsDeviceOwner Then
                Me.EasyZoneClient.Talker.Listeners.Clear()
                Me._EasyZoneClient.Dispose()
            End If
            Me.AssignSoakEngine(Nothing)
            Me._EasyZoneClient = Nothing
        End If
        Me._EasyZoneClient = value
        If value IsNot Nothing Then
            If Me.IsDeviceOwner Then
                Me.EasyZoneClient.AddListeners(Me.Talker)
            Else
                Me.EasyZoneClient.Talker.AddPrivateListener(Me._TraceMessagesBox)
            End If
            AddHandler Me.EasyZoneClient.PropertyChanged, AddressOf Me.EasyZoneClient_PropertyChanged
            Me.OnPropertyChanged(value, NameOf(EasyZone.IsDeviceOpen))
            Me.OnPropertyChanged(value, NameOf(EasyZone.InputErrorRead))
            Me.OnPropertyChanged(value, NameOf(EasyZone.AlarmTypeRead))
            Me.OnPropertyChanged(value, NameOf(EasyZone.AlarmTypeWritten))
            Me.OnPropertyChanged(value, NameOf(EasyZone.AnalogInputRead))
            Me.OnPropertyChanged(value, NameOf(EasyZone.LastInterfaceError))
            Me.OnPropertyChanged(value, NameOf(EasyZone.SetpointRead))
            Me.OnPropertyChanged(value, NameOf(EasyZone.SetpointWritten))
            Me.OnPropertyChanged(value, NameOf(EasyZone.Title))
            Me.OnPropertyChanged(value, NameOf(EasyZone.TransactionElapsedTime))
            Me.AssignSoakEngine(value.SoakEngine)
        End If
        Me.OnDeviceOpenChanged(value)
    End Sub

    ''' <summary> Assign device. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Overridable Sub AssignDevice(ByVal value As EasyZone)
        Me.IsDeviceOwner = False
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Gets or sets the easy zone client. </summary>
    ''' <value> The easy zone client. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property EasyZoneClient As EasyZone

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(sender As EasyZone, propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Watlow.EasyZone.IsDeviceOpen)
                Me.OnDeviceOpenChanged(sender)
            Case NameOf(Watlow.EasyZone.InputErrorRead)
                Me._InputErrorLabel.Text = EasyZone.BuildCompoundInputError(sender.InputErrorRead)
                Me._InputErrorLabel.ForeColor = If(sender.InputErrorRead = InputError.NoError, System.Drawing.Color.Green, System.Drawing.Color.Red)
            Case NameOf(Watlow.EasyZone.AlarmTypeRead)
                Me._AlarmTypeComboBox.Text = sender.AlarmTypeRead.Description
            Case NameOf(Watlow.EasyZone.AlarmTypeWritten)
                Me._AlarmTypeComboBox.Text = sender.AlarmTypeWritten.Description
            Case NameOf(Watlow.EasyZone.AnalogInputRead)
                Me._AnalogInputReadingLabel.Text = If(sender.AnalogInputRead Is Nothing, String.Empty, $"{sender.AnalogInputRead.Value:0.0} {sender.AnalogInputRead.Unit}")
            Case NameOf(Watlow.EasyZone.LastInterfaceError)
                Me._LastErrorTextBox.Text = ModbusBase.BuildCompoundInterfaceError(sender.LastInterfaceError)
                Me._LastErrorTextBox.ForeColor = If(sender.LastInterfaceError = InterfaceError.NoError, System.Drawing.Color.Green, System.Drawing.Color.Red)
            Case NameOf(Watlow.EasyZone.SetpointRead)
                Me._SetpointLabel.Text = If(sender.SetpointRead Is Nothing, String.Empty, $"{sender.SetpointRead.Value:0.0} {sender.SetpointRead.Unit}")
            Case NameOf(Watlow.EasyZone.SetpointWritten)
            Case NameOf(Watlow.EasyZone.Title)
                Me._TitleLabel.Text = sender.Title
            Case NameOf(Watlow.EasyZone.TransactionElapsedTime)
                Me._TransactionElapsedTimeLabel.Text = $"{sender.TransactionElapsedTime.Value:0} {sender.TransactionElapsedTime.Unit}"
        End Select
    End Sub

    ''' <summary> Easy zone client property changed. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub EasyZoneClient_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Dim ez As EasyZone = TryCast(sender, EasyZone)
        If ez Is Nothing OrElse e Is Nothing Then Return
        activity = $"handler {NameOf(EasyZone)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.EasyZoneClient_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, EasyZone), e?.PropertyName)
            End If

        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Applies the alarm type menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031: Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyAlarmTypeMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplyAlarmTypeMenuItem.Click

        Try
            Me._ErrorProvider.Clear()
            If Me._AlarmTypeComboBox.SelectedIndex = 0 Then
                Me.EasyZoneClient.ApplyAlarmType(AlarmType.Off)

            Else
                Me.EasyZoneClient.ApplyAlarmType(AlarmType.Process)
            End If

        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, "Exception applying alarm type")
            Me.Talker?.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                               "Exception applying alarm type;. {0}", ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Reads alarm type menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadAlarmTypeMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadAlarmTypeMenuItem.Click
        Try
            Me._ErrorProvider.Clear()
            Me.EasyZoneClient.ReadAlarmType()
        Catch ex As Exception

            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, "Exception reading alarm type")
            Me.Talker?.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                               "Exception reading alarm type;. {0}", ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Writes an alarm type menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub WriteAlarmTypeMenuItem_Click(sender As Object, e As EventArgs) Handles _WriteAlarmTypeMenuItem.Click
        Try
            Me._ErrorProvider.Clear()
            If Me._AlarmTypeComboBox.SelectedIndex = 0 Then
                Me.EasyZoneClient.WriteAlarmType(AlarmType.Off)
            Else
                Me.EasyZoneClient.WriteAlarmType(AlarmType.Process)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, "Exception writing alarm type")
            Me.Talker?.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                               "Exception writing alarm type;. {0}", ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Reads input button click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadInputButton_Click(sender As Object, e As EventArgs) Handles _ReadInput1Button.Click
        Try
            Me._ErrorProvider.Clear()
            Me.EasyZoneClient.ReadAnalogInput()
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, "Exception reading analog input")
            Me.Talker?.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                               "Exception reading analog input;. {0}", ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Reads input error button click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadInputErrorButton_Click(sender As Object, e As EventArgs) Handles _ReadInputErrorButton.Click
        Dim activity As String = "reading analog input"
        Try
            Me._ErrorProvider.Clear()
            Me.EasyZoneClient.ReadInputError()
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. ")
            Me.PublishException($"Exception {activity};. ", ex)
        End Try
    End Sub

    ''' <summary> Applies the setpoint menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySetpointMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplySetpointMenuItem.Click
        Dim activity As String = "applying setpoint"
        Try
            Me._ErrorProvider.Clear()
            Me.EasyZoneClient.ApplySetpoint(New Arebis.TypedUnits.Amount(CDbl(Me._SetpointNumeric.Value), Me.EasyZoneClient.TemperatureUnit))
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. ")
            Me.PublishException($"Exception {activity};. ", ex)
        End Try
    End Sub

    ''' <summary> Reads setpoint menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadSetpointMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSetpointMenuItem.Click
        Dim activity As String = "reading setpoint"
        Try
            Me._ErrorProvider.Clear()
            Me.EasyZoneClient.ReadSetpoint()
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. ")
            Me.PublishException($"Exception {activity};. ", ex)
        End Try
    End Sub

    ''' <summary> Writes a setpoint menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub WriteSetpointMenuItem_Click(sender As Object, e As EventArgs) Handles _WriteSetpointMenuItem.Click
        Dim activity As String = "writing setpoint"
        Try
            Me._ErrorProvider.Clear()
            Me.EasyZoneClient.WriteSetpoint(New Arebis.TypedUnits.Amount(CDbl(Me._SetpointNumeric.Value), Me.EasyZoneClient.TemperatureUnit))
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. ")
            Me.PublishException($"Exception {activity};. ", ex)
        End Try
    End Sub

    ''' <summary> Recursively enable. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   True to show or False to hide the control. </param>
    Protected Sub RecursivelyEnable(ByVal control As System.Windows.Forms.Control, ByVal value As Boolean)
        If control IsNot Nothing Then
            control.Enabled = value
            If control.Controls IsNot Nothing AndAlso control.Controls.Count > 0 Then
                For Each c As System.Windows.Forms.Control In control.Controls
                    Me.RecursivelyEnable(c, value)
                Next
            End If
        End If
    End Sub

    ''' <summary> Executes the device open changed action. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="value"> Reference tot he <see cref="EasyZone"/> client. </param>
    Protected Sub OnDeviceOpenChanged(ByVal value As EasyZone)
        Dim isOpen As Boolean = (value?.IsDeviceOpen).GetValueOrDefault(False)
        For Each t As Windows.Forms.TabPage In Me._Tabs.TabPages
            If t IsNot Me._MessagesTabPage Then
                For Each c As Windows.Forms.Control In t.Controls : Me.RecursivelyEnable(c, isOpen) : Next
            End If
        Next
        If isOpen Then
            If Not String.Equals(Me.EasyZoneClient.PortName, Me._PortComboBox.Text, StringComparison.OrdinalIgnoreCase) Then
                Me._PortComboBox.Text = Me.EasyZoneClient.PortName
            End If
            Me._OpenPortButton.BackColor = System.Drawing.Color.LightGreen
            Me._OpenPortButton.Text = "Close"
            Me._StatusLabel.Text = $"Open at {Me._PortComboBox.Text}"
            Me.EasyZoneClient.AddListeners(Me.Talker)
            Me.EasyZoneClient.ApplyTalkerTraceLevels(Me.EasyZoneClient.Talker)
        Else
            Me._OpenPortButton.BackColor = System.Drawing.SystemColors.Control
            Me._OpenPortButton.Text = "Open"
            Me._StatusLabel.Text = $"Select port and click {Me._OpenPortButton.Text}"
            Me.EasyZoneClient?.RemoveListeners()
        End If
    End Sub

    ''' <summary> Opens port button click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenPortButton_Click(sender As Object, e As EventArgs) Handles _OpenPortButton.Click
        Dim activity As String = "opening Easy Zone"
        Try
            Me._ErrorProvider.Clear()
            If Me.EasyZoneClient?.IsDeviceOpen Then
                Me.EasyZoneClient.CloseModbusClient()
            Else
                If Me.EasyZoneClient Is Nothing Then
                    Me.IsDeviceOwner = True
                    Me.AssignDevice(New EasyZone(CByte(Me._UnitIdNumeric.Value), Me._PortComboBox.Text))
                End If
                If Not String.Equals(Me.EasyZoneClient.PortName, Me._PortComboBox.Text) Then
                    Me.EasyZoneClient.PortName = Me._PortComboBox.Text
                End If
                If Me.EasyZoneClient.UnitId <> CByte(Me._UnitIdNumeric.Value) Then
                    Me.EasyZoneClient.UnitId = CByte(Me._UnitIdNumeric.Value)
                End If

                If Me.EasyZoneClient.TryOpenModbusClient() Then
                    Me.PublishVerbose($"Watlow unit {Me.EasyZoneClient.UnitId} open on port {Me.EasyZoneClient.PortName};. ")
                Else
                    Me.PublishWarning($"Failed {activity};. Details: {Me.EasyZoneClient.LastInterfaceError.Description}")
                End If
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. ")
            Me.PublishException($"Exception {activity};. ", ex)
        Finally
            Me.OnDeviceOpenChanged(Me.EasyZoneClient)
        End Try
    End Sub

    ''' <summary> Refresh ports button click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RefreshPortsButton_Click(sender As Object, e As EventArgs) Handles _RefreshPortsButton.Click
        Dim activity As String = "refreshing list of ports"
        Try
            Me._ErrorProvider.Clear()
            Me._PortComboBox.Items.Clear()
            Me._PortComboBox.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames)
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. ")
            Me.PublishException($"Exception {activity};. ", ex)
        End Try
    End Sub

#End Region

#Region " MODBUS CLIENT: SOAK "

    ''' <summary> Gets or sets the Soak engine. </summary>
    ''' <value> The Soak engine. </value>
    Public ReadOnly Property SoakEngine As isr.Automata.Finite.Engines.SoakEngine

    ''' <summary> Assign Soak Engine. </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignSoakEngine(ByVal value As isr.Automata.Finite.Engines.SoakEngine)
        If Me.SoakEngine IsNot Nothing Then
            RemoveHandler Me.SoakEngine.PropertyChanged, AddressOf Me.SoakEngine_PropertyChanged
            Me._SoakEngine = Nothing
        End If
        Me._SoakEngine = value
        If value IsNot Nothing Then
            AddHandler Me.SoakEngine.PropertyChanged, AddressOf Me.SoakEngine_PropertyChanged
            Me.SoakEngine.UsingFireAsync = True
            Me.SoakEngine.StateMachine.OnTransitionedAsync(Function(t As StateMachine(Of SoakState, SoakTrigger).Transition)
                                                               Return Task.Run(Sub() Me.OnTransitioned(t))
                                                           End Function)
            Me.HandlePropertyChanged(value, NameOf(SoakEngine.SoakPercentProgress))
            Me.HandlePropertyChanged(value, NameOf(SoakEngine.StateProgress))
        End If
    End Sub

    ''' <summary> Handles the state entry actions. </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="transition"> The transition. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnTransitioned(ByVal transition As StateMachine(Of SoakState, SoakTrigger).Transition)
        Dim activity As String = String.Empty
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of StateMachine(Of SoakState, SoakTrigger).Transition)(AddressOf Me.OnTransitioned), New Object() {transition})
            Else
                If transition Is Nothing Then
                    activity = Me.PublishVerbose($"Entering the {Me.SoakEngine.StateMachine.State} state")
                    Me.OnActivated(Me.SoakEngine.StateMachine.State)
                Else
                    activity = Me.PublishVerbose($"transitioning {Me.SoakEngine.Name}: {transition.Source} --> {transition.Destination}")
                    If transition.IsReentry Then
                        Me.OnReentry(transition.Destination)
                    Else
                        Me.OnExit(transition.Source)
                        Me.OnActivated(transition.Destination)
                    End If
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the 'exit' action. </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="source"> Source for the. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub OnExit(ByVal source As SoakState)
        Select Case source
            Case SoakState.Soak
        End Select
    End Sub

    ''' <summary>
    ''' Executes the 'reentry' action. Is used to update the setpoint information and take new
    ''' readings.
    ''' </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="destination"> Destination for the. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub OnReentry(ByVal destination As SoakState)
        Dim activity As String = $"{Me.SoakEngine.Name} reentered {destination.Description} state"
        Try
            Select Case destination
                Case SoakState.Soak
                Case SoakState.AtTemp
            End Select
        Catch ex As Exception
            ex.Data.Add($"info {ex.Data?.Count}", $"{Me.SoakEngine.Name} exception at Reenter {destination.Description} state")
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the 'activated' action. </summary>
    ''' <remarks> David, 2020-11-14. </remarks>
    ''' <param name="destination"> Destination for the. </param>
    Private Sub OnActivated(ByVal destination As SoakState)
        Select Case destination
            Case SoakState.Idle
                Me.OnStopped()
            Case SoakState.Engaged
                Me.OnEngaged()
            Case SoakState.Terminal
                Me.OnStopped()
        End Select
    End Sub

    ''' <summary> Executes the 'engaged' actions. </summary>
    ''' <remarks> David, 2020-11-24. </remarks>
    Private Sub OnEngaged()
        Me._AlarmToolStrip.Enabled = False
        Me._InputToolStrip.Enabled = False
        Me._ProcessToolStrip.Enabled = False
        Me._AbortSoakMenuItem.Enabled = True
        Me._RefreshSoakMenuItem.Enabled = True

        Me._PauseSoakMenuItem.Enabled = False
        Me._PauseSoakMenuItem.Invalidate()
        Me._PauseSoakMenuItem.Checked = False
        Me._PauseSoakMenuItem.Enabled = True
    End Sub

    ''' <summary> Executes the 'stopped' action. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Private Sub OnStopped()

        Me._AlarmToolStrip.Enabled = True
        Me._InputToolStrip.Enabled = True
        Me._ProcessToolStrip.Enabled = True
        Me._AbortSoakMenuItem.Enabled = False
        Me._RefreshSoakMenuItem.Enabled = False

        Me._PauseSoakMenuItem.Enabled = False
        Me._PauseSoakMenuItem.Invalidate()
        Me._PauseSoakMenuItem.Checked = False
        Me._PauseSoakMenuItem.Enabled = False

    End Sub

    ''' <summary> Handles the Soak Engine property changed event. </summary>
    ''' <remarks> David, 2020-11-24. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal sender As isr.Automata.Finite.Engines.SoakEngine, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(isr.Automata.Finite.Engines.SoakEngine.SoakPercentProgress)
                Me._SoakElapsedTimeLabel.Text = $"Soak: {sender.SoakStopwatch.Elapsed:d\.h\:mm\:ss\.f}"
            Case NameOf(isr.Automata.Finite.Engines.SoakEngine.StateProgress)
                Me._SoakStateLabel.Text = sender.CurrentState.Description
                If sender.CurrentState = Automata.Finite.Engines.SoakState.Soak Then
                    Me._SoakElapsedTimeLabel.Text = $"Soak: {sender.SoakStopwatch.Elapsed:d\.h\:mm\:ss\.f}"
                    Me._SoakAutomatonProgress.Value = sender.SoakPercentProgress
                    Me._SoakAutomatonProgress.ToolTipText = "Soak time progress"
                Else
                    Me._SoakAutomatonProgress.Value = sender.StateProgress
                    Me._SoakAutomatonProgress.ToolTipText = "Soak automaton progress"
                End If
                Me.PublishVerbose($"{sender.Name} soak progress {sender.SoakStopwatch.Elapsed}")
        End Select
    End Sub

    ''' <summary> Event handler. Called by SoakEngine for property changed events. </summary>
    ''' <remarks> David, 2020-11-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SoakEngine_PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim automaton As isr.Automata.Finite.Engines.SoakEngine = TryCast(sender, isr.Automata.Finite.Engines.SoakEngine)
        If automaton Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(isr.Automata.Finite.Engines.SoakEngine)}.{e.PropertyName} change at ({automaton.CurrentState} state)"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.SoakEngine_PropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(automaton, e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException($"Exception {activity};. ", ex)
        End Try
    End Sub

#End Region

#Region " CONTROL EVETN HANDLERS "

    ''' <summary> Builds setpoint information. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <returns> A SetpointInfo. </returns>
    Private Function BuildSetpointInfo() As SetpointInfo
        Return New SetpointInfo(Arebis.StandardUnits.TemperatureUnits.DegreeFahrenheit, Me._SoakSetpointNumeric.Value,
                                TimeSpan.FromSeconds(Me._SampleIntervalNumeric.Value),
                                TimeSpan.FromSeconds(Me._SoakTimeNumeric.Value), Me._SoakWindowNumeric.Value, Me._SoakHysteresisNumeric.Value,
                                TimeSpan.FromSeconds(Me._SoakResetDelayNumeric.Value),
                                CType(Me._OvenControlModeComboBox.ComboBox.SelectedValue, OvenControlMode),
                                TimeSpan.FromSeconds(Me._RampTimeoutNumeric.Value))
    End Function


    ''' <summary> Validates the oven control mode described by item. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="item"> The item. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Private Function ValidateOvenControlMode(ByVal item As ToolStripItem) As Boolean
        Dim result As Boolean = CType(Me._OvenControlModeComboBox.ComboBox.SelectedValue, OvenControlMode) <> OvenControlMode.None
        If Not result Then
            Me._ErrorProvider.Annunciate(item, Core.Controls.InfoProviderLevel.Alert, "Select oven control mode")
            Me.PublishInfo("Oven control mode not selected")
        End If
        Return result
    End Function

    ''' <summary> Soak setpoint numeric value changed. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SoakSetpointNumeric_ValueChanged(sender As Object, e As EventArgs) Handles _SoakSetpointNumeric.ValueChanged
        If Me.InitializingComponents Then Return
        Me.EasyZoneClient.CandidateSetpointTemperature = Me._SoakSetpointNumeric.Value
    End Sub

    ''' <summary> Oven control mode combo box selected index changed. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OvenControlModeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles _OvenControlModeComboBox.SelectedIndexChanged
        If Me.InitializingComponents Then Return
        If Me.SoakEngine.IsActive() Then
            Dim controlMode As OvenControlMode = CType(Me._OvenControlModeComboBox.ComboBox.SelectedValue, OvenControlMode)
            If controlMode <> OvenControlMode.None Then
                Me.EasyZoneClient.CandidateOvenControlMode = controlMode
            End If
        End If
    End Sub

    ''' <summary> Starts soak menu item check state changed. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StartSoakMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _StartSoakMenuItem.CheckStateChanged
        If Me.InitializingComponents Then Return
        sender = Me._SoakControlButton
        Dim activity As String = "Starting"
        Try
            Me._ErrorProvider.Clear()
            Me._StartSoakMenuItem.Enabled = False
            If Me._StartSoakMenuItem.Checked Then
                activity = "starting"
                If Me.ValidateOvenControlMode(Me._OvenControlModeComboBox) Then
                    If Not Me.SoakEngine.IsActive() Then
                        activity = $"Starting setpoint {Me._SetpointNumeric.Value}"
                        Me.PublishInfo(activity)
                        Me.EasyZoneClient.ActivateSoakAutomaton(Me.BuildSetpointInfo)
                    End If
                End If
            Else
                activity = $"stopping; Enqueueing a {SoakTrigger.Terminate} signal"
                If Me.SoakEngine.IsActive Then
                    Me.PublishVerbose(activity)
                    Me.SoakEngine.Terminate()
                End If
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, $"Soak Exception {activity}")
            Me.PublishException($"Exception {activity};. ", ex)
        Finally
            Me._StartSoakMenuItem.Text = If(Me._StartSoakMenuItem.Checked, "Stop", "Start")
            Me._StartSoakMenuItem.Enabled = True
        End Try
    End Sub

    ''' <summary> Abort soak menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AbortSoakMenuItem_Click(sender As Object, e As EventArgs) Handles _AbortSoakMenuItem.Click
        If Me.InitializingComponents Then Return
        sender = Me._SoakControlButton
        Dim activity As String = "issuing a stop request"
        Try
            Me._ErrorProvider.Clear()
            Me.SoakEngine.StopRequested = True
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, $"Soak Exception {activity}")
            Me.PublishException($"Exception {activity};. ", ex)
        Finally
        End Try
    End Sub

    ''' <summary> Pause soak menu item check state changed. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PauseSoakMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _PauseSoakMenuItem.CheckStateChanged
        If Me.InitializingComponents Then Return
        sender = Me._SoakControlButton
        Dim activity As String = "pausing"
        Dim wasEnabled As Boolean = Me._PauseSoakMenuItem.Enabled
        Try
            Me._ErrorProvider.Clear()
            If Me._PauseSoakMenuItem.Enabled Then
                Me._PauseSoakMenuItem.Enabled = False
                Me._PauseSoakMenuItem.Invalidate()
                If Me._PauseSoakMenuItem.Checked Then
                    activity = "pausing"
                    Me.EasyZoneClient.PauseSoakAutomaton()
                Else
                    activity = "resuming"
                    Me.EasyZoneClient.ResumeSoakAutomaton()
                End If
                Me.PublishInfo(activity)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, $"Soak Exception {activity}")
            Me.PublishException($"Exception {activity};. ", ex)
        Finally
            Me._PauseSoakMenuItem.Text = If(Me._PauseSoakMenuItem.Checked, "Resume", "Pause")
            Me._PauseSoakMenuItem.Enabled = wasEnabled
            Me._PauseSoakMenuItem.Invalidate()
        End Try
    End Sub

    ''' <summary> Refresh soak menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RefreshSoakMenuItem_Click(sender As Object, e As EventArgs) Handles _RefreshSoakMenuItem.Click
        If Me.InitializingComponents Then Return
        sender = Me._SoakControlButton
        Dim activity As String = "refreshing"
        Try
            Me._ErrorProvider.Clear()
            Me._RefreshSoakMenuItem.Enabled = False
            If Me.ValidateOvenControlMode(Me._OvenControlModeComboBox) Then
                activity = $"Changing to setpoint {Me._SetpointNumeric.Value}"
                Me.PublishInfo($"Changing to setpoint {Me._SetpointNumeric.Value}")
                Me.EasyZoneClient.ChangeTargetAutomatonSetpoint(Me.BuildSetpointInfo, False)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, Core.Controls.InfoProviderLevel.Error, $"Soak Exception {activity}")
            Me.PublishException($"Exception {activity};. ", ex)
        Finally
            Me._RefreshSoakMenuItem.Enabled = True
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Adds the listeners such as the current trace messages box. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Protected Overridable Overloads Sub AddPrivateListeners()
        Me.AddPrivateListener(Me._TraceMessagesBox)
        Me.EasyZoneClient.Talker.AddPrivateListener(Me._TraceMessagesBox)
    End Sub

    ''' <summary> Adds a listener. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Overrides Sub AddListener(ByVal listener As IMessageListener)
        Me.EasyZoneClient?.AddListener(listener)
        MyBase.AddListener(listener)
    End Sub

    ''' <summary> Adds the listeners. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="listeners"> The listeners. </param>
    Public Overrides Sub AddListeners(ByVal listeners As IList(Of IMessageListener))
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        Me.AddPrivateListeners()
        MyBase.AddListeners(listeners)
    End Sub

    ''' <summary> Applies the trace level to all listeners to the specified type. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        If listenerType = Me._TraceMessagesBox.ListenerType Then Me._TraceMessagesBox.ApplyTraceLevel(value)
        Me.EasyZoneClient?.ApplyListenerTraceLevel(listenerType, value)
        ' this should apply only to the listeners associated with this form
        ' MyBase.ApplyListenerTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the trace level type to all talkers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyTalkerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        MyBase.ApplyTalkerTraceLevel(listenerType, value)
        Me.EasyZoneClient?.ApplyTalkerTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the talker trace levels described by talker. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub ApplyTalkerTraceLevels(ByVal talker As ITraceMessageTalker)
        MyBase.ApplyTalkerTraceLevels(talker)
        Me.EasyZoneClient?.ApplyTalkerTraceLevels(talker)
    End Sub

    ''' <summary> Applies the talker trace levels described by talker. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub ApplyListenerTraceLevels(ByVal talker As ITraceMessageTalker)
        Me.EasyZoneClient?.ApplyListenerTraceLevels(talker)
        ' this should apply only to the listeners associated with this form
        ' MyBase.ApplyListenerTraceLevels(talker)
    End Sub

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the
    ''' message.
    ''' </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return MyBase.Publish(New TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

#Region " MESSAGE BOX EVENTS "

    ''' <summary> Handles the <see cref="_TraceMessagesBox"/> property changed event. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(sender As isr.Core.Forma.TraceMessagesBox, propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        If String.Equals(propertyName, NameOf(isr.Core.Forma.TraceMessagesBox.StatusPrompt)) Then
            Me._StatusLabel.Text = sender.StatusPrompt
            Me._StatusLabel.ToolTipText = sender.StatusPrompt
        End If
    End Sub

    ''' <summary> Trace messages box property changed. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TraceMessagesBox_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _TraceMessagesBox.PropertyChanged
        Dim msgBox As Forma.TraceMessagesBox = TryCast(sender, Forma.TraceMessagesBox)
        If msgBox Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handler {NameOf(Forma.TraceMessagesBox)}.{e.PropertyName} change"
        Try
            ' there was a cross thread exception because this event is invoked from the control thread.
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.TraceMessagesBox_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, isr.Core.Forma.TraceMessagesBox), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException($"Exception {activity};. ", ex)
        End Try
    End Sub

#End Region

#Region " INVOKE TESTERS "

    ''' <summary> The event time builder. </summary>
    Private _EventTimeBuilder As System.Text.StringBuilder

    ''' <summary> The event stopwatch. </summary>
    Private _EventStopwatch As Stopwatch

    ''' <summary> Synchronizes the notify menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SyncNotifyMenuItem_Click(sender As Object, e As EventArgs) Handles _SyncNotifyMenuItem.Click
        Try
            AddHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me._EasyZoneClient.RegisterHandlerExceptionOccurred
            AddHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me.HandleHandlerExceptionOccurred
            Me._EventTimeBuilder = New System.Text.StringBuilder
            Me._EventStopwatch = Stopwatch.StartNew
            ' Me.EasyZoneClient.BeginInvokeHandlerException()
            Me.EasyZoneClient.SyncNotifyHandlerException()
            Me._EventTimeBuilder.AppendFormat($">> {Me._EventStopwatch.ElapsedMilliseconds:G5}ms")
            Me._TransactionElapsedTimeLabel.Text = Me._EventTimeBuilder.ToString
        Finally
            RemoveHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me.HandleHandlerExceptionOccurred
            RemoveHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me._EasyZoneClient.RegisterHandlerExceptionOccurred
        End Try
    End Sub

    ''' <summary> Unsafe invoke menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UnsafeInvokeMenuItem_Click(sender As Object, e As EventArgs) Handles _UnsafeInvokeMenuItem.Click
        Try
            AddHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me._EasyZoneClient.RegisterHandlerExceptionOccurred
            AddHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me.HandleHandlerExceptionOccurred
            Me._EventTimeBuilder = New System.Text.StringBuilder
            Me._EventStopwatch = Stopwatch.StartNew
            Me.EasyZoneClient.UnsafeInvokeHandlerException()
            Me._EventTimeBuilder.AppendFormat($" >>{Me._EventStopwatch.ElapsedMilliseconds:G5}ms ")

            Me._TransactionElapsedTimeLabel.Text = Me._EventTimeBuilder.ToString
        Finally
            RemoveHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me.HandleHandlerExceptionOccurred
            RemoveHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me._EasyZoneClient.RegisterHandlerExceptionOccurred
        End Try
    End Sub

    ''' <summary> Asynchronous notify menu item click. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AsyncNotifyMenuItem_Click(sender As Object, e As EventArgs) Handles _AsyncNotifyMenuItem.Click
        Try
            AddHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me._EasyZoneClient.RegisterHandlerExceptionOccurred
            AddHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me.HandleHandlerExceptionOccurred
            Me._EventTimeBuilder = New System.Text.StringBuilder
            Me._EventStopwatch = Stopwatch.StartNew
            Me.EasyZoneClient.AsyncNotifyHandlerException()

            'Me.EasyZoneClient.SafeBeginInvokeHandlerException()
            Me._EventTimeBuilder.AppendFormat($" >>{Me._EventStopwatch.ElapsedMilliseconds:G5}ms ")
            Me._TransactionElapsedTimeLabel.Text = Me._EventTimeBuilder.ToString
        Finally
            RemoveHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me.HandleHandlerExceptionOccurred
            RemoveHandler Me._EasyZoneClient.HandlerExceptionOccurred, AddressOf Me._EasyZoneClient.RegisterHandlerExceptionOccurred
        End Try
    End Sub

    ''' <summary> Handler, called when the handler exception occurred. </summary>
    ''' <remarks> David, 2020-11-11. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Error event information. </param>
    Private Sub HandleHandlerExceptionOccurred(sender As Object, e As ErrorEventArgs)
        Me._EventTimeBuilder.AppendFormat($" <<{Me._EventStopwatch.ElapsedMilliseconds:G5}ms ")
        Me._TransactionElapsedTimeLabel.Text = Me._EventTimeBuilder.ToString
        Threading.Thread.Sleep(10)
    End Sub

#End Region

End Class

