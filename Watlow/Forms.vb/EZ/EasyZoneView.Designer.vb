<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class EasyZoneView
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EasyZoneView))
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._StatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._ErrorStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ErrorProvider = New isr.Core.Controls.InfoProvider(Me.components)
        Me._ResourceToolStrip = New System.Windows.Forms.ToolStrip()
        Me._UnitIdNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._UnitIdNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._Separator1 = New System.Windows.Forms.ToolStripSeparator()
        Me._RefreshPortsButton = New System.Windows.Forms.ToolStripButton()
        Me._PortComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._PortComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._OpenPortButton = New System.Windows.Forms.ToolStripButton()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._TitleLabel = New System.Windows.Forms.Label()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._Tabs = New System.Windows.Forms.TabControl()
        Me._ReadingTabPage = New System.Windows.Forms.TabPage()
        Me._SoakStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._SoakStateLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._SoakElapsedTimeLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._SoakAutomatonProgress = New System.Windows.Forms.ToolStripProgressBar()
        Me._ToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me._InputToolStrip = New System.Windows.Forms.ToolStrip()
        Me._InputNumberComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._InputNumberComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._ReadInput1Button = New System.Windows.Forms.ToolStripButton()
        Me._ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me._InputErrorLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ReadInputErrorButton = New System.Windows.Forms.ToolStripButton()
        Me._AlarmToolStrip = New System.Windows.Forms.ToolStrip()
        Me._AlarmNumberComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._AlarmNumberComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._AlarmTypeComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._AlarmTypeComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._AlarmTypeActionsButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._WriteAlarmTypeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadAlarmTypeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ApplyAlarmTypeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ProcessToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ProcessLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SetpointNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SetpointNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._SetpointActionButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._WriteSetpointMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSetpointMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ApplySetpointMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._EventNotificationTestSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._AsyncNotifyMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._UnsafeInvokeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SyncNotifyMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SoakToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SoakToolStripLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SoakSetpointNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SoakSetpointNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._SoakTimeNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SoakTimeNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._SoakWindowNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SoakWindowNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._SoakHysteresisNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SoakHysteresisNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._SampleIntervalNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SampleIntervalNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._SoakResetDealyNumericToolStripLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SoakResetDelayNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._RampTimeoutNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._RampTimeoutNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._OvenControlModeComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._OvenControlModeComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._MessagesTabPage = New System.Windows.Forms.TabPage()
        Me._TraceMessagesBox = New isr.Core.Forma.TraceMessagesBox()
        Me._LastErrorTextBox = New System.Windows.Forms.TextBox()
        Me._InfoStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._FillerStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._TransactionElapsedTimeLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._ReadingStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._ComplianceToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._AnalogInputReadingLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._SetpointLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._TbdToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._SoakControlButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._StartSoakMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PauseSoakMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AbortSoakMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RefreshSoakMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._StatusStrip.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ResourceToolStrip.SuspendLayout()
        Me._Layout.SuspendLayout()
        Me._Panel.SuspendLayout()
        Me._Tabs.SuspendLayout()
        Me._ReadingTabPage.SuspendLayout()
        Me._SoakStatusStrip.SuspendLayout()
        Me._ToolStripPanel.SuspendLayout()
        Me._InputToolStrip.SuspendLayout()
        Me._AlarmToolStrip.SuspendLayout()
        Me._ProcessToolStrip.SuspendLayout()
        Me._SoakToolStrip.SuspendLayout()
        Me._MessagesTabPage.SuspendLayout()
        Me._InfoStatusStrip.SuspendLayout()
        Me._ReadingStatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_StatusStrip
        '
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusLabel, Me._ErrorStatusLabel})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 469)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.Padding = New System.Windows.Forms.Padding(1, 0, 16, 0)
        Me._StatusStrip.Size = New System.Drawing.Size(399, 22)
        Me._StatusStrip.TabIndex = 0
        Me._StatusStrip.Text = "StatusStrip1"
        '
        '_StatusLabel
        '
        Me._StatusLabel.Name = "_StatusLabel"
        Me._StatusLabel.Size = New System.Drawing.Size(345, 17)
        Me._StatusLabel.Spring = True
        Me._StatusLabel.Text = "<status>"
        '
        '_ErrorStatusLabel
        '
        Me._ErrorStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ErrorStatusLabel.Name = "_ErrorStatusLabel"
        Me._ErrorStatusLabel.Size = New System.Drawing.Size(37, 17)
        Me._ErrorStatusLabel.Text = "<err>"
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_ResourceToolStrip
        '
        Me._ResourceToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ResourceToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._UnitIdNumericLabel, Me._UnitIdNumeric, Me._Separator1, Me._RefreshPortsButton, Me._PortComboBoxLabel, Me._PortComboBox, Me._OpenPortButton})
        Me._ResourceToolStrip.Location = New System.Drawing.Point(0, 443)
        Me._ResourceToolStrip.Name = "_ResourceToolStrip"
        Me._ResourceToolStrip.Size = New System.Drawing.Size(399, 26)
        Me._ResourceToolStrip.TabIndex = 1
        Me._ResourceToolStrip.Text = "Resource Tool Strip"
        '
        '_UnitIdNumericLabel
        '
        Me._UnitIdNumericLabel.Name = "_UnitIdNumericLabel"
        Me._UnitIdNumericLabel.Size = New System.Drawing.Size(21, 23)
        Me._UnitIdNumericLabel.Text = "ID:"
        '
        '_UnitIdNumeric
        '
        Me._UnitIdNumeric.Name = "_UnitIdNumeric"
        Me._UnitIdNumeric.Size = New System.Drawing.Size(41, 23)
        Me._UnitIdNumeric.Text = "1"
        Me._UnitIdNumeric.ToolTipText = "EZ-Zone unit ID"
        Me._UnitIdNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_Separator1
        '
        Me._Separator1.Name = "_Separator1"
        Me._Separator1.Size = New System.Drawing.Size(6, 26)
        '
        '_RefreshPortsButton
        '
        Me._RefreshPortsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._RefreshPortsButton.Image = Global.isr.Modbus.Watlow.Forms.My.Resources.Resources.View_refresh_5
        Me._RefreshPortsButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._RefreshPortsButton.Name = "_RefreshPortsButton"
        Me._RefreshPortsButton.Size = New System.Drawing.Size(23, 23)
        Me._RefreshPortsButton.Text = "Refresh"
        Me._RefreshPortsButton.ToolTipText = "Refresh list of ports"
        '
        '_PortComboBoxLabel
        '
        Me._PortComboBoxLabel.Name = "_PortComboBoxLabel"
        Me._PortComboBoxLabel.Size = New System.Drawing.Size(32, 23)
        Me._PortComboBoxLabel.Text = "Port:"
        '
        '_PortComboBox
        '
        Me._PortComboBox.Name = "_PortComboBox"
        Me._PortComboBox.Size = New System.Drawing.Size(81, 26)
        Me._PortComboBox.ToolTipText = "Available ports"
        '
        '_OpenPortButton
        '
        Me._OpenPortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._OpenPortButton.Image = CType(resources.GetObject("_OpenPortButton.Image"), System.Drawing.Image)
        Me._OpenPortButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenPortButton.Name = "_OpenPortButton"
        Me._OpenPortButton.Size = New System.Drawing.Size(40, 23)
        Me._OpenPortButton.Text = "Open"
        Me._OpenPortButton.ToolTipText = "Open port"
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 1
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.Controls.Add(Me._TitleLabel, 0, 0)
        Me._Layout.Controls.Add(Me._Panel, 0, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Margin = New System.Windows.Forms.Padding(0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 2
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.Size = New System.Drawing.Size(399, 443)
        Me._Layout.TabIndex = 18
        '
        '_TitleLabel
        '
        Me._TitleLabel.BackColor = System.Drawing.Color.Black
        Me._TitleLabel.CausesValidation = False
        Me._TitleLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._TitleLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TitleLabel.ForeColor = System.Drawing.SystemColors.Info
        Me._TitleLabel.Location = New System.Drawing.Point(0, 0)
        Me._TitleLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._TitleLabel.Name = "_TitleLabel"
        Me._TitleLabel.Size = New System.Drawing.Size(399, 17)
        Me._TitleLabel.TabIndex = 17
        Me._TitleLabel.Text = "RTU Closed"
        Me._TitleLabel.UseMnemonic = False
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._Tabs)
        Me._Panel.Controls.Add(Me._LastErrorTextBox)
        Me._Panel.Controls.Add(Me._InfoStatusStrip)
        Me._Panel.Controls.Add(Me._ReadingStatusStrip)
        Me._Panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Panel.Location = New System.Drawing.Point(0, 17)
        Me._Panel.Margin = New System.Windows.Forms.Padding(0)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(399, 426)
        Me._Panel.TabIndex = 16
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._ReadingTabPage)
        Me._Tabs.Controls.Add(Me._MessagesTabPage)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.ItemSize = New System.Drawing.Size(52, 22)
        Me._Tabs.Location = New System.Drawing.Point(0, 82)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(399, 344)
        Me._Tabs.TabIndex = 5
        '
        '_ReadingTabPage
        '
        Me._ReadingTabPage.Controls.Add(Me._SoakStatusStrip)
        Me._ReadingTabPage.Controls.Add(Me._ToolStripPanel)
        Me._ReadingTabPage.Location = New System.Drawing.Point(4, 26)
        Me._ReadingTabPage.Name = "_ReadingTabPage"
        Me._ReadingTabPage.Size = New System.Drawing.Size(391, 314)
        Me._ReadingTabPage.TabIndex = 0
        Me._ReadingTabPage.Text = "Reading"
        Me._ReadingTabPage.UseVisualStyleBackColor = True
        '
        '_SoakStatusStrip
        '
        Me._SoakStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SoakStateLabel, Me._SoakElapsedTimeLabel, Me._SoakAutomatonProgress})
        Me._SoakStatusStrip.Location = New System.Drawing.Point(0, 292)
        Me._SoakStatusStrip.Name = "_SoakStatusStrip"
        Me._SoakStatusStrip.Size = New System.Drawing.Size(391, 22)
        Me._SoakStatusStrip.TabIndex = 1
        Me._SoakStatusStrip.Text = "Soak Status"
        '
        '_SoakStateLabel
        '
        Me._SoakStateLabel.Name = "_SoakStateLabel"
        Me._SoakStateLabel.Size = New System.Drawing.Size(77, 17)
        Me._SoakStateLabel.Text = "<Soak State>"
        '
        '_SoakElapsedTimeLabel
        '
        Me._SoakElapsedTimeLabel.Name = "_SoakElapsedTimeLabel"
        Me._SoakElapsedTimeLabel.Size = New System.Drawing.Size(63, 17)
        Me._SoakElapsedTimeLabel.Text = "<elapsed>"
        '
        '_SoakAutomatonProgress
        '
        Me._SoakAutomatonProgress.Name = "_SoakAutomatonProgress"
        Me._SoakAutomatonProgress.Size = New System.Drawing.Size(100, 16)
        Me._SoakAutomatonProgress.ToolTipText = "Soak Time"
        '
        '_ToolStripPanel
        '
        Me._ToolStripPanel.Controls.Add(Me._InputToolStrip)
        Me._ToolStripPanel.Controls.Add(Me._AlarmToolStrip)
        Me._ToolStripPanel.Controls.Add(Me._ProcessToolStrip)
        Me._ToolStripPanel.Controls.Add(Me._SoakToolStrip)
        Me._ToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._ToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me._ToolStripPanel.Name = "_ToolStripPanel"
        Me._ToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me._ToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me._ToolStripPanel.Size = New System.Drawing.Size(391, 173)
        '
        '_InputToolStrip
        '
        Me._InputToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._InputToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._InputNumberComboBoxLabel, Me._InputNumberComboBox, Me._ReadInput1Button, Me._ToolStripSeparator1, Me._InputErrorLabel, Me._ReadInputErrorButton})
        Me._InputToolStrip.Location = New System.Drawing.Point(3, 0)
        Me._InputToolStrip.Name = "_InputToolStrip"
        Me._InputToolStrip.Padding = New System.Windows.Forms.Padding(0, 3, 1, 0)
        Me._InputToolStrip.Size = New System.Drawing.Size(208, 26)
        Me._InputToolStrip.TabIndex = 0
        '
        '_InputNumberComboBoxLabel
        '
        Me._InputNumberComboBoxLabel.Name = "_InputNumberComboBoxLabel"
        Me._InputNumberComboBoxLabel.Size = New System.Drawing.Size(38, 20)
        Me._InputNumberComboBoxLabel.Text = "Input:"
        '
        '_InputNumberComboBox
        '
        Me._InputNumberComboBox.AutoSize = False
        Me._InputNumberComboBox.Items.AddRange(New Object() {"1"})
        Me._InputNumberComboBox.Name = "_InputNumberComboBox"
        Me._InputNumberComboBox.Size = New System.Drawing.Size(35, 23)
        Me._InputNumberComboBox.Text = "1"
        Me._InputNumberComboBox.ToolTipText = "Input number"
        '
        '_ReadInput1Button
        '
        Me._ReadInput1Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadInput1Button.Image = CType(resources.GetObject("_ReadInput1Button.Image"), System.Drawing.Image)
        Me._ReadInput1Button.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadInput1Button.Name = "_ReadInput1Button"
        Me._ReadInput1Button.Size = New System.Drawing.Size(37, 20)
        Me._ReadInput1Button.Text = "Read"
        Me._ReadInput1Button.ToolTipText = "Read input 1"
        '
        '_ToolStripSeparator1
        '
        Me._ToolStripSeparator1.Name = "_ToolStripSeparator1"
        Me._ToolStripSeparator1.Size = New System.Drawing.Size(6, 23)
        '
        '_InputErrorLabel
        '
        Me._InputErrorLabel.Name = "_InputErrorLabel"
        Me._InputErrorLabel.Size = New System.Drawing.Size(37, 20)
        Me._InputErrorLabel.Text = "<err>"
        Me._InputErrorLabel.ToolTipText = "Last input error"
        '
        '_ReadInputErrorButton
        '
        Me._ReadInputErrorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadInputErrorButton.Image = CType(resources.GetObject("_ReadInputErrorButton.Image"), System.Drawing.Image)
        Me._ReadInputErrorButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadInputErrorButton.Name = "_ReadInputErrorButton"
        Me._ReadInputErrorButton.Size = New System.Drawing.Size(41, 20)
        Me._ReadInputErrorButton.Text = "Error?"
        Me._ReadInputErrorButton.ToolTipText = "Reads the input error"
        '
        '_AlarmToolStrip
        '
        Me._AlarmToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._AlarmToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._AlarmNumberComboBoxLabel, Me._AlarmNumberComboBox, Me._AlarmTypeComboBoxLabel, Me._AlarmTypeComboBox, Me._AlarmTypeActionsButton})
        Me._AlarmToolStrip.Location = New System.Drawing.Point(3, 26)
        Me._AlarmToolStrip.Name = "_AlarmToolStrip"
        Me._AlarmToolStrip.Padding = New System.Windows.Forms.Padding(0, 3, 1, 0)
        Me._AlarmToolStrip.Size = New System.Drawing.Size(254, 26)
        Me._AlarmToolStrip.TabIndex = 1
        '
        '_AlarmNumberComboBoxLabel
        '
        Me._AlarmNumberComboBoxLabel.Name = "_AlarmNumberComboBoxLabel"
        Me._AlarmNumberComboBoxLabel.Size = New System.Drawing.Size(42, 20)
        Me._AlarmNumberComboBoxLabel.Text = "Alarm:"
        '
        '_AlarmNumberComboBox
        '
        Me._AlarmNumberComboBox.AutoSize = False
        Me._AlarmNumberComboBox.Items.AddRange(New Object() {"1"})
        Me._AlarmNumberComboBox.Name = "_AlarmNumberComboBox"
        Me._AlarmNumberComboBox.Size = New System.Drawing.Size(30, 23)
        Me._AlarmNumberComboBox.Text = "1"
        Me._AlarmNumberComboBox.ToolTipText = "Alarm number"
        '
        '_AlarmTypeComboBoxLabel
        '
        Me._AlarmTypeComboBoxLabel.Name = "_AlarmTypeComboBoxLabel"
        Me._AlarmTypeComboBoxLabel.Size = New System.Drawing.Size(35, 20)
        Me._AlarmTypeComboBoxLabel.Text = "Type:"
        '
        '_AlarmTypeComboBox
        '
        Me._AlarmTypeComboBox.AutoSize = False
        Me._AlarmTypeComboBox.Items.AddRange(New Object() {"Off", "Process"})
        Me._AlarmTypeComboBox.Name = "_AlarmTypeComboBox"
        Me._AlarmTypeComboBox.Size = New System.Drawing.Size(65, 23)
        Me._AlarmTypeComboBox.Text = "Process"
        Me._AlarmTypeComboBox.ToolTipText = "Alarm Type"
        '
        '_AlarmTypeActionsButton
        '
        Me._AlarmTypeActionsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AlarmTypeActionsButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._WriteAlarmTypeMenuItem, Me._ReadAlarmTypeMenuItem, Me._ApplyAlarmTypeMenuItem})
        Me._AlarmTypeActionsButton.Image = CType(resources.GetObject("_AlarmTypeActionsButton.Image"), System.Drawing.Image)
        Me._AlarmTypeActionsButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AlarmTypeActionsButton.Name = "_AlarmTypeActionsButton"
        Me._AlarmTypeActionsButton.Size = New System.Drawing.Size(66, 20)
        Me._AlarmTypeActionsButton.Text = "Actions: "
        Me._AlarmTypeActionsButton.ToolTipText = "Write and then read Alarm Type"
        '
        '_WriteAlarmTypeMenuItem
        '
        Me._WriteAlarmTypeMenuItem.Name = "_WriteAlarmTypeMenuItem"
        Me._WriteAlarmTypeMenuItem.Size = New System.Drawing.Size(165, 22)
        Me._WriteAlarmTypeMenuItem.Text = "Write Alarm Type"
        Me._WriteAlarmTypeMenuItem.ToolTipText = "Writes alarm type to the controller"
        '
        '_ReadAlarmTypeMenuItem
        '
        Me._ReadAlarmTypeMenuItem.Name = "_ReadAlarmTypeMenuItem"
        Me._ReadAlarmTypeMenuItem.Size = New System.Drawing.Size(165, 22)
        Me._ReadAlarmTypeMenuItem.Text = "Read Alarm Type"
        Me._ReadAlarmTypeMenuItem.ToolTipText = "Reads Alarm Type from the controller"
        '
        '_ApplyAlarmTypeMenuItem
        '
        Me._ApplyAlarmTypeMenuItem.Name = "_ApplyAlarmTypeMenuItem"
        Me._ApplyAlarmTypeMenuItem.Size = New System.Drawing.Size(165, 22)
        Me._ApplyAlarmTypeMenuItem.Text = "Write and Read"
        Me._ApplyAlarmTypeMenuItem.ToolTipText = "Writes and then reads the alarm type from the controller"
        '
        '_ProcessToolStrip
        '
        Me._ProcessToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._ProcessToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ProcessLabel, Me._SetpointNumericLabel, Me._SetpointNumeric, Me._SetpointActionButton, Me._EventNotificationTestSplitButton})
        Me._ProcessToolStrip.Location = New System.Drawing.Point(3, 52)
        Me._ProcessToolStrip.Name = "_ProcessToolStrip"
        Me._ProcessToolStrip.Padding = New System.Windows.Forms.Padding(0, 3, 1, 0)
        Me._ProcessToolStrip.Size = New System.Drawing.Size(304, 31)
        Me._ProcessToolStrip.TabIndex = 2
        '
        '_ProcessLabel
        '
        Me._ProcessLabel.Name = "_ProcessLabel"
        Me._ProcessLabel.Size = New System.Drawing.Size(50, 25)
        Me._ProcessLabel.Text = "Process:"
        '
        '_SetpointNumericLabel
        '
        Me._SetpointNumericLabel.Name = "_SetpointNumericLabel"
        Me._SetpointNumericLabel.Size = New System.Drawing.Size(54, 25)
        Me._SetpointNumericLabel.Text = "Setpoint:"
        '
        '_SetpointNumeric
        '
        Me._SetpointNumeric.AutoSize = False
        Me._SetpointNumeric.Name = "_SetpointNumeric"
        Me._SetpointNumeric.Size = New System.Drawing.Size(78, 25)
        Me._SetpointNumeric.Text = "75"
        Me._SetpointNumeric.ToolTipText = "Setpoint"
        Me._SetpointNumeric.Value = New Decimal(New Integer() {75, 0, 0, 0})
        '
        '_SetpointActionButton
        '
        Me._SetpointActionButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SetpointActionButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._WriteSetpointMenuItem, Me._ReadSetpointMenuItem, Me._ApplySetpointMenuItem})
        Me._SetpointActionButton.Image = CType(resources.GetObject("_SetpointActionButton.Image"), System.Drawing.Image)
        Me._SetpointActionButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SetpointActionButton.Name = "_SetpointActionButton"
        Me._SetpointActionButton.Size = New System.Drawing.Size(66, 25)
        Me._SetpointActionButton.Text = "Actions:"
        Me._SetpointActionButton.ToolTipText = "Opens the setpoint action menu"
        '
        '_WriteSetpointMenuItem
        '
        Me._WriteSetpointMenuItem.Name = "_WriteSetpointMenuItem"
        Me._WriteSetpointMenuItem.Size = New System.Drawing.Size(154, 22)
        Me._WriteSetpointMenuItem.Text = "Write Setpoint"
        Me._WriteSetpointMenuItem.ToolTipText = "Writes the setpoint to the controller"
        '
        '_ReadSetpointMenuItem
        '
        Me._ReadSetpointMenuItem.Name = "_ReadSetpointMenuItem"
        Me._ReadSetpointMenuItem.Size = New System.Drawing.Size(154, 22)
        Me._ReadSetpointMenuItem.Text = "Read Setpoint"
        Me._ReadSetpointMenuItem.ToolTipText = "Reads the setpoint from the controller"
        '
        '_ApplySetpointMenuItem
        '
        Me._ApplySetpointMenuItem.Name = "_ApplySetpointMenuItem"
        Me._ApplySetpointMenuItem.Size = New System.Drawing.Size(154, 22)
        Me._ApplySetpointMenuItem.Text = "Write and Read"
        '
        '_EventNotificationTestSplitButton
        '
        Me._EventNotificationTestSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._EventNotificationTestSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._AsyncNotifyMenuItem, Me._UnsafeInvokeMenuItem, Me._SyncNotifyMenuItem})
        Me._EventNotificationTestSplitButton.Image = CType(resources.GetObject("_EventNotificationTestSplitButton.Image"), System.Drawing.Image)
        Me._EventNotificationTestSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._EventNotificationTestSplitButton.Name = "_EventNotificationTestSplitButton"
        Me._EventNotificationTestSplitButton.Size = New System.Drawing.Size(44, 25)
        Me._EventNotificationTestSplitButton.Text = "Test"
        '
        '_AsyncNotifyMenuItem
        '
        Me._AsyncNotifyMenuItem.Name = "_AsyncNotifyMenuItem"
        Me._AsyncNotifyMenuItem.Size = New System.Drawing.Size(189, 22)
        Me._AsyncNotifyMenuItem.Text = "Async Notify Test (unsafe)"
        '
        '_UnsafeInvokeMenuItem
        '
        Me._UnsafeInvokeMenuItem.Name = "_UnsafeInvokeMenuItem"
        Me._UnsafeInvokeMenuItem.Size = New System.Drawing.Size(189, 22)
        Me._UnsafeInvokeMenuItem.Text = "Invoke Test (maybe thread unsafe)"
        '
        '_SyncNotifyMenuItem
        '
        Me._SyncNotifyMenuItem.Name = "_SyncNotifyMenuItem"
        Me._SyncNotifyMenuItem.Size = New System.Drawing.Size(189, 22)
        Me._SyncNotifyMenuItem.Text = "Sync Notify Test (thread safe)"
        '
        '_SoakToolStrip
        '
        Me._SoakToolStrip.AutoSize = False
        Me._SoakToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._SoakToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SoakToolStripLabel, Me._SoakSetpointNumericLabel, Me._SoakSetpointNumeric, Me._SoakTimeNumericLabel, Me._SoakTimeNumeric, Me._SoakWindowNumericLabel, Me._SoakWindowNumeric, Me._SoakHysteresisNumericLabel, Me._SoakHysteresisNumeric, Me._SampleIntervalNumericLabel, Me._SampleIntervalNumeric, Me._SoakResetDealyNumericToolStripLabel, Me._SoakResetDelayNumeric, Me._RampTimeoutNumericLabel, Me._RampTimeoutNumeric, Me._OvenControlModeComboBoxLabel, Me._OvenControlModeComboBox, Me._SoakControlButton})
        Me._SoakToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me._SoakToolStrip.Location = New System.Drawing.Point(3, 83)
        Me._SoakToolStrip.Name = "_SoakToolStrip"
        Me._SoakToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me._SoakToolStrip.Size = New System.Drawing.Size(388, 90)
        Me._SoakToolStrip.TabIndex = 3
        '
        '_SoakToolStripLabel
        '
        Me._SoakToolStripLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SoakToolStripLabel.Margin = New System.Windows.Forms.Padding(0, 4, 0, 2)
        Me._SoakToolStripLabel.Name = "_SoakToolStripLabel"
        Me._SoakToolStripLabel.Size = New System.Drawing.Size(37, 15)
        Me._SoakToolStripLabel.Text = "Soak:"
        '
        '_SoakSetpointNumericLabel
        '
        Me._SoakSetpointNumericLabel.Margin = New System.Windows.Forms.Padding(0, 5, 0, 2)
        Me._SoakSetpointNumericLabel.Name = "_SoakSetpointNumericLabel"
        Me._SoakSetpointNumericLabel.Size = New System.Drawing.Size(54, 15)
        Me._SoakSetpointNumericLabel.Text = "Setpoint:"
        '
        '_SoakSetpointNumeric
        '
        Me._SoakSetpointNumeric.Name = "_SoakSetpointNumeric"
        Me._SoakSetpointNumeric.Size = New System.Drawing.Size(41, 25)
        Me._SoakSetpointNumeric.Text = "0"
        Me._SoakSetpointNumeric.ToolTipText = "Setpoint in degrees F"
        Me._SoakSetpointNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_SoakTimeNumericLabel
        '
        Me._SoakTimeNumericLabel.Margin = New System.Windows.Forms.Padding(8, 5, 0, 2)
        Me._SoakTimeNumericLabel.Name = "_SoakTimeNumericLabel"
        Me._SoakTimeNumericLabel.Size = New System.Drawing.Size(53, 15)
        Me._SoakTimeNumericLabel.Text = "Time (s):"
        '
        '_SoakTimeNumeric
        '
        Me._SoakTimeNumeric.Name = "_SoakTimeNumeric"
        Me._SoakTimeNumeric.Size = New System.Drawing.Size(41, 25)
        Me._SoakTimeNumeric.Text = "0"
        Me._SoakTimeNumeric.ToolTipText = "Soak time in seconds"
        Me._SoakTimeNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_SoakWindowNumericLabel
        '
        Me._SoakWindowNumericLabel.Margin = New System.Windows.Forms.Padding(4, 5, 0, 2)
        Me._SoakWindowNumericLabel.Name = "_SoakWindowNumericLabel"
        Me._SoakWindowNumericLabel.Size = New System.Drawing.Size(54, 15)
        Me._SoakWindowNumericLabel.Text = "Window:"
        '
        '_SoakWindowNumeric
        '
        Me._SoakWindowNumeric.Name = "_SoakWindowNumeric"
        Me._SoakWindowNumeric.Size = New System.Drawing.Size(41, 25)
        Me._SoakWindowNumeric.Text = "2"
        Me._SoakWindowNumeric.ToolTipText = "Soak Window in degrees F"
        Me._SoakWindowNumeric.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        '_SoakHysteresisNumericLabel
        '
        Me._SoakHysteresisNumericLabel.Margin = New System.Windows.Forms.Padding(24, 5, 0, 2)
        Me._SoakHysteresisNumericLabel.Name = "_SoakHysteresisNumericLabel"
        Me._SoakHysteresisNumericLabel.Size = New System.Drawing.Size(63, 15)
        Me._SoakHysteresisNumericLabel.Text = "Hysteresis:"
        '
        '_SoakHysteresisNumeric
        '
        Me._SoakHysteresisNumeric.Name = "_SoakHysteresisNumeric"
        Me._SoakHysteresisNumeric.Size = New System.Drawing.Size(41, 25)
        Me._SoakHysteresisNumeric.Text = "1"
        Me._SoakHysteresisNumeric.ToolTipText = "Soak window hysteresis in degrees F"
        Me._SoakHysteresisNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_SampleIntervalNumericLabel
        '
        Me._SampleIntervalNumericLabel.Margin = New System.Windows.Forms.Padding(0, 5, 0, 2)
        Me._SampleIntervalNumericLabel.Name = "_SampleIntervalNumericLabel"
        Me._SampleIntervalNumericLabel.Size = New System.Drawing.Size(65, 15)
        Me._SampleIntervalNumericLabel.Text = "Sample (s):"
        '
        '_SampleIntervalNumeric
        '
        Me._SampleIntervalNumeric.Name = "_SampleIntervalNumeric"
        Me._SampleIntervalNumeric.Size = New System.Drawing.Size(41, 25)
        Me._SampleIntervalNumeric.Text = "15"
        Me._SampleIntervalNumeric.ToolTipText = "Sample interval in seconds"
        Me._SampleIntervalNumeric.Value = New Decimal(New Integer() {15, 0, 0, 0})
        '
        '_SoakResetDealyNumericToolStripLabel
        '
        Me._SoakResetDealyNumericToolStripLabel.Margin = New System.Windows.Forms.Padding(4, 5, 0, 2)
        Me._SoakResetDealyNumericToolStripLabel.Name = "_SoakResetDealyNumericToolStripLabel"
        Me._SoakResetDealyNumericToolStripLabel.Size = New System.Drawing.Size(55, 15)
        Me._SoakResetDealyNumericToolStripLabel.Text = "Delay (s):"
        '
        '_SoakResetDelayNumeric
        '
        Me._SoakResetDelayNumeric.Name = "_SoakResetDelayNumeric"
        Me._SoakResetDelayNumeric.Size = New System.Drawing.Size(41, 25)
        Me._SoakResetDelayNumeric.Text = "30"
        Me._SoakResetDelayNumeric.ToolTipText = "Soak Reset Delay in seconds"
        Me._SoakResetDelayNumeric.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        '_RampTimeoutNumericLabel
        '
        Me._RampTimeoutNumericLabel.Margin = New System.Windows.Forms.Padding(8, 5, 0, 2)
        Me._RampTimeoutNumericLabel.Name = "_RampTimeoutNumericLabel"
        Me._RampTimeoutNumericLabel.Size = New System.Drawing.Size(71, 15)
        Me._RampTimeoutNumericLabel.Text = "Timeout (s):"
        '
        '_RampTimeoutNumeric
        '
        Me._RampTimeoutNumeric.AutoSize = False
        Me._RampTimeoutNumeric.Name = "_RampTimeoutNumeric"
        Me._RampTimeoutNumeric.Size = New System.Drawing.Size(50, 25)
        Me._RampTimeoutNumeric.Text = "0"
        Me._RampTimeoutNumeric.ToolTipText = "Ramp timeout in seconds. 0 if no timeout."
        Me._RampTimeoutNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_OvenControlModeComboBoxLabel
        '
        Me._OvenControlModeComboBoxLabel.Margin = New System.Windows.Forms.Padding(0, 5, 0, 2)
        Me._OvenControlModeComboBoxLabel.Name = "_OvenControlModeComboBoxLabel"
        Me._OvenControlModeComboBoxLabel.Size = New System.Drawing.Size(38, 15)
        Me._OvenControlModeComboBoxLabel.Text = "Oven:"
        '
        '_OvenControlModeComboBox
        '
        Me._OvenControlModeComboBox.Name = "_OvenControlModeComboBox"
        Me._OvenControlModeComboBox.Size = New System.Drawing.Size(101, 23)
        Me._OvenControlModeComboBox.ToolTipText = "Oven control mode. Quiet not implemented yet."
        '
        '_MessagesTabPage
        '
        Me._MessagesTabPage.Controls.Add(Me._TraceMessagesBox)
        Me._MessagesTabPage.Location = New System.Drawing.Point(4, 26)
        Me._MessagesTabPage.Name = "_MessagesTabPage"
        Me._MessagesTabPage.Size = New System.Drawing.Size(391, 314)
        Me._MessagesTabPage.TabIndex = 3
        Me._MessagesTabPage.Text = "Log"
        Me._MessagesTabPage.UseVisualStyleBackColor = True
        '
        '_TraceMessagesBox
        '
        Me._TraceMessagesBox.AlertLevel = System.Diagnostics.TraceEventType.Warning
        Me._TraceMessagesBox.BackColor = System.Drawing.SystemColors.Info
        Me._TraceMessagesBox.CaptionFormat = "{0} ≡"
        Me._TraceMessagesBox.CausesValidation = False
        Me._TraceMessagesBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._TraceMessagesBox.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TraceMessagesBox.Location = New System.Drawing.Point(0, 0)
        Me._TraceMessagesBox.Multiline = True
        Me._TraceMessagesBox.Name = "_TraceMessagesBox"
        Me._TraceMessagesBox.PresetCount = 100
        Me._TraceMessagesBox.ReadOnly = True
        Me._TraceMessagesBox.ResetCount = 200
        Me._TraceMessagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._TraceMessagesBox.Size = New System.Drawing.Size(391, 314)
        Me._TraceMessagesBox.TabIndex = 0
        '
        '_LastErrorTextBox
        '
        Me._LastErrorTextBox.BackColor = System.Drawing.SystemColors.MenuText
        Me._LastErrorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me._LastErrorTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._LastErrorTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._LastErrorTextBox.ForeColor = System.Drawing.Color.OrangeRed
        Me._LastErrorTextBox.Location = New System.Drawing.Point(0, 64)
        Me._LastErrorTextBox.Name = "_LastErrorTextBox"
        Me._LastErrorTextBox.Size = New System.Drawing.Size(399, 18)
        Me._LastErrorTextBox.TabIndex = 4
        Me._LastErrorTextBox.TabStop = False
        Me._LastErrorTextBox.Text = "000, No Errors"
        '
        '_InfoStatusStrip
        '
        Me._InfoStatusStrip.BackColor = System.Drawing.Color.Black
        Me._InfoStatusStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me._InfoStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FillerStatusLabel, Me._TransactionElapsedTimeLabel})
        Me._InfoStatusStrip.Location = New System.Drawing.Point(0, 42)
        Me._InfoStatusStrip.Name = "_InfoStatusStrip"
        Me._InfoStatusStrip.Size = New System.Drawing.Size(399, 22)
        Me._InfoStatusStrip.SizingGrip = False
        Me._InfoStatusStrip.TabIndex = 6
        Me._InfoStatusStrip.Text = "Additional Device Info"
        '
        '_FillerStatusLabel
        '
        Me._FillerStatusLabel.Name = "_FillerStatusLabel"
        Me._FillerStatusLabel.Size = New System.Drawing.Size(352, 17)
        Me._FillerStatusLabel.Spring = True
        Me._FillerStatusLabel.Text = "<filler>"
        '
        '_TransactionElapsedTimeLabel
        '
        Me._TransactionElapsedTimeLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TransactionElapsedTimeLabel.ForeColor = System.Drawing.SystemColors.Info
        Me._TransactionElapsedTimeLabel.Name = "_TransactionElapsedTimeLabel"
        Me._TransactionElapsedTimeLabel.Size = New System.Drawing.Size(32, 17)
        Me._TransactionElapsedTimeLabel.Text = "0 ms"
        '
        '_ReadingStatusStrip
        '
        Me._ReadingStatusStrip.BackColor = System.Drawing.Color.Black
        Me._ReadingStatusStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me._ReadingStatusStrip.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReadingStatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._ReadingStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ComplianceToolStripStatusLabel, Me._AnalogInputReadingLabel, Me._SetpointLabel, Me._TbdToolStripStatusLabel})
        Me._ReadingStatusStrip.Location = New System.Drawing.Point(0, 0)
        Me._ReadingStatusStrip.Name = "_ReadingStatusStrip"
        Me._ReadingStatusStrip.Size = New System.Drawing.Size(399, 42)
        Me._ReadingStatusStrip.SizingGrip = False
        Me._ReadingStatusStrip.TabIndex = 1
        '
        '_ComplianceToolStripStatusLabel
        '
        Me._ComplianceToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ComplianceToolStripStatusLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ComplianceToolStripStatusLabel.ForeColor = System.Drawing.Color.Red
        Me._ComplianceToolStripStatusLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._ComplianceToolStripStatusLabel.Name = "_ComplianceToolStripStatusLabel"
        Me._ComplianceToolStripStatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._ComplianceToolStripStatusLabel.Size = New System.Drawing.Size(16, 42)
        Me._ComplianceToolStripStatusLabel.Text = "C"
        Me._ComplianceToolStripStatusLabel.ToolTipText = "Compliance"
        '
        '_AnalogInputReadingLabel
        '
        Me._AnalogInputReadingLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AnalogInputReadingLabel.ForeColor = System.Drawing.Color.Red
        Me._AnalogInputReadingLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._AnalogInputReadingLabel.Name = "_AnalogInputReadingLabel"
        Me._AnalogInputReadingLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._AnalogInputReadingLabel.Size = New System.Drawing.Size(174, 42)
        Me._AnalogInputReadingLabel.Spring = True
        Me._AnalogInputReadingLabel.Text = "0.000 °F"
        Me._AnalogInputReadingLabel.ToolTipText = "Reading"
        '
        '_SetpointLabel
        '
        Me._SetpointLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SetpointLabel.ForeColor = System.Drawing.Color.SpringGreen
        Me._SetpointLabel.Name = "_SetpointLabel"
        Me._SetpointLabel.Size = New System.Drawing.Size(174, 37)
        Me._SetpointLabel.Spring = True
        Me._SetpointLabel.Text = "0.000 °F"
        '
        '_TbdToolStripStatusLabel
        '
        Me._TbdToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TbdToolStripStatusLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TbdToolStripStatusLabel.ForeColor = System.Drawing.Color.Aquamarine
        Me._TbdToolStripStatusLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._TbdToolStripStatusLabel.Name = "_TbdToolStripStatusLabel"
        Me._TbdToolStripStatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._TbdToolStripStatusLabel.Size = New System.Drawing.Size(20, 42)
        Me._TbdToolStripStatusLabel.Text = " T"
        Me._TbdToolStripStatusLabel.ToolTipText = "To be defined"
        '
        '_SoakCOntrolButton
        '
        Me._SoakControlButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SoakControlButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StartSoakMenuItem, Me._PauseSoakMenuItem, Me._AbortSoakMenuItem, Me._RefreshSoakMenuItem})
        Me._SoakControlButton.Image = CType(resources.GetObject("_SoakCOntrolButton.Image"), System.Drawing.Image)
        Me._SoakControlButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SoakControlButton.Name = "_SoakCOntrolButton"
        Me._SoakControlButton.Size = New System.Drawing.Size(54, 19)
        Me._SoakControlButton.Text = "Select:"
        Me._SoakControlButton.ToolTipText = "Select soak control"
        '
        '_StartSoakMenuItem
        '
        Me._StartSoakMenuItem.CheckOnClick = True
        Me._StartSoakMenuItem.Name = "_StartSoakMenuItem"
        Me._StartSoakMenuItem.Size = New System.Drawing.Size(152, 22)
        Me._StartSoakMenuItem.Text = "Start"
        Me._StartSoakMenuItem.ToolTipText = "Starts or stops the soak sequence"
        '
        '_PauseSoakMenuItem
        '
        Me._PauseSoakMenuItem.CheckOnClick = True
        Me._PauseSoakMenuItem.Name = "_PauseSoakMenuItem"
        Me._PauseSoakMenuItem.Size = New System.Drawing.Size(152, 22)
        Me._PauseSoakMenuItem.Text = "Pause"
        Me._PauseSoakMenuItem.ToolTipText = "Pause or resume the soak sequence"
        '
        '_AbortSoakMenuItem
        '
        Me._AbortSoakMenuItem.Name = "_AbortSoakMenuItem"
        Me._AbortSoakMenuItem.Size = New System.Drawing.Size(152, 22)
        Me._AbortSoakMenuItem.Text = "Abort"
        Me._AbortSoakMenuItem.ToolTipText = "Aborts the soak sequence"
        '
        '_RefreshSoakMenuItem
        '
        Me._RefreshSoakMenuItem.Name = "_RefreshSoakMenuItem"
        Me._RefreshSoakMenuItem.Size = New System.Drawing.Size(152, 22)
        Me._RefreshSoakMenuItem.Text = "Refresh"
        Me._RefreshSoakMenuItem.ToolTipText = "Changes the soak setpoint"
        '
        'EasyZonePanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._Layout)
        Me.Controls.Add(Me._ResourceToolStrip)
        Me.Controls.Add(Me._StatusStrip)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "EasyZonePanel"
        Me.Size = New System.Drawing.Size(399, 491)
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ResourceToolStrip.ResumeLayout(False)
        Me._ResourceToolStrip.PerformLayout()
        Me._Layout.ResumeLayout(False)
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        Me._Tabs.ResumeLayout(False)
        Me._ReadingTabPage.ResumeLayout(False)
        Me._ReadingTabPage.PerformLayout()
        Me._SoakStatusStrip.ResumeLayout(False)
        Me._SoakStatusStrip.PerformLayout()
        Me._ToolStripPanel.ResumeLayout(False)
        Me._ToolStripPanel.PerformLayout()
        Me._InputToolStrip.ResumeLayout(False)
        Me._InputToolStrip.PerformLayout()
        Me._AlarmToolStrip.ResumeLayout(False)
        Me._AlarmToolStrip.PerformLayout()
        Me._ProcessToolStrip.ResumeLayout(False)
        Me._ProcessToolStrip.PerformLayout()
        Me._SoakToolStrip.ResumeLayout(False)
        Me._SoakToolStrip.PerformLayout()
        Me._MessagesTabPage.ResumeLayout(False)
        Me._MessagesTabPage.PerformLayout()
        Me._InfoStatusStrip.ResumeLayout(False)
        Me._InfoStatusStrip.PerformLayout()
        Me._ReadingStatusStrip.ResumeLayout(False)
        Me._ReadingStatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _StatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _StatusLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _ErrorStatusLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _ToolTip As Windows.Forms.ToolTip
    Private WithEvents _ErrorProvider As isr.Core.Controls.InfoProvider
    Private WithEvents _ResourceToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _PortComboBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _PortComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _OpenPortButton As Windows.Forms.ToolStripButton
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _TitleLabel As Windows.Forms.Label
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _Tabs As Windows.Forms.TabControl
    Private WithEvents _ReadingTabPage As Windows.Forms.TabPage
    Private WithEvents _MessagesTabPage As Windows.Forms.TabPage
    Private WithEvents _LastErrorTextBox As Windows.Forms.TextBox
    Private WithEvents _ReadingStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _ComplianceToolStripStatusLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _AnalogInputReadingLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _TbdToolStripStatusLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _TraceMessagesBox As Core.Forma.TraceMessagesBox
    Private WithEvents _SetpointLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _ToolStripPanel As Windows.Forms.ToolStripPanel
    Private WithEvents _InputToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _InputNumberComboBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _InputNumberComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _ReadInput1Button As Windows.Forms.ToolStripButton
    Private WithEvents _AlarmToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _AlarmNumberComboBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _AlarmNumberComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _AlarmTypeComboBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _AlarmTypeComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _ProcessToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ProcessLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SetpointNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SetpointNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _UnitIdNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _UnitIdNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _Separator1 As Windows.Forms.ToolStripSeparator
    Private WithEvents _InputErrorLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _InfoStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _FillerStatusLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _TransactionElapsedTimeLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _ToolStripSeparator1 As Windows.Forms.ToolStripSeparator
    Private WithEvents _SetpointActionButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _WriteSetpointMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSetpointMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _AlarmTypeActionsButton As Windows.Forms.ToolStripDropDownButton
    Private WithEvents _WriteAlarmTypeMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadAlarmTypeMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _RefreshPortsButton As Windows.Forms.ToolStripButton
    Private WithEvents _ApplySetpointMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ApplyAlarmTypeMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SoakToolStripLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SoakSetpointNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SoakSetpointNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _SoakTimeNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _SoakWindowNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SoakWindowNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _SoakHysteresisNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SoakHysteresisNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _SampleIntervalNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _SoakResetDealyNumericToolStripLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SoakResetDelayNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _SoakToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _SoakStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _SoakStateLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _SoakAutomatonProgress As Windows.Forms.ToolStripProgressBar
    Private WithEvents _SoakElapsedTimeLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _ReadInputErrorButton As Windows.Forms.ToolStripButton
    Private WithEvents _RampTimeoutNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _SoakTimeNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SampleIntervalNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _RampTimeoutNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _OvenControlModeComboBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _OvenControlModeComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _AsyncNotifyMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _UnsafeInvokeMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SyncNotifyMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _EventNotificationTestSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _SoakControlButton As Windows.Forms.ToolStripDropDownButton
    Private WithEvents _StartSoakMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _PauseSoakMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _AbortSoakMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _RefreshSoakMenuItem As Windows.Forms.ToolStripMenuItem
End Class
