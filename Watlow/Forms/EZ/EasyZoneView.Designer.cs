using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Modbus.Watlow.Forms
{
    [DesignerGenerated()]
    public partial class EasyZoneView
    {
        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(EasyZoneView));
            _StatusStrip = new System.Windows.Forms.StatusStrip();
            _StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _ErrorStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _ToolTip = new System.Windows.Forms.ToolTip(components);
            _ErrorProvider = new Core.Controls.InfoProvider(components);
            _ResourceToolStrip = new System.Windows.Forms.ToolStrip();
            _UnitIdNumericLabel = new System.Windows.Forms.ToolStripLabel();
            _UnitIdNumeric = new Core.Controls.ToolStripNumericUpDown();
            _Separator1 = new System.Windows.Forms.ToolStripSeparator();
            __RefreshPortsButton = new System.Windows.Forms.ToolStripButton();
            __RefreshPortsButton.Click += new EventHandler(RefreshPortsButton_Click);
            _PortComboBoxLabel = new System.Windows.Forms.ToolStripLabel();
            _PortComboBox = new System.Windows.Forms.ToolStripComboBox();
            __OpenPortButton = new System.Windows.Forms.ToolStripButton();
            __OpenPortButton.Click += new EventHandler(OpenPortButton_Click);
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _TitleLabel = new System.Windows.Forms.Label();
            _Panel = new System.Windows.Forms.Panel();
            _Tabs = new System.Windows.Forms.TabControl();
            _ReadingTabPage = new System.Windows.Forms.TabPage();
            _SoakStatusStrip = new System.Windows.Forms.StatusStrip();
            _SoakStateLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _SoakElapsedTimeLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _SoakAutomatonProgress = new System.Windows.Forms.ToolStripProgressBar();
            _ToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            _InputToolStrip = new System.Windows.Forms.ToolStrip();
            _InputNumberComboBoxLabel = new System.Windows.Forms.ToolStripLabel();
            _InputNumberComboBox = new System.Windows.Forms.ToolStripComboBox();
            __ReadInput1Button = new System.Windows.Forms.ToolStripButton();
            __ReadInput1Button.Click += new EventHandler(ReadInputButton_Click);
            _ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            _InputErrorLabel = new System.Windows.Forms.ToolStripLabel();
            __ReadInputErrorButton = new System.Windows.Forms.ToolStripButton();
            __ReadInputErrorButton.Click += new EventHandler(ReadInputErrorButton_Click);
            _AlarmToolStrip = new System.Windows.Forms.ToolStrip();
            _AlarmNumberComboBoxLabel = new System.Windows.Forms.ToolStripLabel();
            _AlarmNumberComboBox = new System.Windows.Forms.ToolStripComboBox();
            _AlarmTypeComboBoxLabel = new System.Windows.Forms.ToolStripLabel();
            _AlarmTypeComboBox = new System.Windows.Forms.ToolStripComboBox();
            _AlarmTypeActionsButton = new System.Windows.Forms.ToolStripDropDownButton();
            __WriteAlarmTypeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __WriteAlarmTypeMenuItem.Click += new EventHandler(WriteAlarmTypeMenuItem_Click);
            __ReadAlarmTypeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ReadAlarmTypeMenuItem.Click += new EventHandler(ReadAlarmTypeMenuItem_Click);
            __ApplyAlarmTypeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ApplyAlarmTypeMenuItem.Click += new EventHandler(ApplyAlarmTypeMenuItem_Click);
            _ProcessToolStrip = new System.Windows.Forms.ToolStrip();
            _ProcessLabel = new System.Windows.Forms.ToolStripLabel();
            _SetpointNumericLabel = new System.Windows.Forms.ToolStripLabel();
            _SetpointNumeric = new Core.Controls.ToolStripNumericUpDown();
            _SetpointActionButton = new System.Windows.Forms.ToolStripSplitButton();
            __WriteSetpointMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __WriteSetpointMenuItem.Click += new EventHandler(WriteSetpointMenuItem_Click);
            __ReadSetpointMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ReadSetpointMenuItem.Click += new EventHandler(ReadSetpointMenuItem_Click);
            __ApplySetpointMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ApplySetpointMenuItem.Click += new EventHandler(ApplySetpointMenuItem_Click);
            _EventNotificationTestSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            __AsyncNotifyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __AsyncNotifyMenuItem.Click += new EventHandler(AsyncNotifyMenuItem_Click);
            __UnsafeInvokeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __UnsafeInvokeMenuItem.Click += new EventHandler(UnsafeInvokeMenuItem_Click);
            __SyncNotifyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __SyncNotifyMenuItem.Click += new EventHandler(SyncNotifyMenuItem_Click);
            _SoakToolStrip = new System.Windows.Forms.ToolStrip();
            _SoakToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _SoakSetpointNumericLabel = new System.Windows.Forms.ToolStripLabel();
            __SoakSetpointNumeric = new Core.Controls.ToolStripNumericUpDown();
            __SoakSetpointNumeric.ValueChanged += new EventHandler<EventArgs>(SoakSetpointNumeric_ValueChanged);
            _SoakTimeNumericLabel = new System.Windows.Forms.ToolStripLabel();
            _SoakTimeNumeric = new Core.Controls.ToolStripNumericUpDown();
            _SoakWindowNumericLabel = new System.Windows.Forms.ToolStripLabel();
            _SoakWindowNumeric = new Core.Controls.ToolStripNumericUpDown();
            _SoakHysteresisNumericLabel = new System.Windows.Forms.ToolStripLabel();
            _SoakHysteresisNumeric = new Core.Controls.ToolStripNumericUpDown();
            _SampleIntervalNumericLabel = new System.Windows.Forms.ToolStripLabel();
            _SampleIntervalNumeric = new Core.Controls.ToolStripNumericUpDown();
            _SoakResetDealyNumericToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _SoakResetDelayNumeric = new Core.Controls.ToolStripNumericUpDown();
            _RampTimeoutNumericLabel = new System.Windows.Forms.ToolStripLabel();
            _RampTimeoutNumeric = new Core.Controls.ToolStripNumericUpDown();
            _OvenControlModeComboBoxLabel = new System.Windows.Forms.ToolStripLabel();
            __OvenControlModeComboBox = new System.Windows.Forms.ToolStripComboBox();
            __OvenControlModeComboBox.SelectedIndexChanged += new EventHandler(OvenControlModeComboBox_SelectedIndexChanged);
            _MessagesTabPage = new System.Windows.Forms.TabPage();
            __TraceMessagesBox = new Core.Forma.TraceMessagesBox();
            __TraceMessagesBox.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(TraceMessagesBox_PropertyChanged);
            _LastErrorTextBox = new System.Windows.Forms.TextBox();
            _InfoStatusStrip = new System.Windows.Forms.StatusStrip();
            _FillerStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _TransactionElapsedTimeLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _ReadingStatusStrip = new System.Windows.Forms.StatusStrip();
            _ComplianceToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _AnalogInputReadingLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _SetpointLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _TbdToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _SoakControlButton = new System.Windows.Forms.ToolStripDropDownButton();
            __StartSoakMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __StartSoakMenuItem.CheckStateChanged += new EventHandler(StartSoakMenuItem_CheckStateChanged);
            __PauseSoakMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __PauseSoakMenuItem.CheckStateChanged += new EventHandler(PauseSoakMenuItem_CheckStateChanged);
            __AbortSoakMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __AbortSoakMenuItem.Click += new EventHandler(AbortSoakMenuItem_Click);
            __RefreshSoakMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __RefreshSoakMenuItem.Click += new EventHandler(RefreshSoakMenuItem_Click);
            _StatusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            _ResourceToolStrip.SuspendLayout();
            _Layout.SuspendLayout();
            _Panel.SuspendLayout();
            _Tabs.SuspendLayout();
            _ReadingTabPage.SuspendLayout();
            _SoakStatusStrip.SuspendLayout();
            _ToolStripPanel.SuspendLayout();
            _InputToolStrip.SuspendLayout();
            _AlarmToolStrip.SuspendLayout();
            _ProcessToolStrip.SuspendLayout();
            _SoakToolStrip.SuspendLayout();
            _MessagesTabPage.SuspendLayout();
            _InfoStatusStrip.SuspendLayout();
            _ReadingStatusStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _StatusStrip
            // 
            _StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _StatusLabel, _ErrorStatusLabel });
            _StatusStrip.Location = new System.Drawing.Point(0, 469);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            _StatusStrip.Size = new System.Drawing.Size(399, 22);
            _StatusStrip.TabIndex = 0;
            _StatusStrip.Text = "StatusStrip1";
            // 
            // _StatusLabel
            // 
            _StatusLabel.Name = "_StatusLabel";
            _StatusLabel.Size = new System.Drawing.Size(345, 17);
            _StatusLabel.Spring = true;
            _StatusLabel.Text = "<status>";
            // 
            // _ErrorStatusLabel
            // 
            _ErrorStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _ErrorStatusLabel.Name = "_ErrorStatusLabel";
            _ErrorStatusLabel.Size = new System.Drawing.Size(37, 17);
            _ErrorStatusLabel.Text = "<err>";
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _ResourceToolStrip
            // 
            _ResourceToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            _ResourceToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _UnitIdNumericLabel, _UnitIdNumeric, _Separator1, __RefreshPortsButton, _PortComboBoxLabel, _PortComboBox, __OpenPortButton });
            _ResourceToolStrip.Location = new System.Drawing.Point(0, 443);
            _ResourceToolStrip.Name = "_ResourceToolStrip";
            _ResourceToolStrip.Size = new System.Drawing.Size(399, 26);
            _ResourceToolStrip.TabIndex = 1;
            _ResourceToolStrip.Text = "Resource Tool Strip";
            // 
            // _UnitIdNumericLabel
            // 
            _UnitIdNumericLabel.Name = "_UnitIdNumericLabel";
            _UnitIdNumericLabel.Size = new System.Drawing.Size(21, 23);
            _UnitIdNumericLabel.Text = "ID:";
            // 
            // _UnitIdNumeric
            // 
            _UnitIdNumeric.Name = "_UnitIdNumeric";
            _UnitIdNumeric.Size = new System.Drawing.Size(41, 23);
            _UnitIdNumeric.Text = "1";
            _UnitIdNumeric.ToolTipText = "EZ-Zone unit ID";
            _UnitIdNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _Separator1
            // 
            _Separator1.Name = "_Separator1";
            _Separator1.Size = new System.Drawing.Size(6, 26);
            // 
            // _RefreshPortsButton
            // 
            __RefreshPortsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __RefreshPortsButton.Image =  My.Resources.Resources.view_refresh_5;
            __RefreshPortsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __RefreshPortsButton.Name = "__RefreshPortsButton";
            __RefreshPortsButton.Size = new System.Drawing.Size(23, 23);
            __RefreshPortsButton.Text = "Refresh";
            __RefreshPortsButton.ToolTipText = "Refresh list of ports";
            // 
            // _PortComboBoxLabel
            // 
            _PortComboBoxLabel.Name = "_PortComboBoxLabel";
            _PortComboBoxLabel.Size = new System.Drawing.Size(32, 23);
            _PortComboBoxLabel.Text = "Port:";
            // 
            // _PortComboBox
            // 
            _PortComboBox.Name = "_PortComboBox";
            _PortComboBox.Size = new System.Drawing.Size(81, 26);
            _PortComboBox.ToolTipText = "Available ports";
            // 
            // _OpenPortButton
            // 
            __OpenPortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __OpenPortButton.Image = (System.Drawing.Image)resources.GetObject("_OpenPortButton.Image");
            __OpenPortButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __OpenPortButton.Name = "__OpenPortButton";
            __OpenPortButton.Size = new System.Drawing.Size(40, 23);
            __OpenPortButton.Text = "Open";
            __OpenPortButton.ToolTipText = "Open port";
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 1;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.Controls.Add(_TitleLabel, 0, 0);
            _Layout.Controls.Add(_Panel, 0, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Margin = new System.Windows.Forms.Padding(0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 2;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _Layout.Size = new System.Drawing.Size(399, 443);
            _Layout.TabIndex = 18;
            // 
            // _TitleLabel
            // 
            _TitleLabel.BackColor = System.Drawing.Color.Black;
            _TitleLabel.CausesValidation = false;
            _TitleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _TitleLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TitleLabel.ForeColor = System.Drawing.SystemColors.Info;
            _TitleLabel.Location = new System.Drawing.Point(0, 0);
            _TitleLabel.Margin = new System.Windows.Forms.Padding(0);
            _TitleLabel.Name = "_TitleLabel";
            _TitleLabel.Size = new System.Drawing.Size(399, 17);
            _TitleLabel.TabIndex = 17;
            _TitleLabel.Text = "RTU Closed";
            _TitleLabel.UseMnemonic = false;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(_Tabs);
            _Panel.Controls.Add(_LastErrorTextBox);
            _Panel.Controls.Add(_InfoStatusStrip);
            _Panel.Controls.Add(_ReadingStatusStrip);
            _Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            _Panel.Location = new System.Drawing.Point(0, 17);
            _Panel.Margin = new System.Windows.Forms.Padding(0);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(399, 426);
            _Panel.TabIndex = 16;
            // 
            // _Tabs
            // 
            _Tabs.Controls.Add(_ReadingTabPage);
            _Tabs.Controls.Add(_MessagesTabPage);
            _Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            _Tabs.ItemSize = new System.Drawing.Size(52, 22);
            _Tabs.Location = new System.Drawing.Point(0, 82);
            _Tabs.Name = "_Tabs";
            _Tabs.SelectedIndex = 0;
            _Tabs.Size = new System.Drawing.Size(399, 344);
            _Tabs.TabIndex = 5;
            // 
            // _ReadingTabPage
            // 
            _ReadingTabPage.Controls.Add(_SoakStatusStrip);
            _ReadingTabPage.Controls.Add(_ToolStripPanel);
            _ReadingTabPage.Location = new System.Drawing.Point(4, 26);
            _ReadingTabPage.Name = "_ReadingTabPage";
            _ReadingTabPage.Size = new System.Drawing.Size(391, 314);
            _ReadingTabPage.TabIndex = 0;
            _ReadingTabPage.Text = "Reading";
            _ReadingTabPage.UseVisualStyleBackColor = true;
            // 
            // _SoakStatusStrip
            // 
            _SoakStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _SoakStateLabel, _SoakElapsedTimeLabel, _SoakAutomatonProgress });
            _SoakStatusStrip.Location = new System.Drawing.Point(0, 292);
            _SoakStatusStrip.Name = "_SoakStatusStrip";
            _SoakStatusStrip.Size = new System.Drawing.Size(391, 22);
            _SoakStatusStrip.TabIndex = 1;
            _SoakStatusStrip.Text = "Soak Status";
            // 
            // _SoakStateLabel
            // 
            _SoakStateLabel.Name = "_SoakStateLabel";
            _SoakStateLabel.Size = new System.Drawing.Size(77, 17);
            _SoakStateLabel.Text = "<Soak State>";
            // 
            // _SoakElapsedTimeLabel
            // 
            _SoakElapsedTimeLabel.Name = "_SoakElapsedTimeLabel";
            _SoakElapsedTimeLabel.Size = new System.Drawing.Size(63, 17);
            _SoakElapsedTimeLabel.Text = "<elapsed>";
            // 
            // _SoakAutomatonProgress
            // 
            _SoakAutomatonProgress.Name = "_SoakAutomatonProgress";
            _SoakAutomatonProgress.Size = new System.Drawing.Size(100, 16);
            _SoakAutomatonProgress.ToolTipText = "Soak Time";
            // 
            // _ToolStripPanel
            // 
            _ToolStripPanel.Controls.Add(_InputToolStrip);
            _ToolStripPanel.Controls.Add(_AlarmToolStrip);
            _ToolStripPanel.Controls.Add(_ProcessToolStrip);
            _ToolStripPanel.Controls.Add(_SoakToolStrip);
            _ToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top;
            _ToolStripPanel.Location = new System.Drawing.Point(0, 0);
            _ToolStripPanel.Name = "_ToolStripPanel";
            _ToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            _ToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            _ToolStripPanel.Size = new System.Drawing.Size(391, 173);
            // 
            // _InputToolStrip
            // 
            _InputToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _InputToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _InputNumberComboBoxLabel, _InputNumberComboBox, __ReadInput1Button, _ToolStripSeparator1, _InputErrorLabel, __ReadInputErrorButton });
            _InputToolStrip.Location = new System.Drawing.Point(3, 0);
            _InputToolStrip.Name = "_InputToolStrip";
            _InputToolStrip.Padding = new System.Windows.Forms.Padding(0, 3, 1, 0);
            _InputToolStrip.Size = new System.Drawing.Size(208, 26);
            _InputToolStrip.TabIndex = 0;
            // 
            // _InputNumberComboBoxLabel
            // 
            _InputNumberComboBoxLabel.Name = "_InputNumberComboBoxLabel";
            _InputNumberComboBoxLabel.Size = new System.Drawing.Size(38, 20);
            _InputNumberComboBoxLabel.Text = "Input:";
            // 
            // _InputNumberComboBox
            // 
            _InputNumberComboBox.AutoSize = false;
            _InputNumberComboBox.Items.AddRange(new object[] { "1" });
            _InputNumberComboBox.Name = "_InputNumberComboBox";
            _InputNumberComboBox.Size = new System.Drawing.Size(35, 23);
            _InputNumberComboBox.Text = "1";
            _InputNumberComboBox.ToolTipText = "Input number";
            // 
            // _ReadInput1Button
            // 
            __ReadInput1Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ReadInput1Button.Image = (System.Drawing.Image)resources.GetObject("_ReadInput1Button.Image");
            __ReadInput1Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ReadInput1Button.Name = "__ReadInput1Button";
            __ReadInput1Button.Size = new System.Drawing.Size(37, 20);
            __ReadInput1Button.Text = "Read";
            __ReadInput1Button.ToolTipText = "Read input 1";
            // 
            // _ToolStripSeparator1
            // 
            _ToolStripSeparator1.Name = "_ToolStripSeparator1";
            _ToolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // _InputErrorLabel
            // 
            _InputErrorLabel.Name = "_InputErrorLabel";
            _InputErrorLabel.Size = new System.Drawing.Size(37, 20);
            _InputErrorLabel.Text = "<err>";
            _InputErrorLabel.ToolTipText = "Last input error";
            // 
            // _ReadInputErrorButton
            // 
            __ReadInputErrorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ReadInputErrorButton.Image = (System.Drawing.Image)resources.GetObject("_ReadInputErrorButton.Image");
            __ReadInputErrorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ReadInputErrorButton.Name = "__ReadInputErrorButton";
            __ReadInputErrorButton.Size = new System.Drawing.Size(41, 20);
            __ReadInputErrorButton.Text = "Error?";
            __ReadInputErrorButton.ToolTipText = "Reads the input error";
            // 
            // _AlarmToolStrip
            // 
            _AlarmToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _AlarmToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _AlarmNumberComboBoxLabel, _AlarmNumberComboBox, _AlarmTypeComboBoxLabel, _AlarmTypeComboBox, _AlarmTypeActionsButton });
            _AlarmToolStrip.Location = new System.Drawing.Point(3, 26);
            _AlarmToolStrip.Name = "_AlarmToolStrip";
            _AlarmToolStrip.Padding = new System.Windows.Forms.Padding(0, 3, 1, 0);
            _AlarmToolStrip.Size = new System.Drawing.Size(254, 26);
            _AlarmToolStrip.TabIndex = 1;
            // 
            // _AlarmNumberComboBoxLabel
            // 
            _AlarmNumberComboBoxLabel.Name = "_AlarmNumberComboBoxLabel";
            _AlarmNumberComboBoxLabel.Size = new System.Drawing.Size(42, 20);
            _AlarmNumberComboBoxLabel.Text = "Alarm:";
            // 
            // _AlarmNumberComboBox
            // 
            _AlarmNumberComboBox.AutoSize = false;
            _AlarmNumberComboBox.Items.AddRange(new object[] { "1" });
            _AlarmNumberComboBox.Name = "_AlarmNumberComboBox";
            _AlarmNumberComboBox.Size = new System.Drawing.Size(30, 23);
            _AlarmNumberComboBox.Text = "1";
            _AlarmNumberComboBox.ToolTipText = "Alarm number";
            // 
            // _AlarmTypeComboBoxLabel
            // 
            _AlarmTypeComboBoxLabel.Name = "_AlarmTypeComboBoxLabel";
            _AlarmTypeComboBoxLabel.Size = new System.Drawing.Size(35, 20);
            _AlarmTypeComboBoxLabel.Text = "Type:";
            // 
            // _AlarmTypeComboBox
            // 
            _AlarmTypeComboBox.AutoSize = false;
            _AlarmTypeComboBox.Items.AddRange(new object[] { "Off", "Process" });
            _AlarmTypeComboBox.Name = "_AlarmTypeComboBox";
            _AlarmTypeComboBox.Size = new System.Drawing.Size(65, 23);
            _AlarmTypeComboBox.Text = "Process";
            _AlarmTypeComboBox.ToolTipText = "Alarm Type";
            // 
            // _AlarmTypeActionsButton
            // 
            _AlarmTypeActionsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _AlarmTypeActionsButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __WriteAlarmTypeMenuItem, __ReadAlarmTypeMenuItem, __ApplyAlarmTypeMenuItem });
            _AlarmTypeActionsButton.Image = (System.Drawing.Image)resources.GetObject("_AlarmTypeActionsButton.Image");
            _AlarmTypeActionsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _AlarmTypeActionsButton.Name = "_AlarmTypeActionsButton";
            _AlarmTypeActionsButton.Size = new System.Drawing.Size(66, 20);
            _AlarmTypeActionsButton.Text = "Actions: ";
            _AlarmTypeActionsButton.ToolTipText = "Write and then read Alarm Type";
            // 
            // _WriteAlarmTypeMenuItem
            // 
            __WriteAlarmTypeMenuItem.Name = "__WriteAlarmTypeMenuItem";
            __WriteAlarmTypeMenuItem.Size = new System.Drawing.Size(165, 22);
            __WriteAlarmTypeMenuItem.Text = "Write Alarm Type";
            __WriteAlarmTypeMenuItem.ToolTipText = "Writes alarm type to the controller";
            // 
            // _ReadAlarmTypeMenuItem
            // 
            __ReadAlarmTypeMenuItem.Name = "__ReadAlarmTypeMenuItem";
            __ReadAlarmTypeMenuItem.Size = new System.Drawing.Size(165, 22);
            __ReadAlarmTypeMenuItem.Text = "Read Alarm Type";
            __ReadAlarmTypeMenuItem.ToolTipText = "Reads Alarm Type from the controller";
            // 
            // _ApplyAlarmTypeMenuItem
            // 
            __ApplyAlarmTypeMenuItem.Name = "__ApplyAlarmTypeMenuItem";
            __ApplyAlarmTypeMenuItem.Size = new System.Drawing.Size(165, 22);
            __ApplyAlarmTypeMenuItem.Text = "Write and Read";
            __ApplyAlarmTypeMenuItem.ToolTipText = "Writes and then reads the alarm type from the controller";
            // 
            // _ProcessToolStrip
            // 
            _ProcessToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _ProcessToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _ProcessLabel, _SetpointNumericLabel, _SetpointNumeric, _SetpointActionButton, _EventNotificationTestSplitButton });
            _ProcessToolStrip.Location = new System.Drawing.Point(3, 52);
            _ProcessToolStrip.Name = "_ProcessToolStrip";
            _ProcessToolStrip.Padding = new System.Windows.Forms.Padding(0, 3, 1, 0);
            _ProcessToolStrip.Size = new System.Drawing.Size(304, 31);
            _ProcessToolStrip.TabIndex = 2;
            // 
            // _ProcessLabel
            // 
            _ProcessLabel.Name = "_ProcessLabel";
            _ProcessLabel.Size = new System.Drawing.Size(50, 25);
            _ProcessLabel.Text = "Process:";
            // 
            // _SetpointNumericLabel
            // 
            _SetpointNumericLabel.Name = "_SetpointNumericLabel";
            _SetpointNumericLabel.Size = new System.Drawing.Size(54, 25);
            _SetpointNumericLabel.Text = "Setpoint:";
            // 
            // _SetpointNumeric
            // 
            _SetpointNumeric.AutoSize = false;
            _SetpointNumeric.Name = "_SetpointNumeric";
            _SetpointNumeric.Size = new System.Drawing.Size(78, 25);
            _SetpointNumeric.Text = "75";
            _SetpointNumeric.ToolTipText = "Setpoint";
            _SetpointNumeric.Value = new decimal(new int[] { 75, 0, 0, 0 });
            // 
            // _SetpointActionButton
            // 
            _SetpointActionButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _SetpointActionButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __WriteSetpointMenuItem, __ReadSetpointMenuItem, __ApplySetpointMenuItem });
            _SetpointActionButton.Image = (System.Drawing.Image)resources.GetObject("_SetpointActionButton.Image");
            _SetpointActionButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SetpointActionButton.Name = "_SetpointActionButton";
            _SetpointActionButton.Size = new System.Drawing.Size(66, 25);
            _SetpointActionButton.Text = "Actions:";
            _SetpointActionButton.ToolTipText = "Opens the setpoint action menu";
            // 
            // _WriteSetpointMenuItem
            // 
            __WriteSetpointMenuItem.Name = "__WriteSetpointMenuItem";
            __WriteSetpointMenuItem.Size = new System.Drawing.Size(154, 22);
            __WriteSetpointMenuItem.Text = "Write Setpoint";
            __WriteSetpointMenuItem.ToolTipText = "Writes the setpoint to the controller";
            // 
            // _ReadSetpointMenuItem
            // 
            __ReadSetpointMenuItem.Name = "__ReadSetpointMenuItem";
            __ReadSetpointMenuItem.Size = new System.Drawing.Size(154, 22);
            __ReadSetpointMenuItem.Text = "Read Setpoint";
            __ReadSetpointMenuItem.ToolTipText = "Reads the setpoint from the controller";
            // 
            // _ApplySetpointMenuItem
            // 
            __ApplySetpointMenuItem.Name = "__ApplySetpointMenuItem";
            __ApplySetpointMenuItem.Size = new System.Drawing.Size(154, 22);
            __ApplySetpointMenuItem.Text = "Write and Read";
            // 
            // _EventNotificationTestSplitButton
            // 
            _EventNotificationTestSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _EventNotificationTestSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __AsyncNotifyMenuItem, __UnsafeInvokeMenuItem, __SyncNotifyMenuItem });
            _EventNotificationTestSplitButton.Image = (System.Drawing.Image)resources.GetObject("_EventNotificationTestSplitButton.Image");
            _EventNotificationTestSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _EventNotificationTestSplitButton.Name = "_EventNotificationTestSplitButton";
            _EventNotificationTestSplitButton.Size = new System.Drawing.Size(44, 25);
            _EventNotificationTestSplitButton.Text = "Test";
            // 
            // _AsyncNotifyMenuItem
            // 
            __AsyncNotifyMenuItem.Name = "__AsyncNotifyMenuItem";
            __AsyncNotifyMenuItem.Size = new System.Drawing.Size(189, 22);
            __AsyncNotifyMenuItem.Text = "Async Notify Test (unsafe)";
            // 
            // _UnsafeInvokeMenuItem
            // 
            __UnsafeInvokeMenuItem.Name = "__UnsafeInvokeMenuItem";
            __UnsafeInvokeMenuItem.Size = new System.Drawing.Size(189, 22);
            __UnsafeInvokeMenuItem.Text = "Invoke Test (maybe thread unsafe)";
            // 
            // _SyncNotifyMenuItem
            // 
            __SyncNotifyMenuItem.Name = "__SyncNotifyMenuItem";
            __SyncNotifyMenuItem.Size = new System.Drawing.Size(189, 22);
            __SyncNotifyMenuItem.Text = "Sync Notify Test (thread safe)";
            // 
            // _SoakToolStrip
            // 
            _SoakToolStrip.AutoSize = false;
            _SoakToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _SoakToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _SoakToolStripLabel, _SoakSetpointNumericLabel, __SoakSetpointNumeric, _SoakTimeNumericLabel, _SoakTimeNumeric, _SoakWindowNumericLabel, _SoakWindowNumeric, _SoakHysteresisNumericLabel, _SoakHysteresisNumeric, _SampleIntervalNumericLabel, _SampleIntervalNumeric, _SoakResetDealyNumericToolStripLabel, _SoakResetDelayNumeric, _RampTimeoutNumericLabel, _RampTimeoutNumeric, _OvenControlModeComboBoxLabel, __OvenControlModeComboBox, _SoakControlButton });
            _SoakToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            _SoakToolStrip.Location = new System.Drawing.Point(3, 83);
            _SoakToolStrip.Name = "_SoakToolStrip";
            _SoakToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            _SoakToolStrip.Size = new System.Drawing.Size(388, 90);
            _SoakToolStrip.TabIndex = 3;
            // 
            // _SoakToolStripLabel
            // 
            _SoakToolStripLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SoakToolStripLabel.Margin = new System.Windows.Forms.Padding(0, 4, 0, 2);
            _SoakToolStripLabel.Name = "_SoakToolStripLabel";
            _SoakToolStripLabel.Size = new System.Drawing.Size(37, 15);
            _SoakToolStripLabel.Text = "Soak:";
            // 
            // _SoakSetpointNumericLabel
            // 
            _SoakSetpointNumericLabel.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            _SoakSetpointNumericLabel.Name = "_SoakSetpointNumericLabel";
            _SoakSetpointNumericLabel.Size = new System.Drawing.Size(54, 15);
            _SoakSetpointNumericLabel.Text = "Setpoint:";
            // 
            // _SoakSetpointNumeric
            // 
            __SoakSetpointNumeric.Name = "__SoakSetpointNumeric";
            __SoakSetpointNumeric.Size = new System.Drawing.Size(41, 25);
            __SoakSetpointNumeric.Text = "0";
            __SoakSetpointNumeric.ToolTipText = "Setpoint in degrees F";
            __SoakSetpointNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _SoakTimeNumericLabel
            // 
            _SoakTimeNumericLabel.Margin = new System.Windows.Forms.Padding(8, 5, 0, 2);
            _SoakTimeNumericLabel.Name = "_SoakTimeNumericLabel";
            _SoakTimeNumericLabel.Size = new System.Drawing.Size(53, 15);
            _SoakTimeNumericLabel.Text = "Time (s):";
            // 
            // _SoakTimeNumeric
            // 
            _SoakTimeNumeric.Name = "_SoakTimeNumeric";
            _SoakTimeNumeric.Size = new System.Drawing.Size(41, 25);
            _SoakTimeNumeric.Text = "0";
            _SoakTimeNumeric.ToolTipText = "Soak time in seconds";
            _SoakTimeNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _SoakWindowNumericLabel
            // 
            _SoakWindowNumericLabel.Margin = new System.Windows.Forms.Padding(4, 5, 0, 2);
            _SoakWindowNumericLabel.Name = "_SoakWindowNumericLabel";
            _SoakWindowNumericLabel.Size = new System.Drawing.Size(54, 15);
            _SoakWindowNumericLabel.Text = "Window:";
            // 
            // _SoakWindowNumeric
            // 
            _SoakWindowNumeric.Name = "_SoakWindowNumeric";
            _SoakWindowNumeric.Size = new System.Drawing.Size(41, 25);
            _SoakWindowNumeric.Text = "2";
            _SoakWindowNumeric.ToolTipText = "Soak Window in degrees F";
            _SoakWindowNumeric.Value = new decimal(new int[] { 2, 0, 0, 0 });
            // 
            // _SoakHysteresisNumericLabel
            // 
            _SoakHysteresisNumericLabel.Margin = new System.Windows.Forms.Padding(24, 5, 0, 2);
            _SoakHysteresisNumericLabel.Name = "_SoakHysteresisNumericLabel";
            _SoakHysteresisNumericLabel.Size = new System.Drawing.Size(63, 15);
            _SoakHysteresisNumericLabel.Text = "Hysteresis:";
            // 
            // _SoakHysteresisNumeric
            // 
            _SoakHysteresisNumeric.Name = "_SoakHysteresisNumeric";
            _SoakHysteresisNumeric.Size = new System.Drawing.Size(41, 25);
            _SoakHysteresisNumeric.Text = "1";
            _SoakHysteresisNumeric.ToolTipText = "Soak window hysteresis in degrees F";
            _SoakHysteresisNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _SampleIntervalNumericLabel
            // 
            _SampleIntervalNumericLabel.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            _SampleIntervalNumericLabel.Name = "_SampleIntervalNumericLabel";
            _SampleIntervalNumericLabel.Size = new System.Drawing.Size(65, 15);
            _SampleIntervalNumericLabel.Text = "Sample (s):";
            // 
            // _SampleIntervalNumeric
            // 
            _SampleIntervalNumeric.Name = "_SampleIntervalNumeric";
            _SampleIntervalNumeric.Size = new System.Drawing.Size(41, 25);
            _SampleIntervalNumeric.Text = "15";
            _SampleIntervalNumeric.ToolTipText = "Sample interval in seconds";
            _SampleIntervalNumeric.Value = new decimal(new int[] { 15, 0, 0, 0 });
            // 
            // _SoakResetDealyNumericToolStripLabel
            // 
            _SoakResetDealyNumericToolStripLabel.Margin = new System.Windows.Forms.Padding(4, 5, 0, 2);
            _SoakResetDealyNumericToolStripLabel.Name = "_SoakResetDealyNumericToolStripLabel";
            _SoakResetDealyNumericToolStripLabel.Size = new System.Drawing.Size(55, 15);
            _SoakResetDealyNumericToolStripLabel.Text = "Delay (s):";
            // 
            // _SoakResetDelayNumeric
            // 
            _SoakResetDelayNumeric.Name = "_SoakResetDelayNumeric";
            _SoakResetDelayNumeric.Size = new System.Drawing.Size(41, 25);
            _SoakResetDelayNumeric.Text = "30";
            _SoakResetDelayNumeric.ToolTipText = "Soak Reset Delay in seconds";
            _SoakResetDelayNumeric.Value = new decimal(new int[] { 30, 0, 0, 0 });
            // 
            // _RampTimeoutNumericLabel
            // 
            _RampTimeoutNumericLabel.Margin = new System.Windows.Forms.Padding(8, 5, 0, 2);
            _RampTimeoutNumericLabel.Name = "_RampTimeoutNumericLabel";
            _RampTimeoutNumericLabel.Size = new System.Drawing.Size(71, 15);
            _RampTimeoutNumericLabel.Text = "Timeout (s):";
            // 
            // _RampTimeoutNumeric
            // 
            _RampTimeoutNumeric.AutoSize = false;
            _RampTimeoutNumeric.Name = "_RampTimeoutNumeric";
            _RampTimeoutNumeric.Size = new System.Drawing.Size(50, 25);
            _RampTimeoutNumeric.Text = "0";
            _RampTimeoutNumeric.ToolTipText = "Ramp timeout in seconds. 0 if no timeout.";
            _RampTimeoutNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _OvenControlModeComboBoxLabel
            // 
            _OvenControlModeComboBoxLabel.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            _OvenControlModeComboBoxLabel.Name = "_OvenControlModeComboBoxLabel";
            _OvenControlModeComboBoxLabel.Size = new System.Drawing.Size(38, 15);
            _OvenControlModeComboBoxLabel.Text = "Oven:";
            // 
            // _OvenControlModeComboBox
            // 
            __OvenControlModeComboBox.Name = "__OvenControlModeComboBox";
            __OvenControlModeComboBox.Size = new System.Drawing.Size(101, 23);
            __OvenControlModeComboBox.ToolTipText = "Oven control mode. Quiet not implemented yet.";
            // 
            // _MessagesTabPage
            // 
            _MessagesTabPage.Controls.Add(__TraceMessagesBox);
            _MessagesTabPage.Location = new System.Drawing.Point(4, 26);
            _MessagesTabPage.Name = "_MessagesTabPage";
            _MessagesTabPage.Size = new System.Drawing.Size(391, 314);
            _MessagesTabPage.TabIndex = 3;
            _MessagesTabPage.Text = "Log";
            _MessagesTabPage.UseVisualStyleBackColor = true;
            // 
            // _TraceMessagesBox
            // 
            __TraceMessagesBox.AlertLevel = TraceEventType.Warning;
            __TraceMessagesBox.BackColor = System.Drawing.SystemColors.Info;
            __TraceMessagesBox.CaptionFormat = "{0} ≡";
            __TraceMessagesBox.CausesValidation = false;
            __TraceMessagesBox.Dock = System.Windows.Forms.DockStyle.Fill;
            __TraceMessagesBox.Font = new System.Drawing.Font("Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TraceMessagesBox.Location = new System.Drawing.Point(0, 0);
            __TraceMessagesBox.Multiline = true;
            __TraceMessagesBox.Name = "__TraceMessagesBox";
            __TraceMessagesBox.PresetCount = 100;
            __TraceMessagesBox.ReadOnly = true;
            __TraceMessagesBox.ResetCount = 200;
            __TraceMessagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            __TraceMessagesBox.Size = new System.Drawing.Size(391, 314);
            __TraceMessagesBox.TabIndex = 0;
            // 
            // _LastErrorTextBox
            // 
            _LastErrorTextBox.BackColor = System.Drawing.SystemColors.MenuText;
            _LastErrorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _LastErrorTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            _LastErrorTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _LastErrorTextBox.ForeColor = System.Drawing.Color.OrangeRed;
            _LastErrorTextBox.Location = new System.Drawing.Point(0, 64);
            _LastErrorTextBox.Name = "_LastErrorTextBox";
            _LastErrorTextBox.Size = new System.Drawing.Size(399, 18);
            _LastErrorTextBox.TabIndex = 4;
            _LastErrorTextBox.TabStop = false;
            _LastErrorTextBox.Text = "000, No Errors";
            // 
            // _InfoStatusStrip
            // 
            _InfoStatusStrip.BackColor = System.Drawing.Color.Black;
            _InfoStatusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            _InfoStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _FillerStatusLabel, _TransactionElapsedTimeLabel });
            _InfoStatusStrip.Location = new System.Drawing.Point(0, 42);
            _InfoStatusStrip.Name = "_InfoStatusStrip";
            _InfoStatusStrip.Size = new System.Drawing.Size(399, 22);
            _InfoStatusStrip.SizingGrip = false;
            _InfoStatusStrip.TabIndex = 6;
            _InfoStatusStrip.Text = "Additional Device Info";
            // 
            // _FillerStatusLabel
            // 
            _FillerStatusLabel.Name = "_FillerStatusLabel";
            _FillerStatusLabel.Size = new System.Drawing.Size(352, 17);
            _FillerStatusLabel.Spring = true;
            _FillerStatusLabel.Text = "<filler>";
            // 
            // _TransactionElapsedTimeLabel
            // 
            _TransactionElapsedTimeLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _TransactionElapsedTimeLabel.ForeColor = System.Drawing.SystemColors.Info;
            _TransactionElapsedTimeLabel.Name = "_TransactionElapsedTimeLabel";
            _TransactionElapsedTimeLabel.Size = new System.Drawing.Size(32, 17);
            _TransactionElapsedTimeLabel.Text = "0 ms";
            // 
            // _ReadingStatusStrip
            // 
            _ReadingStatusStrip.BackColor = System.Drawing.Color.Black;
            _ReadingStatusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            _ReadingStatusStrip.Font = new System.Drawing.Font("Segoe UI", 20.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ReadingStatusStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _ReadingStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _ComplianceToolStripStatusLabel, _AnalogInputReadingLabel, _SetpointLabel, _TbdToolStripStatusLabel });
            _ReadingStatusStrip.Location = new System.Drawing.Point(0, 0);
            _ReadingStatusStrip.Name = "_ReadingStatusStrip";
            _ReadingStatusStrip.Size = new System.Drawing.Size(399, 42);
            _ReadingStatusStrip.SizingGrip = false;
            _ReadingStatusStrip.TabIndex = 1;
            // 
            // _ComplianceToolStripStatusLabel
            // 
            _ComplianceToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _ComplianceToolStripStatusLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ComplianceToolStripStatusLabel.ForeColor = System.Drawing.Color.Red;
            _ComplianceToolStripStatusLabel.Margin = new System.Windows.Forms.Padding(0);
            _ComplianceToolStripStatusLabel.Name = "_ComplianceToolStripStatusLabel";
            _ComplianceToolStripStatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            _ComplianceToolStripStatusLabel.Size = new System.Drawing.Size(16, 42);
            _ComplianceToolStripStatusLabel.Text = "C";
            _ComplianceToolStripStatusLabel.ToolTipText = "Compliance";
            // 
            // _AnalogInputReadingLabel
            // 
            _AnalogInputReadingLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _AnalogInputReadingLabel.ForeColor = System.Drawing.Color.Red;
            _AnalogInputReadingLabel.Margin = new System.Windows.Forms.Padding(0);
            _AnalogInputReadingLabel.Name = "_AnalogInputReadingLabel";
            _AnalogInputReadingLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            _AnalogInputReadingLabel.Size = new System.Drawing.Size(174, 42);
            _AnalogInputReadingLabel.Spring = true;
            _AnalogInputReadingLabel.Text = "0.000 °F";
            _AnalogInputReadingLabel.ToolTipText = "Reading";
            // 
            // _SetpointLabel
            // 
            _SetpointLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _SetpointLabel.ForeColor = System.Drawing.Color.SpringGreen;
            _SetpointLabel.Name = "_SetpointLabel";
            _SetpointLabel.Size = new System.Drawing.Size(174, 37);
            _SetpointLabel.Spring = true;
            _SetpointLabel.Text = "0.000 °F";
            // 
            // _TbdToolStripStatusLabel
            // 
            _TbdToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _TbdToolStripStatusLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TbdToolStripStatusLabel.ForeColor = System.Drawing.Color.Aquamarine;
            _TbdToolStripStatusLabel.Margin = new System.Windows.Forms.Padding(0);
            _TbdToolStripStatusLabel.Name = "_TbdToolStripStatusLabel";
            _TbdToolStripStatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            _TbdToolStripStatusLabel.Size = new System.Drawing.Size(20, 42);
            _TbdToolStripStatusLabel.Text = " T";
            _TbdToolStripStatusLabel.ToolTipText = "To be defined";
            // 
            // _SoakCOntrolButton
            // 
            _SoakControlButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _SoakControlButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __StartSoakMenuItem, __PauseSoakMenuItem, __AbortSoakMenuItem, __RefreshSoakMenuItem });
            _SoakControlButton.Image = (System.Drawing.Image)resources.GetObject("_SoakCOntrolButton.Image");
            _SoakControlButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SoakControlButton.Name = "_SoakCOntrolButton";
            _SoakControlButton.Size = new System.Drawing.Size(54, 19);
            _SoakControlButton.Text = "Select:";
            _SoakControlButton.ToolTipText = "Select soak control";
            // 
            // _StartSoakMenuItem
            // 
            __StartSoakMenuItem.CheckOnClick = true;
            __StartSoakMenuItem.Name = "__StartSoakMenuItem";
            __StartSoakMenuItem.Size = new System.Drawing.Size(152, 22);
            __StartSoakMenuItem.Text = "Start";
            __StartSoakMenuItem.ToolTipText = "Starts or stops the soak sequence";
            // 
            // _PauseSoakMenuItem
            // 
            __PauseSoakMenuItem.CheckOnClick = true;
            __PauseSoakMenuItem.Name = "__PauseSoakMenuItem";
            __PauseSoakMenuItem.Size = new System.Drawing.Size(152, 22);
            __PauseSoakMenuItem.Text = "Pause";
            __PauseSoakMenuItem.ToolTipText = "Pause or resume the soak sequence";
            // 
            // _AbortSoakMenuItem
            // 
            __AbortSoakMenuItem.Name = "__AbortSoakMenuItem";
            __AbortSoakMenuItem.Size = new System.Drawing.Size(152, 22);
            __AbortSoakMenuItem.Text = "Abort";
            __AbortSoakMenuItem.ToolTipText = "Aborts the soak sequence";
            // 
            // _RefreshSoakMenuItem
            // 
            __RefreshSoakMenuItem.Name = "__RefreshSoakMenuItem";
            __RefreshSoakMenuItem.Size = new System.Drawing.Size(152, 22);
            __RefreshSoakMenuItem.Text = "Refresh";
            __RefreshSoakMenuItem.ToolTipText = "Changes the soak setpoint";
            // 
            // EasyZonePanel
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_Layout);
            Controls.Add(_ResourceToolStrip);
            Controls.Add(_StatusStrip);
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            Name = "EasyZonePanel";
            Size = new System.Drawing.Size(399, 491);
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            _ResourceToolStrip.ResumeLayout(false);
            _ResourceToolStrip.PerformLayout();
            _Layout.ResumeLayout(false);
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            _Tabs.ResumeLayout(false);
            _ReadingTabPage.ResumeLayout(false);
            _ReadingTabPage.PerformLayout();
            _SoakStatusStrip.ResumeLayout(false);
            _SoakStatusStrip.PerformLayout();
            _ToolStripPanel.ResumeLayout(false);
            _ToolStripPanel.PerformLayout();
            _InputToolStrip.ResumeLayout(false);
            _InputToolStrip.PerformLayout();
            _AlarmToolStrip.ResumeLayout(false);
            _AlarmToolStrip.PerformLayout();
            _ProcessToolStrip.ResumeLayout(false);
            _ProcessToolStrip.PerformLayout();
            _SoakToolStrip.ResumeLayout(false);
            _SoakToolStrip.PerformLayout();
            _MessagesTabPage.ResumeLayout(false);
            _MessagesTabPage.PerformLayout();
            _InfoStatusStrip.ResumeLayout(false);
            _InfoStatusStrip.PerformLayout();
            _ReadingStatusStrip.ResumeLayout(false);
            _ReadingStatusStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.StatusStrip _StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _StatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel _ErrorStatusLabel;
        private System.Windows.Forms.ToolTip _ToolTip;
        private Core.Controls.InfoProvider _ErrorProvider;
        private System.Windows.Forms.ToolStrip _ResourceToolStrip;
        private System.Windows.Forms.ToolStripLabel _PortComboBoxLabel;
        private System.Windows.Forms.ToolStripComboBox _PortComboBox;
        private System.Windows.Forms.ToolStripButton __OpenPortButton;

        private System.Windows.Forms.ToolStripButton _OpenPortButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenPortButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenPortButton != null)
                {
                    __OpenPortButton.Click -= OpenPortButton_Click;
                }

                __OpenPortButton = value;
                if (__OpenPortButton != null)
                {
                    __OpenPortButton.Click += OpenPortButton_Click;
                }
            }
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Label _TitleLabel;
        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.TabControl _Tabs;
        private System.Windows.Forms.TabPage _ReadingTabPage;
        private System.Windows.Forms.TabPage _MessagesTabPage;
        private System.Windows.Forms.TextBox _LastErrorTextBox;
        private System.Windows.Forms.StatusStrip _ReadingStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _ComplianceToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel _AnalogInputReadingLabel;
        private System.Windows.Forms.ToolStripStatusLabel _TbdToolStripStatusLabel;
        private Core.Forma.TraceMessagesBox __TraceMessagesBox;

        private Core.Forma.TraceMessagesBox _TraceMessagesBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TraceMessagesBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TraceMessagesBox != null)
                {
                    __TraceMessagesBox.PropertyChanged -= TraceMessagesBox_PropertyChanged;
                }

                __TraceMessagesBox = value;
                if (__TraceMessagesBox != null)
                {
                    __TraceMessagesBox.PropertyChanged += TraceMessagesBox_PropertyChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripStatusLabel _SetpointLabel;
        private System.Windows.Forms.ToolStripPanel _ToolStripPanel;
        private System.Windows.Forms.ToolStrip _InputToolStrip;
        private System.Windows.Forms.ToolStripLabel _InputNumberComboBoxLabel;
        private System.Windows.Forms.ToolStripComboBox _InputNumberComboBox;
        private System.Windows.Forms.ToolStripButton __ReadInput1Button;

        private System.Windows.Forms.ToolStripButton _ReadInput1Button
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadInput1Button;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadInput1Button != null)
                {
                    __ReadInput1Button.Click -= ReadInputButton_Click;
                }

                __ReadInput1Button = value;
                if (__ReadInput1Button != null)
                {
                    __ReadInput1Button.Click += ReadInputButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStrip _AlarmToolStrip;
        private System.Windows.Forms.ToolStripLabel _AlarmNumberComboBoxLabel;
        private System.Windows.Forms.ToolStripComboBox _AlarmNumberComboBox;
        private System.Windows.Forms.ToolStripLabel _AlarmTypeComboBoxLabel;
        private System.Windows.Forms.ToolStripComboBox _AlarmTypeComboBox;
        private System.Windows.Forms.ToolStrip _ProcessToolStrip;
        private System.Windows.Forms.ToolStripLabel _ProcessLabel;
        private System.Windows.Forms.ToolStripLabel _SetpointNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _SetpointNumeric;
        private System.Windows.Forms.ToolStripLabel _UnitIdNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _UnitIdNumeric;
        private System.Windows.Forms.ToolStripSeparator _Separator1;
        private System.Windows.Forms.ToolStripLabel _InputErrorLabel;
        private System.Windows.Forms.StatusStrip _InfoStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _FillerStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel _TransactionElapsedTimeLabel;
        private System.Windows.Forms.ToolStripSeparator _ToolStripSeparator1;
        private System.Windows.Forms.ToolStripSplitButton _SetpointActionButton;
        private System.Windows.Forms.ToolStripMenuItem __WriteSetpointMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _WriteSetpointMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WriteSetpointMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WriteSetpointMenuItem != null)
                {
                    __WriteSetpointMenuItem.Click -= WriteSetpointMenuItem_Click;
                }

                __WriteSetpointMenuItem = value;
                if (__WriteSetpointMenuItem != null)
                {
                    __WriteSetpointMenuItem.Click += WriteSetpointMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __ReadSetpointMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ReadSetpointMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSetpointMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSetpointMenuItem != null)
                {
                    __ReadSetpointMenuItem.Click -= ReadSetpointMenuItem_Click;
                }

                __ReadSetpointMenuItem = value;
                if (__ReadSetpointMenuItem != null)
                {
                    __ReadSetpointMenuItem.Click += ReadSetpointMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripDropDownButton _AlarmTypeActionsButton;
        private System.Windows.Forms.ToolStripMenuItem __WriteAlarmTypeMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _WriteAlarmTypeMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WriteAlarmTypeMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WriteAlarmTypeMenuItem != null)
                {
                    __WriteAlarmTypeMenuItem.Click -= WriteAlarmTypeMenuItem_Click;
                }

                __WriteAlarmTypeMenuItem = value;
                if (__WriteAlarmTypeMenuItem != null)
                {
                    __WriteAlarmTypeMenuItem.Click += WriteAlarmTypeMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __ReadAlarmTypeMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ReadAlarmTypeMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadAlarmTypeMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadAlarmTypeMenuItem != null)
                {
                    __ReadAlarmTypeMenuItem.Click -= ReadAlarmTypeMenuItem_Click;
                }

                __ReadAlarmTypeMenuItem = value;
                if (__ReadAlarmTypeMenuItem != null)
                {
                    __ReadAlarmTypeMenuItem.Click += ReadAlarmTypeMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __RefreshPortsButton;

        private System.Windows.Forms.ToolStripButton _RefreshPortsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RefreshPortsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RefreshPortsButton != null)
                {
                    __RefreshPortsButton.Click -= RefreshPortsButton_Click;
                }

                __RefreshPortsButton = value;
                if (__RefreshPortsButton != null)
                {
                    __RefreshPortsButton.Click += RefreshPortsButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __ApplySetpointMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ApplySetpointMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySetpointMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySetpointMenuItem != null)
                {
                    __ApplySetpointMenuItem.Click -= ApplySetpointMenuItem_Click;
                }

                __ApplySetpointMenuItem = value;
                if (__ApplySetpointMenuItem != null)
                {
                    __ApplySetpointMenuItem.Click += ApplySetpointMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __ApplyAlarmTypeMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ApplyAlarmTypeMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyAlarmTypeMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyAlarmTypeMenuItem != null)
                {
                    __ApplyAlarmTypeMenuItem.Click -= ApplyAlarmTypeMenuItem_Click;
                }

                __ApplyAlarmTypeMenuItem = value;
                if (__ApplyAlarmTypeMenuItem != null)
                {
                    __ApplyAlarmTypeMenuItem.Click += ApplyAlarmTypeMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _SoakToolStripLabel;
        private System.Windows.Forms.ToolStripLabel _SoakSetpointNumericLabel;
        private Core.Controls.ToolStripNumericUpDown __SoakSetpointNumeric;

        private Core.Controls.ToolStripNumericUpDown _SoakSetpointNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SoakSetpointNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SoakSetpointNumeric != null)
                {
                    __SoakSetpointNumeric.ValueChanged -= SoakSetpointNumeric_ValueChanged;
                }

                __SoakSetpointNumeric = value;
                if (__SoakSetpointNumeric != null)
                {
                    __SoakSetpointNumeric.ValueChanged += SoakSetpointNumeric_ValueChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown _SoakTimeNumeric;
        private System.Windows.Forms.ToolStripLabel _SoakWindowNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _SoakWindowNumeric;
        private System.Windows.Forms.ToolStripLabel _SoakHysteresisNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _SoakHysteresisNumeric;
        private Core.Controls.ToolStripNumericUpDown _SampleIntervalNumeric;
        private System.Windows.Forms.ToolStripLabel _SoakResetDealyNumericToolStripLabel;
        private Core.Controls.ToolStripNumericUpDown _SoakResetDelayNumeric;
        private System.Windows.Forms.ToolStrip _SoakToolStrip;
        private System.Windows.Forms.StatusStrip _SoakStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _SoakStateLabel;
        private System.Windows.Forms.ToolStripProgressBar _SoakAutomatonProgress;
        private System.Windows.Forms.ToolStripStatusLabel _SoakElapsedTimeLabel;
        private System.Windows.Forms.ToolStripButton __ReadInputErrorButton;

        private System.Windows.Forms.ToolStripButton _ReadInputErrorButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadInputErrorButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadInputErrorButton != null)
                {
                    __ReadInputErrorButton.Click -= ReadInputErrorButton_Click;
                }

                __ReadInputErrorButton = value;
                if (__ReadInputErrorButton != null)
                {
                    __ReadInputErrorButton.Click += ReadInputErrorButton_Click;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown _RampTimeoutNumeric;
        private System.Windows.Forms.ToolStripLabel _SoakTimeNumericLabel;
        private System.Windows.Forms.ToolStripLabel _SampleIntervalNumericLabel;
        private System.Windows.Forms.ToolStripLabel _RampTimeoutNumericLabel;
        private System.Windows.Forms.ToolStripLabel _OvenControlModeComboBoxLabel;
        private System.Windows.Forms.ToolStripComboBox __OvenControlModeComboBox;

        private System.Windows.Forms.ToolStripComboBox _OvenControlModeComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OvenControlModeComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OvenControlModeComboBox != null)
                {
                    __OvenControlModeComboBox.SelectedIndexChanged -= OvenControlModeComboBox_SelectedIndexChanged;
                }

                __OvenControlModeComboBox = value;
                if (__OvenControlModeComboBox != null)
                {
                    __OvenControlModeComboBox.SelectedIndexChanged += OvenControlModeComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __AsyncNotifyMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _AsyncNotifyMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AsyncNotifyMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AsyncNotifyMenuItem != null)
                {
                    __AsyncNotifyMenuItem.Click -= AsyncNotifyMenuItem_Click;
                }

                __AsyncNotifyMenuItem = value;
                if (__AsyncNotifyMenuItem != null)
                {
                    __AsyncNotifyMenuItem.Click += AsyncNotifyMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __UnsafeInvokeMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _UnsafeInvokeMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __UnsafeInvokeMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__UnsafeInvokeMenuItem != null)
                {
                    __UnsafeInvokeMenuItem.Click -= UnsafeInvokeMenuItem_Click;
                }

                __UnsafeInvokeMenuItem = value;
                if (__UnsafeInvokeMenuItem != null)
                {
                    __UnsafeInvokeMenuItem.Click += UnsafeInvokeMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __SyncNotifyMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _SyncNotifyMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SyncNotifyMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SyncNotifyMenuItem != null)
                {
                    __SyncNotifyMenuItem.Click -= SyncNotifyMenuItem_Click;
                }

                __SyncNotifyMenuItem = value;
                if (__SyncNotifyMenuItem != null)
                {
                    __SyncNotifyMenuItem.Click += SyncNotifyMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripSplitButton _EventNotificationTestSplitButton;
        private System.Windows.Forms.ToolStripDropDownButton _SoakControlButton;
        private System.Windows.Forms.ToolStripMenuItem __StartSoakMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _StartSoakMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StartSoakMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StartSoakMenuItem != null)
                {
                    __StartSoakMenuItem.CheckStateChanged -= StartSoakMenuItem_CheckStateChanged;
                }

                __StartSoakMenuItem = value;
                if (__StartSoakMenuItem != null)
                {
                    __StartSoakMenuItem.CheckStateChanged += StartSoakMenuItem_CheckStateChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __PauseSoakMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _PauseSoakMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PauseSoakMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PauseSoakMenuItem != null)
                {
                    __PauseSoakMenuItem.CheckStateChanged -= PauseSoakMenuItem_CheckStateChanged;
                }

                __PauseSoakMenuItem = value;
                if (__PauseSoakMenuItem != null)
                {
                    __PauseSoakMenuItem.CheckStateChanged += PauseSoakMenuItem_CheckStateChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __AbortSoakMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _AbortSoakMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortSoakMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortSoakMenuItem != null)
                {
                    __AbortSoakMenuItem.Click -= AbortSoakMenuItem_Click;
                }

                __AbortSoakMenuItem = value;
                if (__AbortSoakMenuItem != null)
                {
                    __AbortSoakMenuItem.Click += AbortSoakMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __RefreshSoakMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _RefreshSoakMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RefreshSoakMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RefreshSoakMenuItem != null)
                {
                    __RefreshSoakMenuItem.Click -= RefreshSoakMenuItem_Click;
                }

                __RefreshSoakMenuItem = value;
                if (__RefreshSoakMenuItem != null)
                {
                    __RefreshSoakMenuItem.Click += RefreshSoakMenuItem_Click;
                }
            }
        }
    }
}
