using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using isr.Automata.Finite.Engines;
using isr.Core;
using isr.Core.EnumExtensions;
using isr.Modbus.Watlow.Forms.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

using Stateless;

namespace isr.Modbus.Watlow.Forms
{

    /// <summary> View for accessing the Watlow EZ-Zone controller. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-09 </para>
    /// </remarks>
    public partial class EasyZoneView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public EasyZoneView()
        {

            // This call is required by the designer.
            this.InitializingComponents = true;
            this.InitializeComponent();

            // note that the caption is not set if this is run inside the On Load function.
            // set defaults for the messages box.
            this._TraceMessagesBox.ResetCount = 500;
            this._TraceMessagesBox.PresetCount = 250;
            this._TraceMessagesBox.TabCaption = "Log";
            // Me._TraceMessagesBox.ContainerPanel = Me._TreePanel.Panel2
            // Me._TraceMessagesBox.AlertsToggleControl = Me._TreePanel.Panel2
            this._TraceMessagesBox.CommenceUpdates();

            // Add any initialization after the InitializeComponent() call.
            this._SetpointNumeric.NumericUpDownControl.DecimalPlaces = 1;
            this._SetpointNumeric.NumericUpDownControl.Minimum = -200;
            this._SetpointNumeric.NumericUpDownControl.Maximum = 400m;
            this._SetpointNumeric.NumericUpDownControl.Value = 78m;

            this._SoakSetpointNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._SoakSetpointNumeric.NumericUpDownControl.Minimum = -200;
            this._SoakSetpointNumeric.NumericUpDownControl.Maximum = 400m;
            this._SoakSetpointNumeric.NumericUpDownControl.Value = 90m;

            this._SoakTimeNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._SoakTimeNumeric.NumericUpDownControl.Minimum = 0m;
            this._SoakTimeNumeric.NumericUpDownControl.Maximum = 3600m;
            this._SoakTimeNumeric.NumericUpDownControl.Value = 60m;

            this._SoakWindowNumeric.NumericUpDownControl.DecimalPlaces = 1;
            this._SoakWindowNumeric.NumericUpDownControl.Increment = 0.1m;
            this._SoakWindowNumeric.NumericUpDownControl.Minimum = 0m;
            this._SoakWindowNumeric.NumericUpDownControl.Maximum = 10m;
            this._SoakWindowNumeric.NumericUpDownControl.Value = 2m;

            this._SoakHysteresisNumeric.NumericUpDownControl.DecimalPlaces = 1;
            this._SoakHysteresisNumeric.NumericUpDownControl.Increment = 0.1m;
            this._SoakHysteresisNumeric.NumericUpDownControl.Minimum = 0m;
            this._SoakHysteresisNumeric.NumericUpDownControl.Maximum = 10m;
            this._SoakHysteresisNumeric.NumericUpDownControl.Value = 1m;

            this._SampleIntervalNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._SampleIntervalNumeric.NumericUpDownControl.Minimum = 0m;
            this._SampleIntervalNumeric.NumericUpDownControl.Maximum = 100m;
            this._SampleIntervalNumeric.NumericUpDownControl.Value = 2m;

            this._SoakResetDelayNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._SoakResetDelayNumeric.NumericUpDownControl.Minimum = 0m;
            this._SoakResetDelayNumeric.NumericUpDownControl.Maximum = 600m;
            this._SoakResetDelayNumeric.NumericUpDownControl.Value = 20m;

            this._RampTimeoutNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._RampTimeoutNumeric.NumericUpDownControl.Minimum = 0m;
            this._RampTimeoutNumeric.NumericUpDownControl.Maximum = 6000m;
            this._RampTimeoutNumeric.NumericUpDownControl.Value = 0m;

            this._OvenControlModeComboBox.ComboBox.DataSource = null;
            this._OvenControlModeComboBox.ComboBox.Items.Clear();
            this._OvenControlModeComboBox.ComboBox.DataSource = typeof( OvenControlMode ).ValueDescriptionPairs().ToList();
            this._OvenControlModeComboBox.ComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._OvenControlModeComboBox.ComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );

            this.InitializingComponents = false;
            this._EventNotificationTestSplitButton.Visible = Debugger.IsAttached;
            this.__RefreshPortsButton.Name = "_RefreshPortsButton";
            this.__OpenPortButton.Name = "_OpenPortButton";
            this.__ReadInput1Button.Name = "_ReadInput1Button";
            this.__ReadInputErrorButton.Name = "_ReadInputErrorButton";
            this.__WriteAlarmTypeMenuItem.Name = "_WriteAlarmTypeMenuItem";
            this.__ReadAlarmTypeMenuItem.Name = "_ReadAlarmTypeMenuItem";
            this.__ApplyAlarmTypeMenuItem.Name = "_ApplyAlarmTypeMenuItem";
            this.__WriteSetpointMenuItem.Name = "_WriteSetpointMenuItem";
            this.__ReadSetpointMenuItem.Name = "_ReadSetpointMenuItem";
            this.__ApplySetpointMenuItem.Name = "_ApplySetpointMenuItem";
            this.__AsyncNotifyMenuItem.Name = "_AsyncNotifyMenuItem";
            this.__UnsafeInvokeMenuItem.Name = "_UnsafeInvokeMenuItem";
            this.__SyncNotifyMenuItem.Name = "_SyncNotifyMenuItem";
            this.__SoakSetpointNumeric.Name = "_SoakSetpointNumeric";
            this.__OvenControlModeComboBox.Name = "_OvenControlModeComboBox";
            this.__TraceMessagesBox.Name = "_TraceMessagesBox";
            this.__StartSoakMenuItem.Name = "_StartSoakMenuItem";
            this.__PauseSoakMenuItem.Name = "_PauseSoakMenuItem";
            this.__AbortSoakMenuItem.Name = "_AbortSoakMenuItem";
            this.__RefreshSoakMenuItem.Name = "_RefreshSoakMenuItem";
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">   Identifier for the unit. </param>
        /// <param name="portName"> Name of the port. </param>
        public EasyZoneView( int unitId, string portName ) : this()
        {
            this.IsDeviceOwner = true;
            this.AssignDeviceThis( new EasyZone( ( byte ) unitId, portName ) );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="device"> The device. </param>
        public EasyZoneView( EasyZone device ) : this()
        {
            this.IsDeviceOwner = false;
            this.AssignDeviceThis( device );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    this._TraceMessagesBox.SuspendUpdatesReleaseIndicators();
                    this.AssignDevice( null );
                }

                if ( this.components is object )
                {
                    this.components.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                this._TraceMessagesBox.ContainerPanel = this._MessagesTabPage;
                this.AddPrivateListener( this._TraceMessagesBox );
                this._ErrorStatusLabel.Text = string.Empty;
                this._ComplianceToolStripStatusLabel.Visible = false;
                this._TbdToolStripStatusLabel.Visible = false;
                this._TransactionElapsedTimeLabel.Text = "0 ms";
                this._PortComboBox.Items.Clear();
                this._PortComboBox.Items.AddRange( System.IO.Ports.SerialPort.GetPortNames() );
                this.OnDeviceOpenChanged( this.EasyZoneClient );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        #endregion

        #region " MODBUS CLIENT "

        /// <summary>
        /// Gets or sets the sentinel indicating if this panel owns the device and, therefore, needs to
        /// dispose of this device.
        /// </summary>
        /// <value> The is device owner. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool IsDeviceOwner { get; set; }

        /// <summary> Assign device. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        private void AssignDeviceThis( EasyZone value )
        {
            if ( this.EasyZoneClient is object )
            {
                this.EasyZoneClient.Talker.RemovePrivateListener( this._TraceMessagesBox );
                this.RemovePrivateListener( this._TraceMessagesBox );
                this.EasyZoneClient.PropertyChanged -= this.EasyZoneClient_PropertyChanged;
                if ( this.IsDeviceOwner )
                {
                    this.EasyZoneClient.Talker.Listeners.Clear();
                    this.EasyZoneClient.Dispose();
                }

                this.AssignSoakEngine( null );
                this.EasyZoneClient = null;
            }

            this.EasyZoneClient = value;
            if ( value is object )
            {
                if ( this.IsDeviceOwner )
                {
                    this.EasyZoneClient.AddListeners( this.Talker );
                }
                else
                {
                    this.EasyZoneClient.Talker.AddPrivateListener( this._TraceMessagesBox );
                }

                this.EasyZoneClient.PropertyChanged += this.EasyZoneClient_PropertyChanged;
                this.OnPropertyChanged( value, nameof( EasyZone.IsDeviceOpen ) );
                this.OnPropertyChanged( value, nameof( EasyZone.InputErrorRead ) );
                this.OnPropertyChanged( value, nameof( EasyZone.AlarmTypeRead ) );
                this.OnPropertyChanged( value, nameof( EasyZone.AlarmTypeWritten ) );
                this.OnPropertyChanged( value, nameof( EasyZone.AnalogInputRead ) );
                this.OnPropertyChanged( value, nameof( EasyZone.LastInterfaceError ) );
                this.OnPropertyChanged( value, nameof( EasyZone.SetpointRead ) );
                this.OnPropertyChanged( value, nameof( EasyZone.SetpointWritten ) );
                this.OnPropertyChanged( value, nameof( EasyZone.Title ) );
                this.OnPropertyChanged( value, nameof( EasyZone.TransactionElapsedTime ) );
                this.AssignSoakEngine( value.SoakEngine );
            }

            this.OnDeviceOpenChanged( value );
        }

        /// <summary> Assign device. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public virtual void AssignDevice( EasyZone value )
        {
            this.IsDeviceOwner = false;
            this.AssignDeviceThis( value );
        }

        /// <summary> Gets or sets the easy zone client. </summary>
        /// <value> The easy zone client. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private EasyZone EasyZoneClient { get; set; }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( EasyZone sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( EasyZone.IsDeviceOpen ):
                    {
                        this.OnDeviceOpenChanged( sender );
                        break;
                    }

                case nameof( EasyZone.InputErrorRead ):
                    {
                        this._InputErrorLabel.Text = EasyZone.BuildCompoundInputError( sender.InputErrorRead );
                        this._InputErrorLabel.ForeColor = sender.InputErrorRead == InputError.NoError ? System.Drawing.Color.Green : System.Drawing.Color.Red;
                        break;
                    }

                case nameof( EasyZone.AlarmTypeRead ):
                    {
                        this._AlarmTypeComboBox.Text = sender.AlarmTypeRead.Description();
                        break;
                    }

                case nameof( EasyZone.AlarmTypeWritten ):
                    {
                        this._AlarmTypeComboBox.Text = sender.AlarmTypeWritten.Description();
                        break;
                    }

                case nameof( EasyZone.AnalogInputRead ):
                    {
                        this._AnalogInputReadingLabel.Text = sender.AnalogInputRead is null ? string.Empty : $"{sender.AnalogInputRead.Value:0.0} {sender.AnalogInputRead.Unit}";
                        break;
                    }

                case nameof( EasyZone.LastInterfaceError ):
                    {
                        this._LastErrorTextBox.Text = ModbusBase.BuildCompoundInterfaceError( sender.LastInterfaceError );
                        this._LastErrorTextBox.ForeColor = sender.LastInterfaceError == InterfaceError.NoError ? System.Drawing.Color.Green : System.Drawing.Color.Red;
                        break;
                    }

                case nameof( EasyZone.SetpointRead ):
                    {
                        this._SetpointLabel.Text = sender.SetpointRead is null ? string.Empty : $"{sender.SetpointRead.Value:0.0} {sender.SetpointRead.Unit}";
                        break;
                    }

                case nameof( EasyZone.SetpointWritten ):
                    {
                        break;
                    }

                case nameof( EasyZone.Title ):
                    {
                        this._TitleLabel.Text = sender.Title;
                        break;
                    }

                case nameof( EasyZone.TransactionElapsedTime ):
                    {
                        this._TransactionElapsedTimeLabel.Text = $"{sender.TransactionElapsedTime.Value:0} {sender.TransactionElapsedTime.Unit}";
                        break;
                    }
            }
        }

        /// <summary> Easy zone client property changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property Changed event information. </param>
        private void EasyZoneClient_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            if ( sender is not EasyZone ez || e is null )
                return;
            activity = $"handler {nameof( EasyZone )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.EasyZoneClient_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as EasyZone, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Applies the alarm type menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ApplyAlarmTypeMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                _ = this._AlarmTypeComboBox.SelectedIndex == 0
                    ? this.EasyZoneClient.ApplyAlarmType( AlarmType.Off )
                    : this.EasyZoneClient.ApplyAlarmType( AlarmType.Process );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, "Exception applying alarm type" );
                _ = (this.Talker?.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception applying alarm type;. {0}", ex.ToFullBlownString() ));
            }
        }

        /// <summary> Reads alarm type menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ReadAlarmTypeMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.ReadAlarmType();
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, "Exception reading alarm type" );
                _ = (this.Talker?.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception reading alarm type;. {0}", ex.ToFullBlownString() ));
            }
        }

        /// <summary> Writes an alarm type menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void WriteAlarmTypeMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                _ = this._AlarmTypeComboBox.SelectedIndex == 0
                    ? this.EasyZoneClient.WriteAlarmType( AlarmType.Off )
                    : this.EasyZoneClient.WriteAlarmType( AlarmType.Process );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, "Exception writing alarm type" );
                _ = (this.Talker?.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception writing alarm type;. {0}", ex.ToFullBlownString() ));
            }
        }

        /// <summary> Reads input button click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ReadInputButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.ReadAnalogInput();
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, "Exception reading analog input" );
                _ = (this.Talker?.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception reading analog input;. {0}", ex.ToFullBlownString() ));
            }
        }

        /// <summary> Reads input error button click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ReadInputErrorButton_Click( object sender, EventArgs e )
        {
            string activity = "reading analog input";
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.ReadInputError();
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. " );
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
        }

        /// <summary> Applies the setpoint menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ApplySetpointMenuItem_Click( object sender, EventArgs e )
        {
            string activity = "applying setpoint";
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.ApplySetpoint( new Arebis.TypedUnits.Amount( ( double ) this._SetpointNumeric.Value, this.EasyZoneClient.TemperatureUnit ) );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. " );
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
        }

        /// <summary> Reads setpoint menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSetpointMenuItem_Click( object sender, EventArgs e )
        {
            string activity = "reading setpoint";
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.ReadSetpoint();
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. " );
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
        }

        /// <summary> Writes a setpoint menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void WriteSetpointMenuItem_Click( object sender, EventArgs e )
        {
            string activity = "writing setpoint";
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.WriteSetpoint( new Arebis.TypedUnits.Amount( ( double ) this._SetpointNumeric.Value, this.EasyZoneClient.TemperatureUnit ) );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. " );
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
        }

        /// <summary> Recursively enable. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   True to show or False to hide the control. </param>
        protected void RecursivelyEnable( Control control, bool value )
        {
            if ( control is object )
            {
                control.Enabled = value;
                if ( control.Controls is object && control.Controls.Count > 0 )
                {
                    foreach ( Control c in control.Controls )
                        this.RecursivelyEnable( c, value );
                }
            }
        }

        /// <summary> Executes the device open changed action. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> Reference tot he <see cref="EasyZone"/> client. </param>
        protected void OnDeviceOpenChanged( EasyZone value )
        {
            bool isOpen = (value?.IsDeviceOpen).GetValueOrDefault( false );
            foreach ( TabPage t in this._Tabs.TabPages )
            {
                if ( !ReferenceEquals( t, this._MessagesTabPage ) )
                {
                    foreach ( Control c in t.Controls )
                        this.RecursivelyEnable( c, isOpen );
                }
            }

            if ( isOpen )
            {
                if ( !string.Equals( this.EasyZoneClient.PortName, this._PortComboBox.Text, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._PortComboBox.Text = this.EasyZoneClient.PortName;
                }

                this._OpenPortButton.BackColor = System.Drawing.Color.LightGreen;
                this._OpenPortButton.Text = "Close";
                this._StatusLabel.Text = $"Open at {this._PortComboBox.Text}";
                this.EasyZoneClient.AddListeners( this.Talker );
                this.EasyZoneClient.ApplyTalkerTraceLevels( this.EasyZoneClient.Talker );
            }
            else
            {
                this._OpenPortButton.BackColor = System.Drawing.SystemColors.Control;
                this._OpenPortButton.Text = "Open";
                this._StatusLabel.Text = $"Select port and click {this._OpenPortButton.Text}";
                this.EasyZoneClient?.RemoveListeners();
            }
        }

        /// <summary> Opens port button click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OpenPortButton_Click( object sender, EventArgs e )
        {
            string activity = "opening Easy Zone";
            try
            {
                this._ErrorProvider.Clear();
                if ( this.EasyZoneClient?.IsDeviceOpen == true )
                {
                    this.EasyZoneClient.CloseModbusClient();
                }
                else
                {
                    if ( this.EasyZoneClient is null )
                    {
                        this.IsDeviceOwner = true;
                        this.AssignDevice( new EasyZone( ( byte ) this._UnitIdNumeric.Value, this._PortComboBox.Text ) );
                    }

                    if ( !string.Equals( this.EasyZoneClient.PortName, this._PortComboBox.Text ) )
                    {
                        this.EasyZoneClient.PortName = this._PortComboBox.Text;
                    }

                    if ( this.EasyZoneClient.UnitId != ( byte ) this._UnitIdNumeric.Value )
                    {
                        this.EasyZoneClient.UnitId = ( byte ) this._UnitIdNumeric.Value;
                    }

                    _ = this.EasyZoneClient.TryOpenModbusClient()
                        ? this.PublishVerbose( $"Watlow unit {this.EasyZoneClient.UnitId} open on port {this.EasyZoneClient.PortName};. " )
                        : this.PublishWarning( $"Failed {activity};. Details: {this.EasyZoneClient.LastInterfaceError.Description()}" );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. " );
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
            finally
            {
                this.OnDeviceOpenChanged( this.EasyZoneClient );
            }
        }

        /// <summary> Refresh ports button click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void RefreshPortsButton_Click( object sender, EventArgs e )
        {
            string activity = "refreshing list of ports";
            try
            {
                this._ErrorProvider.Clear();
                this._PortComboBox.Items.Clear();
                this._PortComboBox.Items.AddRange( System.IO.Ports.SerialPort.GetPortNames() );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, $"Exception {activity};. " );
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
        }

        #endregion

        #region " MODBUS CLIENT: SOAK "

        /// <summary> Gets or sets the Soak engine. </summary>
        /// <value> The Soak engine. </value>
        public SoakEngine SoakEngine { get; private set; }

        /// <summary> Assign Soak Engine. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignSoakEngine( SoakEngine value )
        {
            if ( this.SoakEngine is object )
            {
                this.SoakEngine.PropertyChanged -= this.SoakEngine_PropertyChanged;
                this.SoakEngine = null;
            }

            this.SoakEngine = value;
            if ( value is object )
            {
                this.SoakEngine.PropertyChanged += this.SoakEngine_PropertyChanged;
                this.SoakEngine.UsingFireAsync = true;
                this.SoakEngine.StateMachine.OnTransitionedAsync( ( t ) => Task.Run( () => this.OnTransitioned( t ) ) );
                this.HandlePropertyChanged( value, nameof( this.SoakEngine.SoakPercentProgress ) );
                this.HandlePropertyChanged( value, nameof( this.SoakEngine.StateProgress ) );
            }
        }

        /// <summary> Handles the state entry actions. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="transition"> The transition. </param>
        private void OnTransitioned( StateMachine<SoakState, SoakTrigger>.Transition transition )
        {
            string activity = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<StateMachine<SoakState, SoakTrigger>.Transition>( this.OnTransitioned ), new object[] { transition } );
                }
                else if ( transition is null )
                {
                    activity = this.PublishVerbose( $"Entering the {this.SoakEngine.StateMachine.State} state" );
                    this.OnActivated( this.SoakEngine.StateMachine.State );
                }
                else
                {
                    activity = this.PublishVerbose( $"transitioning {this.SoakEngine.Name}: {transition.Source} --> {transition.Destination}" );
                    if ( transition.IsReentry )
                    {
                        this.OnReentry( transition.Destination );
                    }
                    else
                    {
                        this.OnExit( transition.Source );
                        this.OnActivated( transition.Destination );
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Executes the 'exit' action. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="source"> Source for the. </param>
        private void OnExit( SoakState source )
        {
            switch ( source )
            {
                case SoakState.Soak:
                    {
                        break;
                    }
            }
        }

        /// <summary>
        /// Executes the 'reentry' action. Is used to update the setpoint information and take new
        /// readings.
        /// </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="destination"> Destination for the. </param>
        private void OnReentry( SoakState destination )
        {
            string activity = $"{this.SoakEngine.Name} reentered {destination.Description()} state";
            try
            {
                switch ( destination )
                {
                    case SoakState.Soak:
                        {
                            break;
                        }

                    case SoakState.AtTemp:
                        {
                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"info {ex.Data?.Count}", $"{this.SoakEngine.Name} exception at Reenter {destination.Description()} state" );
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Executes the 'activated' action. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="destination"> Destination for the. </param>
        private void OnActivated( SoakState destination )
        {
            switch ( destination )
            {
                case SoakState.Idle:
                    {
                        this.OnStopped();
                        break;
                    }

                case SoakState.Engaged:
                    {
                        this.OnEngaged();
                        break;
                    }

                case SoakState.Terminal:
                    {
                        this.OnStopped();
                        break;
                    }
            }
        }

        /// <summary> Executes the 'engaged' actions. </summary>
        /// <remarks> David, 2020-11-24. </remarks>
        private void OnEngaged()
        {
            this._AlarmToolStrip.Enabled = false;
            this._InputToolStrip.Enabled = false;
            this._ProcessToolStrip.Enabled = false;
            this._AbortSoakMenuItem.Enabled = true;
            this._RefreshSoakMenuItem.Enabled = true;
            this._PauseSoakMenuItem.Enabled = false;
            this._PauseSoakMenuItem.Invalidate();
            this._PauseSoakMenuItem.Checked = false;
            this._PauseSoakMenuItem.Enabled = true;
        }

        /// <summary> Executes the 'stopped' action. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        private void OnStopped()
        {
            this._AlarmToolStrip.Enabled = true;
            this._InputToolStrip.Enabled = true;
            this._ProcessToolStrip.Enabled = true;
            this._AbortSoakMenuItem.Enabled = false;
            this._RefreshSoakMenuItem.Enabled = false;
            this._PauseSoakMenuItem.Enabled = false;
            this._PauseSoakMenuItem.Invalidate();
            this._PauseSoakMenuItem.Checked = false;
            this._PauseSoakMenuItem.Enabled = false;
        }

        /// <summary> Handles the Soak Engine property changed event. </summary>
        /// <remarks> David, 2020-11-24. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SoakEngine sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Automata.Finite.Engines.SoakEngine.SoakPercentProgress ):
                    {
                        this._SoakElapsedTimeLabel.Text = $"Soak: {sender.SoakStopwatch.Elapsed:d\\.h\\:mm\\:ss\\.f}";
                        break;
                    }

                case nameof( Automata.Finite.Engines.SoakEngine.StateProgress ):
                    {
                        this._SoakStateLabel.Text = sender.CurrentState.Description();
                        if ( sender.CurrentState == SoakState.Soak )
                        {
                            this._SoakElapsedTimeLabel.Text = $"Soak: {sender.SoakStopwatch.Elapsed:d\\.h\\:mm\\:ss\\.f}";
                            this._SoakAutomatonProgress.Value = sender.SoakPercentProgress;
                            this._SoakAutomatonProgress.ToolTipText = "Soak time progress";
                        }
                        else
                        {
                            this._SoakAutomatonProgress.Value = sender.StateProgress;
                            this._SoakAutomatonProgress.ToolTipText = "Soak automaton progress";
                        }

                        _ = this.PublishVerbose( $"{sender.Name} soak progress {sender.SoakStopwatch.Elapsed}" );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by SoakEngine for property changed events. </summary>
        /// <remarks> David, 2020-11-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void SoakEngine_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is not SoakEngine automaton || e is null )
                return;
            string activity = $"handling {nameof( Automata.Finite.Engines.SoakEngine )}.{e.PropertyName} change at ({automaton.CurrentState} state)";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SoakEngine_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( automaton, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
        }

        #endregion

        #region " CONTROL EVETN HANDLERS "

        /// <summary> Builds setpoint information. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> A SetpointInfo. </returns>
        private SetpointInfo BuildSetpointInfo()
        {
            return new SetpointInfo( Arebis.StandardUnits.TemperatureUnits.DegreeFahrenheit, ( double ) this._SoakSetpointNumeric.Value, TimeSpan.FromSeconds( ( double ) this._SampleIntervalNumeric.Value ), TimeSpan.FromSeconds( ( double ) this._SoakTimeNumeric.Value ), ( double ) this._SoakWindowNumeric.Value, ( double ) this._SoakHysteresisNumeric.Value, TimeSpan.FromSeconds( ( double ) this._SoakResetDelayNumeric.Value ), ( OvenControlMode ) Conversions.ToInteger( this._OvenControlModeComboBox.ComboBox.SelectedValue ), TimeSpan.FromSeconds( ( double ) this._RampTimeoutNumeric.Value ) );
        }


        /// <summary> Validates the oven control mode described by item. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="item"> The item. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        private bool ValidateOvenControlMode( ToolStripItem item )
        {
            bool result = ( OvenControlMode ) Conversions.ToInteger( this._OvenControlModeComboBox.ComboBox.SelectedValue ) != OvenControlMode.None;
            if ( !result )
            {
                _ = this._ErrorProvider.Annunciate( item, Core.Controls.InfoProviderLevel.Alert, "Select oven control mode" );
                _ = this.PublishInfo( "Oven control mode not selected" );
            }

            return result;
        }

        /// <summary> Soak setpoint numeric value changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SoakSetpointNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.EasyZoneClient.CandidateSetpointTemperature = ( double? ) this._SoakSetpointNumeric.Value;
        }

        /// <summary> Oven control mode combo box selected index changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OvenControlModeComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            if ( this.SoakEngine.IsActive() )
            {
                OvenControlMode controlMode = ( OvenControlMode ) Conversions.ToInteger( this._OvenControlModeComboBox.ComboBox.SelectedValue );
                if ( controlMode != OvenControlMode.None )
                {
                    this.EasyZoneClient.CandidateOvenControlMode = controlMode;
                }
            }
        }

        /// <summary> Starts soak menu item check state changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void StartSoakMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            sender = this._SoakControlButton;
            string activity = "Starting";
            try
            {
                this._ErrorProvider.Clear();
                this._StartSoakMenuItem.Enabled = false;
                if ( this._StartSoakMenuItem.Checked )
                {
                    activity = "starting";
                    if ( this.ValidateOvenControlMode( this._OvenControlModeComboBox ) )
                    {
                        if ( !this.SoakEngine.IsActive() )
                        {
                            activity = $"Starting setpoint {this._SetpointNumeric.Value}";
                            _ = this.PublishInfo( activity );
                            this.EasyZoneClient.ActivateSoakAutomaton( this.BuildSetpointInfo() );
                        }
                    }
                }
                else
                {
                    activity = $"stopping; Enqueueing a {SoakTrigger.Terminate} signal";
                    if ( this.SoakEngine.IsActive() )
                    {
                        _ = this.PublishVerbose( activity );
                        this.SoakEngine.Terminate();
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, $"Soak Exception {activity}" );
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
            finally
            {
                this._StartSoakMenuItem.Text = this._StartSoakMenuItem.Checked ? "Stop" : "Start";
                this._StartSoakMenuItem.Enabled = true;
            }
        }

        /// <summary> Abort soak menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void AbortSoakMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            sender = this._SoakControlButton;
            string activity = "issuing a stop request";
            try
            {
                this._ErrorProvider.Clear();
                this.SoakEngine.StopRequested = true;
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, $"Soak Exception {activity}" );
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
            finally
            {
            }
        }

        /// <summary> Pause soak menu item check state changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void PauseSoakMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            sender = this._SoakControlButton;
            string activity = "pausing";
            bool wasEnabled = this._PauseSoakMenuItem.Enabled;
            try
            {
                this._ErrorProvider.Clear();
                if ( this._PauseSoakMenuItem.Enabled )
                {
                    this._PauseSoakMenuItem.Enabled = false;
                    this._PauseSoakMenuItem.Invalidate();
                    if ( this._PauseSoakMenuItem.Checked )
                    {
                        activity = "pausing";
                        this.EasyZoneClient.PauseSoakAutomaton();
                    }
                    else
                    {
                        activity = "resuming";
                        this.EasyZoneClient.ResumeSoakAutomaton();
                    }

                    _ = this.PublishInfo( activity );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, $"Soak Exception {activity}" );
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
            finally
            {
                this._PauseSoakMenuItem.Text = this._PauseSoakMenuItem.Checked ? "Resume" : "Pause";
                this._PauseSoakMenuItem.Enabled = wasEnabled;
                this._PauseSoakMenuItem.Invalidate();
            }
        }

        /// <summary> Refresh soak menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void RefreshSoakMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            sender = this._SoakControlButton;
            string activity = "refreshing";
            try
            {
                this._ErrorProvider.Clear();
                this._RefreshSoakMenuItem.Enabled = false;
                if ( this.ValidateOvenControlMode( this._OvenControlModeComboBox ) )
                {
                    activity = $"Changing to setpoint {this._SetpointNumeric.Value}";
                    _ = this.PublishInfo( $"Changing to setpoint {this._SetpointNumeric.Value}" );
                    this.EasyZoneClient.ChangeTargetAutomatonSetpoint( this.BuildSetpointInfo(), false );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, Core.Controls.InfoProviderLevel.Error, $"Soak Exception {activity}" );
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
            finally
            {
                this._RefreshSoakMenuItem.Enabled = true;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Adds the listeners such as the current trace messages box. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected virtual void AddPrivateListeners()
        {
            this.AddPrivateListener( this._TraceMessagesBox );
            this.EasyZoneClient.Talker.AddPrivateListener( this._TraceMessagesBox );
        }

        /// <summary> Adds a listener. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="listener"> The listener. </param>
        public override void AddListener( IMessageListener listener )
        {
            this.EasyZoneClient?.AddListener( listener );
            base.AddListener( listener );
        }

        /// <summary> Adds the listeners. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public override void AddListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
                throw new ArgumentNullException( nameof( listeners ) );
            this.AddPrivateListeners();
            base.AddListeners( listeners );
        }

        /// <summary> Applies the trace level to all listeners to the specified type. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            if ( listenerType == this._TraceMessagesBox.ListenerType )
                this._TraceMessagesBox.ApplyTraceLevel( value );
            this.EasyZoneClient?.ApplyListenerTraceLevel( listenerType, value );
            // this should apply only to the listeners associated with this form
            // MyBase.ApplyListenerTraceLevel(listenerType, value)
        }

        /// <summary> Applies the trace level type to all talkers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="listenerType"> Type of the trace level. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyTalkerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            base.ApplyTalkerTraceLevel( listenerType, value );
            this.EasyZoneClient?.ApplyTalkerTraceLevel( listenerType, value );
        }

        /// <summary> Applies the talker trace levels described by talker. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="talker"> The talker. </param>
        public override void ApplyTalkerTraceLevels( ITraceMessageTalker talker )
        {
            base.ApplyTalkerTraceLevels( talker );
            this.EasyZoneClient?.ApplyTalkerTraceLevels( talker );
        }

        /// <summary> Applies the talker trace levels described by talker. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="talker"> The talker. </param>
        public override void ApplyListenerTraceLevels( ITraceMessageTalker talker )
        {
            this.EasyZoneClient?.ApplyListenerTraceLevels( talker );
            // this should apply only to the listeners associated with this form
            // MyBase.ApplyListenerTraceLevels(talker)
        }

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the
        /// message.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

        #region " MESSAGE BOX EVENTS "

        /// <summary> Handles the <see cref="_TraceMessagesBox"/> property changed event. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( Core.Forma.TraceMessagesBox sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            if ( string.Equals( propertyName, nameof( Core.Forma.TraceMessagesBox.StatusPrompt ) ) )
            {
                this._StatusLabel.Text = sender.StatusPrompt;
                this._StatusLabel.ToolTipText = sender.StatusPrompt;
            }
        }

        /// <summary> Trace messages box property changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        private void TraceMessagesBox_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is not Core.Forma.TraceMessagesBox msgBox || e is null )
                return;
            string activity = $"handler {nameof( Core.Forma.TraceMessagesBox )}.{e.PropertyName} change";
            try
            {
                // there was a cross thread exception because this event is invoked from the control thread.
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TraceMessagesBox_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as Core.Forma.TraceMessagesBox, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( $"Exception {activity};. ", ex );
            }
        }

        #endregion

        #region " INVOKE TESTERS "

        /// <summary> The event time builder. </summary>
        private System.Text.StringBuilder _EventTimeBuilder;

        /// <summary> The event stopwatch. </summary>
        private Stopwatch _EventStopwatch;

        /// <summary> Synchronizes the notify menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SyncNotifyMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.EasyZoneClient.HandlerExceptionOccurred += this.EasyZoneClient.RegisterHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred += this.HandleHandlerExceptionOccurred;
                this._EventTimeBuilder = new System.Text.StringBuilder();
                this._EventStopwatch = Stopwatch.StartNew();
                // Me.EasyZoneClient.BeginInvokeHandlerException()
                this.EasyZoneClient.SyncNotifyHandlerException();
                _ = this._EventTimeBuilder.AppendFormat( $">> {this._EventStopwatch.ElapsedMilliseconds:G5}ms" );
                this._TransactionElapsedTimeLabel.Text = this._EventTimeBuilder.ToString();
            }
            finally
            {
                this.EasyZoneClient.HandlerExceptionOccurred -= this.HandleHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred -= this.EasyZoneClient.RegisterHandlerExceptionOccurred;
            }
        }

        /// <summary> Unsafe invoke menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void UnsafeInvokeMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.EasyZoneClient.HandlerExceptionOccurred += this.EasyZoneClient.RegisterHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred += this.HandleHandlerExceptionOccurred;
                this._EventTimeBuilder = new System.Text.StringBuilder();
                this._EventStopwatch = Stopwatch.StartNew();
                this.EasyZoneClient.UnsafeInvokeHandlerException();
                _ = this._EventTimeBuilder.AppendFormat( $" >>{this._EventStopwatch.ElapsedMilliseconds:G5}ms " );
                this._TransactionElapsedTimeLabel.Text = this._EventTimeBuilder.ToString();
            }
            finally
            {
                this.EasyZoneClient.HandlerExceptionOccurred -= this.HandleHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred -= this.EasyZoneClient.RegisterHandlerExceptionOccurred;
            }
        }

        /// <summary> Asynchronous notify menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void AsyncNotifyMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.EasyZoneClient.HandlerExceptionOccurred += this.EasyZoneClient.RegisterHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred += this.HandleHandlerExceptionOccurred;
                this._EventTimeBuilder = new System.Text.StringBuilder();
                this._EventStopwatch = Stopwatch.StartNew();
                this.EasyZoneClient.AsyncNotifyHandlerException();

                // Me.EasyZoneClient.SafeBeginInvokeHandlerException()
                _ = this._EventTimeBuilder.AppendFormat( $" >>{this._EventStopwatch.ElapsedMilliseconds:G5}ms " );
                this._TransactionElapsedTimeLabel.Text = this._EventTimeBuilder.ToString();
            }
            finally
            {
                this.EasyZoneClient.HandlerExceptionOccurred -= this.HandleHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred -= this.EasyZoneClient.RegisterHandlerExceptionOccurred;
            }
        }

        /// <summary> Handler, called when the handler exception occurred. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Error event information. </param>
        private void HandleHandlerExceptionOccurred( object sender, System.IO.ErrorEventArgs e )
        {
            _ = this._EventTimeBuilder.AppendFormat( $" <<{this._EventStopwatch.ElapsedMilliseconds:G5}ms " );
            this._TransactionElapsedTimeLabel.Text = this._EventTimeBuilder.ToString();
            System.Threading.Thread.Sleep( 10 );
        }

        #endregion

    }
}
