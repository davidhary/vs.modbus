using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Easy.Zone.Console
{
    /// <summary> An easy zone console. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-5-10 </para>
    /// </remarks>
    public partial class EasyZoneConsole : Core.Forma.FormBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-11-28. </remarks>
        public EasyZoneConsole()
        {
            this.InitializeComponent();
        }

        #region " CONSTRUCTION "

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing && this.components is object )
                {
                    this.components.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );

                // set the form caption
                this.Text = Core.ApplicationInfo.BuildApplicationTitleCaption( this.Name );
                this._EasyZoneView.AddListener( My.MyProject.Application.Logger );
                this._EasyZoneView.ApplyTalkerTraceLevel( Core.ListenerType.Logger, My.Settings.Default.TraceLevel );
                this._EasyZoneView.ApplyTalkerTraceLevel( Core.ListenerType.Logger, My.Settings.Default.TraceLevel );
                My.MyApplication.Appliance.Identify( this._EasyZoneView.Talker );
                Application.DoEvents();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( "@isr", "Exception loading the form." );
                My.MyProject.Application.Logger.WriteExceptionDetails( ex, My.MyApplication.TraceEventId );
                if ( Core.MyDialogResult.Abort == Core.WindowsForms.ShowDialogAbortIgnore( ex, Core.MyMessageBoxIcon.Error ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnLoad( e );
                this.Cursor = Cursors.Default;
                Trace.CorrelationManager.StopLogicalOperation();
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            try
            {
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // allow form rendering time to complete: process all messages currently in the queue.
                Application.DoEvents();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( "@isr", "Exception showing the form." );
                My.MyProject.Application.Logger.WriteExceptionDetails( ex, My.MyApplication.TraceEventId );
                if ( Core.MyDialogResult.Abort == Core.WindowsForms.ShowDialogAbortIgnore( ex, Core.MyMessageBoxIcon.Error ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnShown( e );
                this.Cursor = Cursors.Default;
                Trace.CorrelationManager.StopLogicalOperation();
            }
        }

        #endregion

    }
}
