using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Easy.Zone.Console
{
    [DesignerGenerated()]
    public partial class EasyZoneConsole
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(EasyZoneConsole));
            _EasyZoneView = new Modbus.Watlow.Forms.EasyZoneView();
            SuspendLayout();
            // 
            // _EasyZoneView
            // 
            _EasyZoneView.Dock = DockStyle.Fill;
            _EasyZoneView.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _EasyZoneView.Location = new Point(0, 0);
            _EasyZoneView.Margin = new Padding(3, 4, 3, 4);
            _EasyZoneView.Name = "_EasyZoneView";
            _EasyZoneView.Size = new Size(386, 503);
            _EasyZoneView.TabIndex = 0;
            // 
            // EasyZoneConsole
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(386, 503);
            Controls.Add(_EasyZoneView);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "EasyZoneConsole";
            Text = "Easy Zone Console";
            ResumeLayout(false);
        }

        private Modbus.Watlow.Forms.EasyZoneView _EasyZoneView;
    }
}
