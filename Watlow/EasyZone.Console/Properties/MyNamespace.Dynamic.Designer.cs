﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Easy.Zone.Console.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public EasyZoneConsole m_EasyZoneConsole;

            public EasyZoneConsole EasyZoneConsole
            {
                [DebuggerHidden]
                get
                {
                    m_EasyZoneConsole = Create__Instance__(m_EasyZoneConsole);
                    return m_EasyZoneConsole;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_EasyZoneConsole))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_EasyZoneConsole);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public MySplashScreen m_MySplashScreen;

            public MySplashScreen MySplashScreen
            {
                [DebuggerHidden]
                get
                {
                    m_MySplashScreen = Create__Instance__(m_MySplashScreen);
                    return m_MySplashScreen;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_MySplashScreen))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_MySplashScreen);
                }
            }
        }
    }
}