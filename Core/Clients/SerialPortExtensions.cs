﻿using System;
using System.IO.Ports;

namespace isr.Modbus.SerialPortExtensions
{

    /// <summary> Serial port extension methods. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class Methods
    {

        /// <summary>
        /// Returns the bit count for the port baud date, data bits, stop bits and parity.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="serialPort"> The serial port. </param>
        /// <returns> An Integer. </returns>
        public static int BitCount( this SerialPort serialPort )
        {
            if ( serialPort is null )
                throw new ArgumentNullException( nameof( serialPort ) );
            int result = 1 + serialPort.DataBits;
            result += serialPort.Parity == Parity.None ? 0 : 1;
            switch ( serialPort.StopBits )
            {
                case StopBits.One:
                    {
                        result += 1;
                        break;
                    }

                case StopBits.OnePointFive:
                case StopBits.Two: // Ceiling
                    {
                        result += 2;
                        break;
                    }
            }

            if ( result <= 0 )
                throw new InvalidOperationException( $"Bit count for {serialPort.PortName}:{serialPort.BaudRate}:{serialPort.DataBits}:{serialPort.StopBits}:{serialPort.Parity} of {result} is invalid" );
            return result;
        }

        /// <summary> Returns the estimated character rate per second. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="serialPort"> Serial Port. </param>
        /// <returns> A Double. </returns>
        public static double CharacterRate( this SerialPort serialPort )
        {
            if ( serialPort is null )
                throw new ArgumentNullException( nameof( serialPort ) );
            return serialPort.BaudRate / ( double ) serialPort.BitCount();
        }

        /// <summary> Character timespan. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="serialPort"> Serial Port. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan CharacterTimespan( this SerialPort serialPort )
        {
            return TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerMillisecond * (1000d / serialPort.CharacterRate())) );
        }
    }
}