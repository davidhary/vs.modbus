﻿using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;

namespace isr.Modbus
{

    /// <summary> Modbus serial Client class. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    public sealed class ModbusClientSerial : ModbusClientBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="type"> Type of serial protocol. </param>
        /// <param name="port"> Serial port name. </param>
        public ModbusClientSerial( ModbusSerialType type, SerialPort port ) : base()
        {

            // Set device states
            switch ( type )
            {
                case ModbusSerialType.RTU:
                    {
                        this.ConnectionType = ConnectionType.SerialRTU;
                        break;
                    }

                case ModbusSerialType.ASCII:
                    {
                        this.ConnectionType = ConnectionType.SerialAscii;
                        break;
                    }
            }

            this._SerialPort = port;

            // Get inter frame delay
            this.InterFrameDelay = ModbusServerSerial.InterFrameDelayTimespan( port );

            // Get inter char delay
            this.InterCharacterDelay = ModbusServerSerial.InterCharacterDelayTimespan( port );
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {
                        if ( this._SerialPort is object )
                        {
                            this._SerialPort.Dispose();
                            this._SerialPort = null;
                        }
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Construct port. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="port">      Serial port name. </param>
        /// <param name="baudRate">  Baud rate. </param>
        /// <param name="dataBits">  Data bits. </param>
        /// <param name="parity">    Parity. </param>
        /// <param name="stopBits">  Stop bits. </param>
        /// <param name="handshake"> Handshake. </param>
        /// <returns> A SerialPort. </returns>
        public static SerialPort ConstructPort( string port, int baudRate, int dataBits, Parity parity, StopBits stopBits, Handshake handshake )
        {
            // Set serial port instance
            var sp = new SerialPort( port, baudRate, parity, dataBits, stopBits ) { Handshake = handshake };
            return sp;
        }

        #endregion

        #region " CONNECT / DISCONNECT FUNCTIONS "

        /// <summary>
        /// Serial port instance
        /// </summary>
        private SerialPort _SerialPort;

        /// <summary> Connect function. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void Connect()
        {
            this._SerialPort.Open();
            if ( this._SerialPort.IsOpen )
            {
                this.Connected = true;
            }
        }

        /// <summary> Disconnect function. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void Disconnect()
        {
            this._SerialPort.Close();
            this.Connected = false;
        }

        #endregion

        #region " PROTOCOL FUNCTIONS "

        /// <summary> Send transmission buffer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="buffer"> The buffer. </param>
        protected override void Send( IEnumerable<byte> buffer )
        {
            this.TxBuffer().AddRange( buffer );
            this._SerialPort.Write( this.TxBuffer().ToArray(), 0, this.TxBuffer().Count );
        }

        /// <summary> Gets the status of device data available. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if a new device data is available; otherwise <c>false</c> </returns>
        public override bool DeviceDataAvailable()
        {
            return this._SerialPort.BytesToRead > 0;
        }

        /// <summary> Reads a byte from the device. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> Byte read or nothing if no data are present. </returns>
        protected override byte? ReadByte()
        {

            // Check if there are available bytes
            while ( this.DeviceDataAvailable() )
            {
                int value = this._SerialPort.ReadByte();
                if ( value >= 0 )
                {
                    this.RxDeviceBuffer().Add( ( byte ) (value & 0xFF) );
                }
                else
                {
                    break;
                }

                Core.ApplianceBase.DoEventsWait( this.InterCharacterDelay );
            }

            if ( this.RxDeviceBuffer().Any() )
            {
                // There are available bytes in temporary rx buffer, read the first and delete it
                byte ret = this.RxDeviceBuffer()[0];
                this.RxDeviceBuffer().RemoveAt( 0 );
                return ret;
            }
            else
            {
                return new byte?();
            }
        }

        /// <summary> Flushes device input and output buffers and cached input buffers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected override void FlushDeviceBuffers()
        {
            base.FlushDeviceBuffers();
            if ( this._SerialPort?.IsOpen == true )
            {
                this._SerialPort.DiscardInBuffer();
                this._SerialPort.DiscardOutBuffer();
            }
        }

        #endregion

    }
}