using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Modbus
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Modbus Client UDP class. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    public class ModbusClientUDP : ModbusClientBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="remoteHost"> Remote hostname or IP address. </param>
        /// <param name="port">       Remote Modbus TCP port. </param>
        public ModbusClientUDP( string remoteHost, int port ) : base()
        {

            // Set device states
            this.ConnectionType = ConnectionType.UdpIP;

            // Set socket client
            this._RemoteHost = remoteHost;
            this._Port = port;
            this._UdpClient = new UdpClient();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {
                        if ( this._UdpClient is object )
                        {
                            this._UdpClient.Dispose();
                            this._UdpClient = null;
                        }
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }



        #endregion

        #region " GLOBAL VARIABLES "

        /// <summary>
        /// Remote host name or IP address
        /// </summary>
        private readonly string _RemoteHost;

        /// <summary>
        /// Remote Modbus TCP port
        /// </summary>
        private readonly int _Port;

        #endregion

        #region " CONNECT / DISCONNECT FUNCTIONS "

        /// <summary>
        /// UDP Client
        /// </summary>
        private UdpClient _UdpClient;

        /// <summary> Connect function. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void Connect()
        {
            this._UdpClient.Connect( this._RemoteHost, this._Port );
            if ( this._UdpClient.Client.Connected )
            {
                this.Connected = true;
            }
        }

        /// <summary> Disconnect function. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void Disconnect()
        {
            this._UdpClient.Close();
            this.Connected = false;
        }

        #endregion

        #region " PROTOCOL FUNCTIONS "

        /// <summary> Send transmission buffer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="buffer"> The buffer. </param>
        protected override void Send( IEnumerable<byte> buffer )
        {
            this.TxBuffer().AddRange( buffer );
            _ = this._UdpClient.Send( this.TxBuffer().ToArray(), this.TxBuffer().Count );
        }

        /// <summary> Gets the status of device data available. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if a new device data is available; otherwise <c>false</c> </returns>
        public override bool DeviceDataAvailable()
        {
            return this._UdpClient.Available > 0;
        }

        /// <summary> Reads a byte from the device. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> Byte read or nothing if no data are present. </returns>
        protected override byte? ReadByte()
        {
            var ipe = new IPEndPoint( IPAddress.Any, 0 );

            // Check if there are available bytes
            if ( this.DeviceDataAvailable() )
            {
                // Enqueue bytes to temporary rx buffer
                this.RxDeviceBuffer().AddRange( this._UdpClient.Receive( ref ipe ) );
            }

            if ( this.RxDeviceBuffer().Any() )
            {
                // There are available bytes in temporary rx buffer, read the first and delete it
                byte ret = this.RxDeviceBuffer()[0];
                this.RxDeviceBuffer().RemoveAt( 0 );
                return ret;
            }
            else
            {
                return new byte?();
            }
        }

        /// <summary> Flushes device input and output buffers and cached input buffers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected override void FlushDeviceBuffers()
        {
            base.FlushDeviceBuffers();
            var ipe = new IPEndPoint( IPAddress.Any, 0 );
            while ( this._UdpClient.Available > 0 )
                _ = this._UdpClient.Receive( ref ipe );
        }

        #endregion

    }
}
