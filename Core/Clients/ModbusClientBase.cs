using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using isr.Core.BitConverterExtensions;

using Microsoft.VisualBasic.CompilerServices;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Modbus
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Base abstract class for Modbus client instances. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    public abstract class ModbusClientBase : ModbusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected ModbusClientBase() : base()
        {
            this._TxPayload = new List<byte>();
            this._TxBuffer = new List<byte>();
            this._RxPayload = new List<byte>();
            this._RxBuffer = new List<byte>();
            this._RxDeviceBuffer = new List<byte>();
            this.DeviceType = DeviceType.Client;
            this.RxTimeSeries = new TimeSeriesCollection();
        }

        #region " BUFFER "

        /// <summary> The transmit payload. </summary>
        private readonly List<byte> _TxPayload;

        /// <summary> Transmission payload buffer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> A List(Of Byte) </returns>
        protected List<byte> TxPayload()
        {
            return this._TxPayload;
        }

        /// <summary> Format transmit payload. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The formatted transmit payload. </returns>
        public string FormatTxPayload()
        {
            return FormatBuffer( this.TxPayload() );
        }

        /// <summary> Buffer for transmit data. </summary>
        private readonly List<byte> _TxBuffer;

        /// <summary> Transmission Buffer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> A List(Of Byte) </returns>
        protected List<byte> TxBuffer()
        {
            return this._TxBuffer;
        }

        /// <summary> Format transmit Buffer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The formatted transmit Buffer. </returns>
        public string FormatTxBuffer()
        {
            return FormatBuffer( this.TxBuffer() );
        }

        /// <summary> The receive payload. </summary>
        private readonly List<byte> _RxPayload;

        /// <summary> Received payload after trimming the transmission bits. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> A List(Of Byte) </returns>
        protected List<byte> RxPayload()
        {
            return this._RxPayload;
        }

        /// <summary> Returns the formatted received payload. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The formatted received payload. </returns>
        public string FormatRxPayload()
        {
            return FormatBuffer( this.RxPayload() );
        }

        /// <summary> Buffer for receive device data. </summary>
        private readonly List<byte> _RxDeviceBuffer;

        /// <summary> Received Buffer including the transmission bits. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> A List(Of Byte) </returns>
        protected List<byte> RxDeviceBuffer()
        {
            return this._RxDeviceBuffer;
        }

        /// <summary> Buffer for receive data. </summary>
        private readonly List<byte> _RxBuffer;

        /// <summary> Received Buffer including the transmission bits. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> A List(Of Byte) </returns>
        protected List<byte> RxBuffer()
        {
            return this._RxBuffer;
        }

        /// <summary> Returns the formatted received Buffer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The formatted received Buffer. </returns>
        public string FormatRxBuffer()
        {
            return FormatBuffer( this.RxBuffer() );
        }

        /// <summary> Gets or sets the received time series. </summary>
        /// <value> The received time series for use in debugging. </value>
        public TimeSeriesCollection RxTimeSeries { get; private set; }

        /// <summary> Modbus transaction ID (only Modbus TCP/UDP) </summary>
        /// <value> The identifier of the transaction. </value>
        [CLSCompliant( false )]
        protected ushort TransactionId { get; set; } = 0;

        /// <summary> Format buffer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="buffer"> The buffer. </param>
        /// <returns> The formatted buffer. </returns>
        public static string FormatBuffer( IEnumerable<byte> buffer )
        {
            var builder = new StringBuilder();
            if ( buffer?.Any() == true )
            {
                foreach ( byte b in buffer )
                {
                    if ( builder.Length > 0 )
                        _ = builder.Append( "," );
                    _ = builder.AppendFormat( "{0:X2}", b );
                }
            }

            return builder.ToString();
        }

        #endregion

        #region " PARAMETERS "

        /// <summary>
        /// Remote host connection status
        /// </summary>

        /// <summary> Get remote host connection status. </summary>
        /// <value> The connected. </value>
        public bool Connected { get; protected set; } = false;

        #endregion

        #endregion

        #region " CONNECT/DISCONNECT FUNCTIONS "

        /// <summary> Open connection. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public abstract void Connect();

        /// <summary> Close connection. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public abstract void Disconnect();

        #endregion

        #region " SEND/RECEIVE FUNCTIONS "

        /// <summary> Function To send transmission buffer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="buffer"> The buffer. </param>
        protected abstract void Send( IEnumerable<byte> buffer );

        /// <summary> Gets the status of device data available. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if a new device data is available; otherwise <c>false</c> </returns>
        public abstract bool DeviceDataAvailable();

        /// <summary> Determines if we either device or buffered device data is available. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool RxDataAvailable()
        {
            return this.RxDeviceBuffer().Any() || this.DeviceDataAvailable();
        }

        /// <summary> Waits for available device data. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="timeout">       The timeout. </param>
        /// <param name="sleepInterval"> The sleep interval. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool AwaitDeviceDataAvailable( TimeSpan timeout, TimeSpan sleepInterval )
        {
            if ( timeout > TimeSpan.Zero )
            {
                var sw = Stopwatch.StartNew();
                while ( !this.DeviceDataAvailable() && sw.Elapsed <= timeout )
                {
                    Core.ApplianceBase.DoEvents();
                    if ( sleepInterval > TimeSpan.Zero )
                        Core.ApplianceBase.Delay( sleepInterval );
                }
            }

            return this.DeviceDataAvailable();
        }

        /// <summary> Waits for the next byte. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool AwaitNextByteAvailable()
        {
            return this.RxDataAvailable() || this.AwaitDeviceDataAvailable( this.InterFrameDelay, this.InterCharacterDelay );
        }

        /// <summary> Waits for the first byte. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool AwaitFirstByteAvailable()
        {
            return this.AwaitDeviceDataAvailable( this.RxTimeout, this.PollInterval );
        }

        /// <summary> Reads a byte from the device. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> Byte read or nothing if no data are present. </returns>
        protected abstract byte? ReadByte();

        /// <summary> Reads a byte from a device after a waiting for the inter character delay. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> Byte read or nothing if no data are present. </returns>
        protected byte? WaitReadByte()
        {
            return this.AwaitNextByteAvailable() ? this.ReadByte() : new byte?();
        }

        /// <summary> Flushes device input and output buffers and cached input buffers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected virtual void FlushDeviceBuffers()
        {
            this.RxDeviceBuffer().Clear();
        }

        #endregion

        #region " TRANSMIT PROTOCOL FUNCTIONS "

        /// <summary> Initialize a new Modbus TCP/UDP message. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected void InitTcpUdpClientMessage()
        {
            // Increase transaction_id
            this.TransactionId += 1;
            // Tx buffer emptying
            this.TxPayload().Clear();
            this.TxBuffer().Clear();
        }

        /// <summary> Build Modbus Application Protocol Header (MBAP) header for Modbus TCP/UDP. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="buffer">             The buffer. </param>
        /// <param name="destinationAddress"> Destination address. </param>
        /// <param name="messageLength">      Message length. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build header in this collection.
        /// </returns>
        protected IEnumerable<byte> BuildHeader( IEnumerable<byte> buffer, byte destinationAddress, int messageLength )
        {
            var result = new List<byte>( buffer );

            // Transaction ID (incremented by 1 on each transmission)
            result.InsertRange( 0, this.TransactionId.BigEndInt8() );

            // Protocol ID (fixed value)
            result.InsertRange( 2, ProtocolId.BigEndInt8() );

            // Message length
            result.InsertRange( 4, Conversions.ToUShort( messageLength ).BigEndInt8() );

            // Remote unit ID
            result.Insert( 6, destinationAddress );
            return result;
        }

        /// <summary> Build Modbus Application Protocol Header (MBAP) header for Modbus TCP/UDP. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="buffer">             The buffer. </param>
        /// <param name="destinationAddress"> Destination address. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build header in this collection.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        protected IEnumerable<byte> BuildHeader( IEnumerable<byte> buffer, byte destinationAddress )
        {
            var result = new List<byte>( buffer );

            // Insert 'unit_id' in front of the message
            result.Insert( 0, destinationAddress );

            // Append CRC16
            result.AddRange( BitConverter.GetBytes( CRC16.Evaluate( result.ToArray(), 0, result.Count ) ) );
            return result;
        }

        /// <summary>
        /// Build Modbus Application Protocol Header (MBAP) header for Modbus Serial Ascii.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="buffer">             The buffer. </param>
        /// <param name="destinationAddress"> Destination address. </param>
        /// <param name="termination">        The termination. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build header in this collection.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        protected IEnumerable<byte> BuildHeader( IEnumerable<byte> buffer, byte destinationAddress, char[] termination )
        {
            var result = new List<byte>( buffer );

            // Add destination device address
            result.Insert( 0, destinationAddress );

            // Calculate message LCR
            byte v = LRC.Evaluate( result.ToArray(), 0, result.Count );
            var lrc = (new byte[] { v }).ToAsciiBytes();

            // Convert 'send_buffer' from binary to ASCII
            var sb = result.ToArray().ToAsciiBytes();
            result.Clear();
            result.AddRange( sb );

            // Add LRC at the end of the message
            result.AddRange( lrc );

            // Insert the start frame char
            result.Insert( 0, Encoding.ASCII.GetBytes( new char[] { AsciiStartFrameCharacter } ).First() );

            // Insert stop frame chars
            result.AddRange( Encoding.ASCII.GetBytes( termination ) );
            return result;
        }

        /// <summary> Executes a write to a destination Client. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="payload">       The payload. </param>
        /// <param name="unitId">        Server device address. </param>
        /// <param name="messageLength"> Message length. </param>
        private void WriteThis( IEnumerable<byte> payload, byte unitId, int messageLength )
        {

            // Set errors to null
            this.LastError = InterfaceError.NoError;

            // Start to build message
            switch ( this.ConnectionType )
            {
                case ConnectionType.SerialAscii:
                    {
                        var end_frame = new char[] { AsciiEndFrameFirstCharacter, AsciiEndFrameSecondCharacter };
                        this.Send( this.BuildHeader( payload, unitId, end_frame ) );
                        break;
                    }

                case ConnectionType.SerialRTU:
                    {

                        // Wait for inter-frame delay
                        Core.ApplianceBase.Delay( this.InterFrameDelay );
                        this.Send( this.BuildHeader( payload, unitId ) );
                        break;
                    }

                case ConnectionType.UdpIP:
                case ConnectionType.TcpIP:
                    {
                        this.Send( this.BuildHeader( payload, unitId, messageLength ) );
                        break;
                    }
            }
        }

        #endregion

        #region " RECEIVE PROTOCOL FUNCTIONS "

        /// <summary> Gets the length of the minimum frame. </summary>
        /// <value> The length of the minimum frame. </value>
        public int MinimumFrameLength
        {
            get {
                int value;
                switch ( this.ConnectionType )
                {
                    case ConnectionType.SerialAscii:
                        {
                            value = 11;
                            break;
                        }

                    case ConnectionType.UdpIP:
                    case ConnectionType.TcpIP:
                        {
                            value = 9;
                            break;
                        }

                    default:
                        {
                            value = 5;
                            break;
                        }
                }

                return value;
            }
        }

        /// <summary> Parse the received data. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="expectedFrameId"> Identifier for the expected frame. </param>
        /// <param name="buffer">          The buffer. </param>
        /// <returns> <c>true</c> if receive frame complete; otherwise <c>false</c> </returns>
        private bool IsReceiveFrameComplete( byte expectedFrameId, IEnumerable<byte> buffer )
        {
            bool result = false;
            var rx = new List<byte>( buffer );
            switch ( this.ConnectionType )
            {
                case ConnectionType.SerialAscii:
                    {

                        // Check message consistency
                        if ( rx[0] != expectedFrameId )
                        {
                            // parsing is expected to detect an error
                            result = true;
                            break;
                        }

                        if ( rx.Count < this.MinimumFrameLength )
                        {
                            result = false;
                            break;
                        }

                        // remove start char
                        rx.RemoveRange( 0, 1 );
                        // Check and remove stop chars
                        var orig_end_frame = new char[] { AsciiEndFrameFirstCharacter, AsciiEndFrameSecondCharacter };
                        var rec_end_frame = Encoding.ASCII.GetChars( rx.GetRange( rx.Count - 2, 2 ).ToArray() );
                        if ( !orig_end_frame.SequenceEqual( rec_end_frame ) )
                        {
                            // not completed yet
                            result = false;
                            break;
                        }

                        // remove termination characters.
                        rx.RemoveRange( rx.Count - 2, 2 );

                        // Convert receive buffer from ASCII to binary
                        var rb = rx.ToArray().FromAsciiBytes();
                        rx.Clear();
                        rx.AddRange( rb );

                        // Check and remove message LRC
                        byte lrc_calculated = LRC.Evaluate( rx.ToArray(), 0, rx.Count - 1 );
                        byte lrc_received = rx[rx.Count - 1];
                        if ( lrc_calculated == lrc_received )
                        {
                            // completed
                            result = true;
                            break;
                        }

                        break;
                    }

                case ConnectionType.SerialRTU:
                    {

                        // Check message consistency
                        if ( rx[0] != expectedFrameId )
                        {
                            // parsing is expected to detect an error
                            result = true;
                            break;
                        }

                        if ( rx.Count < this.MinimumFrameLength )
                        {
                            result = false;
                            break;
                        }

                        // Check message 16-bit CRC
                        ushort calc_crc = CRC16.Evaluate( rx.ToArray(), 0, rx.Count - 2 );
                        ushort rec_crc = BitConverter.ToUInt16( rx.ToArray(), rx.Count - 2 );
                        if ( rec_crc == calc_crc )
                        {
                            // completed
                            result = true;
                            break;
                        }

                        break;
                    }

                case ConnectionType.UdpIP:
                case ConnectionType.TcpIP:
                    {
                        if ( rx.Count < this.MinimumFrameLength )
                        {
                            result = false;
                            break;
                        }

                        // Check MBAP header
                        ushort tid = rx.ToArray().BigEndUnsignedInt16( 0 );
                        if ( tid != this.TransactionId )
                        {
                            // parsing is expected to detect an error
                            result = true;
                            break;
                        }

                        ushort pid = rx.ToArray().BigEndUnsignedInt16( 2 );
                        if ( pid != ProtocolId )
                        {
                            // parsing is expected to detect an error
                            result = true;
                            break;
                        }

                        byte uid = rx[6];
                        if ( uid != expectedFrameId )
                        {
                            // parsing is expected to detect an error
                            result = true;
                            break;
                        }

                        ushort len = rx.ToArray().BigEndUnsignedInt16( 4 );
                        if ( rx.Count - MbapHeaderLength + 1 >= len )
                        {
                            result = true;
                            break;
                        }

                        break;
                    }
            }

            // Control eventual error message 
            if ( rx.Count > 1 && rx[0] > 0x80 && rx[1] >= 1 && rx[1] <= 4 )
            {
                // parsing is expected to detect an error
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Fills the receive buffer checking completion after receiving the minimum frame length.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="timeoutStopwatch"> The timeout stopwatch. </param>
        private void FillReceiveBuffer( TimeoutStopwatch timeoutStopwatch )
        {
            byte frameId = 0;
            switch ( this.ConnectionType )
            {
                case ConnectionType.SerialAscii:
                case ConnectionType.SerialRTU:
                    {
                        frameId = this.TxBuffer()[0];
                        break;
                    }

                case ConnectionType.TcpIP:
                case ConnectionType.UdpIP:
                    {
                        frameId = this.TxBuffer()[6];
                        break;
                    }
            }

            do
            {
                var rcv = this.WaitReadByte();
                this.RxTimeSeries.Add( timeoutStopwatch.Elapsed, rcv );
                if ( rcv.HasValue )
                {
                    if ( this.ConnectionType == ConnectionType.SerialAscii )
                    {
                        if ( ( byte ) rcv == Encoding.ASCII.GetBytes( new char[] { AsciiStartFrameCharacter } ).First() )
                        {
                            // this is the first character, reset the reading.
                            this.RxBuffer().Clear();
                        }
                    }

                    this.RxBuffer().Add( ( byte ) rcv );
                }
            }
            // loop while more data is available or not (completed or timeout)
            while ( this.AwaitNextByteAvailable() || !(this.IsReceiveFrameComplete( frameId, this.RxBuffer() ) || timeoutStopwatch.IsTimeout()) );
            if ( this.LastError == InterfaceError.NoError && this.RxBuffer().Count < this.MinimumFrameLength )
            {
                this.LastError = InterfaceError.MessageTooShort;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Rx {this.FormatRxBuffer()} has fewer than {this.MinimumFrameLength} values; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
            }
        }

        /// <summary> Reads this. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        private void ReadThis()
        {

            // Set errors to null
            this.LastError = InterfaceError.NoError;

            // Start receiving...
            this.RxPayload().Clear();
            this.RxBuffer().Clear();
            this.RxDeviceBuffer().Clear();
            this.RxTimeSeries.Clear();

            // start timing before waiting to get a clue on how long it take to get a response from the device.
            var stopwatch = TimeoutStopwatch.StartNew( this.RxTimeout );

            // wait for first byte.
            if ( this.AwaitFirstByteAvailable() )
            {
                // fill the receive buffer with data
                this.FillReceiveBuffer( stopwatch );
            }
            // Me._FillReceiveBuffer(stopwatch)
            else
            {
                this.LastError = InterfaceError.ReceiveTimeout;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Timeout waiting for the first character";
            }
        }

        /// <summary> Parse the received data. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="expectedFrameId"> Identifier for the expected frame. </param>
        /// <param name="buffer">          The buffer. </param>
        private void ParseThis( byte expectedFrameId, IEnumerable<byte> buffer )
        {

            // Set errors to null
            this.LastError = InterfaceError.NoError;

            // build the payload.
            this.RxPayload().AddRange( buffer );
            switch ( this.ConnectionType )
            {
                case ConnectionType.SerialAscii:
                    {

                        // Check message consistency
                        if ( this.RxPayload()[0] != expectedFrameId )
                        {
                            this.LastError = InterfaceError.StartCharNotFound;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Invalid start. Rx: '{FormatBuffer( buffer )}(0)<>{expectedFrameId}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                        // remove start char
                        this.RxPayload().RemoveRange( 0, 1 );

                        // Check and remove stop chars
                        var orig_end_frame = new char[] { AsciiEndFrameFirstCharacter, AsciiEndFrameSecondCharacter };
                        var rec_end_frame = Encoding.ASCII.GetChars( this.RxPayload().GetRange( this.RxPayload().Count - 2, 2 ).ToArray() );
                        if ( !orig_end_frame.SequenceEqual( rec_end_frame ) )
                        {
                            this.LastError = InterfaceError.EndCharsNotFound;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Invalid termination. Rx: '{FormatBuffer( buffer )}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                        this.RxPayload().RemoveRange( this.RxPayload().Count - 2, 2 );
                        // Convert receive buffer from ASCII to binary
                        var rb = this.RxPayload().ToArray().FromAsciiBytes();
                        this.RxPayload().Clear();
                        this.RxPayload().AddRange( rb );

                        // Check and remove message LRC
                        byte lrc_calculated = LRC.Evaluate( this.RxPayload().ToArray(), 0, this.RxPayload().Count - 1 );
                        byte lrc_received = this.RxPayload()[this.RxPayload().Count - 1];
                        if ( lrc_calculated != lrc_received )
                        {
                            this.LastError = InterfaceError.WrongLrc;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong LRC {lrc_received}<>{lrc_calculated}. Rx: '{FormatBuffer( buffer )}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                        this.RxPayload().RemoveRange( this.RxPayload().Count - 1, 1 );
                        // Remove address byte
                        this.RxPayload().RemoveRange( 0, 1 );
                        break;
                    }

                case ConnectionType.SerialRTU:
                    {

                        // Check message consistency
                        if ( this.RxPayload()[0] != expectedFrameId )
                        {
                            this.LastError = InterfaceError.WrongResponseAddress;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Invalid start. Rx: '{FormatBuffer( buffer )}(0)<>{expectedFrameId}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                        // Check message 16-bit CRC
                        ushort calc_crc = CRC16.Evaluate( this.RxPayload().ToArray(), 0, this.RxPayload().Count - 2 );
                        ushort rec_crc = BitConverter.ToUInt16( this.RxPayload().ToArray(), this.RxPayload().Count - 2 );
                        if ( rec_crc != calc_crc )
                        {
                            this.LastError = InterfaceError.WrongCrc;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong CRC {rec_crc}<>{calc_crc}. Rx: '{FormatBuffer( buffer )}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                        // Remove address
                        this.RxPayload().RemoveRange( 0, 1 );

                        // Remove CRC
                        this.RxPayload().RemoveRange( this.RxPayload().Count - 2, 2 );
                        break;
                    }

                case ConnectionType.UdpIP:
                case ConnectionType.TcpIP:
                    {
                        // Check MBAP header
                        ushort tid = this.RxPayload().ToArray().BigEndUnsignedInt16( 0 );
                        if ( tid != this.TransactionId )
                        {
                            this.LastError = InterfaceError.WrongTransactionId;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong transaction id {tid}<>{this.TransactionId}. Rx: '{FormatBuffer( buffer )}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                        ushort pid = this.RxPayload().ToArray().BigEndUnsignedInt16( 2 );
                        if ( pid != ProtocolId )
                        {
                            this.LastError = InterfaceError.WrongProtocolId;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong protocol id {pid}<>{ProtocolId}. Rx: '{FormatBuffer( buffer )}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                        ushort len = this.RxPayload().ToArray().BigEndUnsignedInt16( 4 );
                        int receivedLength = this.RxPayload().Count - MbapHeaderLength + 1;
                        if ( receivedLength < len )
                        {
                            this.LastError = InterfaceError.MessageTooShort;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Message too short {receivedLength}<{len}. Rx: '{FormatBuffer( buffer )}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                        byte uid = this.RxPayload()[6];
                        if ( uid != expectedFrameId )
                        {
                            this.LastError = InterfaceError.WrongResponseUnitId;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Invalid response id. Rx: '{FormatBuffer( buffer )}(6)<>{expectedFrameId}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }
                        // Let only useful bytes in receive buffer                       
                        this.RxPayload().RemoveRange( 0, MbapHeaderLength );
                        break;
                    }
            }

            // Control eventual error message 
            if ( this.LastError == InterfaceError.NoError && this.RxPayload()[0] > 0x80 )
            {
                // parse error codes
                switch ( this.RxPayload()[1] )
                {
                    case 1:
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Illegal function. Rx: '{FormatBuffer( buffer )}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                    case 2:
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Illegal data address. Rx: '{FormatBuffer( buffer )}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                    case 3:
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataValue;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Illegal data value. Rx: '{FormatBuffer( buffer )}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }

                    case 4:
                        {
                            this.LastError = InterfaceError.ExceptionServerDeviceFailure;
                            this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Server failure. Rx: '{FormatBuffer( buffer )}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                            break;
                        }
                }
            }
        }

        /// <summary> Executes a query to a destination Client. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">        Salve device address. </param>
        /// <param name="messageLength"> Message length. </param>
        [CLSCompliant( false )]
        protected void Query( byte unitId, int messageLength )
        {
            this.OnQueryStarted();

            // flush device buffers before starting a new query
            this.FlushDeviceBuffers();
            this.WriteThis( this.TxPayload(), unitId, messageLength );
            if ( this.LastError == InterfaceError.NoError )
                this.ReadThis();
            if ( this.LastError == InterfaceError.NoError )
            {
                byte frameId = 0;
                switch ( this.ConnectionType )
                {
                    case ConnectionType.SerialAscii:
                    case ConnectionType.SerialRTU:
                        {
                            frameId = this.TxBuffer()[0];
                            break;
                        }

                    case ConnectionType.TcpIP:
                    case ConnectionType.UdpIP:
                        {
                            frameId = this.TxBuffer()[6];
                            break;
                        }
                }

                this.ParseThis( frameId, this.RxBuffer() );
            }

            this.OnQueryFinished();
        }

        /// <summary> Read coil registers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">       Server device address. </param>
        /// <param name="startAddress"> Address of first register to be read. </param>
        /// <param name="len">          Number of registers to be read. </param>
        /// <returns> Array of read registers. </returns>
        [CLSCompliant( false )]
        public bool[] ReadCoils( byte unitId, ushort startAddress, ushort len )
        {
            if ( len < 1 )
            {
                this.LastError = InterfaceError.ZeroRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Incorrect Read Coils argument; {len}<1";
                return null;
            }

            if ( len > MaxCoilsInReadMessage )
            {
                this.LastError = InterfaceError.TooManyRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Incorrect Read Coils argument; {len}>{MaxCoilsInReadMessage}";
                return null;
            }

            ushort msg_len = 6;
            this.InitTcpUdpClientMessage();
            this.TxPayload().Add( ( byte ) ModbusCodes.ReadCoils );
            this.TxPayload().AddRange( startAddress.BigEndInt8() );
            this.TxPayload().AddRange( len.BigEndInt8() );
            this.Query( unitId, msg_len );
            if ( this.LastError == InterfaceError.NoError )
            {
                var ba = new BitArray( this.RxPayload().GetRange( 2, this.RxPayload().Count - 2 ).ToArray() );
                var ret = new bool[ba.Count];
                ba.CopyTo( ret, 0 );
                return ret;
            }
            else
            {
                return null;
            }
        }

        /// <summary> Read a set of discrete inputs. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">       Server device address. </param>
        /// <param name="startAddress"> Address of first register to be read. </param>
        /// <param name="len">          Number of registers to be read. </param>
        /// <returns> Array of read registers. </returns>
        [CLSCompliant( false )]
        public bool[] ReadDiscreteInputs( byte unitId, ushort startAddress, ushort len )
        {
            if ( len < 1 )
            {
                this.LastError = InterfaceError.ZeroRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Incorrect Read Discrete Inputs argument; {len}<1";
                return null;
            }

            if ( len > MaxDiscreteInputsInReadMessage )
            {
                this.LastError = InterfaceError.TooManyRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Incorrect Read Discrete Inputs argument; {len}>{MaxDiscreteInputsInReadMessage}";
                return null;
            }

            ushort msg_len = 6;
            this.InitTcpUdpClientMessage();
            this.TxPayload().Add( ( byte ) ModbusCodes.ReadDiscreteInputs );
            this.TxPayload().AddRange( startAddress.BigEndInt8() );
            this.TxPayload().AddRange( len.BigEndInt8() );
            this.Query( unitId, msg_len );
            if ( this.LastError == InterfaceError.NoError )
            {
                var ba = new BitArray( this.RxPayload().GetRange( 2, this.RxPayload().Count - 2 ).ToArray() );
                var ret = new bool[ba.Count];
                ba.CopyTo( ret, 0 );
                return ret;
            }
            else
            {
                return null;
            }
        }

        /// <summary> Tries to read a holding registers value. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">       Server device address. </param>
        /// <param name="startAddress"> Address of first register to be read. </param>
        /// <param name="value">        [in,out] Value to write. </param>
        /// <returns> <c>true</c> if the test passes, <c>false</c> if the test fails. </returns>
        [CLSCompliant( false )]
        public bool TryReadHoldingRegistersValue( byte unitId, ushort startAddress, ref ushort value )
        {
            var values = this.ReadHoldingRegisters( unitId, startAddress, 1 );
            if ( this.LastError == InterfaceError.NoError )
            {
                if ( values?.Any() == true )
                {
                    value = values.First();
                }
                else
                {
                    this.LastError = InterfaceError.WrongResponseValue;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Unable to get first element; Rx: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                }
            }

            return this.LastError == InterfaceError.NoError;
        }

        /// <summary> Tries to read a holding registers value. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">       Server device address. </param>
        /// <param name="startAddress"> Address of first register to be read. </param>
        /// <param name="value">        [in,out] Value to write. </param>
        /// <returns> <c>true</c> if the test passes, <c>false</c> if the test fails. </returns>
        [CLSCompliant( false )]
        public bool TryReadHoldingRegistersValue( byte unitId, ushort startAddress, ref float value )
        {
            var values = this.ReadHoldingRegisters( unitId, startAddress, 2 );
            if ( this.LastError == InterfaceError.NoError )
            {
                if ( values?.Any() == true )
                {
                    if ( values.Count() >= 2 )
                    {
                        value = values.BigEndSingle( 0 );
                    }
                    else
                    {
                        this.LastError = InterfaceError.WrongResponseValue;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Insufficient data for parsing a floating point value; Rx: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                    }
                }
                else
                {
                    this.LastError = InterfaceError.WrongResponseValue;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Receive value is empty; Rx: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                }
            }

            return this.LastError == InterfaceError.NoError;
        }

        /// <summary> Read a set of holding registers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">       Server device address. </param>
        /// <param name="startAddress"> Address of first register to be read. </param>
        /// <param name="len">          Number of registers to be read. </param>
        /// <returns> Array of read registers. </returns>
        [CLSCompliant( false )]
        public ushort[] ReadHoldingRegisters( byte unitId, ushort startAddress, ushort len )
        {
            if ( len < 1 )
            {
                this.LastError = InterfaceError.ZeroRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Incorrect Read Holding Registers argument:; {len}<1";
                return null;
            }

            if ( len > MaxHoldingRegistersInReadMessage )
            {
                this.LastError = InterfaceError.TooManyRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Incorrect Read Holding Registers argument:; {len}>{MaxHoldingRegistersInReadMessage}";
                return null;
            }

            ushort msg_len = 6;
            this.InitTcpUdpClientMessage();
            this.TxPayload().Add( ( byte ) ModbusCodes.ReadHoldingRegisters );
            this.TxPayload().AddRange( startAddress.BigEndInt8() );
            this.TxPayload().AddRange( len.BigEndInt8() );
            this.Query( unitId, msg_len );
            if ( this.LastError == InterfaceError.NoError )
            {
                var ret = new List<ushort>( this.ExtractValues() );
                return this.LastError == InterfaceError.NoError ? ret.ToArray() : null;
            }
            else
            {
                return null;
            }
        }

        /// <summary> Extracts the values. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The extracted values. </returns>
        private ushort[] ExtractValues()
        {
            var ret = new List<ushort>();
            int offset = 2;
            if ( this.RxPayload().Any() )
            {
                if ( this.RxPayload().Count < offset )
                {
                    this.LastError = InterfaceError.WrongResponseValue;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Rx too short: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                }
                else if ( this.RxPayload().Count < this.RxPayload()[1] + offset )
                {
                    this.LastError = InterfaceError.WrongResponseValue;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Rx shorter than specified: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                }
                else
                {
                    ret = new List<ushort>( ExtractValues( this.RxPayload()[1] / 2, offset, this.RxPayload().ToArray() ) );
                }
            }
            else
            {
                this.LastError = InterfaceError.WrongResponseValue;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Empty Rx: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
            }

            return ret.ToArray();
        }

        /// <summary> Extracts the values. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="count">  Number of. </param>
        /// <param name="offset"> The offset. </param>
        /// <param name="values"> Array of values to be write. </param>
        /// <returns> The extracted values. </returns>
        private static ushort[] ExtractValues( int count, int offset, byte[] values )
        {
            var ret = new List<ushort>();
            int stepSize = 2;
            while ( ret.Count < count )
            {
                ret.Add( values.BigEndUnsignedInt16( offset ) );
                offset += stepSize;
            }

            return ret.ToArray();
        }

        /// <summary> Read a set of input registers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">       Server device address. </param>
        /// <param name="startAddress"> Address of first register to be read. </param>
        /// <param name="len">          Number of registers to be read. </param>
        /// <returns> Array of read registers. </returns>
        [CLSCompliant( false )]
        public ushort[] ReadInputRegisters( byte unitId, ushort startAddress, ushort len )
        {
            if ( len < 1 )
            {
                this.LastError = InterfaceError.ZeroRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Incorrect Read Input Registers argument:; {len}<0";
                return null;
            }

            if ( len > MaxInputRegistersInReadMessage )
            {
                this.LastError = InterfaceError.TooManyRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Incorrect Read Input Registers argument:; {len}>{MaxInputRegistersInReadMessage}";
                return null;
            }

            ushort msg_len = 6;
            this.InitTcpUdpClientMessage();
            this.TxPayload().Add( ( byte ) ModbusCodes.ReadInputRegisters );
            this.TxPayload().AddRange( startAddress.BigEndInt8() );
            this.TxPayload().AddRange( len.BigEndInt8() );
            this.Query( unitId, msg_len );
            if ( this.LastError == InterfaceError.NoError )
            {
                var ret = new List<ushort>( this.ExtractValues() );
                return this.LastError == InterfaceError.NoError ? ret.ToArray() : null;
            }
            else
            {
                return null;
            }
        }

        /// <summary> Write a coil register. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">  Server device address. </param>
        /// <param name="address"> Register address. </param>
        /// <param name="value">   Value to write. </param>
        [CLSCompliant( false )]
        public void WriteSingleCoil( byte unitId, ushort address, bool value )
        {
            ushort msg_len = 6;
            this.InitTcpUdpClientMessage();
            this.TxPayload().Add( ( byte ) ModbusCodes.WriteSingleCoil );
            this.TxPayload().AddRange( address.BigEndInt8() );
            this.TxPayload().AddRange( (( ushort ) (value == true ? 0xFF00 : 0x0)).BigEndInt8() );
            this.Query( unitId, msg_len );
            if ( this.LastError == InterfaceError.NoError )
            {
                if ( this.RxPayload().Any() )
                {
                    int offset = 1;
                    if ( this.RxPayload().Count < offset )
                    {
                        this.LastError = InterfaceError.MessageTooShort;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Insufficient elements ({this.RxPayload().Count}<{offset}) to validate address: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }

                    ushort addr = this.RxPayload().ToArray().BigEndUnsignedInt16( offset );
                    if ( addr != address )
                    {
                        this.LastError = InterfaceError.WrongResponseAddress;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong response address ({addr}<{address}): '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }

                    offset = 3;
                    if ( this.RxPayload().Count < offset )
                    {
                        this.LastError = InterfaceError.MessageTooShort;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Insufficient elements ({this.RxPayload().Count}<{offset}) to validate value: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }

                    ushort regval = this.RxPayload().ToArray().BigEndUnsignedInt16( offset );
                    if ( regval == 0xFF00 && !value )
                    {
                        this.LastError = InterfaceError.WrongResponseValue;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong response value ({regval}=&HFF00 AND NOT {value}): '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }
                }
                else
                {
                    this.LastError = InterfaceError.MessageTooShort;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Empty Rx: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                }
            }
        }

        /// <summary> Write an holding register. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">  Server device address. </param>
        /// <param name="address"> Register address. </param>
        /// <param name="value">   Value to write. </param>
        [CLSCompliant( false )]
        public void WriteSingleRegister( byte unitId, ushort address, ushort value )
        {
            ushort msg_len = 6;
            this.InitTcpUdpClientMessage();
            this.TxPayload().Add( ( byte ) ModbusCodes.WriteSingleRegister );
            this.TxPayload().AddRange( address.BigEndInt8() );
            this.TxPayload().AddRange( value.BigEndInt8() );
            this.Query( unitId, msg_len );
            if ( this.LastError == InterfaceError.NoError )
            {
                if ( this.RxPayload().Any() )
                {
                    int offset = 1;
                    if ( this.RxPayload().Count < offset )
                    {
                        this.LastError = InterfaceError.MessageTooShort;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Insufficient elements ({this.RxPayload().Count}<{offset}) to validate address: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }

                    ushort addr = this.RxPayload().ToArray().BigEndUnsignedInt16( offset );
                    if ( addr != address )
                    {
                        this.LastError = InterfaceError.WrongResponseAddress;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong response address ({addr}<{address}): '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }
                }
                else
                {
                    this.LastError = InterfaceError.MessageTooShort;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Empty Rx: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                }
            }
        }

        /// <summary> Write a set of coil registers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">       Server device address. </param>
        /// <param name="startAddress"> Address of first register to be write. </param>
        /// <param name="values">       Array of values to write. </param>
        [CLSCompliant( false )]
        public void WriteMultipleCoils( byte unitId, ushort startAddress, bool[] values )
        {
            if ( values is null )
            {
                this.LastError = InterfaceError.ZeroRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Empty argument '{nameof( values )}' in Write Multiple Coils";
                return;
            }

            if ( values.Length < 1 )
            {
                this.LastError = InterfaceError.ZeroRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Zero registers requested in Write Multiple Coils argument:; {values.Length}<1";
                return;
            }

            if ( values.Length > MaxCoilsInWriteMessage )
            {
                this.LastError = InterfaceError.TooManyRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Too many registers requested in Write Multiple Coils argument:; {values.Length}>{MaxCoilsInWriteMessage}";
                return;
            }

            byte byte_cnt = ( byte ) (values.Length / 8 + (values.Length % 8 == 0 ? 0 : 1));
            ushort msg_len = ( ushort ) (1 + 6 + byte_cnt);
            var data = new byte[byte_cnt];
            var ba = new BitArray( values );
            ba.CopyTo( data, 0 );
            this.InitTcpUdpClientMessage();
            this.TxPayload().Add( ( byte ) ModbusCodes.WriteMultipleCoils );
            this.TxPayload().AddRange( startAddress.BigEndInt8() );
            this.TxPayload().AddRange( (( ushort ) values.Length).BigEndInt8() );
            this.TxPayload().Add( byte_cnt );
            this.TxPayload().AddRange( data );
            this.Query( unitId, msg_len );
            if ( this.LastError == InterfaceError.NoError )
            {
                if ( this.RxPayload().Any() )
                {
                    int offset = 1;
                    if ( this.RxPayload().Count < offset )
                    {
                        this.LastError = InterfaceError.MessageTooShort;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Insufficient elements ({this.RxPayload().Count}<{offset}) to validate coil address: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }

                    ushort sa = this.RxPayload().ToArray().BigEndUnsignedInt16( offset );
                    if ( sa != startAddress )
                    {
                        this.LastError = InterfaceError.WrongResponseAddress;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong response address ({sa}<{startAddress}): '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }

                    offset = 3;
                    if ( this.RxPayload().Count < offset )
                    {
                        this.LastError = InterfaceError.MessageTooShort;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Insufficient elements ({this.RxPayload().Count}<{offset}) to validate coil: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }

                    ushort nr = this.RxPayload().ToArray().BigEndUnsignedInt16( offset );
                    if ( nr != values.Length )
                    {
                        this.LastError = InterfaceError.WrongResponseRegisters;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong response register ({nr}<{values.Length}): '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }
                }
                else
                {
                    this.LastError = InterfaceError.MessageTooShort;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Empty Rx: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                }
            }
        }

        /// <summary> Writes the registers value. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">       Server device address. </param>
        /// <param name="startAddress"> Address of first register to be write. </param>
        /// <param name="value">        Value to write. </param>
        [CLSCompliant( false )]
        public void WriteRegistersValue( byte unitId, ushort startAddress, float value )
        {
            var bytes = value.BigEndBytes();
            var values = new ushort[] { bytes.BigEndUnsignedShort( 2 ), bytes.BigEndUnsignedShort( 0 ) };
            this.WriteMultipleRegisters( unitId, startAddress, values );
        }

        /// <summary> Writes the registers value. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">       Server device address. </param>
        /// <param name="startAddress"> Address of first register to be write. </param>
        /// <param name="value">        Value to write. </param>
        [CLSCompliant( false )]
        public void WriteRegistersValue( byte unitId, ushort startAddress, ushort value )
        {
            this.WriteMultipleRegisters( unitId, startAddress, new ushort[] { value } );
        }

        /// <summary> Write a set of holding registers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">       Server device address. </param>
        /// <param name="startAddress"> Address of first register to be write. </param>
        /// <param name="values">       Array of values to write. </param>
        [CLSCompliant( false )]
        public void WriteMultipleRegisters( byte unitId, ushort startAddress, ushort[] values )
        {
            if ( values is null )
            {
                this.LastError = InterfaceError.ZeroRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Empty argument '{nameof( values )}' in Write Multiple Registers";
                return;
            }

            if ( values.Length < 1 )
            {
                this.LastError = InterfaceError.ZeroRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Zero registers requested in Write Multiple Registers argument:; {values.Length}<1";
                return;
            }

            if ( values.Length > MaxHoldingRegistersInWriteMessage )
            {
                this.LastError = InterfaceError.TooManyRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Too many registers requested in Write Multiple Registers argument:; {values.Length}>{MaxHoldingRegistersInWriteMessage}";
                return;
            }

            ushort msg_len = ( ushort ) (7 + values.Length * 2);
            this.InitTcpUdpClientMessage();
            this.TxPayload().Add( ( byte ) ModbusCodes.WriteMultipleRegisters );
            this.TxPayload().AddRange( startAddress.BigEndInt8() );
            this.TxPayload().AddRange( (( ushort ) values.Length).BigEndInt8() );
            this.TxPayload().Add( ( byte ) (values.Length * 2) );
            for ( int ii = 0, loopTo = values.Length - 1; ii <= loopTo; ii++ )
                this.TxPayload().AddRange( values[ii].BigEndInt8() );
            this.Query( unitId, msg_len );
            if ( this.LastError == InterfaceError.NoError )
            {
                if ( this.RxPayload().Any() )
                {
                    int offset = 1;
                    if ( this.RxPayload().Count < offset )
                    {
                        this.LastError = InterfaceError.MessageTooShort;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Insufficient elements to validate register address: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }

                    ushort sa = this.RxPayload().ToArray().BigEndUnsignedInt16( offset );
                    if ( sa != startAddress )
                    {
                        this.LastError = InterfaceError.WrongResponseAddress;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong response address ({sa}<{startAddress}): '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }

                    offset = 3;
                    if ( this.RxPayload().Count < offset )
                    {
                        this.LastError = InterfaceError.MessageTooShort;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Insufficient elements to validate register: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }

                    ushort nr = this.RxPayload().ToArray().BigEndUnsignedInt16( offset );
                    if ( nr != values.Length )
                    {
                        this.LastError = InterfaceError.WrongResponseRegisters;
                        this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong response register ({nr}<{values.Length}): '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                        return;
                    }
                }
                else
                {
                    this.LastError = InterfaceError.MessageTooShort;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Empty Rx: '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                }
            }
        }

        /// <summary> Make an AND and OR mask of a single holding register. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">  Server device address. </param>
        /// <param name="address"> Register address. </param>
        /// <param name="andMask"> AND mask. </param>
        /// <param name="orMask">  OR mask. </param>
        [CLSCompliant( false )]
        public void MaskWriteRegister( byte unitId, ushort address, ushort andMask, ushort orMask )
        {
            ushort msg_len = 8;
            this.InitTcpUdpClientMessage();
            this.TxPayload().Add( ( byte ) ModbusCodes.MaskWriteRegister );
            this.TxPayload().AddRange( address.BigEndInt8() );
            this.TxPayload().AddRange( andMask.BigEndInt8() );
            this.TxPayload().AddRange( orMask.BigEndInt8() );
            this.Query( unitId, msg_len );
            if ( this.LastError == InterfaceError.NoError )
            {
                // Check address
                ushort addr = this.RxPayload().ToArray().BigEndUnsignedInt16( 1 );
                if ( address != addr )
                {
                    this.LastError = InterfaceError.WrongResponseAddress;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong response address ({addr}<{address}): '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                    return;
                }
                // Check AND mask
                ushort am = this.RxPayload().ToArray().BigEndUnsignedInt16( 3 );
                if ( andMask != am )
                {
                    this.LastError = InterfaceError.WrongResponseAndMask;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong response AND mask ({am}<{andMask}): '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                    return;
                }
                // Check OR mask
                ushort om = this.RxPayload().ToArray().BigEndUnsignedInt16( 5 );
                if ( orMask != om )
                {
                    this.LastError = InterfaceError.WrongResponseOrMask;
                    this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Wrong response OR mask ({om}<{orMask}): '{this.FormatRxPayload()}'; Time series: {this.RxTimeSeries.FormatTimeSeries()}";
                    return;
                }
            }
        }

        /// <summary> Read and write a set of holding registers in a single shot. </summary>
        /// <remarks> Write is the first operation, than the read operation. </remarks>
        /// <param name="unitId">            Server device address. </param>
        /// <param name="readStartAddress">  Address of first registers to be read. </param>
        /// <param name="readLen">           Number of registers to be read. </param>
        /// <param name="writeStartAddress"> Address of first registers to be write. </param>
        /// <param name="values">            Array of values to be write. </param>
        /// <returns> Array of read registers. </returns>
        [CLSCompliant( false )]
        public ushort[] ReadWriteMultipleRegisters( byte unitId, ushort readStartAddress, ushort readLen, ushort writeStartAddress, ushort[] values )
        {
            if ( values is null )
            {
                this.LastError = InterfaceError.ZeroRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Empty argument '{nameof( values )}' in Read Write Multiple Registers";
                return null;
            }

            if ( readLen < 1 || values.Length < 1 )
            {
                this.LastError = InterfaceError.ZeroRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Zero registers requested in Read Write Multiple Registers argument:; {readLen}<1 or {values.Length}<1";
                return null;
            }

            if ( readLen > MaxHoldingRegistersToReadInReadWriteMessage || values.Length > MaxHoldingRegistersToWriteInReadWriteMessage )
            {
                this.LastError = InterfaceError.TooManyRegistersRequested;
                this.LastErrorDetails = $"{this.InterfaceErrorCaption()}: Too many registers requested in Read Write Multiple Registers argument:; {readLen}>{MaxHoldingRegistersToReadInReadWriteMessage} or {values.Length}>{MaxHoldingRegistersInWriteMessage}";
                return null;
            }

            ushort msg_len = ( ushort ) (11 + values.Length * 2);
            this.InitTcpUdpClientMessage();
            this.TxPayload().Add( ( byte ) ModbusCodes.ReadWriteMultipleRegisters );
            this.TxPayload().AddRange( readStartAddress.BigEndInt8() );
            this.TxPayload().AddRange( readLen.BigEndInt8() );
            this.TxPayload().AddRange( writeStartAddress.BigEndInt8() );
            this.TxPayload().AddRange( (( ushort ) values.Length).BigEndInt8() );
            this.TxPayload().Add( ( byte ) (values.Length * 2) );
            for ( int ii = 0, loopTo = values.Length - 1; ii <= loopTo; ii++ )
                this.TxPayload().AddRange( values[ii].BigEndInt8() );
            this.Query( unitId, msg_len );
            if ( this.LastError == InterfaceError.NoError )
            {
                var ret = new List<ushort>( this.ExtractValues() );
                return this.LastError != InterfaceError.NoError ? null : ret.ToArray();
            }
            else
            {
                return null;
            }
        }

        #endregion

    }

    /// <summary> A time series value. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public struct TimeSeriesValue
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="timespan"> The timespan. </param>
        /// <param name="value">    The value. </param>
        public TimeSeriesValue( TimeSpan timespan, byte? value )
        {
            this.Timespan = timespan;
            this.Value = value;
        }

        /// <summary> Gets or sets the timespan. </summary>
        /// <value> The timespan. </value>
        public TimeSpan Timespan { get; set; }

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public byte? Value { get; set; }

        /// <summary> Format byte value. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="timespanFormat"> The timespan format. </param>
        /// <returns> The formatted byte value. </returns>
        public string FormatByteValue( string timespanFormat )
        {
            return $"({this.Timespan.ToString( timespanFormat )},{(this.Value.HasValue ? this.Value.Value.ToString( "X2" ) : "..")})";
        }

        #region " EQUALS "

        /// <summary> Compares two Time Series Values. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="left">  Specifies the Time Series Value to compare to. </param>
        /// <param name="right"> Specifies the Time Series Value to compare. </param>
        /// <returns> <c>True</c> if the Time Series Values are equal. </returns>
        public new static bool Equals( object left, object right )
        {
            return Equals( ( TimeSeriesValue ) left, ( TimeSeriesValue ) right );
        }

        /// <summary> Compares two Time Series Values. </summary>
        /// <remarks>
        /// The two Time Series Values are the same if they have the same X and Y coordinates.
        /// </remarks>
        /// <param name="left">  Specifies the Time Series Value to compare to. </param>
        /// <param name="right"> Specifies the Time Series Value to compare. </param>
        /// <returns> <c>True</c> if the Time Series Values are equal. </returns>
        public static bool Equals( TimeSeriesValue left, TimeSeriesValue right )
        {
            return left.Equals( right );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( TimeSeriesValue ) obj );
        }

        /// <summary> Compares two Time Series Values. </summary>
        /// <remarks>
        /// The two Time Series Values are the same if they have the same X1 and Y1 coordinates.
        /// </remarks>
        /// <param name="other"> Specifies the other Time Series Value. </param>
        /// <returns> <c>True</c> if the Time Series Values are equal. </returns>
        public bool Equals( TimeSeriesValue other )
        {
            return other.Timespan.Equals( this.Timespan ) && other.Value.Equals( this.Value );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( TimeSeriesValue left, TimeSeriesValue right )
        {
            return left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( TimeSeriesValue left, TimeSeriesValue right )
        {
            return !left.Equals( right );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Timespan.GetHashCode() ^ this.Value.GetHashCode();
        }

        #endregion

    }

    /// <summary> Collection of time series. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-1-12 </para>
    /// </remarks>
    public class TimeSeriesCollection : System.Collections.ObjectModel.Collection<TimeSeriesValue>
    {

        /// <summary> Adds timespan. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="timespan"> The timespan. </param>
        /// <param name="value">    The value. </param>
        public void Add( TimeSpan timespan, byte? value )
        {
            this.Add( new TimeSeriesValue( timespan, value ) );
        }

        /// <summary> Format values. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The formatted values. </returns>
        public string FormatValues()
        {
            var builder = new StringBuilder();
            foreach ( TimeSeriesValue v in this )
            {
                if ( builder.Length > 0 )
                    _ = builder.Append( "," );
                if ( v.Value.HasValue )
                {
                    _ = builder.AppendFormat( "{0:X2}", v.Value );
                }
                else
                {
                    _ = builder.Append( ".." );
                }
            }

            return builder.ToString();
        }

        /// <summary> Format time series. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> The formatted time series. </returns>
        public string FormatTimeSeries()
        {
            var builder = new StringBuilder();
            foreach ( TimeSeriesValue v in this )
            {
                if ( builder.Length > -0 )
                    _ = builder.Append( "," );
                _ = builder.Append( v.FormatByteValue( @"s\.ffff" ) );
            }

            return builder.ToString();
        }
    }
}
