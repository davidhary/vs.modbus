﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;

namespace isr.Modbus
{

    /// <summary> Modbus Client TCP Class. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    public class ModbusClientTCP : ModbusClientBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="remoteHost"> Remote hostname or IP address. </param>
        /// <param name="port">       Remote host Modbus TCP listening port. </param>
        public ModbusClientTCP( string remoteHost, int port ) : base()
        {
            // Set device states
            this.ConnectionType = ConnectionType.TcpIP;
            // Set socket client
            this._RemoteHost = remoteHost;
            this._Port = port;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {
                        if ( this._NetworkStream is object )
                        {
                            this._NetworkStream.Dispose();
                            this._NetworkStream = null;
                        }

                        if ( this._TcpClient is object )
                        {
                            this._TcpClient.Dispose();
                            this._TcpClient = null;
                        }
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " CONNECT/DISCONNECT FUNCTIONS "

        /// <summary>
        /// Remote hostname or IP address
        /// </summary>
        private readonly string _RemoteHost;

        /// <summary>
        /// Remote host Modbus TCP listening port
        /// </summary>
        private readonly int _Port;

        /// <summary>
        /// TCP Client
        /// </summary>
        private TcpClient _TcpClient;

        /// <summary>
        /// Network stream
        /// </summary>
        private NetworkStream _NetworkStream;

        /// <summary> Connect function. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void Connect()
        {
            if ( this._TcpClient is null )
            {
                this._TcpClient = new TcpClient();
            }

            this._TcpClient.Connect( this._RemoteHost, this._Port );
            if ( this._TcpClient.Connected )
            {
                this._NetworkStream = this._TcpClient.GetStream();
                this.Connected = true;
            }
        }

        /// <summary> Disconnect function. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void Disconnect()
        {
            this._NetworkStream.Close();
            this._TcpClient.Close();
            this._TcpClient = null;
            this.Connected = false;
        }

        #endregion

        #region " PROTOCOL FUNCTIONS "

        /// <summary> Send transmission buffer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="buffer"> The buffer. </param>
        protected override void Send( IEnumerable<byte> buffer )
        {
            this.TxBuffer().AddRange( buffer );
            this._NetworkStream.Write( this.TxBuffer().ToArray(), 0, this.TxBuffer().Count );
        }

        /// <summary> Gets the status of device data available. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> <c>true</c> if a new device data is available; otherwise <c>false</c> </returns>
        public override bool DeviceDataAvailable()
        {
            return this._NetworkStream.DataAvailable;
        }

        /// <summary> Reads a byte from the device. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> Byte read or nothing if no data are present. </returns>
        protected override byte? ReadByte()
        {

            // Check if there are available bytes
            while ( this.DeviceDataAvailable() )
            {
                int value = this._NetworkStream.ReadByte();
                if ( value >= 0 )
                {
                    this.RxDeviceBuffer().Add( ( byte ) (value & 0xFF) );
                }
                else
                {
                    break;
                }

                Core.ApplianceBase.DoEvents();
            }

            if ( this.RxDeviceBuffer().Any() )
            {
                // There are available bytes in temporary rx buffer, read the first and delete it
                byte ret = this.RxDeviceBuffer()[0];
                this.RxDeviceBuffer().RemoveAt( 0 );
                return ret;
            }
            else
            {
                return new byte?();
            }
        }

        /// <summary> Flushes device input and output buffers and cached input buffers. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected override void FlushDeviceBuffers()
        {
            base.FlushDeviceBuffers();
            if ( this._NetworkStream.DataAvailable )
                this._NetworkStream.Flush();
        }

        #endregion

    }
}