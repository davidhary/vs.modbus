using System.ComponentModel;

namespace isr.Modbus.My
{

    /// <summary> Values that represent project trace event identifiers. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    public enum ProjectTraceEventId
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not specified" )]
        None,

        /// <summary>   An enum constant representing the core option. </summary>
        [Description( "Modbus Core" )]
        Core = isr.Core.ProjectTraceEventId.Modbus,

        /// <summary>   An enum constant representing the watlow library option. </summary>
        [Description( "Watlow Library" )]
        WatlowLibrary = Core + 0x1,

        /// <summary>   An enum constant representing the watlow console option. </summary>
        [Description( "Watlow Console" )]
        WatlowConsole = Core + 0xF
    }
}
