## ISR Modbus Core<sub>&trade;</sub>: Modbus Core Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [About Modbus](#About-Modbus)

### Revision History [](#){name=Revision-History}

*1.2.7637 2020-11-27*  
Converted to C#.

*1.1.6667 2018-04-03*  
2018 release.

*1.0.6222 2017-01-13*  
Uses nullable values when reading the device. Checks
for buffered values before checking for completion of frame. Updates
data available for the Serial RTU to allow time for the next character.
Updates protocol to add Data Available to the check of completion.
Updates t=protocol to return nullable values when reading the buffer.
Flushes device buffers before each query. Uses a timeout stop watch to
delay action for device settling time.

*1.0.6221 2017-01-12*  
Uses payload validation algorithm to detect end of
received frame. Adds a time series collection to store the input values.
Adds a function to wait for available data to use before reading. Split
read function to a wait for data followed by reading characters with
inter-frame timeout and inter-character polling wait. Traces buffer
information of failure and success. Serial client: Adds a frame wait
before reading each byte. Updates assembly product name to 2017. Adds
payload buffers.

*1.0.6047 2016-07-22*  
Changes code for reading byte from the serial port and
determining the end of record based. Uses time span type for timing.

*1.0.5992 2016-05-28*  
Clears buffers upon completion of transactions.

*1.0.5969 2016-05-07*  
Created.

\(C\) 2016 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Typed Units Libraries](https://bitbucket.org/davidhary/arebis.typedunits)  
[MODBUS Libraries](https://bitbucket.org/davidhary/vs.modbus)  
[Free .Net Modbus](https://code.google.com/p/free-dotnet-modbus)

### About Modbus [](#){name=About-Modbus}
For additional information about the MODBUS protocol, see:  
[Simply Modbus](http://www.simplymodbus.ca/FAQ.htm)  
[BB-Electronics FAQ](http://www.bb-elec.com/Learning-Center/All-White-Papers/Modbus/The-Answer-to-the-14-Most-Frequently-Asked-Modbus.aspx)
