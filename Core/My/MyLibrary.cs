﻿
namespace isr.Modbus.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) ProjectTraceEventId.Core;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Modbus Core Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Modbus Core Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Modbus.Core";
    }
}