using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace isr.Modbus
{

    /// <summary> UDP data class. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    internal class UDPData : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="udpClient">      UDP client. </param>
        /// <param name="receiveData">    Received data buffer. </param>
        /// <param name="remoteEndPoint"> Remote endpoint. </param>
        public UDPData( UdpClient udpClient, byte[] receiveData, IPEndPoint remoteEndPoint ) : base()
        {
            this._Client = udpClient;
            this._RemoteEndPoint = remoteEndPoint;
            this._MemoryStream = new MemoryStream( receiveData );
        }

        #region " IDisposable Support "

        /// <summary> True to disposed value. </summary>
        private bool _DisposedValue; // To detect redundant calls

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this._DisposedValue )
                {
                    if ( disposing )
                    {
                        // TODO: dispose managed state (managed objects).
                        if ( this._MemoryStream is object )
                        {
                            this._MemoryStream.Dispose();
                            this._MemoryStream = null;
                        }
                    }
                    // TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                    // TODO: set large fields to null.
                }
            }
            finally
            {
                this._DisposedValue = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
        }
        #endregion

        #endregion

        #region " GLOBAL VARIABLES "

        /// <summary>
        /// Input stream
        /// </summary>
        private MemoryStream _MemoryStream;

        /// <summary>
        /// UDP Client
        /// </summary>
        private readonly UdpClient _Client;

        /// <summary>
        /// Remote endpoint
        /// </summary>
        private readonly IPEndPoint _RemoteEndPoint;

        #endregion

        #region " PARAMETERS "

        /// <summary> Get stream length. </summary>
        /// <value> The length. </value>
        public long Length => this._MemoryStream.Length;

        #endregion

        #region " CLASS FUNCTIONS "

        /// <summary> Read a byte from input stream. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> Read byte or <c>-1</c> if there are any bytes. </returns>
        public int ReadByte()
        {
            return this._MemoryStream.ReadByte();
        }

        /// <summary> Send an response buffer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="buffer"> Buffer to send. </param>
        /// <param name="offset"> Buffer offset. </param>
        /// <param name="size">   Data length. </param>
        public void WriteResponse( byte[] buffer, int offset, int size )
        {
            var tmp_buffer = new byte[size];
            Buffer.BlockCopy( buffer, offset, tmp_buffer, 0, size );
            _ = this._Client.Send( tmp_buffer, size, this._RemoteEndPoint );
        }

        /// <summary> Close input stream. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public void Close()
        {
            if ( this._MemoryStream is object )
            {
                this._MemoryStream.Close();
            }
        }

        #endregion
    }
}
