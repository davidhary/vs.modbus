using System;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Modbus
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> LRC Class. </summary>
    /// <remarks>
    /// Each Modbus ASCII message is terminated with an error checking byte called an LRC or
    /// Longitudinal Redundancy Check. <para>
    /// (c) 2013 Simone Assunti. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07. Source: https://code.google.com/p/free-dotnet- modbus/. </para>
    /// </remarks>
    public sealed class LRC
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        private LRC()
        {
        }

        /// <summary> Compute the Longitudinal redundancy check (LRC). </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values"> Data values. </param>
        /// <param name="offset"> Offset. </param>
        /// <param name="length"> Data length. </param>
        /// <returns> Computed LRC. </returns>
        public static byte Evaluate( byte[] values, int offset, int length )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( offset < 0 )
                throw new ArgumentOutOfRangeException( nameof( offset ), offset, "value out of range" );
            if ( offset + length > values.Length )
                throw new ArgumentOutOfRangeException( $"{nameof( offset )}({offset})+{nameof( length )}({length})>length of {nameof( values )}({values.Length})" );
            byte result = 0;
            while ( length > 0 )
            {
                result += values[offset];
                offset += 1;
                length -= 1;
            }
            // For ii As Integer = 0 To length - 1 : result += values(ii + offset) : Next ii
            return ( byte ) -( sbyte ) result;
        }
    }
}
