using System;
using System.Net;

namespace isr.Modbus
{

    /// <summary> Event arguments  for remote endpoint connection. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07 </para>
    /// </remarks>
    public sealed class ClientConnectedEventArgs : EventArgs
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> Remote EndPoint. </param>
        public ClientConnectedEventArgs( IPEndPoint value )
        {
            this.RemoteEndPoint = value;
        }

        /// <summary> Remote EndPoint. </summary>
        /// <value> The remote end point. </value>
        public IPEndPoint RemoteEndPoint { get; private set; }
    }
}
