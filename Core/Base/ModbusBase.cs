using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.Core.EnumExtensions;

namespace isr.Modbus
{

    /// <summary> The modbus base. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    public abstract class ModbusBase : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected ModbusBase() : base()
        {
            this.RxTimeout = this._DefaultReceiveTimeout;
            this._QueryStopWatch = new Stopwatch();
            this.SettlingTimeStopwatch = TimeoutStopwatch.StartNew( this._DefaultDeviceRefractoryPeriod );
            this._ContinuousFailureDictionary = new Dictionary<InterfaceError, int>();
            this._ErrorCountDictionary = new Dictionary<InterfaceError, long>();
        }

        #region " IDisposable Support "

        /// <summary> Gets or sets the disposed. </summary>
        /// <value> The disposed. </value>
        protected bool Disposed { get; set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {
                    }

                    this.SettlingTimeStopwatch = null;
                }
            }
            finally
            {
                this.Disposed = true;
            }
        }

        // TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
        // Protected Overrides Sub Finalize()
        // ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        // Dispose(False)
        // MyBase.Finalize()
        // End Sub

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            // uncomment the following line if Finalize() is overridden above.
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        #region " CONSTANTS "

        /// <summary>
        /// Modbus protocol identifier (only TCP and UDP)
        /// </summary>
        [CLSCompliant( false )]
        protected const ushort ProtocolId = 0x0;

        /// <summary>
        /// Default rx timeout in milliseconds
        /// </summary>
        private TimeSpan _DefaultReceiveTimeout = TimeSpan.FromMilliseconds( 100d );

        /// <summary> The default device refractory period. </summary>
        private TimeSpan _DefaultDeviceRefractoryPeriod = TimeSpan.FromMilliseconds( 100d );

        /// <summary>
        /// Length in bytes of MBAP header
        /// </summary>
        protected const int MbapHeaderLength = 7;

        /// <summary>
        /// Start frame character (only Modbus serial ASCII)
        /// </summary>
        protected const char AsciiStartFrameCharacter = ':';

        /// <summary>
        /// End frame first character (only Modbus serial ASCII)
        /// </summary>
        protected const char AsciiEndFrameFirstCharacter = '\r';

        /// <summary>
        /// End frame second character (only Modbus serial ASCII)
        /// </summary>
        protected const char AsciiEndFrameSecondCharacter = '\n';

        /// <summary>
        /// Max number of coil registers that can be read
        /// </summary>
        [CLSCompliant( false )]
        public const ushort MaxCoilsInReadMessage = 2000;

        /// <summary>
        /// Max number of discrete inputs registers that can be read
        /// </summary>
        [CLSCompliant( false )]
        public const ushort MaxDiscreteInputsInReadMessage = 2000;

        /// <summary>
        /// Max number of holding registers that can be read
        /// </summary>
        [CLSCompliant( false )]
        public const ushort MaxHoldingRegistersInReadMessage = 125;

        /// <summary>
        /// Max number of input registers that can be read
        /// </summary>
        [CLSCompliant( false )]
        public const ushort MaxInputRegistersInReadMessage = 125;

        /// <summary>
        /// Max number of coil registers that can be written
        /// </summary>
        [CLSCompliant( false )]
        public const ushort MaxCoilsInWriteMessage = 1968;

        /// <summary>
        /// Max number of holding registers that can be written
        /// </summary>
        [CLSCompliant( false )]
        public const ushort MaxHoldingRegistersInWriteMessage = 123;

        /// <summary>
        /// Max number of holding registers that can be read in a read/write message
        /// </summary>
        [CLSCompliant( false )]
        public const ushort MaxHoldingRegistersToReadInReadWriteMessage = 125;

        /// <summary>
        /// Max number of holding registers that can be written in a read/write message
        /// </summary>
        [CLSCompliant( false )]
        public const ushort MaxHoldingRegistersToWriteInReadWriteMessage = 121;

        #endregion

        #region " GLOBAL VARIABLES "

        /// <summary> Gets or sets the type of the connection. </summary>
        /// <value> The type of the connection. </value>
        protected ConnectionType ConnectionType { get; set; }

        /// <summary> Gets or sets the type of the device. </summary>
        /// <value> The type of the device. </value>
        protected DeviceType DeviceType { get; set; }

        /// <summary> Gets or sets the delay between two Modbus serial RTU frame. </summary>
        /// <value> The inter frame delay. </value>
        protected TimeSpan InterFrameDelay { get; set; }

        /// <summary> Gets or sets the delay between two Modbus serial RTU characters. </summary>
        /// <value> The inter character delay. </value>
        protected TimeSpan InterCharacterDelay { get; set; }

        /// <summary>
        /// Gets or sets the device settling time after completing a query before a new query can be
        /// executed.
        /// </summary>
        /// <value> The device settling time period. </value>
        public TimeSpan DeviceSettlingTime
        {
            get => this.SettlingTimeStopwatch.Duration;

            set => this.SettlingTimeStopwatch.Duration = value;
        }

        /// <summary> Gets or sets the settling time stopwatch. </summary>
        /// <value> The settling time stopwatch. </value>
        protected TimeoutStopwatch SettlingTimeStopwatch { get; set; }

        #endregion

        #region " PARAMETERS "

        /// <summary> Get or set reception timeout. </summary>
        /// <value> The receive timeout. </value>
        public TimeSpan RxTimeout { get; set; }

        /// <summary> Gets or sets the poll interval. </summary>
        /// <value> The poll interval. </value>
        public TimeSpan PollInterval { get; set; }

        private readonly Dictionary<InterfaceError, long> _ErrorCountDictionary;

        /// <summary> Error count. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <param name="interfaceError"> The interface error. </param>
        /// <returns> A Long. </returns>
        public long ErrorCount( InterfaceError interfaceError )
        {
            return this._ErrorCountDictionary.ContainsKey( interfaceError ) ? this._ErrorCountDictionary[interfaceError] : 0L;
        }

        /// <summary> Last error count. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <returns> An Integer. </returns>
        public long LastErrorCount()
        {
            return this._ErrorCountDictionary[this.LastError];
        }

        private readonly Dictionary<InterfaceError, int> _ContinuousFailureDictionary;

        /// <summary> Continuous failure count. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <param name="interfaceError"> The interface error. </param>
        /// <returns> An Integer. </returns>
        public int ContinuousFailureCount( InterfaceError interfaceError )
        {
            return this._ContinuousFailureDictionary.ContainsKey( interfaceError ) ? this._ContinuousFailureDictionary[interfaceError] : 0;
        }

        /// <summary> Last error continuous failure count. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <returns> An Integer. </returns>
        public int LastErrorContinuousFailureCount()
        {
            return this.ContinuousFailureCount( this.LastError );
        }

        /// <summary> Gets or sets the number of continuous errors. </summary>
        /// <value> The number of continuous errors. </value>
        public int ContinuousErrorCount { get; protected set; }

        /// <summary>
        /// Modbus errors
        /// </summary>
        private InterfaceError _LastError;

        /// <summary> Get last error code. </summary>
        /// <value> The last error. </value>
        public InterfaceError LastError
        {
            get => this._LastError;

            protected set {
                if ( InterfaceError.NoError == value )
                {
                    this.ContinuousErrorCount = 0;
                    this._ContinuousFailureDictionary.Clear();
                }
                else
                {
                    this.ContinuousErrorCount += 1;
                    if ( value == this.LastError )
                    {
                        if ( this._ContinuousFailureDictionary.ContainsKey( value ) )
                        {
                            this._ContinuousFailureDictionary[value] += 1;
                        }
                        else
                        {
                            this._ContinuousFailureDictionary.Add( value, 0 );
                        }
                    }
                }

                if ( this._ErrorCountDictionary.ContainsKey( value ) )
                {
                    this._ErrorCountDictionary[value] += 1L;
                }
                else
                {
                    this._ErrorCountDictionary.Add( value, 1L );
                }

                this._LastError = value;
            }
        }

        /// <summary> Interface error caption. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <param name="interfaceError"> The interface error. </param>
        /// <returns> A String. </returns>
        public string InterfaceErrorCaption( InterfaceError interfaceError )
        {
            int cfa = this.ContinuousFailureCount( interfaceError );
            return InterfaceError.NoError == interfaceError ? string.Empty : 1 < cfa ? $"{interfaceError} #{cfa}" : interfaceError.ToString();
        }

        /// <summary> Interface error caption. </summary>
        /// <remarks> David, 2020-11-25. </remarks>
        /// <returns> A String. </returns>
        public string InterfaceErrorCaption()
        {
            return this.InterfaceErrorCaption( this.LastError );
        }

        /// <summary> Builds compound interface error. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String. </returns>
        public static string BuildCompoundInterfaceError( InterfaceError value )
        {
            return $"{( int ) value},{value.Description()}";
        }

        /// <summary> Gets the compound interface error. </summary>
        /// <value> The compound interface error. </value>
        public string CompoundInterfaceError => BuildCompoundInterfaceError( this.LastError );

        /// <summary> Get last error details. </summary>
        /// <value> The last error details. </value>
        public string LastErrorDetails { get; protected set; }

        /// <summary> The query stop watch. </summary>
        private Stopwatch _QueryStopWatch;

        /// <summary> Gets the elapsed time of the query. </summary>
        /// <value> The transaction elapsed time. </value>
        public TimeSpan QueryElapsedTime => this._QueryStopWatch.Elapsed;

        /// <summary> Executes the transaction started action. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected void OnQueryStarted()
        {

            // start counting including the refractory period
            this._QueryStopWatch = Stopwatch.StartNew();

            // wait for the refractory period to complete
            this.SettlingTimeStopwatch.Wait();
        }

        /// <summary> Executes the query finished action. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected void OnQueryFinished()
        {
            this._QueryStopWatch.Stop();

            // start a new refractory period
            this.SettlingTimeStopwatch.Restart();
        }

        #endregion

    }
}
