using System;

namespace isr.Modbus
{

    /// <summary> Data store class. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    public sealed class DataStore
    {

        #region " CONSTRUCTORS "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">                Device ID. </param>
        /// <param name="discreteInputsCount">   Input registers (read-only - 1 bit) </param>
        /// <param name="coilsCount">            Coils registers (read/write - 1 bit) </param>
        /// <param name="inputRegistersCount">   Input registers (read-only - 16 bit) </param>
        /// <param name="holdingRegistersCount"> Holding registers (read/write - 16 bit) </param>
        public DataStore( byte unitId, int discreteInputsCount, int coilsCount, int inputRegistersCount, int holdingRegistersCount ) : base()
        {

            // Set device ID
            this.UnitId = unitId;
            discreteInputsCount = discreteInputsCount < 0 ? 0 : discreteInputsCount > _MAX_ELEMENTS ? _MAX_ELEMENTS : discreteInputsCount;
            this._DiscreteInputs = new bool[discreteInputsCount];
            coilsCount = coilsCount < 0 ? 0 : coilsCount > _MAX_ELEMENTS ? _MAX_ELEMENTS : coilsCount;
            this._Coils = new bool[coilsCount];
            inputRegistersCount = inputRegistersCount < 0 ? 0 : inputRegistersCount > _MAX_ELEMENTS ? _MAX_ELEMENTS : inputRegistersCount;
            this._InputRegisters = new ushort[inputRegistersCount];
            holdingRegistersCount = holdingRegistersCount < 0 ? 0 : holdingRegistersCount > _MAX_ELEMENTS ? _MAX_ELEMENTS : holdingRegistersCount;
            this._HoldingRegisters = new ushort[holdingRegistersCount];
        }

        /// <summary> Constructor. </summary>
        /// <remarks> The database if initialized at the maximum capacity allowed. </remarks>
        /// <param name="unitId"> Device ID. </param>
        public DataStore( byte unitId ) : this( unitId, _MAX_ELEMENTS, _MAX_ELEMENTS, _MAX_ELEMENTS, _MAX_ELEMENTS )
        {
        }

        #endregion

        #region " CONSTANTS "

        /// <summary>
        /// Max DB elements
        /// </summary>
        private const int _MAX_ELEMENTS = 65536;

        #endregion

        #region " MODBUS DATABASE "

        /// <summary>
        /// Discrete Inputs - Read Only - 1 bit
        /// </summary>
        private readonly bool[] _DiscreteInputs;

        /// <summary> Discrete Input registers (read-only - 1 bit) </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> A Boolean() </returns>
        public bool[] DiscreteInputs()
        {
            return this._DiscreteInputs;
        }

        /// <summary>
        /// Coils - Read/Write - 1 bit
        /// </summary>
        private readonly bool[] _Coils;

        /// <summary> Coils registers (read/write - 1 bit) </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> A Boolean() </returns>
        public bool[] Coils()
        {
            return this._Coils;
        }

        /// <summary>
        /// Input registers - Read Only - 16 bit
        /// </summary>
        private readonly ushort[] _InputRegisters;

        /// <summary> Input registers (read-only - 16 bit) </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> An UShort() </returns>
        [CLSCompliant( false )]
        public ushort[] InputRegisters()
        {
            return this._InputRegisters;
        }

        /// <summary>
        /// Holding registers - Read/Write - 16 bit
        /// </summary>
        private readonly ushort[] _HoldingRegisters;

        /// <summary> Holding registers (read/write - 16 bit) </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> An UShort() </returns>
        [CLSCompliant( false )]
        public ushort[] HoldingRegisters()
        {
            return this._HoldingRegisters;
        }

        /// <summary> Device ID. </summary>
        /// <value> The identifier of the unit. </value>
        public byte UnitId { get; private set; }

        #endregion

    }
}
