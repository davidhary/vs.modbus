using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;

using isr.Modbus.SerialPortExtensions;

namespace isr.Modbus
{

    /// <summary> Serial Modbus Server Class. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    public sealed class ModbusServerSerial : ModbusServerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="dataStore"> Modbus database. </param>
        /// <param name="type">      Type of serial modbus protocol (RTU or ASCII) </param>
        /// <param name="port">      Serial port name. </param>
        /// <param name="baudRate">  Baud rate. </param>
        /// <param name="dataBits">  Data bits. </param>
        /// <param name="parity">    Parity. </param>
        /// <param name="stopBits">  Stop bits. </param>
        /// <param name="handshake"> Control flux. </param>
        public ModbusServerSerial( DataStore[] dataStore, ModbusSerialType type, string port, int baudRate, int dataBits, Parity parity, StopBits stopBits, Handshake handshake ) : base( dataStore )
        {
            // Set modbus serial protocol type
            switch ( type )
            {
                case ModbusSerialType.ASCII:
                    {
                        this.ConnectionType = ConnectionType.SerialAscii;
                        break;
                    }

                case ModbusSerialType.RTU:
                    {
                        this.ConnectionType = ConnectionType.SerialRTU;
                        break;
                    }
            }

            // Set serial port instance
            this.InitPort( port, baudRate, dataBits, parity, stopBits );
            this._SerialPort.Handshake = handshake;

            // Get inter frame delay
            this.InterFrameDelay = InterFrameDelayTimespan( this._SerialPort );

            // Get inter char delay
            this.InterCharacterDelay = InterCharacterDelayTimespan( this._SerialPort );
        }

        /// <summary> Initializes the port. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="port">     Serial port name. </param>
        /// <param name="baudRate"> Baud rate. </param>
        /// <param name="dataBits"> Data bits. </param>
        /// <param name="parity">   Parity. </param>
        /// <param name="stopBits"> Stop bits. </param>
        private void InitPort( string port, int baudRate, int dataBits, Parity parity, StopBits stopBits )
        {
            this._SerialPort = new SerialPort( port, baudRate, parity, dataBits, stopBits );
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {
                        if ( this._SerialPort is object )
                        {
                            this._SerialPort.Dispose();
                            this._SerialPort = null;
                        }
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }


        #endregion

        #region " HELPERS "

        /// <summary> Get delay time between two modbus RTU frame in milliseconds. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> Serial Port. </param>
        /// <returns> Calculated delay (milliseconds) </returns>
        public static TimeSpan InterFrameDelayTimespan( SerialPort value )
        {
            return TimeSpan.FromTicks( ( long ) (3.5d * value.CharacterTimespan().Ticks) );
        }

        /// <summary> Inter character delay timespan. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> Serial Port. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan InterCharacterDelayTimespan( SerialPort value )
        {
            return TimeSpan.FromTicks( ( long ) (1.5d * value.CharacterTimespan().Ticks) );
        }

        /// <summary> Get delay time between two modbus RTU frame in milliseconds. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> Serial Port. </param>
        /// <returns> Calculated delay (milliseconds) </returns>
        [Obsolete( "Use InterFrameDelayTimespan()" )]
        public static int InterFrameDelayMilliseconds( SerialPort value )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            int result;
            if ( value.BaudRate > 19200 )
            {
                result = 2; // Fixed value = 1.75ms up rounded
            }
            else
            {
                int nbits = 1 + value.DataBits;
                nbits += value.Parity == Parity.None ? 0 : 1;
                switch ( value.StopBits )
                {
                    case StopBits.One:
                        {
                            nbits += 1;
                            break;
                        }

                    case StopBits.OnePointFive:
                    case StopBits.Two: // Ceiling
                        {
                            nbits += 2;
                            break;
                        }
                }

                result = Convert.ToInt32( Math.Ceiling( 1d / (value.BaudRate / (nbits * 3.5d) / 1000d) ) );
            }

            return result;
        }

        /// <summary>
        /// Get max delay time in milliseconds between received chars in modbus RTU transmission.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> Serial Port. </param>
        /// <returns> Calculated delay (milliseconds) </returns>
        [Obsolete( "Use InterCharacterDelayTimespan()" )]
        public static int InterCharacterDelayMilliseconds( SerialPort value )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            int result;
            if ( value.BaudRate > 19200 )
            {
                result = 1; // Fixed value = 0.75 ms up rounded
            }
            else
            {
                int nbits = 1 + value.DataBits;
                nbits += value.Parity == Parity.None ? 0 : 1;
                switch ( value.StopBits )
                {
                    case StopBits.One:
                        {
                            nbits += 1;
                            break;
                        }

                    case StopBits.OnePointFive:
                    case StopBits.Two: // Ceiling
                        {
                            nbits += 2;
                            break;
                        }
                }

                result = Convert.ToInt32( Math.Ceiling( 1d / (value.BaudRate / (nbits * 1.5d) / 1000d) ) );
            }

            return result;
        }


        #endregion

        #region " INSTANCES "

        /// <summary>
        /// Serial Port instance
        /// </summary>
        private SerialPort _SerialPort;

        #endregion

        #region " THREAD FOR GUEST INCOMING CALLS "

        /// <summary> Thread for guest incoming calls. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void GuestRequests()
        {
            try
            {
                var send_buffer = new List<byte>();
                var receive_buffer = new List<byte>();
                while ( this.Running )
                {
                    this.IncomingMessagePolling( send_buffer, receive_buffer, this._SerialPort );
                    Core.ApplianceBase.Delay( 1d );
                }
            }
            catch
            {
            }
        }

        #endregion

        #region " START AND STOP FUNCTIONS "

        /// <summary> Start listening function. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void StartListen()
        {
            this._SerialPort.Open();
            if ( this._SerialPort.IsOpen )
            {
                this._SerialPort.DiscardInBuffer();
                this._SerialPort.DiscardOutBuffer();
                if ( this.GuestRequestThread is null )
                {
                    this.Running = true;
                    this.GuestRequestThread = new Thread( this.GuestRequests );
                    this.GuestRequestThread.Start();
                }
            }
        }

        /// <summary> Stop listening function. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void StopListen()
        {
            if ( this.GuestRequestThread is object )
            {
                this.Running = false;
                this.GuestRequestThread.Join();
                this.GuestRequestThread = null;
            }

            if ( this._SerialPort is object )
            {
                if ( this._SerialPort.IsOpen )
                {
                    this._SerialPort.Close();
                }
            }
        }

        #endregion

    }
}
