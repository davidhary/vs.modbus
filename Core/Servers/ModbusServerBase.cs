using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using isr.Core.BitConverterExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Modbus
{

    /// <summary> Base abstract class for Modbus Server instances. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    public abstract class ModbusServerBase : ModbusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="dataStore"> Database Modbus. </param>
        protected ModbusServerBase( DataStore[] dataStore ) : base()
        {
            // Device status
            this.DeviceType = DeviceType.Server;

            // Assign modbus database
            this._ModbusDataStore = dataStore;

            // Initialize timer
            this._InterCharacterTimeoutStopwatch = new Stopwatch();
        }

        #endregion

        #region " GLOBAL VARIABLES "

        /// <summary> Execution status of thread that manage calls. Was volatile. </summary>
        /// <value> The running. </value>
        protected bool Running { get; set; } = false;

        #endregion

        #region " INSTANCES "

        /// <summary> Incoming calls management thread. </summary>
        /// <value> The guest request thread. </value>
        protected Thread GuestRequestThread { get; set; }

        /// <summary>
        /// Inter-character timeout timer
        /// </summary>
        private readonly Stopwatch _InterCharacterTimeoutStopwatch;

        #endregion

        #region " PARAMETERS "

        /// <summary>
        /// Database Modbus
        /// </summary>
        private readonly DataStore[] _ModbusDataStore;

        /// <summary> Database Modbus. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> A DataStore() </returns>
        public DataStore[] ModbusDataStore()
        {
            return this._ModbusDataStore;
        }

        #endregion

        #region " INCOMING CALLS MANAGEMENT THREAD "

        /// <summary> Incoming calls management thread. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected abstract void GuestRequests();

        #endregion

        #region " START AND STOP LISTENING MESSAGES "

        /// <summary> Function Prototype for start listening messages. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public abstract void StartListen();

        /// <summary> Function Prototype for stop listening messages. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public abstract void StopListen();

        #endregion

        #region " PROTOCOL FUNCTIONS "

        /// <summary>
        /// Check if all registers from (starting_address + quantity_of_registers) are present in device
        /// database.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">          Unit ID. </param>
        /// <param name="table">           database modbus table. </param>
        /// <param name="startingAddress"> Starting address (offset) in database. </param>
        /// <param name="registersCount">  Number of registers to read/write. </param>
        /// <returns> True if register are present, otherwise False. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private bool IsAllRegistersPresent( byte unitId, ModbusDatabaseTable table, ushort startingAddress, ushort registersCount )
        {
            bool ret = true;
            switch ( table )
            {
                case ModbusDatabaseTable.DiscreteInputsRegisters:
                    {
                        try
                        {
                            _ = this._ModbusDataStore.Single( x => x.UnitId == unitId ).DiscreteInputs().ToList().GetRange( startingAddress, registersCount );
                        }
                        catch
                        {
                            ret = false;
                        }

                        break;
                    }

                case ModbusDatabaseTable.CoilsRegisters:
                    {
                        try
                        {
                            _ = this._ModbusDataStore.Single( x => x.UnitId == unitId ).Coils().ToList().GetRange( startingAddress, registersCount );
                        }
                        catch
                        {
                            ret = false;
                        }

                        break;
                    }

                case ModbusDatabaseTable.InputRegisters:
                    {
                        try
                        {
                            _ = this._ModbusDataStore.Single( x => x.UnitId == unitId ).InputRegisters().ToList().GetRange( startingAddress, registersCount );
                        }
                        catch
                        {
                            ret = false;
                        }

                        break;
                    }

                case ModbusDatabaseTable.HoldingRegisters:
                    {
                        try
                        {
                            _ = this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters().ToList().GetRange( startingAddress, registersCount );
                        }
                        catch
                        {
                            ret = false;
                        }

                        break;
                    }

                default:
                    {
                        ret = false;
                        break;
                    }
            }

            return ret;
        }

        /// <summary> Build an exception message. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sendBuffer">     Send buffer. </param>
        /// <param name="code">           Modbus operation code. </param>
        /// <param name="interfaceError"> interface Error code. </param>
        private static void BuildExceptionMessage( List<byte> sendBuffer, ModbusCodes code, InterfaceError interfaceError )
        {
            byte exception_code = 0;
            sendBuffer.Clear();
            switch ( interfaceError )
            {
                case InterfaceError.ExceptionIllegalFunction:
                    {
                        exception_code = 1;
                        break;
                    }

                case InterfaceError.ExceptionIllegalDataAddress:
                    {
                        exception_code = 2;
                        break;
                    }

                case InterfaceError.ExceptionIllegalDataValue:
                    {
                        exception_code = 3;
                        break;
                    }

                case InterfaceError.ExceptionServerDeviceFailure:
                    {
                        exception_code = 4;
                        break;
                    }
            }
            // Add exception identifier
            sendBuffer.Add( ( byte ) (0x80 + ( byte ) code) );
            // Add exception code
            sendBuffer.Add( exception_code );
        }

        /// <summary> Read a byte from a specified flux. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="flux"> Flux of reading bytes. </param>
        /// <returns> Read byte or -1 if no data is present. </returns>
        private int ReadByte( object flux )
        {
            int ret = -1;
            switch ( this.ConnectionType )
            {
                case ConnectionType.UdpIP:
                    {
                        UDPData udpd = ( UDPData ) flux;
                        if ( udpd.Length > 0L )
                        {
                            ret = udpd.ReadByte();
                        }

                        break;
                    }

                case ConnectionType.TcpIP:
                    {
                        NetworkStream ns = ( NetworkStream ) flux;
                        if ( ns.DataAvailable )
                        {
                            ret = ns.ReadByte();
                        }

                        break;
                    }

                case ConnectionType.SerialAscii:
                case ConnectionType.SerialRTU:
                    {
                        SerialPort sp = ( SerialPort ) flux;
                        ret = -1;
                        // Start timeout counter when reading the first character
                        if ( !this._InterCharacterTimeoutStopwatch.IsRunning )
                            this._InterCharacterTimeoutStopwatch.Start();
                        do
                        {
                            if ( sp.BytesToRead > 0 )
                                ret = sp.ReadByte();
                        }
                        while ( ret < 0 && this._InterCharacterTimeoutStopwatch.Elapsed <= this.InterCharacterDelay );

                        // Char received with no errors...reset timeout counter for next char!
                        if ( ret >= 0 )
                            this._InterCharacterTimeoutStopwatch.Stop();
                        break;
                    }
            }

            return ret;
        }

        /// <summary> Write a byte on a specified flux. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="flux">   Writing byte flux. </param>
        /// <param name="buffer"> Buffer of bytes. </param>
        /// <param name="offset"> Buffer starting offset. </param>
        /// <param name="size">   Number of bytes to write. </param>
        private void WriteBuffer( object flux, byte[] buffer, int offset, int size )
        {
            switch ( this.ConnectionType )
            {
                case ConnectionType.UdpIP:
                    {
                        UDPData udpd = ( UDPData ) flux;
                        udpd.WriteResponse( buffer, offset, size );
                        break;
                    }

                case ConnectionType.TcpIP:
                    {
                        NetworkStream ns = ( NetworkStream ) flux;
                        ns.Write( buffer, offset, size );
                        break;
                    }

                case ConnectionType.SerialAscii:
                case ConnectionType.SerialRTU:
                    {
                        SerialPort sp = ( SerialPort ) flux;
                        sp.Write( buffer, offset, size );
                        break;
                    }
            }
        }

        /// <summary> Modbus Server response manager. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="sendBuffer">    Send buffer. </param>
        /// <param name="receiveBuffer"> Receive buffer. </param>
        /// <param name="flux">          Object for flux manager. </param>
        protected void IncomingMessagePolling( List<byte> sendBuffer, List<byte> receiveBuffer, object flux )
        {
            if ( sendBuffer is null )
                throw new ArgumentNullException( nameof( sendBuffer ) );
            if ( receiveBuffer is null )
                throw new ArgumentNullException( nameof( receiveBuffer ) );
            if ( flux is null )
                throw new ArgumentNullException( nameof( flux ) );
            ushort transaction_id = 0;
            byte unit_id = 0;
            var sw = new Stopwatch();

            // Set errors at 0
            this.LastError = InterfaceError.NoError;
            // Empty reception buffer
            receiveBuffer.Clear();
            // Start reception loop
            sw.Start();
            // true if read the first byte.
            bool in_ric = false;
            // count this many inter-character timeouts to determine if message completed.
            int messageCompleteCountDown = 3;
            // DH: new code resets the character timeout stop watch upon reading each character. 
            this._InterCharacterTimeoutStopwatch.Stop();
            do
            {
                int data = this.ReadByte( flux );
                if ( data != -1 )
                {
                    messageCompleteCountDown = 3;
                    if ( !in_ric )
                        in_ric = true;
                    if ( this.ConnectionType == ConnectionType.SerialAscii )
                    {
                        if ( ( byte ) data == Encoding.ASCII.GetBytes( new char[] { AsciiStartFrameCharacter } ).First() )
                        {
                            receiveBuffer.Clear();
                        }
                    }

                    receiveBuffer.Add( ( byte ) data );
                }
                else if ( in_ric && data < 0 )
                {
                    messageCompleteCountDown -= 1;
                }
                else
                {
                    Core.ApplianceBase.Delay( 1d );
                }
            }
            while ( sw.Elapsed < this.RxTimeout && this.Running && messageCompleteCountDown > 0 );
            this._InterCharacterTimeoutStopwatch.Stop();
            sw.Stop();
            // Check for a stop request
            if ( !this.Running )
            {
                this.LastError = InterfaceError.ThreadBlockRequest;
                return;
            }
            // Check for timeout error
            if ( sw.Elapsed > this.RxTimeout )
            {
                this.LastError = InterfaceError.ReceiveTimeout;
                return;
            }
            // Check message length
            if ( receiveBuffer.Count < 3 )
            {
                this.LastError = InterfaceError.MessageTooShort;
                return;
            }
            // Message received, start decoding...
            switch ( this.ConnectionType )
            {
                case ConnectionType.SerialAscii:
                    {
                        // Check and delete start char
                        if ( Encoding.ASCII.GetChars( receiveBuffer.ToArray() ).FirstOrDefault() != AsciiStartFrameCharacter )
                        {
                            this.LastError = InterfaceError.StartCharNotFound;
                            return;
                        }

                        receiveBuffer.RemoveAt( 0 );
                        // Check and delete stop chars
                        var end_chars = new char[] { AsciiEndFrameFirstCharacter, AsciiEndFrameSecondCharacter };
                        var last_two = Encoding.ASCII.GetChars( receiveBuffer.GetRange( receiveBuffer.Count - 2, 2 ).ToArray() );
                        if ( !end_chars.SequenceEqual( last_two ) )
                        {
                            this.LastError = InterfaceError.EndCharsNotFound;
                            return;
                        }

                        receiveBuffer.RemoveRange( receiveBuffer.Count - 2, 2 );
                        // Recode message in binary
                        receiveBuffer = receiveBuffer.ToArray().FromAsciiBytes().ToList();
                        // Calculate and remove LRC
                        byte msg_lrc = receiveBuffer[receiveBuffer.Count - 1];
                        byte calc_lrc = LRC.Evaluate( receiveBuffer.ToArray(), 0, receiveBuffer.Count - 1 );
                        if ( msg_lrc != calc_lrc )
                        {
                            this.LastError = InterfaceError.WrongLrc;
                            return;
                        }

                        receiveBuffer.RemoveAt( receiveBuffer.Count - 1 );
                        // Analyze destination address, if not present in database, discard message and continue
                        unit_id = receiveBuffer[0];
                        if ( !this._ModbusDataStore.Any( x => x.UnitId == unit_id ) )
                        {
                            return;
                        }

                        receiveBuffer.RemoveAt( 0 );
                        break;
                    }

                case ConnectionType.SerialRTU:
                    {
                        // Check CRC
                        ushort msg_crc = BitConverter.ToUInt16( receiveBuffer.ToArray(), receiveBuffer.Count - 2 );
                        ushort calc_crc = CRC16.Evaluate( receiveBuffer.ToArray(), 0, receiveBuffer.Count - 2 );
                        if ( msg_crc != calc_crc )
                        {
                            this.LastError = InterfaceError.WrongCrc;
                            return;
                        }
                        // Analyze destination address, if not present in database, discard message and continue
                        unit_id = receiveBuffer[0];
                        if ( !this._ModbusDataStore.Any( x => x.UnitId == unit_id ) )
                        {
                            return;
                        }
                        // Message is okay, remove unit_id and CRC                    
                        receiveBuffer.RemoveRange( 0, 1 );
                        receiveBuffer.RemoveRange( receiveBuffer.Count - 2, 2 );
                        break;
                    }

                case ConnectionType.UdpIP:
                case ConnectionType.TcpIP:
                    {
                        // Decode MBAP Header
                        transaction_id = receiveBuffer.ToArray().BigEndUnsignedInt16( 0 );
                        // Check protocol ID
                        // INSTANT VB NOTE: The variable protocol_id was renamed since Visual Basic does not handle local variables named the same as class members well:
                        ushort protocol_id_Renamed = receiveBuffer.ToArray().BigEndUnsignedInt16( 2 );
                        if ( protocol_id_Renamed != ProtocolId )
                        {
                            this.LastError = InterfaceError.WrongProtocolId;
                            return;
                        }
                        // Acquire data length and check it                    
                        ushort len = receiveBuffer.ToArray().BigEndUnsignedInt16( 4 );
                        if ( receiveBuffer.Count - 6 < len )
                        {
                            this.LastError = InterfaceError.MessageTooShort;
                        }
                        else if ( receiveBuffer.Count - 6 > len )
                        {
                            this.LastError = InterfaceError.MessageTooLong;
                            return;
                        }
                        // Analyze destination address, if not present in database, discard message and continue
                        unit_id = receiveBuffer[6];
                        if ( !this._ModbusDataStore.Any( x => x.UnitId == unit_id ) )
                        {
                            return;
                        }
                        // Message is okay, remove MBAP header for reception buffer                    
                        receiveBuffer.RemoveRange( 0, MbapHeaderLength );
                        break;
                    }
            }
            // Adjust data and build response
            this.AdjAndReply( sendBuffer, receiveBuffer, flux, unit_id, transaction_id );
        }

        /// <summary> Adjust received data and build response. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sendBuffer">    Send buffer. </param>
        /// <param name="receiveBuffer"> Receive buffer. </param>
        /// <param name="flux">          Object for flux guest. </param>
        /// <param name="unitId">        Server id. </param>
        /// <param name="transactionId"> Transaction id of TCP Server. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AdjAndReply( List<byte> sendBuffer, List<byte> receiveBuffer, object flux, byte unitId, ushort transactionId )
        {
            ushort sa, sa1, qor, qor1, bc, val, and_mask, or_mask;
            int bytes;
            bool cv;
            bool[] ret_vals;
            ModbusCodes mdbcode;

            // Empty reception buffer
            sendBuffer.Clear();
            // Adjust data
            mdbcode = ( ModbusCodes ) Conversions.ToInteger( receiveBuffer[0] );
            switch ( mdbcode )
            {
                case ModbusCodes.ReadCoils:
                    {
                        // Read received commands
                        sa = receiveBuffer.ToArray().BigEndUnsignedInt16( 1 );
                        qor = receiveBuffer.ToArray().BigEndUnsignedInt16( 3 );
                        if ( this._ModbusDataStore.Single( x => x.UnitId == unitId ).Coils().Length == 0 )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !(qor >= 1 && qor <= MaxCoilsInReadMessage) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataValue;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.CoilsRegisters, sa, qor ) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }
                        // Reply
                        sendBuffer.Add( ( byte ) ModbusCodes.ReadCoils );
                        bytes = qor / 8 + (qor % 8 == 0 ? 0 : 1);
                        sendBuffer.Add( ( byte ) bytes );
                        ret_vals = new bool[(bytes * 8)];
                        try
                        {
                            this._ModbusDataStore.Single( x => x.UnitId == unitId ).Coils().ToList().GetRange( sa, qor ).CopyTo( ret_vals );
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }

                        for ( int ii = 0, loopTo = bytes - 1; ii <= loopTo; ii++ )
                            sendBuffer.Add( ret_vals.EightBitToByte( ii * 8 ) );
                        break;
                    }

                case ModbusCodes.ReadDiscreteInputs:
                    {
                        // Read received commands                    
                        sa = receiveBuffer.ToArray().BigEndUnsignedInt16( 1 );
                        qor = receiveBuffer.ToArray().BigEndUnsignedInt16( 3 );
                        if ( this._ModbusDataStore.Single( x => x.UnitId == unitId ).DiscreteInputs().Length == 0 )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !(qor >= 1 && qor <= MaxDiscreteInputsInReadMessage) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataValue;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.DiscreteInputsRegisters, sa, qor ) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }
                        // Reply
                        sendBuffer.Add( ( byte ) ModbusCodes.ReadDiscreteInputs );
                        bytes = qor / 8 + (qor % 8 == 0 ? 0 : 1);
                        sendBuffer.Add( ( byte ) bytes );
                        ret_vals = new bool[(bytes * 8)];
                        try
                        {
                            this._ModbusDataStore.Single( x => x.UnitId == unitId ).DiscreteInputs().ToList().GetRange( sa, qor ).CopyTo( ret_vals );
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }

                        for ( int ii = 0, loopTo1 = bytes - 1; ii <= loopTo1; ii++ )
                            sendBuffer.Add( ret_vals.EightBitToByte( ii * 8 ) );
                        break;
                    }

                case ModbusCodes.ReadHoldingRegisters:
                    {
                        // Read received commands
                        sa = receiveBuffer.ToArray().BigEndUnsignedInt16( 1 );
                        qor = receiveBuffer.ToArray().BigEndUnsignedInt16( 3 );
                        if ( this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters().Length == 0 )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !(qor >= 1 && qor <= MaxHoldingRegistersInReadMessage) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataValue;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.HoldingRegisters, sa, qor ) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }
                        // Reply
                        sendBuffer.Add( ( byte ) ModbusCodes.ReadHoldingRegisters );
                        sendBuffer.Add( ( byte ) (2 * qor) );
                        try
                        {
                            for ( int ii = 0, loopTo2 = qor * 2 - 1; ii <= loopTo2; ii += 2 )
                                sendBuffer.AddRange( this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters()[sa + ii / 2].BigEndInt8() );
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }

                        break;
                    }

                case ModbusCodes.ReadInputRegisters:
                    {
                        // Read received commands
                        sa = receiveBuffer.ToArray().BigEndUnsignedInt16( 1 );
                        qor = receiveBuffer.ToArray().BigEndUnsignedInt16( 3 );
                        if ( this._ModbusDataStore.Single( x => x.UnitId == unitId ).InputRegisters().Length == 0 )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !(qor >= 1 && qor <= MaxInputRegistersInReadMessage) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataValue;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.InputRegisters, sa, qor ) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }
                        // Reply
                        sendBuffer.Add( ( byte ) ModbusCodes.ReadInputRegisters );
                        sendBuffer.Add( ( byte ) (2 * qor) );
                        try
                        {
                            for ( int ii = 0, loopTo3 = qor * 2 - 1; ii <= loopTo3; ii += 2 )
                                sendBuffer.AddRange( this._ModbusDataStore.Single( x => x.UnitId == unitId ).InputRegisters()[sa + ii / 2].BigEndInt8() );
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }

                        break;
                    }

                case ModbusCodes.WriteSingleCoil:
                    {
                        // Adjusting
                        sa = receiveBuffer.ToArray().BigEndUnsignedInt16( 1 );
                        val = receiveBuffer.ToArray().BigEndUnsignedInt16( 3 );
                        switch ( val )
                        {
                            case 0x0:
                                {
                                    cv = false;
                                    break;
                                }

                            case 0xFF00:
                                {
                                    cv = true;
                                    break;
                                }

                            default:
                                {
                                    this.LastError = InterfaceError.ExceptionIllegalDataValue;
                                    cv = false; // Dummy
                                    break;
                                }
                        }

                        if ( this._ModbusDataStore.Single( x => x.UnitId == unitId ).Coils().Length == 0 )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( this.LastError == InterfaceError.ExceptionIllegalDataValue )
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.CoilsRegisters, sa, 1 ) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( this.LastError == InterfaceError.WrongWriteSingleCoilValue )
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionIllegalDataValue );
                            break;
                        }

                        try
                        {
                            this._ModbusDataStore.Single( x => x.UnitId == unitId ).Coils()[sa] = cv;
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }
                        // Reply
                        sendBuffer.Add( ( byte ) ModbusCodes.WriteSingleCoil );
                        sendBuffer.AddRange( sa.BigEndInt8() );
                        sendBuffer.AddRange( val.BigEndInt8() );
                        break;
                    }

                case ModbusCodes.WriteSingleRegister:
                    {
                        // Adjusting
                        sa = receiveBuffer.ToArray().BigEndUnsignedInt16( 1 );
                        val = receiveBuffer.ToArray().BigEndUnsignedInt16( 3 );
                        if ( this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters().Length == 0 )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !(val >= 0x0 && val <= 0xFFFF) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataValue;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.HoldingRegisters, sa, 1 ) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        try
                        {
                            this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters()[sa] = val;
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }
                        // Reply
                        sendBuffer.Add( ( byte ) ModbusCodes.WriteSingleRegister );
                        sendBuffer.AddRange( sa.BigEndInt8() );
                        sendBuffer.AddRange( val.BigEndInt8() );
                        break;
                    }

                case ModbusCodes.WriteMultipleCoils:
                    {
                        // Adjusting
                        sa = receiveBuffer.ToArray().BigEndUnsignedInt16( 1 );
                        qor = receiveBuffer.ToArray().BigEndUnsignedInt16( 3 );
                        if ( this._ModbusDataStore.Single( x => x.UnitId == unitId ).Coils().Length == 0 )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !(qor >= 1 && qor <= MaxCoilsInWriteMessage) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataValue;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.CoilsRegisters, sa, qor ) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        bc = receiveBuffer[5];
                        var buffer = receiveBuffer.GetRange( 6, bc ).ToArray();
                        var ba = new BitArray( buffer );
                        try
                        {
                            ba.CopyTo( this._ModbusDataStore.Single( x => x.UnitId == unitId ).Coils(), sa );
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }
                        // Reply
                        sendBuffer.Add( ( byte ) ModbusCodes.WriteMultipleCoils );
                        sendBuffer.AddRange( sa.BigEndInt8() );
                        sendBuffer.AddRange( qor.BigEndInt8() );
                        break;
                    }

                case ModbusCodes.WriteMultipleRegisters:
                    {
                        // Adjusting
                        sa = receiveBuffer.ToArray().BigEndUnsignedInt16( 1 );
                        qor = receiveBuffer.ToArray().BigEndUnsignedInt16( 3 );
                        if ( this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters().Length == 0 )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !(qor >= 1 && qor <= MaxHoldingRegistersInWriteMessage) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataValue;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.HoldingRegisters, sa, qor ) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        bc = receiveBuffer[5];
                        try
                        {
                            int ii = 0;
                            while ( ii < bc )
                            {
                                this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters()[sa + ii / 2] = receiveBuffer.ToArray().BigEndUnsignedInt16( 6 + ii );
                                ii += 2;
                            }
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }
                        // Reply
                        sendBuffer.Add( ( byte ) ModbusCodes.WriteMultipleRegisters );
                        sendBuffer.AddRange( sa.BigEndInt8() );
                        sendBuffer.AddRange( qor.BigEndInt8() );
                        break;
                    }

                case ModbusCodes.MaskWriteRegister:
                    {
                        // Adjusting
                        sa = receiveBuffer.ToArray().BigEndUnsignedInt16( 1 );
                        and_mask = receiveBuffer.ToArray().BigEndUnsignedInt16( 3 );
                        or_mask = receiveBuffer.ToArray().BigEndUnsignedInt16( 5 );
                        if ( this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters().Length == 0 )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !(and_mask >= 0x0 && and_mask <= 0xFFFF || and_mask >= 0x0 && and_mask <= 0xFFFF) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataValue;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.HoldingRegisters, sa, 1 ) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        try
                        {
                            this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters()[sa] = ( ushort ) (this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters()[sa] & and_mask | or_mask & ~and_mask);
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }
                        // Reply
                        sendBuffer.Add( ( byte ) ModbusCodes.MaskWriteRegister );
                        sendBuffer.AddRange( sa.BigEndInt8() );
                        sendBuffer.AddRange( and_mask.BigEndInt8() );
                        sendBuffer.AddRange( or_mask.BigEndInt8() );
                        break;
                    }

                case ModbusCodes.ReadWriteMultipleRegisters:
                    {
                        // Adjusting
                        sa = receiveBuffer.ToArray().BigEndUnsignedInt16( 1 );
                        qor = receiveBuffer.ToArray().BigEndUnsignedInt16( 3 );
                        sa1 = receiveBuffer.ToArray().BigEndUnsignedInt16( 5 );
                        qor1 = receiveBuffer.ToArray().BigEndUnsignedInt16( 7 );
                        if ( this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters().Length == 0 )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalFunction;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !(qor >= 1 && qor <= MaxHoldingRegistersToReadInReadWriteMessage || qor1 >= 1 && qor1 <= MaxHoldingRegistersToWriteInReadWriteMessage) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataValue;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        if ( !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.HoldingRegisters, sa, qor ) || !this.IsAllRegistersPresent( unitId, ModbusDatabaseTable.HoldingRegisters, sa1, qor1 ) )
                        {
                            this.LastError = InterfaceError.ExceptionIllegalDataAddress;
                            BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                            break;
                        }

                        bc = receiveBuffer[9];
                        // First: Exec writing...
                        try
                        {
                            for ( int ii = 0, loopTo4 = bc - 1; ii <= loopTo4; ii += 2 )
                                this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters()[sa1 + ii / 2] = receiveBuffer.ToArray().BigEndUnsignedInt16( 10 + ii );
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }
                        // Second: Exec reading and prepare the reply
                        sendBuffer.Add( ( byte ) ModbusCodes.ReadWriteMultipleRegisters );
                        sendBuffer.AddRange( (( ushort ) (qor * 2)).BigEndInt8() );
                        try
                        {
                            for ( int ii = 0, loopTo5 = qor * 2 - 1; ii <= loopTo5; ii += 2 )
                                sendBuffer.AddRange( this._ModbusDataStore.Single( x => x.UnitId == unitId ).HoldingRegisters()[sa + ii / 2].BigEndInt8() );
                        }
                        catch
                        {
                            BuildExceptionMessage( sendBuffer, mdbcode, InterfaceError.ExceptionServerDeviceFailure );
                            break;
                        }

                        break;
                    }

                default:
                    {
                        this.LastError = InterfaceError.ExceptionIllegalFunction;
                        BuildExceptionMessage( sendBuffer, mdbcode, this.LastError );
                        break;
                    }
            }
            // Send response
            this.SendReply( sendBuffer, flux, unitId, transactionId );
        }

        /// <summary> Send the response. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="send_buffer">   Send buffer. </param>
        /// <param name="flux">          Object for flux guest. </param>
        /// <param name="unitId">        Server ID. </param>
        /// <param name="transactionId"> Transaction id of TCP Server. </param>
        private void SendReply( List<byte> send_buffer, object flux, byte unitId, ushort transactionId )
        {
            switch ( this.ConnectionType )
            {
                case ConnectionType.SerialAscii:
                    {
                        // Add unit ID
                        send_buffer.Insert( 0, unitId );
                        // Enqueue LRC
                        send_buffer.Add( LRC.Evaluate( send_buffer.ToArray(), 0, send_buffer.Count ) );
                        // ASCII encoding
                        send_buffer = send_buffer.ToArray().ToAsciiBytes().ToList();
                        // Add START character
                        send_buffer.Insert( 0, Encoding.ASCII.GetBytes( new char[] { AsciiStartFrameCharacter } ).First() );
                        // Enqueue STOP chars
                        send_buffer.AddRange( Encoding.ASCII.GetBytes( new char[] { AsciiEndFrameFirstCharacter, AsciiEndFrameSecondCharacter } ) );
                        break;
                    }

                case ConnectionType.SerialRTU:
                    {

                        // Add unit ID
                        send_buffer.Insert( 0, unitId );

                        // Enqueue CRC
                        send_buffer.AddRange( BitConverter.GetBytes( CRC16.Evaluate( send_buffer.ToArray(), 0, send_buffer.Count ) ) );

                        // Wait for inter frame delay
                        Core.ApplianceBase.Delay( this.InterFrameDelay );
                        break;
                    }

                case ConnectionType.UdpIP:
                case ConnectionType.TcpIP:
                    {
                        // Build MBAP header
                        send_buffer.InsertRange( 0, transactionId.BigEndInt8() );
                        send_buffer.InsertRange( 2, ProtocolId.BigEndInt8() );
                        send_buffer.InsertRange( 4, (( ushort ) (1 + send_buffer.Count - 4)).BigEndInt8() );
                        send_buffer.Insert( 6, unitId );
                        break;
                    }
            }
            // Send the buffer
            this.WriteBuffer( flux, send_buffer.ToArray(), 0, send_buffer.Count );
        }

        #endregion

    }
}
