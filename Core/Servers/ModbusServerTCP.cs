using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace isr.Modbus
{

    /// <summary> Modbus TCP Server class. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07. https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    public sealed class ModbusServerTCP : ModbusServerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="dataStore">    Modbus database array. </param>
        /// <param name="localAddress"> Local listening IP addresses. </param>
        /// <param name="port">         Listening TCP port. </param>
        public ModbusServerTCP( DataStore[] dataStore, IPAddress localAddress, int port ) : base( dataStore )
        {
            // Set device states
            this.ConnectionType = ConnectionType.TcpIP;
            // Crete TCP listener
            this._TcpListener = new TcpListener( localAddress, port );
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {
                        if ( TcpClientConnected is object )
                        {
                            foreach ( Delegate d in TcpClientConnected.GetInvocationList() )
                                TcpClientConnected -= ( EventHandler<ClientConnectedEventArgs> ) d;
                        }

                        if ( TcpClientDisconnected is object )
                        {
                            foreach ( Delegate d in TcpClientDisconnected.GetInvocationList() )
                                TcpClientDisconnected -= ( EventHandler<ClientConnectedEventArgs> ) d;
                        }

                        if ( this._ManualResetEvent is object )
                        {
                            this._ManualResetEvent.Dispose();
                            this._ManualResetEvent = null;
                        }

                        if ( this._TcpListener is object )
                        {
                            this._TcpListener = null;
                        }
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " INSTANCES "

        /// <summary>
        /// Listener TCP
        /// </summary>
        private TcpListener _TcpListener;

        /// <summary>
        /// Manual reset event
        /// </summary>
        private ManualResetEvent _ManualResetEvent = new ManualResetEvent( false );

        #endregion

        #region " EVENTS "

        /// <summary>
        /// Client connected event
        /// </summary>
        public event EventHandler<ClientConnectedEventArgs> TcpClientConnected;

        /// <summary>
        /// Client disconnected event
        /// </summary>
        public event EventHandler<ClientConnectedEventArgs> TcpClientDisconnected;

        #endregion

        #region " PARAMETERS "

        /// <summary> The remote clients connected. </summary>
        private readonly List<IPEndPoint> _RemoteClientsConnected = new List<IPEndPoint>();

        /// <summary> Connected clients. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> An IPEndPoint() </returns>
        public IPEndPoint[] RemoteClientsConnected()
        {
            return this._RemoteClientsConnected.ToArray();
        }

        #endregion

        #region " MODULE FUNCTIONS "

        /// <summary> Check if TcpClient is already connected. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="client"> TcpClient instance to check. </param>
        /// <returns> Connection status. </returns>
        private static bool IsClientConnected( TcpClient client )
        {
            bool ret = true;
            if ( client.Client.Poll( 0, SelectMode.SelectRead ) )
            {
                var check_connection = new byte[1];
                if ( client.Client.Receive( check_connection, SocketFlags.Peek ) == 0 )
                {
                    ret = false;
                }
            }

            return ret;
        }

        #endregion

        #region " INPUT CONNECTIONS CALLBACK "

        /// <summary> Input connections callback. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="asyncResult"> The result. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DoAcceptTcpClientCallback( IAsyncResult asyncResult )
        {
            TcpListener listener = null;
            TcpClient client = null;
            NetworkStream ns = null;
            IPEndPoint ipe = null;
            try
            {
                var send_buffer = new List<byte>();
                var receive_buffer = new List<byte>();

                // Copy listener instance
                listener = ( TcpListener ) asyncResult.AsyncState;
                // Get client
                client = listener.EndAcceptTcpClient( asyncResult );
                // Fire event
                ipe = ( IPEndPoint ) client.Client.RemoteEndPoint;
                this._RemoteClientsConnected.Add( ipe );
                var con_handler = TcpClientConnected;
                if ( con_handler is object )
                {
                    con_handler( this, new ClientConnectedEventArgs( ipe ) );
                }
                // Set manual reset event
                _ = this._ManualResetEvent.Set();
                // Get network stream
                ns = client.GetStream();
                // Processing incoming connections
                while ( this.Running )
                {
                    if ( !IsClientConnected( client ) )
                    {
                        break;
                    }

                    this.IncomingMessagePolling( send_buffer, receive_buffer, ns );
                    Core.ApplianceBase.Delay( 1d );
                }
            }
            catch
            {
            }
            finally
            {
                // Close IO stream
                if ( ns is object )
                {
                    ns.Close();
                }
                // Close client connection                
                if ( client is object )
                {
                    client.Close();
                }
                // Fire event
                if ( ipe is object )
                {
                    _ = this._RemoteClientsConnected.Remove( ipe );
                    var discon_handler = TcpClientDisconnected;
                    if ( discon_handler is object )
                    {
                        discon_handler( this, new ClientConnectedEventArgs( ipe ) );
                    }
                }
            }
        }

        #endregion

        #region " PROCESS THREAD FOR INCOMING CONNECTIONS "

        /// <summary> The body of the incoming call manager. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected override void GuestRequests()
        {
            while ( this.Running )
            {
                // Reset event
                _ = this._ManualResetEvent.Reset();
                // Asynchronous call to incoming connections
                _ = this._TcpListener.BeginAcceptTcpClient( new AsyncCallback( this.DoAcceptTcpClientCallback ), this._TcpListener );
                // Wait for event
                _ = this._ManualResetEvent.WaitOne();
            }
        }

        #endregion

        #region " START AND STOP FUNCTIONS "

        /// <summary> Start listening. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void StartListen()
        {
            this._TcpListener.Start();
            if ( this.GuestRequestThread is null )
            {
                this.Running = true;
                this.GuestRequestThread = new Thread( this.GuestRequests );
                this.GuestRequestThread.Start();
            }
        }

        /// <summary> Stop listening. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void StopListen()
        {
            if ( this.GuestRequestThread is object )
            {
                this.Running = false;
                _ = this._ManualResetEvent.Set();
                this.GuestRequestThread.Join();
                this.GuestRequestThread = null;
            }

            this._TcpListener.Stop();
        }

        #endregion

    }
}
