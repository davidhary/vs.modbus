using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace isr.Modbus
{

    /// <summary> Modbus Server UDP Class. </summary>
    /// <remarks>
    /// (c) 2013 Simone Assunti. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-07,https://code.google.com/p/free-dotnet-modbus/. </para>
    /// </remarks>
    public sealed class ModbusServerUDP : ModbusServerBase
    {

        #region " CONSTRUCTOR "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="dataStore">    Modbus databases array. </param>
        /// <param name="localAddress"> Local listening IP addresses. </param>
        /// <param name="port">         Listening TCP port. </param>
        public ModbusServerUDP( DataStore[] dataStore, IPAddress localAddress, int port ) : base( dataStore )
        {
            // Set device states
            this.ConnectionType = ConnectionType.UdpIP;
            // Create UDP listener
            this._UdpListener = new UdpClient( new IPEndPoint( localAddress, port ) );
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed )
                {
                    if ( disposing )
                    {
                        if ( this._ManualResetEvent is object )
                        {
                            this._ManualResetEvent.Dispose();
                            this._ManualResetEvent = null;
                        }

                        if ( this._UdpListener is object )
                        {
                            this._UdpListener.Dispose();
                            this._UdpListener = null;
                        }
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }


        #endregion

        #region " INSTANCES "

        /// <summary>
        /// UDP Listener
        /// </summary>
        private UdpClient _UdpListener;

        /// <summary>
        /// Manual reset event
        /// </summary>
        private ManualResetEvent _ManualResetEvent = new ManualResetEvent( false );

        #endregion

        #region " INCOMING CONNECTIONS CALLBACK "

        /// <summary> Incoming connection callback. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="asyncResult"> Result. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DoAcceptUdpDataCallback( IAsyncResult asyncResult )
        {
            UDPData udp_data = null;
            IPEndPoint remote_ep = null;
            try
            {
                var send_buffer = new List<byte>();
                var receive_buffer = new List<byte>();

                // Copy listener instance
                UdpClient listener = ( UdpClient ) asyncResult.AsyncState;
                // Get input frame and remote endpoint
                var rx_buffer = listener.EndReceive( asyncResult, ref remote_ep );
                // Instance UDPData class
                udp_data = new UDPData( listener, rx_buffer, remote_ep );
                // Set event
                _ = this._ManualResetEvent.Set();
                // Process incoming call
                this.IncomingMessagePolling( send_buffer, receive_buffer, udp_data );
            }
            catch
            {
            }
            finally
            {
                if ( udp_data is object )
                {
                    udp_data.Close();
                }
            }
        }

        #endregion

        #region " INCOMING CALL PROCESS THREAD "

        /// <summary> Incoming call process thread. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        protected override void GuestRequests()
        {
            while ( this.Running )
            {

                // Reset event
                _ = this._ManualResetEvent.Reset();

                // Asynchronous call to process callback
                _ = this._UdpListener.BeginReceive( new AsyncCallback( this.DoAcceptUdpDataCallback ), this._UdpListener );

                // wait for event
                _ = this._ManualResetEvent.WaitOne();
            }
        }

        #endregion

        #region " START AND STOP FUNCTIONS "

        /// <summary> Start listening. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void StartListen()
        {
            if ( this.GuestRequestThread is null )
            {
                this.Running = true;
                this.GuestRequestThread = new Thread( this.GuestRequests );
                this.GuestRequestThread.Start();
            }
        }

        /// <summary> Stop listening. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public override void StopListen()
        {
            if ( this.GuestRequestThread is object )
            {
                this.Running = false;
                this.GuestRequestThread.Join();
                this.GuestRequestThread = null;
            }

            this._UdpListener.Close();
        }

        #endregion

    }
}
