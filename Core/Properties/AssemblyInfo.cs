using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Modbus.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Modbus.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Modbus.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
