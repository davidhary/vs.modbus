# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.2.8138] - 2022-04-13
* Remove the Constructs project and add the Primitives, Concurrent and Binding Lists projects.
* Use project file package reference.

## [1.2.7637] - 2020-11-27
* Converted to C#.

## [1.2.7636] - 2020-11-26
### Watlow - Changed
* Move the Easy Zone control to the Forms library.
* Use Stateless soak state machine.

## [1.1.6667] - 2017-04-03
### Modbus 
* 2018 release.

## [1.1.6440] - 2017-08-19
### Watlow 
* Uses modified trace message and logger. 

## [1.0.6290] - 2017-03-22
### Watlow 
* Adds a function to determine if the soak automaton is staying At Temp..

## [1.0.6289] - 2017-03-21
### Watlow 
* Adds transition from Off Soak to Ramp.

## [1.0.6288] - 2017-03-20
### Watlow 
* Fixes soak time stop watch.

## [1.0.6274] - 2017-03-06
### Watlow 
* Fixes how the target and interim setpoint changes interact.

## [1.0.6265] - 2017-02-25
### Watlow 
* Adds control of temporary or new setpoint during soak automation to allow moving the setpoint.

## [1.0.6222] - 2017-01-13
### Modbus 
* Uses nullable values when reading the device. Checks for buffered values before checking for completion of frame. Updates data available for the Serial RTU to allow time for the next character. Updates protocol to add Data Available to the check of completion. Updates t=protocol to return nullable values when reading the buffer. Flushes device buffers before each query. Uses a timeout stop watch to delay action for device settling time.

## [1.0.6221] - 2017-01-12
* Uses payload validation algorithm to detect end of received frame. Adds a time series collection to store the input values. Adds a function to wait for available data to use before reading. Split read function to a wait for data followed by reading characters with inter-frame timeout and inter-character polling wait. Traces buffer information of failure and success. Serial client: Adds a frame wait before reading each byte. Updates assembly product name to 2017. Adds payload buffers.

## [1.0.6220] - 2017-01-11
### Watlow 
* Reports buffer contents on failures and successes. Streamline the soak machine algorithm. Updates assembly product name to 2017.

## [1.0.6170] - 2016-11-22
### Watlow 
* Uses cancel details event arguments in place of reference strings to report failures reading the bus.

## [1.0.6064] - 2016-08-08
### Watlow 
* Updates soak state machine: Clears values on entering idle state; Moves setpoint to idle state; Moves oven on to Ready state. Adds oven control mode to allow setting a setpoint with oven off.

## [1.0.6047] - 2016-07-22
### Modbus 
* Changes code for reading byte from the serial port and determining the end of record based. Uses time span type for timing.

## [1.0.5992] - 2016-05-28
### Modbus 
* Clears buffers upon completion of transactions.

### Watlow 
* Adds refractory time spans to all operations.

## [1.0.5969] - 2016-05-07
* Created.

\(C\) 2016 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
