# MODBUS Libraries

Supporting MODBUS communication and control of WATLOW Easy Zone devices.

* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)
* [About Modbus](#About-Modbus)
* [About EZ-Zone](#About-EZ-Zone)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [Core](https://www.bitbucket.org/davidhary/vs.core) - Core Libraries
* [Automata](https://www.bitbucket.org/davidhary/vs.automata.moore) - Automata Libraries
* [Typed Units](https://www.bitbucket.org/davidhary/arebis.typedunits) - Typed Units Libraries
* [MODBUS](https://www.bitbucket.org/davidhary/vs.modbus) - MODBUS Libraries

```
git clone git@bitbucket.org:davidhary/vs.core.git
git clone git@bitbucket.org:davidhary/vs.automata.moore.git
git clone git@bitbucket.org:davidhary/vs.typeunits.git
git clone git@bitbucket.org:davidhary/vs.modbus.git
```

Clone the repositories into the following relative path(s) (parents of the .git folder):
```
.\Libraries\VS\Core\Core
.\Libraries\VS\Core\TypedUnits
.\Libraries\VS\Algorithms\Moore
.\Libraries\VS\IO\Modbus
```

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository]((https://www.bitbucket.org/davidhary/vs.ide)).

Restoring Editor Configuration assuming c:\My is the root folder of the .NET solutions):
```
xcopy /Y c:\My\.editorconfig c:\My\.editorconfig.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.editorconfig c:\My\.editorconfig
```

Restoring Run Settings assuming c:\user\<me> is the root user folder:
```
xcopy /Y c:\user\<me>\.runsettings c:\user\<me>\.runsettings.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.runsettings c:\user\<me>\.runsettings
```

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows
* [Free .Net MODBUS](https://code.google.com/p/free-dotnet-modbus) - MODBUS Library for .NET

<a name="Authors"></a>
## Authors
* [ATE Coder](https://www.IntegratedScientificResources.com)

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix](https://www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com) - Joel Spolsky

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Automata Libraries](https://bitbucket.org/davidhary/vs.automata.moore)  
[Typed Units Libraries](https://bitbucket.org/davidhary/arebis.typedunits)  
[Free .Net MODBUS](https://code.google.com/p/free-dotnet-modbus)

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Automata Libraries](https://bitbucket.org/davidhary/vs.automata.moore)

<a name="About-Modbus"></a>
### About Modbus
For additional information about the MODBUS protocol, see:  
[Simply Modbus](http://www.simplymodbus.ca/FAQ.htm)  
[BB-Electronics FAQ](http://www.bb-elec.com/Learning-Center/All-White-Papers/Modbus/The-Answer-to-the-14-Most-Frequently-Asked-Modbus.aspx)

<a name="About-EZ-Zone"></a>
### About EZ-Zone
Additional information about EZ-Zone is available at the following sites:  
[Watlow resources and support](https://www.watlow.com/resources-and-support)

